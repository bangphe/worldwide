<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

/**
 * Class Currency
 * @package app\models
 */
class Currency extends \app\models\auto\Currency
{
    /**
     * Constants
     */
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'symbol', 'code'], 'required'],
            [['status'], 'string'],
            [['name', 'symbol', 'code'], 'string', 'max' => 100],
        ];
    }

    /**
     * @return auto\Currency[]|array
     */
    public static function getActiveCurrencies()
    {
        return static::find()->where(['status' => self::STATUS_ACTIVE])->all();
    }

}
