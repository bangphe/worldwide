<?php
use yii\bootstrap\ActiveForm;
use app\assets\AccountAsset;
use yii\authclient\widgets\AuthChoice;

AccountAsset::register($this);
?>
<div class="sign-in">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-md-4 col-md-push-4 col-sm-6 col-sm-push-3 col-xs-12">
                <h1><?= t('app','Register or Sign In');?></h1>
                <?php $form = ActiveForm::begin([
                    'id'        => 'signin-form',
                    'method'    => 'post',
                ]); ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'email', [
                                    'template'      => '{input} {error}',
                            ])->textInput([ 'placeholder' => 'Email', 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'password', [
                                'template' => '{input} {error}',
                            ])->passwordInput(['placeholder' => 'Password', 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <?= $form->field($model, 'rememberMe', [
                                'options' => [
                                    'class' => 'checkbox icheck'
                                ],
                            ])->checkbox(['template' => '{input} {label}'],['value'=>true]); ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <a href="<?= url(['account/forgot']);?>" class="forgot pull-right"><?= t('app','Forgot password?');?></a>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className())->label(false) ?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <button type="submit" class="btn-as" value="Search"><?=t('app','Log in');?></button>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <a href="<?= url(['account/register']);?>" class="btn-as" style="margin-top: 8px;"><?= t('app','Register');?></a>
                            </div>
                        </div>
                        <?php if (options()->get('app.settings.common.siteFacebookId', '') && options()->get('app.settings.common.siteFacebookSecret', '')) { ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=t('app','Or sign in with');?></span>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <?php
                                    $authAuthChoice = AuthChoice::begin([
                                        'baseAuthUrl' => ['account/auth']
                                    ]);
                                ?>
                                <table align="center" width="100%">
                                    <tr>
                                    <?php
                                    foreach ($authAuthChoice->getClients() as $client) {
                                        //echo $authAuthChoice->clientLink($client,'facebook',['class'=>'btn-as facebook']);
                                        //echo $authAuthChoice->clientLink($client,$client->name,['class'=>'btn-as ']);
                                        ?>
                                        <td width="50px">
                                        <?php
                                        echo $authAuthChoice->clientLink($client);
                                        ?>
                                        </td>
                                        <?php
                                    }
                                    AuthChoice::end();
                                ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>