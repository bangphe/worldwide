
jQuery(document).ready(function ($) {


    // MY ACCOUNT MENU SLIDER *************************************************************************************************************************

    $(window).on('load resize', function () {
        $("#account-nav").mdtabs({
            color: "#009688",
            height: 3,
            duration: 550,
            leeway: $(window).width() > 767 ? 35 : 0
        });
    });

    if (window.location.hash == '#company-block') {
        setTimeout(function () {
            window.scrollTo(0, 330);
        }, 1);
        $('#company-block').collapse("show");
    }

    $(document).on('submit', '#form-change-about, #form-change-company, #form-change-password, #form-change-email, #form-change-email-recovery, #form-terminate-account', function (e) {
        notify.remove();
        var $this = $(this);
        if ($this.find('.has-error').length) return false;
        e.preventDefault();
        $.post($this.attr('action'), $this.serialize(), function (json) {

            if (json.result == 'success') {
                notify.addSuccess(json.msg).show();
            } else if (json.errors) {
                for (var i in json.errors) {
                    $('#' + $this.attr('id')).yiiActiveForm('updateAttribute', 'customer-' + i.toLowerCase(), json.errors[i]);
                }
            } else if (json.error) {
                notify.addError(json.error).show();
            }
        });
    });

    $(document).on('submit', '#form-change-avatar', function (e) {
        notify.remove();
        var $this = $(this);
        if ($this.find('.has-error').length) return false;
        e.preventDefault();

        var formData = new FormData();
        formData.append('Customer[avatarUpload]', $('[name=\'Customer[avatarUpload]\']')[1].files[0]);

        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (json) {
                if (json.result == 'success') {
                    notify.addSuccess(json.msg).show();
                } else if (json.errors) {
                    for (var i in json.errors) {
                        $('#' + $this.attr('id')).yiiActiveForm('updateAttribute', 'customer-' + i.toLowerCase(), json.errors[i]);
                    }
                } else if (json.error) {
                    notify.addError(json.error).show();
                }
            }
        });
    });

    // trigger modal for delete
    $(document).on('click', '#my-listings .delete-listing', function (e) {
        e.preventDefault();
        $('#modal-post-listing-delete .delete-listing').data('url', $(this).data('url')).data('listing-id', $(this).data('listing-id'));
        $('#modal-post-listing-delete').modal('show');
    });

    // action delete
    $(document).on('click', '#modal-post-listing-delete:visible .delete-listing', function (e) {
        e.preventDefault();
        notify.remove();
        var $this = $(this);
        $.post($this.data('url'), {
            listing_id: $this.data('listing-id'),
        }, function (json) {
            if (json.result == 'success') {
                notify.addSuccess(json.msg).show();
                window.location.reload(false);
            } else {
                notify.addError(json.msg).show();
            }
        }, 'json');

    });

    // trigger modal for remove favorite
    $(document).on('click', '#favorites .delete-favorite-listing', function (e) {
        e.preventDefault();
        $('#modal-favorite-delete .delete-favorite').data('url', $(this).data('favorite-url')).data('listing-id', $(this).data('listing-id'));
        $('#modal-favorite-delete').modal('show');
    });

    // action delete
    $(document).on('click', '#modal-favorite-delete:visible .delete-favorite', function (e) {
        e.preventDefault();
        notify.remove();
        var $this = $(this);
        $.post($this.data('url'), {
            listing_id: $this.data('listing-id'),
        }, function (json) {
            if (json.result == 'success') {
                notify.addSuccess(json.msg).show();
                setTimeout(function () { window.location.reload(false); }, 200);

            } else {
                notify.addError(json.msg).show();
            }
        }, 'json');

    });

    // $("#customer-address_street").change(function () {
    //     $("#isaddressgoogle").val('0');
    // });

    // $(document).on('click', '#submit-account-info-about', function () {
    //     if ($("#isaddressgoogle").val() != 1) {
    //         alert("Please fill proper address.");
    //         return false;
    //     }
    //     return true;
    // });

});