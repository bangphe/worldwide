<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use app\helpers\ImageHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * Class ListingImage
 * @package app\models
 */
class ListingImage extends \app\models\auto\ListingImage
{
    /**
     * Image default width
     */
    CONST IMG_WIDTH = 2000;

    /**
     * Image default height
     */
    CONST IMG_HEIGHT = 2000;

    /**
     * @var
     */
    public $imagesGallery;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'listing_id',*/ 'image_path'], 'required'],
            [['sort', 'listing_id'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return ImageHelper::resize($this->image_path, self::IMG_WIDTH, self::IMG_HEIGHT);
    }

    /**
     * @param null $adId
     * @return ListingImage[]
     * @throws \Exception
     */
    public function handleUploadedImages($adId = null)
    {
        $result = [];

        $session = \Yii::$app->session;

        $new_images = $session->get('new_images', []);

        if ($this->hasErrors()) {
            return [];
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/listings');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                throw new \Exception(t('app', 'The images storage directory({path}) does not exists and cannot be created!' . $storagePath));
            }
        }

        if($adId == null){
            if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                throw new \Exception(t('app', 'Please add at least one image'));
            }

            foreach ($imagesGallery as $imageGallery) {

                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                /** @var ListingImage $model */
                $model = new static();
                $model->image_path = Yii::getAlias('@web/uploads/images/listings/' . $newGalleryImageName);

                $model->save();

                $result[] = $model;

                $new_images[] = $model->image_id;

                $session->set('new_images', $new_images);
            }
        } else {
            $sort = 0;
            if ($new_images && $models = ListingImage::find()->where(['image_id' => $new_images])->all()) {
                /** @var ListingImage[] $models */
                foreach ($models as $model) {
                    $sort++;
                    $model->listing_id = $adId;
                    $model->sort_order = $sort;
                    $model->save();

                    $result[] = $model;
                }

                $session->remove('new_images');
            }

            $existingImages = self::find()->where(['listing_id' => $adId])->orderBy('sort_order ASC')->all();

            if (empty($existingImages)) {
                if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                    throw new \Exception(t('app', 'Please add at least one image to your ad post!'));
                }
            } else {
                if (!($imagesGallery = UploadedFile::getInstances($this, 'imagesGallery'))) {
                    return [];
                }
            }

            // $sort = end($existingImages)->sort_order;
            $sort = 0;

            foreach ($imagesGallery as $imageGallery) {
                $sort++;
                $newGalleryImageName = $imageGallery->name;
                if (!$imageGallery->saveAs($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(t('app', 'Something went wrong while saving images, please try again later.'));
                }
                if (!is_file($storagePath . '/' . $newGalleryImageName)) {
                    throw new \Exception(t('app', 'Cannot move the image into the correct storage folder!'));
                }

                $existing_file = $storagePath . '/' . $newGalleryImageName;

                $this->image_fix_orientation($existing_file);

                $newGalleryImageName = substr(sha1(time()), 0, 6) . $newGalleryImageName;
                rename($existing_file, $storagePath . '/' . $newGalleryImageName);

                $model = new static();
                $model->image_path = Yii::getAlias('@web/uploads/images/listings/' . $newGalleryImageName);
                $model->listing_id = $adId;
                $model->sort_order = $sort;

                $model->save();

                $result[] = $model;
            }
        }

        return $result;
    }

    protected function image_fix_orientation($filename) {
        $exif = exif_read_data($filename);
        if (!empty($exif['Orientation'])) {
            $image = imagecreatefromjpeg($filename);
            switch ($exif['Orientation']) {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;

                case 6:
                    $image = imagerotate($image, -90, 0);
                    break;

                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }

            imagejpeg($image, $filename, 90);
        }
    }
}