<?php
use yii\helpers\Url;
$theme = Url::base().'/assets/metronic';
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?=html_encode($title);?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-widget4">
        <!-- <ul class="users-list clearfix"> -->
            <?php foreach ($data as $item) { ?>
            <div class="m-widget4__item">
                <div class="m-widget4__img m-widget4__img--logo">
                    <img src="<?= $item->mainImage->image; ?>" alt="">
                </div>
                <div class="m-widget4__info">
                    <span class="m-widget4__title">
                        <?= html_encode($item->title); ?>
                    </span>
                    <br>
                    <span class="m-widget4__sub">
                        <?php $formatter = app()->formatter; ?>
                        <?= $formatter->asDatetime($item->created_at, 'short'); ?>
                    </span>
                </div>
                <span class="m-widget4__ext">
                    <!-- <span class="m-widget4__number m--font-danger"><?//= $item->created_at;?></span> -->
                    <a href="<?=url(['/listing/index/', 'slug' => $item->slug]);?>" class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary" target="_blank">Open</a>
                </span>
            </div>
            <?php } ?>
        <!-- </ul> -->
        </div>
    </div>
    <div class="m-portlet__foot">
        <a href="<?=$link;?>" class="uppercase"><?=t('app', 'View all items');?></a>
    </div>
</div>