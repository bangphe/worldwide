<?php
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\SvgHelper;
use app\assets\AppAsset;
use app\components\AdsListWidget;

AppAsset::register($this);

$view = \Yii::$app->session->get('ads-view', 'list');

if ($view == 'grid') {
    $section_class = 'listings-list';
    $item_view = '_ad-item-grid';
    $item_class = 'col-lg-4 col-md-4 col-sm-4 col-xs-6 item item-grid';
} else {
    $section_class = 'listings-list-2';
    $item_view = '_ad-item';
    $item_class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12 item item-list';
}
?>

<!-- <?php if($category->slug == 'b2b'){?>
<?= '<script type="text/javascript">window.location.href="/listing/b2bform"</script>'?>
<?= '<script type="text/javascript">window.location.href="/listing/post/36"</script>'?>
<?php }?> -->

<!-- <?php if($category->slug == 'selling'){?> -->
<!--<?= $this->render('_selling'); ?>-->
<!-- <?= '<script type="text/javascript">window.location.href="/listing/post/37"</script>'?> -->
<!-- <?php }?> -->

<!-- <?php if($category->slug == 'buying'){?> -->
<!--<?= $this->render('_buying'); ?>-->
<!-- <?= '<script type="text/javascript">window.location.href="/listing/post/38"</script>'?> -->
<!-- <?php }?> -->

<!-- <?php if($category->slug == 'partnerships'){?>
<?= $this->render('_partnerships'); ?>
<?= '<script type="text/javascript">window.location.href="/listing/post/39"</script>'?>
<?php }?> -->

<!-- <?php if($category->slug == 'investment-funding'){?>
<?= $this->render('_investment_funding'); ?>
<?= '<script type="text/javascript">window.location.href="/listing/post/40"</script>'?>
<?php }?> -->

<!-- <?php if($category->slug == 'investment-proposal'){?>
<?= $this->render('_investment_proposal'); ?>
<?= '<script type="text/javascript">window.location.href="/listing/post/41"</script>'?>
<?php }?> -->

<!-- <?php if($category->slug == 'digital-currencies'){?>
<?= '<script type="text/javascript">window.location.href="/listing/post/37"</script>'?>
<?php }?> -->

<!-- <?php if($category->slug == 'searching-for-services'){?>
<?= '<script type="text/javascript">window.location.href="/listing/post/45"</script>'?>
<?php }?> -->

<section class="main-search">
    <?= $this->render('_main-search', [
        'categories'              => $categories,
        'categoryPlaceholderText' => $categoryPlaceholderText,
        'searchModel'             => $searchModel,
        'customFields'            => $customFields,
        'locationDetails'         => $locationDetails,
        'selectedCategory'        => $category,
    ]); ?>
</section>

<?php 
    if($category->slug == 'b2b' || $category->slug == 'selling' || $category->slug == 'partnerships' || $category->slug == 'investment-funding' || $category->slug == 'investment-proposal'){
        $abovetext = 'Post Ads for';
    }else{
        $abovetext = '';
    }

?>

<section class="<?=$section_class?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-push-2 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1>
                            <?=$abovetext?> <?= !empty($category) ? html_encode($category->name) : t('app', 'All categories'); ?>
                            <?php
                            if ($view == 'grid') {
                                echo \yii\helpers\Html::a(
                                    '<i class="glyphicon glyphicon-th-list"></i>',
                                    ['', 'view' => 'list', 'slug' => $category->slug]
                                );
                            } else {
                                echo \yii\helpers\Html::a(
                                    '<i class="glyphicon glyphicon-th"></i>',
                                    ['', 'view' => 'grid', 'slug' => $category->slug]
                                );
                            }
                            ?>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div id="msg" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                    </div>
                </div>

                <!-- child categories -->
                <?php if (!empty($childCategories)) { ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="child-categories-list">
                                <?= SvgHelper::getByName('arrow-top');?>
                                <ul>
                                    <?php foreach ($childCategories as $childCat) { ?>
                                        <li><?= Html::a(html_encode($childCat->name), ['category/index', 'slug' => $childCat->slug]) ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?= ListView::widget([
                    'id'               => 'category-listings',
                    'dataProvider'     => $adsProvider,
                    'layout'           => '
                        <div class="row">
                            {items}
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pagination-custom">
                                    <div class="row">
                                        {pager}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ',
                    'itemView'         => $item_view,
                    'emptyText'        => '',
                    'itemOptions'      => [
                        'tag'   => 'div',
                        'class' => $item_class,
                    ],
                    'emptyTextOptions' => ['tag' => 'ul', 'class' => 'list invoice'],
                    'pager'            => [
                        'class'         => 'app\widgets\CustomLinkPager',
                        'prevPageLabel' => SvgHelper::getByName('arrow-left'),
                        'nextPageLabel' => SvgHelper::getByName('arrow-right')
                    ]
                ]); ?>

                <?php if ($isNothingFound) { ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <span class="no-results"><?= t('app', 'No results found') ?></span> 
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<section class="others-listings">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-push-2 col-md-12 col-sm-12 col-xs-12">
                <?php if ($isNothingFound) { ?>
                    <?= AdsListWidget::widget([
                        'listType'  => AdsListWidget::LIST_TYPE_PROMOTED,
                        'title'     => t('app', 'Other ads'),
                        'quantity'  => 3
                    ]);
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    function savedata(){
        var msg = '<div class="alert alert-success alert-dismissable">';
        msg += '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
        msg += '<strong>Success!</strong> Thanks for submitting your details, we will get in touch with you!';
        msg += '</div>';
        $('#msg').append(msg);
        //window.location.href="/category/business";
        return false;
    }
</script>
