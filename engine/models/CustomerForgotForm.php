<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class ForgotForm
 * @package app\models
 */
class CustomerForgotForm extends Model
{
    /**
     * @var string
     */
    public $email = '';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
        ];
    }

    /**
     * Validates the email.
     * This method serves as the inline validation for email.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateEmail($attribute, $params)
    {
        if ($this->hasErrors()) {
            return;
        }

        $model = Customer::findOne([
            'email'  => $this->$attribute,
            'status' => 'active'
        ]);

        if (empty($model)) {
            $this->addError('email', t('app', 'Invalid email address!'));
        }
    }

    /**
     * @return bool
     */
    public function sendEmail()
    {
        if (!$this->validate()) {
            return false;
        }

        if (!($model = Customer::findByEmail($this->email))
            && !($model = Customer::find()->where(['email_recovery' => $this->email])->limit(1)->one())
        ) {
            return false;
        }

        $model->password_reset_key = security()->generateRandomString();
        $model->save(false);
        $confirmation_url = url(['account/reset_password', 'key' => $model->password_reset_key], true);

        // return app()->mailSystem->add(
        //     'customer-password-change-request',
        //     ['customer_email' => $model->email]
        // ) && (!$model->email_recovery || app()->mailSystem->add(
        //     'customer-password-change-request',
        //     ['customer_email' => $model->email_recovery]
        // ));
        try {
            $email = mailer()->compose(['html' =>'forgot-password-email'],['model'=>$model, 'confirmation_url'=>$confirmation_url])
                ->setFrom(['admin@worldwideadverts.info' => 'Administrator Worldwideadverts'])
                ->setTo($model->email)
                ->setSubject('Password Change Request')
                ->send();

            return true;
        } catch (Exception $e) {
            throw new Exception("Error Processing Request", $e);
        }
    }
}
