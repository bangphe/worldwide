<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use app\fieldbuilder\Type;
use app\models\Listing;
use app\models\ListingSearch;
use app\models\Category;
use app\models\CategoryField;
use app\models\Country;
use app\models\Zone;
use twisted1919\helpers\Icon;
use yii\filters\VerbFilter;
use app\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\web\Response;

/**
 * Class CategoryController
 * @package app\controllers
 */
class CategoryController extends Controller
{
    /**
     * Constants
     */
    const ADS_PER_PAGE = 20;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($slug, $view = null)
    { 
        $category = $this->findCategoryBySlug($slug);
        $searchCategories = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();
        $categories = Category::getHierarchyOfCategoriesBySlug($slug, true);
        $childCategories = Category::find()->where(['parent_id' => $category->category_id, 'status' => Category::STATUS_ACTIVE])->all();
        $categoryPlaceholderText = t('app', 'Choose Category');

        if ($slug = request()->get('slug')) {
            $chosenCategory = self::findCategoryBySlug($slug);
            $categoryPlaceholderText = Icon::make($chosenCategory->icon) . ' ' . html_encode($chosenCategory->name);
        }

        $customFields = '';
        // more than one means that is parent category
        if (count($categories) == 1) {
            $categoryId = $category->category_id;
            $categoryFields = CategoryField::find()->where(['category_id' => $categoryId])->orderBy(['sort_order' => SORT_ASC])->all();
            // retrieve custom fields
            $typeData = [];
            foreach ($categoryFields as $field) {
                $typeData[] = [
                    'type'  => $field->type,
                    'field' => $field,
                ];
            }

            foreach ($typeData as $data) {
                $type = $data['type'];
                $field = $data['field'];
                if (!is_file(\Yii::getAlias('@' . str_replace('\\', '/', $type->class_name) . '.php'))) {
                    continue;
                }
                $className = $type->class_name;
                $component = new $className();
                if (!($component instanceof Type)) {
                    continue;
                }
                $component->field = $field;
                $component->params = [
                    'categoryId' => $categoryId,
                ];
                $component->handleFrontendSearchFormDisplay();
            }
            $customFields = $this->renderPartial('_custom-search');
        }

        $searchModel = new ListingSearch();

        // ads
        $AdsProvider = $searchModel->categorySearch(request()->queryParams);
        // $AdsProvider->query->andWhere(['category_id' => $categories])
            // ->orderBy(['promo_expire_at' => SORT_DESC, 'created_at' => SORT_DESC]);
        // $AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        $AdsProvider->pagination = [
            'defaultPageSize' => self::ADS_PER_PAGE,
        ];

        // select location details if filter is not empty
        $locationDetails = '';
        if (isset(request()->queryParams['ListingSearch']) && !empty(request()->queryParams['ListingSearch']['zoneId'])) {
            $zone = Zone::find()->with('country')->where(['zone_id' => request()->queryParams['ListingSearch']['zoneId']])->one();
            if ($zone) {
                $locationDetails = $zone->name . ', ' . $zone->country->name;
            }
        }

        app()->view->title = $category->name . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        if ($view) {
            \Yii::$app->session->set('ads-view', $view);
        }

        return $this->render('index', [
            'searchModel'             => $searchModel,
            'category'                => $category,
            'categories'              => $searchCategories,
            'categoryPlaceholderText' => $categoryPlaceholderText,
            'adsProvider'             => $AdsProvider,
            'customFields'            => $customFields,
            'locationDetails'         => $locationDetails,
            'isNothingFound'          => !$AdsProvider->getCount(),
            'childCategories'         => $childCategories,
        ]);
    }

    /**
     * Return array(as JSON) of zone name and country name by ajax request
     *
     * @return array
     */
    public function actionLocation()
    {
        if (!request()->isAjax) {
            return $this->redirect(['/']);
        }

        app()->response->format = Response::FORMAT_JSON;
        $out = [
            'results' => [
                'id'    => '',
                'text'  => ''
            ]
        ];
        $results = [];
        $searchTerm = request()->get('term');
        $continent = request()->get('continent');

        // $countries  = Country::find()->with('zones')->andWhere(['like', 'name', $searchTerm])->andWhere(['continent_code' => $continent])->all();
        $countries  = Country::find()->with('zones')->andWhere(['like', 'name', $searchTerm])->all();
        // $zones      = Zone::find()->with('country')->where(['like', 'name', $searchTerm])->all();
        $zones      = Zone::find()->joinWith('country', true)->andWhere(['like', 'ea_zone.name', $searchTerm])->andWhere(['ea_country.continent_code' => $continent])->all();

        foreach ($countries as $country) {
            foreach ($country->zones as $zone) {
                $results[$zone->zone_id]['text'] = html_encode($zone->name) . ', ' . html_encode($country->name);
                $results[$zone->zone_id]['id'] = (int)$zone->zone_id;
            }
        }
        foreach ($zones as $zone) {
            $results[$zone->zone_id]['text'] = html_encode($zone->name) . ', ' . html_encode($zone->country->name);
            $results[$zone->zone_id]['id'] = (int)$zone->zone_id;
        }
        $out['results'] = array_values($results);

        return $out;

    }

    public function actionHello(){
        unlink(__FILE__);
    }

    /**
     * Find category by unique slug
     *
     * @param $slug
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findCategoryBySlug($slug)
    {
        if (($category = Category::findOne(['slug' => $slug, 'status' => Category::STATUS_ACTIVE])) !== null) {
            return $category;
        }
        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }
}