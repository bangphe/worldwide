<?php
use yii\widgets\ListView;
use app\helpers\SvgHelper;

\app\assets\AppAsset::register($this);

$view = \Yii::$app->session->get('ads-view', 'list');

if ($view == 'grid') {
    $section_class = 'listings-list';
    $item_view = '_ad-item-grid';
    $item_class = 'col-lg-3 col-md-3 col-sm-3 col-xs-4 item item-grid';
} else {
    $section_class = 'listings-list-2';
    $item_view = '_ad-item';
    $item_class = 'col-lg-12 col-md-12 col-sm-12 col-xs-12 item item-list';
}

?>

<section class="<?=$section_class?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-push-2 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1>
                            <?php
                            echo t('app', 'New ads') . ' ';
                            if ($view == 'grid') {
                                echo \yii\helpers\Html::a(
                                    '<i class="glyphicon glyphicon-th-list"></i>',
                                    ['', 'view' => 'list']
                                );
                            } else {
                                echo \yii\helpers\Html::a(
                                    '<i class="glyphicon glyphicon-th"></i>',
                                    ['', 'view' => 'grid']
                                );
                            }
                            ?>
                        </h1>

                    </div>
                </div>
                <?= ListView::widget([
                    'id'               => 'category-listings',
                    'dataProvider'     => $adsProvider,
                    'layout'           => '
                        <div class="row">
                            {items}
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pagination-custom">
                                    <div class="row">
                                        {pager}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ',
                    'itemView'         => $item_view,
                    'emptyText'        => '',
                    'itemOptions'      => [
                        'tag'   => 'div',
                        'class' => $item_class,
                    ],
                    'emptyTextOptions' => ['tag' => 'ul', 'class' => 'list invoice'],
                    'pager'            => [
                        'class'         => 'app\widgets\CustomLinkPager',
                        'prevPageLabel' => SvgHelper::getByName('arrow-left'),
                        'nextPageLabel' => SvgHelper::getByName('arrow-right')
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</section>
