<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

/**
 * Class CategoryFieldValue
 * @package app\models
 */
class Categoryb2b extends \app\models\auto\CategoryFieldValue
{
    /**
     * @inheritdoc
     */
    public $title; public $bsns_type; public $location; public $description; public $proposl_b2b; public $bsns_term; public $category_id;
    public function rules()
    {
        return [
            [['title', 'bsns_type', 'location', 'description', 'proposl_b2b', 'bsns_term', 'category_id'], 'required'],
        ];
    }
}