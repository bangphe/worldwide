<?php
    use yii\helpers\Url;
    use app\models\Listing;
?>
<link href="<?= Url::base(); ?>/assets/site/css/categories.css" rel="stylesheet" type="text/css" />

<section class="listings-list" style="padding: 10px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1><?= \yii\helpers\Html::a(html_encode('All Categories'), ['site/all-categories']);?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php foreach (\app\models\Category::getCategoriesByParentId() as $item) { ?>
                <div class="col-xs-6 col-md-4 col-lg-3 mb-4">
                    <div class="test-item category-item">
                        <a href="<?= url(['category/' . $item->slug]); ?>" class="d-block overflow-hidden rounded">
                            <img alt="Agriculture" src="https://testversions.quantiklab.com/classify/public/uploads/categories/1608657722-home.jpg" class="img-fit category-img">
                            <span class="category-overlay-layer">
                                <span class="d-block center w-100 p-3">
                                    <span class="box-70 mx-auto mb-2"><i class="<?= 'fa ' . $item->icon . ' fa-3x category-icon'; ?>"></i></span>
                                    <span class="d-block text-center category-title"><?= strtoupper($item->name); ?></span>
                                    <!-- <span class="d-block text-center category-title"><?= Listing::countAdsPerCategory($item->category_id); ?></span> -->
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
