<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0.1
 */

namespace app\modules\admin\controllers;

use app\helpers\FileSystemHelper;
use yii\data\ArrayDataProvider;

/**
 * Class ExtensionsController
 * @package app\modules\admin\controllers
 */
class ExtensionsController extends \app\modules\admin\yii\web\Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        // extensions parser
        $extensions = FileSystemHelper::getDirectoryNames(\Yii::getAlias('@app/extensions'));

        $extensionData =  [];
        foreach ($extensions as $extension){

            $className = 'app\extensions\\' . $extension . '\\' . ucfirst(strtolower($extension));
            $instance = new $className();
            $extensionData[] = [
                'id'            => $extension,
                'name'          => $instance->name,
                'description'   => $instance->description,
                'version'       => $instance->version,
                'author'        => $instance->author,
                'status'        => (options()->get('app.extensions.'.$extension.'.status', 'disabled') == 'enabled') ? 'enabled' : 'disabled',
            ];
        }

        $extensionsProvider = new ArrayDataProvider([
            'allModels'     => $extensionData,
            'pagination'    => [
                'pageSize' => 50,
            ],
        ]);

        return $this->render('list', [
            'model' => $extensionsProvider,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionEnable($id)
    {
        $className = 'app\extensions\\' . $id . '\\' . ucfirst(strtolower($id));
        if(!class_exists($className)) {
            notify()->addError(t('app', 'The extension was not found in extensions folder!'));
        } else {
            options()->set('app.extensions.'.$id.'.status', 'enabled');
            notify()->addSuccess(t('app', 'The extension has been successfully enabled!'));
        }
        return $this->redirect(['/admin/extensions']);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDisable($id)
    {
        $className = 'app\extensions\\' . $id . '\\' . ucfirst(strtolower($id));
        if(!class_exists($className)) {
            notify()->addError(t('app', 'The extension was not found in extensions folder!'));
        } else {
            options()->set('app.extensions.'.$id.'.status', 'disabled');
            notify()->addSuccess(t('app', 'The extension has been successfully disabled!'));
        }
        return $this->redirect(['/admin/extensions']);
    }
}
