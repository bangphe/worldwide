<?php
use app\components\CategoriesListWidget;
use app\components\PageSectionWidget;
use app\components\SocialMediaListWidget;
use app\models\Page;
?>

<script>var model = new Object()</script>

<footer id="footer">
    <div class="post-add-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <span class="pull-right"><?= t('app', 'Easy and quick!')?></span>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                            <div class="dropdown-menu" id="main_menu" style="width:200px; top: -426px;">
                                <?php
                                /** @var \app\models\Category $item */
                                foreach (\app\models\Category::getCategoriesByParentId() as $item) {
                                    $children = \app\models\Category::getCategoriesByParentId($item->category_id);

                                    if ($children) {
                                        echo \yii\helpers\Html::a(
                                            ($item->icon ? '<i class="fa ' . $item->icon . '" aria-hidden="true"></i>' : '') .
                                            strtoupper($item->name),
                                            'javascript:void(0)',
                                            [
                                                'id' => 'footer-' . $item->slug . '_menu',
                                                'onmouseover' => new \yii\web\JsExpression("show_menu('footer-{$item->slug}')")
                                            ]
                                        );
                                        echo '<div class="dropdown-menu show_sub" style="width:200px; display:none;" id="footer-' . $item->slug . '">';
                                        /** @var \app\models\Category $child */
                                        foreach ($children as $child) {
                                            echo \yii\helpers\Html::a(
                                                ($child->icon ? '<i class="fa ' . $child->icon . '" aria-hidden="true"></i>' : '') .
                                                strtoupper($child->name),
                                                ['/listing/post/' . $child->category_id]
                                            );
                                        }
                                        echo '</div>';
                                    } else {
                                        echo \yii\helpers\Html::a(
                                            ($item->icon ? '<i class="fa ' . $item->icon . '" aria-hidden="true"></i>' : '') .
                                            strtoupper($item->name),
                                            ['/listing/post/' . $item->category_id]
                                        );
                                    }
                                }
                                ?>
                            </div>
                            <a href="<?=url(['/listing/post']);?>"  class="btn-as reverse">
                                <i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Post new ad');?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <?= CategoriesListWidget::widget(['title' => t('app', 'Categories')]) ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= PageSectionWidget::widget(['sectionType' => Page::SECTION_ONE]) ?>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <?= PageSectionWidget::widget(['sectionType' => Page::SECTION_TWO]) ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <?= SocialMediaListWidget::widget(['title' => t('app', 'Connect')]) ?>
                <a href='https://play.google.com/store/apps/details?id=com.worldwide.advertss&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png' style='width: 160px;'/></a>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            ko.applyBindings(model);
            $('#myTabs a').click(function (e) {
              e.preventDefault();
              $(this).tab('show');
            })
            $('.dropdown-submenu a.test').on("click", function(e){
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
            $(".carousel-responsive").each(function(index, el) {
                var containerHeight = $(el).height();
                $(el).find("img .featured").each(function(index, img) {
                  var w = $(img).prop('naturalWidth');
                  var h = $(img).prop('naturalHeight');
                  $(img).css({
                    'width': Math.round(containerHeight * w / h) + 'px',
                    // 'height': containerHeight + 'px'
                  });
                }),
                $(el).owlCarousel({
                  autoWidth: true
                });
            });
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayBtn: "linked",
                // clearBtn: true,
                todayHighlight: true
            });
            $('.times').timepicker({
              showInputs: false
            });

            $("#startChat").click(function () {
                var listing_id = $(this).attr('data-listing-id');
                var customer_name = $(this).attr('data-customer');
                var customer_id = $(this).attr('data-customer-id');
                var user_id = $(this).attr('data-user-id');
                loadMessage(listing_id, customer_name, customer_id, user_id);
                setReadMessages(listing_id, customer_id);
            });
                  
            $("#endChat").click(function () {
                $('#box-chat').removeClass('popup-box-on');
            });

            $('#btn-chat').click(function() {
                var to_user_id = $(this).attr('data-to-user-id');
                var from_user_id = $(this).attr('data-from-user-id');
                var listing_id = $(this).attr('data-listing-id');
                sendMessage(to_user_id, from_user_id, listing_id);
            });

            $('#chat-message').on('keydown', function(e) {
                if (e.which == 13) {
                    e.preventDefault();
                    var to_user_id = $(this).attr('data-to-user-id');
                    var from_user_id = $(this).attr('data-from-user-id');
                    var listing_id = $(this).attr('data-listing-id');
                    sendMessage(to_user_id, from_user_id, listing_id);
                }
            });

            $('#sendEmail').click(function() {
                var user_id = $(this).attr('data-customer-id');
                var message = $('#message-email').val();
                var sender_id = $(this).attr('data-sender');
                // console.log(user_id);
                var url = '<?= \yii\helpers\Url::to(['listing/send-ad-email']); ?>';
                var param = {
                    user_id: user_id,
                    message: message,
                    sender_id: sender_id,
                };
                if (message == "") {
                    swal({ title: "Warning!", text: "Message form must be filled in", type: "warning" });
                } else {
                    $.ajax({
                        url: url,
                        method: "POST",
                        data: param,
                        beforeSend: function () {
                            // ... your initialization code here (so show loader) ...
                            $('#modalEmail').modal('hide');
                            model.isLoading(true);
                        },
                        complete: function () {
                            // ... your finalization code here (hide loader) ...
                            model.isLoading(false);
                        },
                        success: function(data) {
                            // console.log("data :: ", data);
                            $('#message-email').val('');
                            swal({ title: "Success!", text: "Your message has been successfully sent by email. Thank you", type: "success" });
                        }
                    });
                }
            });

            $('#twidget').twidget({
                locale: 'en',
                marker: 78606,
                type: 'hotel'
            });
        });

        model.isLoading = function (show) {
            if (show) {
                $('#loader').css('opacity', 0).show().animate({
                    opacity: 1
                }, 'fast')
            } else {
                $('#loader').animate({
                    opacity: 0
                }, 'fast', function () {
                    $('#loader').hide().css('opacity', 1)
                })
            }
        }

        function setReadMessages(listing_id, customer_id) {
            var url = '<?= \yii\helpers\Url::to(['listing/set-message']); ?>';
            var param = {
                listing_id: listing_id,
                customer_id: customer_id,
            };
            $.ajax({
                url: url,
                method: "POST",
                data: param,
                success: function(data) {
                    console.log("data :: ", data);
                }
            });
        }

        function loadMessage(listing_id, customer_name, customer_id, user_id) {
            $("#chat-details").html('');
            var url = '<?= \yii\helpers\Url::to(['listing/load-message']); ?>';
            var param = {
                listing_id: listing_id,
                customer_id: customer_id,
                user_id: user_id
            };
            $.ajax({
                url: url,
                method: "POST",
                data: param,
                success: function(data) {
                    var res = data.result;
                    var span = document.getElementById("customer");
                    span.textContent = data.sender;//customer_name;
                    // console.log("res :: ", res);
                    for (var i = 0; i < res.length; i++) {
                        var modal_content = '<div id="user_dialog_'+res[i].chat_id+'" class="direct-chat-msg doted-border">';
                        modal_content += '<div class="direct-chat-info clearfix">';
                        modal_content += '<span class="direct-chat-name pull-left">'+res[i].first_name;
                        modal_content += '</span>';
                        modal_content += '</div>';
                        modal_content += '<div class="direct-chat-text">';
                        modal_content += res[i].message;
                        modal_content += '</div><div class="direct-chat-info clearfix">';
                        modal_content += '<span class="direct-chat-timestamp pull-right">';
                        modal_content += formatAMPM(new Date(res[i].timestamp));
                        modal_content += '</span></div>';
                        $("#chat-details").append(modal_content);   
                    }
                }
            });
            setTimeout(function(){
                $('#box-chat').addClass('popup-box-on');
            }, 500);
        }

        function sendMessage(to_user_id, from_user_id, listing_id) {
            var chat_message = $('#chat-message').val();
            var url = '<?= \yii\helpers\Url::to(['listing/save-message']); ?>';
            var param = {
                to_user_id: to_user_id,
                from_user_id: from_user_id,
                listing_id: listing_id,
                chat_message: chat_message,
            };
            $.ajax({
                url: url,
                method: "POST",
                data: param,
                success: function(data) {
                    var res = data.result;
                    var modal_content = '<div id="user_dialog_'+res.chat_id+'" class="direct-chat-msg doted-border">';
                    modal_content += '<div class="direct-chat-info clearfix">';
                    modal_content += '<span class="direct-chat-name pull-left">'+data.sender;
                    modal_content += '</span>';
                    modal_content += '</div>';
                    modal_content += '<div class="direct-chat-text">';
                    modal_content += res.message;
                    modal_content += '</div><div class="direct-chat-info clearfix">';
                    modal_content += '<span class="direct-chat-timestamp pull-right">';
                    modal_content += formatAMPM(new Date(res.timestamp));
                    modal_content += '</span></div>';
                    $('#chat-message').val('');
                    $("#chat-details").append(modal_content);
                }
            });
        }

        function formatAMPM(date) {
            var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var d = date.getDate();
            var m = strArray[date.getMonth()];
            var y = date.getFullYear();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return (d <= 9 ? '0' + d : d) + '-' + m + '-' + y + ', ' + strTime;
        }

        // setInterval(startrefresh, 1000);
        // function startrefresh() { 
        //     getchatconverlist();
        // }
        // function getchatconverlist() {
        //     $("#chat-details").html('');
        //     var listing_id = $(this).attr('data-listing-id');
        //     var url = '<?//= \yii\helpers\Url::to(['listing/load-message']); ?>';
        //     var param = {
        //         listing_id: listing_id,
        //     };
        //     $.ajax({
        //         url: url,
        //         method: "POST",
        //         data: param,
        //         success: function(data) {
        //             var res = data.result;
        //             for (var i = 0; i < res.length; i++) {
        //                 var modal_content = '<div id="user_dialog_'+res[i].chat_id+'" class="direct-chat-msg doted-border">';
        //                 modal_content += '<div class="direct-chat-info clearfix">';
        //                 modal_content += '<span class="direct-chat-name pull-left">'+data.sender;
        //                 modal_content += '</span>';
        //                 modal_content += '</div>';
        //                 modal_content += '<div class="direct-chat-text">';
        //                 modal_content += res[i].message;
        //                 modal_content += '</div><div class="direct-chat-info clearfix">';
        //                 modal_content += '<span class="direct-chat-timestamp pull-right">';
        //                 modal_content += formatAMPM(new Date(res[i].timestamp));
        //                 modal_content += '</span></div>';
        //                 $("#chat-details").append(modal_content);   
        //             }
        //         }
        //     });
        //     setTimeout(function(){
        //         $('#box-chat').addClass('popup-box-on');
        //     }, 500);
        // }
    </script>
    <style>
        #footer .dropdown-menu.show_sub{
            width: 200px;
            left: -200px;
            top: 24px;
        }
    </style>
</footer>

