<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdminActionLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('app', 'Admin Action Logs');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Clear logs'), ['index'], [
                        'class' => 'm-portlet__nav-link btn btn-danger m-btn m-btn--pill m-btn--air',
                        'data'  => [
                            'confirm' => t('app', 'Are you sure you want to delete all logs?'),
                        ],
                        'data-content'      => t('app', 'This action will delete all the logs permanently'),
                        'data-container'    => 'body',
                        'data-toggle'       => 'popover',
                        'data-trigger'      => 'hover',
                        'data-placement'    => 'top'
                    ]) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState' => true,
            'timeout'         => 10000
        ]); ?>
        <?= GridView::widget([
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                [
                    'label'     => 'Section',
                    'attribute' => 'controller_name',
                ],
                [
                    'label'     => 'Action',
                    'attribute' => 'action_name',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'action_name',
                        \app\models\AdminActionLog::getListOfLoggedActions(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                ],
                'element',
                [
                    'format'    => 'ntext',
                    'attribute' => 'changed_data',
                    'value'     => function ($model) {
                        return StringHelper::truncate($model->changed_data, 40);
                    },
                ],
                [
                    'label'     => 'Staff',
                    'format'    => 'ntext',
                    'attribute' => 'changed_by',
                    'filter'    => Html::activeDropDownList(
                        $searchModel,
                        'changed_by',
                        \app\models\User::getListOfUsers(),
                        ['class' => 'form-control', 'prompt' => 'All']
                    ),
                    'value'     => function ($model) {
                        return ($model->changedBy != null) ? $model->changedBy->getFullName() : t('app', 'Visitor');
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'filter'    => DatePicker::widget(
                        [
                            'model'      => $searchModel,
                            'attribute'  => 'created_at',
                            'options'    => [
                                'class' => 'form-control',
                            ],
                            'dateFormat' => 'yyyy-MM-dd',
                        ]
                    )
                ],
                [
                    'class'    => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
