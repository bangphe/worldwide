<?php
use app\models\Advertisement;
use app\assets\AdAsset;

/** @var \yii\web\View $this */
/** @var \app\models\Listing[] $ads */
/** @var int $col */

$formatter = app()->formatter;

AdAsset::register($this);

$this->registerCss('
    .home-advertisement .item {
        padding: 0;
        color: #fff;
    }
    .home-advertisement .item .overlay {
        background-color: rgba(0, 0, 0, 0.4);
        padding: 4rem 7rem;
    }
    .home-advertisement .owl-nav .owl-prev {
        left: 0;
        top: 40%;
    }
    .home-advertisement .owl-nav .owl-next {
        right: 0;
        top: 40%;
    }
    .home-advertisement .item h2 {
        font-weight: bold;
        margin-bottom: 2rem;
    }
    .home-advertisement .item p {
        font-size: 16px;
        margin-bottom: 2rem;
    }
    .home-advertisement .item a {
        background: #fff;
        text-transform: uppercase;
        font-size: 18px;
        font-weight: bold;
        color: #000000;
    }
');

if ($col == 6) {
    $this->registerJs('var owl = $(\'.home-advertisement\');
    owl.owlCarousel({
        items:' . $col . ',
        loop:true,
        autoHeight: true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        navText: [\'<i class="fa fa-chevron-circle-left"></i>\',\'<i class="fa fa-chevron-circle-right"></i>\'],
        responsive:{
            0:{
                items:4
            },
            600:{
                items:5
            },
            768:{
                items:2
            },
            992:{
                items:4
            },
            1280:{
                items:5
            }
        },
        dots:false
    });');
} else {
    $this->registerJs('var owl = $(\'.home-advertisement\');
    owl.owlCarousel({
        items:' . $col . ',
        loop:true,
        autoHeight: true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        navText: [\'<i class="fa fa-chevron-circle-left"></i>\',\'<i class="fa fa-chevron-circle-right"></i>\'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        },
        dots:false
    });');
}

?>
<section class="listings-list">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>Advertisements</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="home-advertisement owl-carousel carousel-responsive owl-theme">
                    <?php
                        foreach ($ads as $ad) {
                        $check_path = file_exists($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@web').$ad->avatar);
                        
                        if ($check_path) {
                            $imgSRC = Yii::getAlias('@web'.$ad->avatar); 
                        } else { 
                            $imgSRC = Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png');
                        } 
                    ?>
                        <div class="container-fluid no-padding item" style="background-image: url('<?php echo $ad->avatar; ?>'); background-size: 100% 100%;">
                            <div class="overlay">
                                <h2><?php echo $ad->title; ?></h2>
                                <p><?php echo $ad->description; ?></p>
                                <a href="<?= $ad->link; ?>" class="btn" target="_blank">Visit Us!</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-3" style="text-align: center; padding: 0 50px;">                
                <br><br>
                <a href="https://www.dailysteals.com/collections/just-added-deals/products/essence-forbidden-volume-rebel-mascara-3-pack">$8.99 with 3RBL code  -Enjoy this 3 pack of Forbidden Volume Mascara by essence. Features a chaotic fiber brush to further boost lashes for an intensely voluminous look</a>
                <br><br>
                <a href="https://www.dailysteals.com/collections/just-added-deals/products/synthetic-leather-envelope-clutch-style-purse-6-colors">$16.99 with CLTCH code   -This stylish envelope clutch is easy to carry, with plenty of space for what you need. It includes a detachable shoulder strap for convenient carrying.</a>
                <br><br>
                <a href="https://www.dailysteals.com/products/verizon-usb-c-car-charger-with-fast-charge-technology">$9.99 - This Fast Charge compatible accessory supports USB 2.0 Power delivery and works with most devices that use USB–C charging ports. It sells on elsewhere for $20.</a>
            </div>
        </div>
    </div>
</section>
