<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use Yii;
use app\helpers\StringHelper;
use app\models\CustomerStore;
use app\models\Location;
use app\models\Listing;
use app\models\ListingFavorite;
use app\models\Invoice;
use app\models\Customer;
use app\models\CustomerForgotForm;
use app\models\CustomerLoginForm;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use app\yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use app\yii\filters\OwnerAccessRule;

/**
 * Class AccountController
 * @package app\controllers
 */
class AccountController extends Controller
{
    const INVOICES_PER_PAGE = 10;

    const MYADS_PER_PAGE = 10;

    const FAVORITES_PER_PAGE = 10;

    /**
     * init
     */
    public function init()
    {
        parent::init();
    }
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ownerAccess' => [
                'class'      => AccessControl::className(),
                'only'       => ['generate-invoice-pdf', 'send-invoice'],
                'rules'      => [['actions' => ['generate-invoice-pdf', 'send-invoice']]],
                'ruleConfig' => ['class' => OwnerAccessRule::className()],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'auth' => [
                'class'           => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
                'successUrl'      => url(['account/index'], true),
            ],
        ];
    }

    /**
     * @param $client
     * @return bool
     */
    public function oAuthSuccess($client)
    {
        // get the client name (ex : fb/twitter/google/linkedin)
        $clientId = $client->getId();
        // get the user data attributes
        $userAttributes = $client->getUserAttributes();
        // die(var_dump($userAttributes));
        
        if (empty($userAttributes)) {
            notify()->addError(t('app', 'Something went wrong!'));
            return false;
        }

        if ($clientId == 'facebook') {
            $userAttributes['first_name'] = (!empty($userAttributes['name'])) ? explode(' ', $userAttributes['name'])[0] : null;
            $userAttributes['last_name'] = (!empty($userAttributes['name'])) ? explode(' ', $userAttributes['name'])[1] : null;
        } elseif ($clientId == 'twitter') {
            $explName = explode(' ', $userAttributes['name']);
            $userAttributes['first_name'] = $explName[0];
            $userAttributes['last_name'] = $explName[1] == '' ? $userAttributes['screen_name'] : $explName[1];
        }

        $customer = Customer::findByEmail($userAttributes['email']);
        
        if (empty($customer)) {
            $customer               = new Customer();
            $customer->first_name   = $userAttributes['first_name'];
            $customer->last_name    = $userAttributes['last_name'];
            $customer->email        = $userAttributes['email'];
            $customer->password     = StringHelper::random(8);
            $customer->source       = ucfirst($clientId);
            $customer->save(false);

            $customer->sendRegistrationEmail();
        }

        $customer->activate();
        app()->customer->login($customer, 0);
        notify()->addSuccess(t('app', 'Successful action!'));

        return true;
    }

    /**
     * @return Response
     */
    public function actionIndex()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/login']);
        } else {
            return $this->redirect(['account/my-listings']);
        }
    }

    /**
     * @return string|Response
     */
    public function actionMyListings()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/index']);
        }

        $myAdsProvider = new ActiveDataProvider([
            'query'      => Listing::find()->where(['customer_id' => app()->customer->identity->id]),
            'sort'       => ['defaultOrder' => ['listing_id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => self::MYADS_PER_PAGE,
            ],
        ]);

        app()->view->title = t('app','My Ads') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('my-listings', ['myAdsProvider' => $myAdsProvider]);
    }

    /**
     * @return string|Response
     */
    public function actionFavorites()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/index']);
        }
        $favoritesProvider = new ActiveDataProvider([
            'query'      => ListingFavorite::find()->Where(['customer_id' => app()->customer->identity->id]
            ),
            'sort'       => ['defaultOrder' => ['favorite_id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => self::FAVORITES_PER_PAGE,
            ],
        ]);

        app()->view->title = t('app','My Favorites') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('favorites', ['favoritesProvider' => $favoritesProvider]);
    }

    /**
     * Render list of invoices of current user
     *
     * @return string
     */
    public function actionInvoices()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/index']);
        }

        $invoicesProvider = new ActiveDataProvider([
            'query'      => Invoice::find()->joinWith([
                'order' => function ($query) {
                    $query->onCondition(['customer_id' => app()->customer->identity->id]);
                },
            ], true, 'INNER JOIN'),
            'sort'       => ['defaultOrder' => ['invoice_id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => self::INVOICES_PER_PAGE,
            ],
        ]);

        app()->view->title = t('app','My Invoices') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('invoices', ['invoicesProvider' => $invoicesProvider]);
    }

    /**
     * Generate invoice pdf
     *
     * @param $id
     * @return mixed
     */
    public function actionGenerateInvoicePdf($id)
    {
        return app()->generateInvoicePdf->generate($id);
    }

    /**
     * Send invoice by email
     *
     * @param $id
     * @return Response
     */
    public function actionSendInvoice($id)
    {
        if (app()->sendInvoice->send($id)) {
            notify()->addSuccess(t('app', 'Invoice was sent successfully!'));
        } else {
            notify()->addError(t('app', 'Something went wrong!'));
        }

        return $this->redirect(['account/invoices']);
    }

    /**
     * @return string|Response
     */
    public function actionInfo()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/index']);
        }

        $id = app()->customer->id;

        $customerLocation = Location::find()->where(['customer_id' => $id])->one();
        $location = $customerLocation !== null ? $customerLocation : new Location();

        $modelAbout = $this->findModel($id);

        $modelPassword = clone $modelAbout;
        $modelPassword->scenario = 'update_password';

        $modelEmail = clone $modelAbout;
        $modelEmail->scenario = 'update_email';

        $modelCompany = CustomerStore::findOne(['customer_id' => $id]);
        if (empty($modelCompany)) {
            $modelCompany = new CustomerStore();
        }

        app()->view->title = t('app','Account Info') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        /* render the view */

        return $this->render('info_new', [
            'modelAbout'    => $modelAbout,
            'modelPassword' => $modelPassword,
            'modelEmail'    => $modelEmail,
            'modelCompany'  => $modelCompany,
            'modelLocation'  => $location,
        ]);
    }
    
    public function actionChangeAvatar()
    {
        $customer_id = $_POST['customer_id'];
        
        $model = $this->findModel($customer_id);

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $error = null;

        $transaction = db()->beginTransaction();
        try {
            // $models = $images->handleUploadedImages((int)request()->post('id', 0));
            $model->handleUploadedImage($model);
            $transaction->commit();

        } catch (\Exception $e) {
            $error = $e->getMessage() . ' at ' . $e->getFile() . ":" . $e->getLine() . PHP_EOL . $e->getTraceAsString();
            $transaction->rollBack();
        }

        if (!$error && $model) {
            $expPath = explode('/', $model->avatar);
            $imageName = $expPath[4];
            $resp = [
                'initialPreview' => [
                    $model->avatar
                ],
                'initialPreviewConfig' => [[
                    'caption' => $imageName,
                    'url' => url(['/account/remove-avatar']),
                    'key' => $model->id,
                    'exif' => null,
                ]]
            ];
        } else {
            $resp = ['error' => $error ? $error : ($images->hasErrors() ? $images->getFirstErrors() : 'Unknown')];
        }

        return $resp;
    }
    
    public function actionRemoveAvatar()
    {
        $customer_id = $_POST['key'];
        
        $model = $this->findModel($customer_id);

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $error = null;

        $transaction = db()->beginTransaction();
        try {
            $model->avatar = NULL;
            $model->save(false);
            $transaction->commit();

        } catch (\Exception $e) {
            $error = $e->getMessage() . ' at ' . $e->getFile() . ":" . $e->getLine() . PHP_EOL . $e->getTraceAsString();
            $transaction->rollBack();
        }

        if (!$error && $model) {
            // $expPath = explode('/', $model->avatar);
            // $imageName = $expPath[4];
            $resp = [
                'initialPreview' => [
                    $model->avatar
                ],
            ];
        } else {
            $resp = ['error' => $error ? $error : ($images->hasErrors() ? $images->getFirstErrors() : 'Unknown')];
        }

        return $resp;
    }

    /**
     * @return array|Response
     */
    public function actionUpdateInfoAbout()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->identity->id;
        $model = Customer::findOne($id);
        $model->scenario = Customer::SCENARIO_UPDATE;
        $customerLocation = Location::find()->where(['customer_id' => $id])->one();
        $modelLocation = new Location();
        if ($model->load(request()->post()) && $modelLocation->load(request()->post())) {
            // $model->handleUploadedImage();
            $model->currency_id = $_POST['Customer']['currency_id'];
            if ($model->website != NULL || $model->business_description != NULL) {
                $model->website = $_POST['Customer']['website'];
                $model->business_description = $_POST['Customer']['business_description'];
            }
            // $model->birthday = $_POST['Customer']['birthday'];
            if (!$model->save()) {
                return ['result' => 'error', 'errors' => $model->getErrors()];
            }

            if ($customerLocation != null) {
                $dataLocation = Location::findOne($customerLocation->location_id);
                $dataLocation->country_id = $modelLocation->country_id;
                $dataLocation->zone_id = $modelLocation->zone_id;
                $dataLocation->city = $modelLocation->city;
                $dataLocation->zip = $modelLocation->zip;
                if (!$dataLocation->save()) {
                    return ['result' => 'error', 'errors' => $modelLocation->getErrors()];
                }
            } else {
                $modelLocation->customer_id = $id;
                if (!$modelLocation->save()) {
                    return ['result' => 'error', 'errors' => $modelLocation->getErrors()];
                }
            }
        }

        return ['result' => 'success', 'msg' => t('app', 'Your information has been updated.')];
    }

    /**
     * @return array|Response
     */
    public function actionUpdateCompany()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->identity->id;
        $model = Customer::findOne($id);
        $model->scenario = Customer::SCENARIO_UPDATE;

        $store = CustomerStore::find()->where(['customer_id' => $id])->one();
        if (empty($store)) {
            $store = new CustomerStore();
        }

        if (request()->post()) {
            $transaction = db()->beginTransaction();
            $error = false;
            try {
                $groupId = ArrayHelper::getValue(request()->post('Customer'), 'group_id');
                $model->group_id = $groupId;
                $model->save(false);

                if ($groupId == 1) {
                    $store->delete();
                } else {
                    $store->attributes = request()->post('CustomerStore');
                    $store->store_name = request()->post('CustomerStore')['company_name'];
                    $store->customer_id = $id;
                    $store->save();
                }

                $transaction->commit();
            } catch (\Exception $e) {
                return ['result' => 'error', 'msg' => $e->getMessage()];
                $error = true;
                $transaction->rollBack();
            }
            if (!$error) {
                notify()->addSuccess(t('app', 'Your information has been updated.'));
                return $this->redirect(['/account/info']);
            }
        }
    }

    /**
     * @return array|Response
     */
    public function actionUpdatePassword()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->id;
        $model = Customer::findOne($id);
        $model->scenario = 'update_password';
        $model->load(request()->post());

        if (!$model->save()) {
            return ['result' => 'error', 'errors' => $model->getErrors()];
        }

        return ['result' => 'success', 'msg' => t('app', 'Your information has been updated.')];
    }

    /**
     * @return array|Response
     */
    public function actionUpdateEmail()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->id;
        $model = Customer::findOne($id);
        $model->scenario = 'update_email';
        $model->load(request()->post());

        if (!$model->save()) {
            return ['result' => 'error', 'errors' => $model->getErrors()];
        }

        return ['result' => 'success', 'msg' => t('app', 'Your information has been updated.')];
    }

    /**
     * @return array|Response
     */
    public function actionUpdateEmailRecovery()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->id;
        $model = Customer::findOne($id);
        $model->scenario = 'update_email';
        $model->load(request()->post());

        if (!$model->save()) {
            return ['result' => 'error', 'errors' => $model->getErrors()];
        }

        return ['result' => 'success', 'msg' => t('app', 'Your information has been updated.')];
    }

    /**
     * @return Response
     */
    public function actionTerminateAccount()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['account/index']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $id = app()->customer->id;
        $model = Customer::findOne($id);

        $model->deactivate();

        app()->customer->logout();
        notify()->addSuccess(t('app', 'We are sorry to see you leave'));

        return $this->redirect('/');
    }

    /**
     * @return string|Response
     */
    public function actionRegister()
    {
        if (app()->customer->isGuest == false) {
            return $this->redirect(['account/index']);
        }
        
        $model = new Customer([
            'scenario' => Customer::SCENARIO_CREATE
        ]);

        // $location = new Location();

        app()->view->title = t('app','Register') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        if ($model->load(request()->post()) && $model->save()) {

            $cookival = Yii::$app->request->cookies;
            $aid = $cookival->getValue('aid', '');

            if ($aid != '') {
                Customer::affiliateRequest($aid);
            }

            //update user ip data
            $userIP = Customer::getUserIP();
            Customer::updateUserIP($model->customer_id,$userIP);

            // $location->customer_id = $model->customer_id;
            // if ($location->load(request()->post()) && $location->save()) {
                $this->sendRegEmail($model->customer_id);
                $model->login();

                return $this->redirect(['/']);
            // }
        }

        return $this->render('register', [
            'action' => 'create',
            'model'  => $model,
            // 'location'  => $location,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionLogin()
    {
        if (app()->customer->isGuest == false) {
            return $this->redirect(['account/index']);
        }
        $model = new CustomerLoginForm();

        $LoginForm = request()->post('CustomerLoginForm');
        if (isset($LoginForm)) {
            $customer = Customer::findByEmail($LoginForm['email']);
        }
        
        app()->view->title = t('app','Login') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        /* if form is submitted */
        if ($model->load(request()->post()) && $model->login()) {
            if ($customer && $customer->status == Customer::STATUS_DEACTIVATED) {
                notify()->addSuccess(t('app', 'Your email is linked to a deactivated account, We reactivated your account now.'));
                $customer->activate();
            }

            //update user ip data
            $userIP = Customer::getUserIP();
            Customer::updateUserIP($customer->customer_id,$userIP);
            return $this->redirect(['account/index']);
        }

        /* render the view */
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * @return string|Response
     */
    public function actionForgot()
    {

        $model = new CustomerForgotForm();

        /* if form is submitted */
        if ($model->load(request()->post()) && $model->sendEmail()) {
            notify()->addSuccess(t('app', 'Please check your email for confirmation!'));

            return $this->redirect(['account/index']);
        }

        app()->view->title = t('app','Forgot password') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        /* render the view */

        return $this->render('forgot', [
            'model' => $model,
        ]);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function actionReset_password($key)
    {
        if (!($model = Customer::findByPasswordResetKey($key))) {
            notify()->addError(t('app', 'Invalid password reset key!'));

            return $this->redirect(['account/index']);
        }

        $password = security()->generateRandomString(12);
        $model->password = $password;
        $model->password_reset_key = null;
        $model->save(false);

        notify()->addSuccess(t('app', 'Your new password is: {password}', [
            'password' => sprintf('<b>%s</b>', $password),
        ]));

        return $this->redirect(['account/index']);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        app()->customer->logout();
        notify()->addSuccess(t('app', 'Successfully logged out!'));

        return $this->redirect(app()->customer->loginUrl);
    }

    /**
     * @return Response
     */
    public function actionImpersonate()
    {
        if (!session()->get('impersonating_customer')) {
            return $this->redirect(['/account']);
        }

        $backURL = session()->get('impersonating_customer_back_url');

        app()->customer->logout();

        if (!empty($backURL)) {
            return $this->redirect($backURL);
        }
        return $this->redirect(['/admin/customers']);
    }
    
    public function actionExportListingsData()
    {
        if (app()->customer->isGuest == true) {
            return $this->redirect(['account/index']);
        }
        
        ini_set('memory_limit', '8192M'); 
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        
        $model = [];

        //get data
        $header = [
            "Package",
            "Customer Name",
            "Country",
            "Zone",
            "City",
            "Zip",
            "Latitude",
            "Longitude",
            "Category",
            "Currency",
            "Price",
            "Title",
            "Description",
            "Status",
            "Created At",
            "Updated At"
        ];

        $model = new ActiveDataProvider([
            'query'      => Listing::find()->where(['customer_id' => app()->customer->identity->id]),
            'sort'       => ['defaultOrder' => ['listing_id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => self::MYADS_PER_PAGE,
            ],
        ]);
        
        $filename = "Listings-Data-".date('YmdHi').".csv";
        $newModel = [];
        foreach ($model->models as $key => $value) {
            $newModel[$key][] = $value->package == NULL ? 'Free' : $value->package->title;
            $newModel[$key][] = $value->customer->first_name.' '.$value->customer->last_name;
            $newModel[$key][] = $value->location->country->name;
            $newModel[$key][] = $value->location->zone->name;
            $newModel[$key][] = $value->location->city;
            $newModel[$key][] = $value->location->zip;
            $newModel[$key][] = $value->location->latitude;
            $newModel[$key][] = $value->location->longitude;
            $newModel[$key][] = $value->category->name;
            $newModel[$key][] = $value->currency->name;
            $newModel[$key][] = $value->currency->symbol.' '.$value->price;
            $newModel[$key][] = $value->title;
            $newModel[$key][] = $value->description;
            $newModel[$key][] = $value->status;
            $newModel[$key][] = $value->created_at;
            $newModel[$key][] = $value->updated_at;
        }

        $fp = fopen('php://output', 'w');
        
        // $list = array (
        //   array("Peter", "Griffin" ,"Oslo", "Norway"),
        //   array("Glenn", "Quagmire", "Oslo", "Norway")
        // );
        // die(var_dump($list));
        
        header('Content-Type: application/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);

        fputcsv($fp, $header);

        foreach ($newModel as $data)
        {
            fputcsv($fp, $data);
        }
        
        fclose($fp);

        exit;
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }

    protected function sendRegEmail($customer_id)
    {
        $model = $this->findModel($customer_id);
        
        try {
            // send email
            $email = mailer()->compose(['html' =>'registration-email'],['model'=>$model])
                ->setFrom(['admin@worldwideadverts.info' => 'Administrator Worldwideadverts'])
                ->setTo($model->email)
                ->setSubject('Registration Confirmation')
                ->send();
            // if ($email->send()) {
            //     return ['result' => 'success', 'response' => $model];
            // }
        } catch (Exception $e) {
            throw new Exception("Error Processing Request", $e);
        }

        return true;
    }
}