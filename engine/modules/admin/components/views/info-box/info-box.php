<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<span class="m-portlet__head-icon m--hide">
					<i class="flaticon-statistics"></i>
				</span>
				<h3 class="m-portlet__head-text" style="font-size: 16px !important;">
					<?= html_encode($title); ?>
				</h3>
				<h2 class="m-portlet__head-label m-portlet__head-label--accent">
					<span><?= html_encode($data); ?></span>
				</h2>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="<?= $url; ?>" class="m-portlet__nav-link m-portlet__nav-link--icon">
						<i class="fa <?= $icon; ?>"></i>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">
		
	</div>
</div>