<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

if ($action == 'update') {
    $this->title = t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Groups',
        ]) . $model->name;
    $this->params['breadcrumbs'][] = ['label' => t('app', 'Groups'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->group_id]];
    $this->params['breadcrumbs'][] = t('app', 'Update');
}else{
    $this->title = t('app', 'Create Group');
    $this->params['breadcrumbs'][] = ['label' => t('app', 'Groups'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="row">
    <div class="col-md-12 listings-packages-<?= $action; ?>">
        <div class="m-portlet m-portlet--head">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= Html::encode($this->title); ?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools"></div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="m-portlet__body">
                <div class="groups-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'status')->dropDownList([ 'active' => 'active', 'inactive' => 'inactive', ]) ?>
                        </div>
                    </div>
                </div>

                <?php
                foreach ($routesAccess as $key=>$value) { ?>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="heading-<?=$key;?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?=$key;?>" aria-expanded="true" aria-controls="collapse-<?=$key;?>">
                                    <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> <?=Html::encode($value['controller']['name']);?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-<?=$key;?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-<?=$key;?>">
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <div class="page-header">
                                        <h3 class="panel-title"><i class="fa fa-info-circle" aria-hidden="true"></i> <?=Html::encode($value['controller']['description']);?></h3>
                                    </div>
                                </div>
                                <?php foreach ($value['routes'] as $route) {?>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <?= Html::label($route->name, null) ?>
                                            <?php if(trim($route->name) == "Countries") { ?>
                                                <?= Html::dropDownList(
                                                    'GroupRouteAccess['.$route->route.']',
                                                    $route->access,
                                                    $route->getCountries(),
                                                    [
                                                        'id'                => '',
                                                        'class'             => 'form-control',
                                                        'data-content'      => $route->description,
                                                        'data-title'        => 'Info',
                                                        'data-container'    => 'body',
                                                        'data-toggle'       => 'popover',
                                                        'data-trigger'      => 'hover',
                                                        'data-placement'    => 'top',
                                                    ]
                                                ); ?>
                                            <?php } else { ?>
                                            <?= Html::dropDownList(
                                                'GroupRouteAccess['.$route->route.']',
                                                $route->access,
                                                $route->getAccessOptions(),
                                                [
                                                    'id'                => '',
                                                    'class'             => 'form-control',
                                                    'data-content'      => $route->description,
                                                    'data-title'        => 'Info',
                                                    'data-container'    => 'body',
                                                    'data-toggle'       => 'popover',
                                                    'data-trigger'      => 'hover',
                                                    'data-placement'    => 'top',
                                                ]
                                            ); ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="m-portlet__foot">
                <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(t('app', 'Cancel'), ['index'], ['class' => 'btn btn-xs btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>