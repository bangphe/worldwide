<?php
use kartik\mpdf\Pdf;

Yii::setAlias('@webdir', realpath(dirname(__FILE__) . '/../../'));

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'EasyAds',
    'name' => 'EasyAds',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@assets' => '../../assets',
    ],
    'bootstrap' => [
        'log',
        'app\modules\admin\yii\base\AdminBootstrapInterface',
        'app\yii\base\Settings',
    ],
    'on beforeRequest' => ['\app\init\Application', 'webBeforeRequest'],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'defaultRoute' => 'admin',
        ],
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LeDZ5EbAAAAAPcyNONm-eaWVsK1torZRcm82Yq3',
            'secret' => '6LeDZ5EbAAAAAD9Fpakk-_Y3yKrPcY5hO3llOOtR',
        ],
        // 'session' => [
        //    'name' => 'PHPSESSID',
        //    'savePath' => __DIR__ . '/../runtime/sessions',
        // ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '186985254845-v2ltstj354s4mj4m5hpis8kukq2vo3g4.apps.googleusercontent.com',
                    'clientSecret' => 'd2yVPDJdaoTNVztV-nAFIKLO',
                    'returnUrl' => 'http://localhost:8888/worldwide/account/auth?authclient=google',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '812173319162216',
                    'clientSecret' => '632ff9dfe0570572311a7458c3a83a5c',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'attributeParams' => [
                        'include_email' => 'true',
                    ],
                    'consumerKey' => 'TST0FVboFgSfPSU08iZYe0fok',
                    'consumerSecret' => 'kLEqR62SCFCIJzQwN4GG04mRXax6OWRS7zXj6KOzaC3FTQ2oWc',
                ],
                'linkedin' => [
                    'class' => 'yii\authclient\clients\LinkedIn',
                    'clientId' => '81zz6e1bczc4dd',
                    'clientSecret' => 'oEX3ZlT76zMx3DGB',
                ],
                // 'instagram' => [
                //     'class' => 'yii\authclient\clients\Instagram',
                //     'clientId' => 'c45f122e891b4482be878b43910bf01f',
                //     'clientSecret' => '5370dc39a9354e9c8b234a02aeaa5781',
                // ],      
            ],
        ],
        'request' => require(__DIR__ . '/request.php'),
        'db' => require(__DIR__ . '/db.php'),
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'identityCookie' => ['name' => '_identity_user'],
            'loginUrl' => ['/admin'],
        ],
        'customer' => [
            'class' => 'app\yii\web\Customer',
            'identityClass' => 'app\models\Customer',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'loginUrl' => ['/account/login'],
            'identityCookie' => ['name' => '_identity_customer'],
            'idParam' => '__customer_id',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'app\yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'viewPath' => '@app/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.worldwideadverts.info',
                'username' => 'admin@worldwideadverts.info',
                'password' => 'Admin12345',
                'port' => 587,
                // 'encryption' => 'ssl',
             ],
        ],
        'httpClient' => [
            'class' => 'yii\httpclient\Client',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'options' => [
            'class' => '\twisted1919\options\Options'
        ],
        'notify' => [
            'class' => '\twisted1919\notify\Notify',
        ],
        'assetManager' => [
            'baseUrl' => '@web/assets/cache',
            'basePath' => '@webroot/assets/cache',
            'linkAssets' => false,
            'appendTimestamp' => true,
            'assetMap' => [
                'jquery.js' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'enableStrictParsing' => false,
            'rules' => [
                'search' => 'site/search',
                'featured' => 'site/featured',
                'promoted' => 'site/promoted',
                'new' => 'site/new',
                'all-categories' => 'site/all-categories',
                '<controller:account>/<action:invoices>/<page:\d+>' => '<controller>/<action>',
                '<controller:listing>/<action:index|update|package|preview|post>/<slug:[a-z0-9_\-]+>' => '<controller>/<action>',
                'page/<slug:[a-z0-9_\-]+>' => 'pages/index',
                '<controller:category>/<action:location>' => '<controller>/<action>',
                [
                    'pattern' => 'category/<slug:[a-z0-9_\-]+>/<page:\d+>',
                    'route' => 'category/index',
                ],
                [
                    'pattern' => 'category/<slug:[a-z0-9_\-]+>',
                    'route' => 'category/index',
                ],
                [
                    'pattern' => 'store/<slug:[a-z0-9_\-]+>/<page:\d+>',
                    'route' => 'store/index',
                ],
                [
                    'pattern' => 'store/<slug:[a-z0-9_\-]+>',
                    'route' => 'store/index',
                ],
                [
                    'pattern' => 'admin/ads/<slug:[a-z0-9_\-]+>',
                    'route' => 'admin/ads/index',
                ],
                '<url:.+/>' => 'site/redirect',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['apilisting'],
                    'pluralize' => false,
                ],
            ],
        ],
        'formatter' => [
            'decimalSeparator' => '.',
        ],
        'pdf' => [
            'mode' => Pdf::MODE_UTF8,
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
        ],
        'generateInvoicePdf' => [
            'class' => 'app\components\GenerateInvoicePdfComponent',
        ],
        'sendInvoice' => [
            'class' => 'app\components\SendInvoiceComponent',
        ],
        'mailQueue' => [
            'class' => 'app\components\mail\queue\MailQueueComponent',
        ],
        'twigTemplate' => [
            'class' => 'app\components\mail\template\MailTemplateComponent',
        ],
        'mailSystem' => [
            'class' => 'app\components\mail\MailSystemComponent',
        ],
        'migration' => [
            'class' => 'twisted1919\helpers\Migration',
        ],
        'geoip' => ['class' => 'lysenkobv\GeoIP\GeoIP'],
    ],
    'params' => $params,
];

if (YII_ENV != 'production') {
    // configuration adjustments for 'dev' environments
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*']
    ];

    // to clear asset cache
    $config['components']['assetManager']['forceCopy'] = true;
}


if (is_file($file = __DIR__ . "/web-local.php")) {
    $config = \yii\helpers\ArrayHelper::merge($config, require $file);
}
return $config;
