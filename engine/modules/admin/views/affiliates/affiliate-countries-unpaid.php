<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;


$this->title = t('app', 'Performance Report by Day/Week/Month/Year');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary listings-unpaid">
    <div class="box-header">
        <div class="pull-left">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="pull-right">
            <?= Html::a(t('app', 'Refresh'), ['unpaid'], ['class' => 'btn btn-xs btn-success']) ?>
        </div>
    </div>
    <div class="box-body">
        <?php Pjax::begin(['enablePushState'=>true]); ?>
        <?= GridView::widget([
            'id' => 'countries-ads',
            'tableOptions' => ['class' => 'table table-bordered table-hover table-striped'],
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'label' => t('app', 'Country'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->country->name : t('app','No country');
                    },
                ],
                [
                    'label' => t('app', 'Zone'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->zone->name : t('app','No zone');
                    },
                ],
                [
                    'label' => t('app', 'City'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->city : t('app','No city');
                    },
                ],
                [
                    'label' => t('app', 'Package'),
                    'attribute' => 'package_id',
                    'value' => function($model) {
                        $package = $model->package;
                        return $package ? $package->title : t('app','No package');
                    },
                ],
                [
                    'label' => t('app', 'Ad Spesification'),
                    'attribute' => 'title',
                    'value' => function($model) {
                        return $model->title;
                    },
                ],
                [
                    'label' => t('app', 'Category'),
                    'attribute' => 'category_id',
                    'value' => function($model) {
                        $category = $model->category;
                        return $category->parent_id != null ? $category->getParentById($category->parent_id) : $category->name;
                    },
                ],
                [
                    'label' => t('app', 'Day'),
                    'attribute' => 'currency_id',
                    'value' => function($model) {
                        return 0;
                    },
                ],
                [
                    'label' => t('app', 'Week'),
                    'attribute' => 'price',
                    'value' => function($model) {
                        return 0;
                    }
                ],
                [
                    'label' => t('app', 'Month'),
                    'attribute' => 'price',
                    'value' => function($model) {
                        return 0;
                    }
                ],
                [
                    'label' => t('app', 'Year'),
                    'attribute' => 'price',
                    'value' => function($model) {
                        return 0;
                    }
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
