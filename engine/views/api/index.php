<!--
 API Documentation HTML Template  - 1.0.1
 Copyright © 2016 Florian Nicolas
 Licensed under the MIT license.
 https://github.com/ticlekiwi/API-Documentation-HTML-Template
 !-->
<?php
use yii\helpers\Url;
$theme = Url::base().'/assets/api';
?>

<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <title>API Worldwideadverts - Documentation</title>
    <meta name="description" content="">
    <meta name="author" content="ticlekiwi">

    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="<?= $theme; ?>/css/hightlightjs-dark.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/highlight.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,500|Source+Code+Pro:300" rel="stylesheet">
    <link rel="stylesheet" href="<?= $theme; ?>/css/style.css" media="all">
    <script>hljs.initHighlightingOnLoad();</script>
</head>

<body>
<div class="left-menu">
    <div class="content-logo">
        <img alt="platform by Emily van den Heever from the Noun Project" title="platform by Emily van den Heever from the Noun Project" src="<?= $theme; ?>/images/logo.png" height="32" />
        <span>API Documentation</span>
    </div>
    <div class="content-menu">
        <ul>
            <li class="scroll-to-link active" data-target="introduction">
                <a>Introduction</a>
            </li>
            <li class="scroll-to-link" data-target="get-ads">
                <a>Create Ads</a>
            </li>
            <li class="scroll-to-link" data-target="errors">
                <a>Errors</a>
            </li>
        </ul>
    </div>
</div>
<div class="content-page">
    <div class="content-code"></div>
    <div class="content">
        <div class="overflow-hidden content-section" id="content-introduction">
            <h1 id="introduction">Introduction</h1>
            <pre>
    API Endpoint

        https://worldwideadverts.info/apilisting/
                </pre>
            <p>
                The Worldwideadverts API provides programmatic access to read Worldwideadverts data. Retrieve an ads, create an ads, filter them, etc.
            </p>
        </div>
        <div class="overflow-hidden content-section" id="content-get-ads">
            <h2 id="get-ads">create ads</h2>
            <pre><code class="bash">
# Here is a curl example
curl \
-X POST https://worldwideadverts.info/apilisting \
-F 'title=Test title' \
-F 'description=Test description' \
-F 'type=international'
                </code></pre>
            <p>
                To create ads you need to make a POST call to the following url :<br>
                <code class="higlighted">https://worldwideadverts.info/apilisting</code>
            </p>
            <br>
            <pre><code class="json">
Result example :

{
    "category_id": "38",
    "title": "Test title",
    "description": "Test description",
    "type": "international",
    "location_id": "286",
    "currency_id": "1",
    "customer_id": 110,
    "package_id": 1,
    "slug": "test-title-nq955ohm6m1cd",
    "price": 0,
    "created_at": {
        "expression": "NOW()",
        "params": []
    },
    "updated_at": {
        "expression": "NOW()",
        "params": []
    },
    "promo_expire_at": {
        "expression": "NOW()",
        "params": []
    },
    "listing_id": 1147
}
                </code></pre>
            <h4>QUERY PARAMETERS</h4>
            <table>
                <thead>
                <tr>
                    <th>Field</th>
                    <th>Type</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    <!-- <tr>
                        <td>customer_id</td>
                        <td>Integer</td>
                        <td>(required) User that want to create an ads</td>
                    </tr> -->
                <!-- <tr>
                    <td>category_id</td>
                    <td>Integer</td>
                    <td>(required) Category ads</td>
                </tr> -->
                <tr>
                    <td>title</td>
                    <td>String</td>
                    <td>(required) A title from ads</td>
                </tr>
                <tr>
                    <td>description</td>
                    <td>String</td>
                    <td>
                        (required) A description from ads
                    </td>
                </tr>
                <tr>
                    <td>type</td>
                    <td>String</td>
                    <td>
                        (required) An options by type:<br>
                        international<br>
                        local
                    </td>
                </tr>
                <tr>
                    <td>package_id</td>
                    <td>Integer</td>
                    <td>
                        (required) An options by package:<br>
                        1: free<br>
                        3: paid advert<br>
                        4: fatured advert<br>
                        5: paid advert
                    </td>
                </tr>
                <!-- <tr>
                    <td>location_id</td>
                    <td>Integer</td>
                    <td>(required) Location user</td>
                </tr>
                <tr>
                    <td>currency_id</td>
                    <td>Integer</td>
                    <td>(required) Currency ads</td>
                </tr> -->
                <tr>
                    <td>images[]</td>
                    <td>File image</td>
                    <td>Multiple file images</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="overflow-hidden content-section" id="content-errors">
            <h2 id="errors">Errors</h2>
            <p>
                The Worldwideadverts API uses the following error codes:
            </p>
            <table>
                <thead>
                <tr>
                    <th>Error Code</th>
                    <th>Meaning</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>500</td>
                    <td>
                        Some parameters are missing. This error appears when you don't pass every mandatory parameters.
                    </td>
                </tr>
                <tr>
                    <td>404</td>
                    <td>
                        Unknown or invalid <code class="higlighted">attributes</code>. This error appears if you use an unknown parameter <code class="higlighted">attributes</code>.
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="content-code"></div>
</div>
<!-- Github Corner Ribbon - to remove (Ribbon created with : http://tholman.com/github-corners/ )-->
<a href="https://github.com/ticlekiwi/API-Documentation-HTML-Template" class="github-corner" aria-label="View source on Github" title="View source on Github"><svg width="80" height="80" viewBox="0 0 250 250" style="z-index:99999; fill:#70B7FD; color:#fff; position: fixed; top: 0; border: 0; right: 0;" aria-hidden="true"><path d="M0,0 L115,115 L130,115 L142,142 L250,250 L250,0 Z"></path><path d="M128.3,109.0 C113.8,99.7 119.0,89.6 119.0,89.6 C122.0,82.7 120.5,78.6 120.5,78.6 C119.2,72.0 123.4,76.3 123.4,76.3 C127.3,80.9 125.5,87.3 125.5,87.3 C122.9,97.6 130.6,101.9 134.4,103.2" fill="currentColor" style="transform-origin: 130px 106px;" class="octo-arm"></path><path d="M115.0,115.0 C114.9,115.1 118.7,116.5 119.8,115.4 L133.7,101.6 C136.9,99.2 139.9,98.4 142.2,98.6 C133.8,88.0 127.5,74.4 143.8,58.0 C148.5,53.4 154.0,51.2 159.7,51.0 C160.3,49.4 163.2,43.6 171.4,40.1 C171.4,40.1 176.1,42.5 178.8,56.2 C183.1,58.6 187.2,61.8 190.9,65.4 C194.5,69.0 197.7,73.2 200.1,77.6 C213.8,80.2 216.3,84.9 216.3,84.9 C212.7,93.1 206.9,96.0 205.4,96.6 C205.1,102.4 203.0,107.8 198.3,112.5 C181.9,128.9 168.3,122.5 157.7,114.1 C157.9,116.9 156.7,120.9 152.7,124.9 L141.0,136.5 C139.8,137.7 141.6,141.9 141.8,141.8 Z" fill="currentColor" class="octo-body"></path></svg></a><style>.github-corner:hover .octo-arm{animation:octocat-wave 560ms ease-in-out}@keyframes octocat-wave{0%,100%{transform:rotate(0)}20%,60%{transform:rotate(-25deg)}40%,80%{transform:rotate(10deg)}}@media (max-width:500px){.github-corner:hover .octo-arm{animation:none}.github-corner .octo-arm{animation:octocat-wave 560ms ease-in-out}}</style>
<!-- END Github Corner Ribbon - to remove -->
<script src="<?= $theme; ?>/js/script.js"></script>
</body>
</html>