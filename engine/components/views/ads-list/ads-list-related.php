<?php
use app\models\ListingStat;
$formatter = app()->formatter;
?>
<!-- <style type="text/css">
.test {
    height: auto !important;
    width: auto !important;
}
</style> -->

<style type="text/css">
.adslot_1 { display:inline-block; width: 300px; height: 600px; }
@media (max-width:400px) { .adslot_1 { display: none; } }
@media (min-width:500px) { .adslot_1 { width: 200px; height: 400px; } }
@media (min-width:800px) { .adslot_1 { width: 300px; height: 600px; } }
</style>

<section class="listings-list">
    <div class="container">
        <?php if ($title) { ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1><?= $moreLink ? \yii\helpers\Html::a(html_encode($title), $moreLink) : html_encode($title);?></h1>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12" style="margin-left: -15px;">
                <?php foreach ($ads as $ad) { ?>
                    <?php $isFavorite = (!empty($ad->favorite)) ? true : false; ?>
                    <div class="col-xs-6 col-lg-<?=$col;?> col-md-<?=$col;?> item">
                    <!-- <?php
                    //if(html_encode($ad->getPriceAsCurrency($ad->currency->code))!="$0.00")
                    {
                    ?>
                        <div class="price">
                            <?//=html_encode($ad->getPriceAsCurrency($ad->currency->code));?>
                        </div>
                    <?php
                    }
                    ?> -->
                        <div class="image">
                            <a href="#" data-listing-id="<?= (int)$ad->listing_id; ?>" data-stats-type="<?= ListingStat::FAVORITE; ?>" data-add-msg="<?= t('app', 'Add to favorites'); ?>" data-remove-msg="<?= t('app', 'Remove Favorite'); ?>" data-favorite-url="<?= url(['/listing/toggle-favorite']); ?>" class="action-icon favorite-listing <?= (!$isFavorite) ? 'track-stats' : ''; ?>">
                                <?php if ($isFavorite) { ?>
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                <?php } else { ?>
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                <?php } ?>
                            </a>
                            <?php $check_path = file_exists($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@web').$ad->mainImage->image_path);
                                if ($check_path) {
                            ?>
                                <a class="img-link" href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>');">
                                    <img class="lazyloaded" src="<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>" data-src="<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>" alt="" />
                            <?php } else { ?>
                                <a class="img-link" href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>');">
                                    <img class="lazyloaded" src="<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>" data-src="<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>" alt="" />
                            <?php } ?>
                            </a>
                        </div>
                        <div class="info">
                            <?php
                            // if(html_encode($ad->getPriceAsCurrency($ad->currency->code))!="$0.00")
                            // {
                            ?>
                                <div style="color: #3cce69; font-weight: 600;">
                                    <?=html_encode($ad->getPriceAsCurrency($ad->currency->code));?>
                                </div>
                            <?php
                            // }
                            ?>
                            <a href="<?= url(['category/index', 'slug' => $ad->category->slug]); ?>" class="category"><i class="fa <?= $ad->category->icon ? html_encode($ad->category->icon) : 'fa-tags'; ?>" aria-hidden="true"></i> <?= html_encode($ad->category->name); ?></a>
                            <a href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" class="name">
                                <?= html_encode(strtoupper($ad->title)); ?>
                            </a>
                            <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= html_encode($ad->getZoneCountryString()); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
