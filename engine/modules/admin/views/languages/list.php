<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\LanguagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('app', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Create Language'), ['create'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState'=>true,
            ]); ?>
            <?= GridView::widget([
                'id' => 'languages',
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover table-striped',
                ],
                'options'          => ['class' => 'table-responsive grid-view'],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'name',
                     'language_code',
                     'region_code',
                    [
                     'attribute'=>'created_at',
                     'filter'=>  DatePicker::widget([
                                                        'model' => $searchModel,
                                                        'attribute' => 'created_at',
                                                        'options'=>[
                                                            'class'=>'form-control',
                                                            ],
                                                        'dateFormat' => 'yyyy-MM-dd',
                                                    ])
                    ],
                    [
                        'attribute'=>'is_default',
                        'value'=> 'is_default',
                        'filter' => Html::activeDropDownList($searchModel, 'status', [ 'yes' => 'yes', 'no' => 'no', ],['class'=>'form-control','prompt' => 'All'])
                    ],
                    [
                     'attribute'=>'status',
                     'value'=> 'status',
                     'filter' => Html::activeDropDownList($searchModel, 'status', [ 'active' => 'active', 'inactive' => 'inactive', ],['class'=>'form-control','prompt' => 'All'])
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'style'=>'width:100px',
                            'class'=>'table-actions'
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
