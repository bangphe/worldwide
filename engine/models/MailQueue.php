<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Class MailQueue
 * @package app\models
 */
class MailQueue extends \app\models\auto\MailQueue
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors[] = [
            'class'              => TimestampBehavior::className(),
            'value'              => new Expression('NOW()'),
            'updatedAtAttribute' => 'last_attempt_time',
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'attempts', 'last_attempt_time', 'sent_time', 'message_template_type'], 'integer'],
            [['time_to_send', 'swift_message', 'message_template_type'], 'required'],
            [['subject'], 'safe'],
        ];
    }

    /**
     * @return mixed
     */
    public function toMessage()
    {
        return unserialize(base64_decode($this->swift_message));
    }
}
