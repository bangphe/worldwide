<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Invoice;

/**
 * InvoiceSearch represents the model behind the search form about `app\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    const INVOICES_PER_PAGE = 10;

    public $firstName;
    public $lastName;
    public $company;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'order_id'], 'integer'],
            [['created_at', 'updated_at', 'firstName', 'lastName', 'company'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invoice::find();

        // add conditions that should always apply here
        $query->joinWith(['order']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => ['defaultOrder' => ['invoice_id' => SORT_DESC]],
            'pagination' => [
                'defaultPageSize' => self::INVOICES_PER_PAGE,
            ],
        ]);

        $dataProvider->sort->attributes['firstName'] = [
            'asc' => ['first_name' => SORT_ASC],
            'desc' => ['first_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['lastName'] = [
            'asc' => ['last_name' => SORT_ASC],
            'desc' => ['last_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['company'] = [
            'asc' => ['company_name' => SORT_ASC],
            'desc' => ['company_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invoice_id' => $this->invoice_id,
            'order_id'   => $this->order_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->firstName]);
        $query->andFilterWhere(['like', 'last_name', $this->lastName]);
        $query->andFilterWhere(['like', 'company_name', $this->company]);

        return $dataProvider;
    }
}
