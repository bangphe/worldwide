<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\extensions\paypal2\models;

/**
 * Class Paypal2
 * @package app\models\options\gateway
 */
class Paypal2 extends \app\models\options\Base
{
    /**
     * @var string
     */
    public $name = 'PayPal REST';

    /**
     * @var string
     */
    public $status = 'active';

    /**
     * @var string
     */
    public $description = '';

    /**
     * @var string
     */
    public $cid = '';

    /**
     * @var string
     */
    public $secret = '';

    /**
     * @var string
     */
    public $mode = 'sandbox';


    /**
     * @var string
     */
    protected $categoryName = 'app.gateway.paypal2';


    public function rules()
    {
        return [
            [['cid','secret', 'description'], 'required', 'when' => function ($model) {return $model->status == 'active';}],
            [['cid', 'secret'], 'string'],
            ['mode', 'in', 'range' => array_keys($this->getModeList())],
            ['status', 'in', 'range' => array_keys($this->getStatusList())],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description'   => t('app', 'Frontend gateway Description'),
            'cid'      => t('app', 'API Client ID'),
            'secret'      => t('app', 'API Secret'),
            'mode'          => t('app', 'Mode'),
            'status'        => t('app', 'Status'),
        ];
    }

    /**
     * @return array
     */
    public function attributeHelpTexts()
    {
        return [
            'description'    => t('app', 'Gateway Description'),
            'cid'       => t('app', 'Paypal Client ID'),
            'secret'       => t('app', 'Paypal Secret'),
            'mode'           => t('app', 'Gateway Mode'),
            'status'         => t('app', 'Gateway Status'),
        ];
    }

    /**
     * @return array
     */
    public static function getModeList()
    {
        return [
            'live'    => t('app', 'Live'),
            'sandbox' => t('app', 'Sandbox'),
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            'active'    => t('app', 'Active'),
            'inactive' => t('app', 'Inactive'),
        ];
    }
}