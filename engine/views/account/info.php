<?php
use app\assets\AccountAsset;
use yii\bootstrap\ActiveForm;
use app\helpers\DateTimeHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
use app\models\Country;

AccountAsset::register($this);

echo $this->render('_navigation.php', []);

$avatarImage = ($modelAbout->avatar) ? $modelAbout->avatar : Yii::getAlias('@web/assets/admin/img/default.jpg');
?>
<section class="my-account">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Profile picture');?><span class="info"><?=t('app','Manage your avatar');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#avatar-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="avatar-block">
                        <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-avatar',
                            'method'    => 'post',
                            'action'    => ['account/update-info-about'],
                            'enableAjaxValidation' => false,
                        ]); ?>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="image-upload-wrapper">
                                    <div class="image-upload-error" style="display: none"></div>
                                    <?= $form->field($modelAbout, 'avatarUpload')->widget(\kartik\file\FileInput::classname(), [
                                        'options' => [
                                            'class'=>'image-upload',
                                            'data-image'=>$avatarImage,
                                        ],
                                        'pluginOptions' => [
                                            'language' => options()->get('app.settings.common.siteLanguage', 'en'),
                                            'overwriteInitial'=> true,
                                            'maxFileSize'=> 1500,
                                            'showClose'=> false,
                                            'showRemove' => false,
                                            'showCaption'=> false,
                                            'showBrowse'=> false,
                                            'browseOnZoneClick'=> true,
                                            'removeLabel'=> '',
                                            'removeIcon'=> '<i class="glyphicon glyphicon-remove"></i>',
                                            'removeTitle'=> 'Cancel or reset changes',
                                            'elErrorContainer'=> '.image-upload-error',
                                            'msgErrorClass'=> 'alert alert-block alert-danger',
                                            'defaultPreviewContent'=> '<img src="'.$avatarImage.'" alt="Your Avatar" style="width:160px"><h6 class="text-muted">' . t('app','Click to change picture') . '</h6>',
                                            'layoutTemplates'=> ['main2'=> '{preview} {remove} {browse}'],
                                            'allowedFileTypes' => ["image"],
                                        ]
                                    ])->label(false);
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <button type="submit" id="submit-account-info-about" class="btn-as" value="Submit"><?=t('app','Submit');?></button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Tell us about you');?><span class="info"><?=t('app','Please take a few moments to keep this information up to date');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#about-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="about-block">
                            <?php $form = ActiveForm::begin([
                                'id'        => 'form-change-about',
                                'method'    => 'post',
                                'action'    => ['account/update-info-about'],
                                'enableAjaxValidation' => false,
                            ]); ?>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelAbout, 'first_name', [
                                            'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','First Name'), 'class' => ''])->label(false); ?>
                                    </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelAbout, 'last_name', [
                                            'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','Last Name'), 'class' => ''])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'phone', [
                                            'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','Phone'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'gender', [
                                            'template'      => '{input} {error}',
                                    ])->dropDownList([ 'M' => 'Male', 'F' => 'Female', ],['prompt' => t('app','Gender')])->label(false); ?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'currency_id', [
                                        'template'      => '{input} {error}',
                                    ])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency')])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'birthdayDay', [
                                            'template'      => '{input} {error}',
                                    ])->dropDownList(DateTimeHelper::getStaticMonthDays(),['prompt' => t('app','Day')])->label(false);
                                    ?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'birthdayMonth', [
                                            'template'      => '{input} {error}',
                                    ])->dropDownList(DateTimeHelper::getStaticMonths(),['prompt' => t('app','Month')])->label(false);
                                    ?>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'birthdayYear', [
                                                'template'      => '{input} {error}',
                                        ])->dropDownList(DateTimeHelper::getStaticBirthdayYears(),['prompt' => t('app','Year')])->label(false);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                    <?//= $form->field($modelAbout, 'address_country', [
                                      //  'template'      => '{input} {error}',
                                    //])->textInput([ 'placeholder' => t('app','Country'), 'class' => ''])->label(false);
                                    ?>
                                    <?= $form->field($modelLocation, 'country_id', [
                                        'template'      => '{input} {error}',
                                        ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
                                    ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12" id="listing-select-zones-wrapper" data-url=<?=url(['/listing/get-country-zones']);?> data-zone=<?=(int)$modelLocation->zone_id;?>>
                                    <?//= $form->field($modelAbout, 'address_city', [
                                      //  'template'      => '{input} {error}',
                                    //])->textInput([ 'placeholder' => t('app','City'), 'class' => ''])->label(false);
                                    ?>
                                    <?= $form->field($modelLocation, 'zone_id', [
                                        'template'      => '{input} {error}',
                                    ])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                    <?//= $form->field($modelAbout, 'address_city', [
                                      //  'template'      => '{input} {error}',
                                    //])->textInput([ 'placeholder' => t('app','City'), 'class' => ''])->label(false);
                                    ?>
                                    <?= $form->field($modelLocation, 'city', [
                                        'template'      => '{input} {error}',
                                    ])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control'])->label(false); ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                    <?= $form->field($modelLocation, 'zip', [
                                        'template'      => '{input} {error}',
                                    ])->textInput(['placeholder' => t('app','Zip code'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <?= $form->field($modelAbout, 'address_street', [
                                        'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','Street'), 'class' => ''])->label(false);
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'address_house', [
                                        'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','House'), 'class' => ''])->label(false);
                                    ?>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                    <?= $form->field($modelAbout, 'address_flat', [
                                        'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','Flat'), 'class' => ''])->label(false);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <button type="submit" id="submit-account-info-about" class="btn-as" value="Submit"><?=t('app','Submit');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Upgrade to store');?><span class="info"><?=t('app','You can upgrade your account to store and have your own store page');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#company-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="company-block">
                        <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-company',
                            'method'    => 'post',
                            'action'    => ['account/update-company'],
                            'enableAjaxValidation' => false,
                        ]); ?>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <?= $form->field($modelAbout, 'group_id', [
                                    'template'      => '{input} {error}',
                                ])->dropDownList([2 => 'Enable', 1 => 'Disable'])->label(false);
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?//= $form->field($modelCompany, 'store_name', [
                                //    'template'      => '{input} {error}',
                                //])->textInput([ 'placeholder' => t('app','Store Name'), 'class' => ''])->label(false); ?>
                            </div> -->
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <?= $form->field($modelCompany, 'company_name', [
                                    'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Company Name'), 'class' => ''])->label(false); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?= $form->field($modelCompany, 'company_no', [
                                    'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Company No'), 'class' => ''])->label(false); ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <?= $form->field($modelCompany, 'vat', [
                                    'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','VAT'), 'class' => ''])->label(false); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <button type="submit" id="submit-company-info" class="btn-as" value="Submit"><?=t('app','Submit');?></button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Change Password');?><span class="info"><?=t('app','Choose a unique password to protect your account');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#change-password-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="change-password-block">
                        <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-password',
                            'method'    => 'post',
                            'action'    => ['account/update-password'],
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                        ]); ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p><?=t('app','Passwords are case-sensitive and must be at least 6 characters');?>.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelPassword, 'passwordCurrent', [
                                            'template'      => '{input} {error}',
                                    ])->passwordInput([ 'placeholder' => t('app','Enter current password'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelPassword, 'password', [
                                            'template'      => '{input} {error}',
                                    ])->passwordInput([ 'placeholder' => t('app','Enter new password'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelPassword, 'passwordConfirm', [
                                            'template'      => '{input} {error}',
                                    ])->passwordInput([ 'placeholder' => t('app','Reenter new password'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <button type="submit" class="btn-as" value="Search"><?=t('app','Submit');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Change Email Address');?><span class="info"><?=t('app','Choose a unique email for log in');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#email-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="email-block">
                        <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-email',
                            'method'    => 'post',
                            'action'    => ['account/update-email'],
                            'enableAjaxValidation' => false,
                        ]); ?>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelEmail, 'passwordCurrent', [
                                            'template'      => '{input} {error}',
                                    ])->passwordInput([ 'placeholder' => t('app','Enter current password'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <?= $form->field($modelEmail, 'newEmail', [
                                            'template'      => '{input} {error}',
                                    ])->textInput([ 'placeholder' => t('app','Enter New Email'), 'class' => ''])->label(false); ?>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <button type="submit" class="btn-as" value="Submit"><?=t('app','Submit');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Change Recovery Email Address');?><span class="info"><?=t('app','Choose a recovery email');?>.</span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#email-recovery-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="email-recovery-block">
                        <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-email-recovery',
                            'method'    => 'post',
                            'action'    => ['account/update-email-recovery'],
                            'enableAjaxValidation' => false,
                        ]); ?>
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <?= $form->field($modelEmail, 'passwordCurrent', [
                                    'template'      => '{input} {error}',
                                ])->passwordInput([ 'placeholder' => t('app','Enter current password'), 'class' => ''])->label(false); ?>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <?= $form->field($modelEmail, 'newEmailRecovery', [
                                    'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Enter New Recovery Email'), 'class' => ''])->label(false); ?>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <button type="submit" class="btn-as" value="Submit"><?=t('app','Submit');?></button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <h1 class="header-info"><?=t('app','Terminate Account');?><span class="info"><?=t('app','Close your account');?></span></h1>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <a href="#terminate-block" data-toggle="collapse" class="btn-trigger-collapse collapsed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="block-body collapse" id="terminate-block">
                            <?php $form = ActiveForm::begin([
                                'id'        => 'form-terminate-account',
                                'method'    => 'post',
                                'action'    => ['account/terminate-account'],
                                'enableAjaxValidation' => false,
                            ]); ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p><?=t('app','Are you sure you want to delete this account?');?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <button type="submit" class="btn-as" value="Sure"><?=t('app','I\'m Sure');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
