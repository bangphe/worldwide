<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * Class Location
 * @package app\models
 */
class Location extends \app\models\auto\Location
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'zone_id', 'city'], 'required'],
            [['country_id', 'zone_id', 'city', 'zip'], 'trim'],

            [['country_id', 'zone_id'], 'integer'],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => 'country_id'],
            [['zone_id'], 'exist', 'skipOnError' => true, 'targetClass' => Zone::className(), 'targetAttribute' => 'zone_id', 'filter' => function($query) {
                $query->andWhere(['country_id' => (int)$this->country_id]);
            }],
            [['city'], 'string', 'max' => 150],
            [['zip'], 'string', 'max' => 10],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'zone_id'         => t('app', 'Zone'),
            'country_id'      => t('app', 'Country'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        foreach ($this->attributes as $key => $value) {
            if (empty($value)) {
                $this->$key = null;
            }
        }

        $this->updateCoordinates();

        return parent::beforeSave($insert);
    }
    /**
     * @return Location
     */
    public function updateCoordinates()
    {
        if ($this->latitude !== null && $this->longitude !== null) {
            $this->retries = 0;
//            return $this;
        }

        if ($this->hasReachedRetries()) {
            return $this;
        }

        $this->retries  = (int)$this->retries + 1;
        $this->latitude = $this->longitude = null;

        $condition = [];
        foreach (['country_id', 'zone_id', 'city', 'zip'] as $attribute) {
            if (!empty($this->$attribute)) {
                $condition[$attribute] = $this->$attribute;
            }
        }

        if (empty($condition)) {
            return $this;
        }

        $location = static::findOne($condition);

        if ($location !== null && $location->latitude !== null && $location->longitude !== null) {
            $this->latitude  = $location->latitude;
            $this->longitude = $location->longitude;
            return $this;
        }

        $location = [];
        if (!empty($this->country_id) && !empty($this->country)) {
            $location[] = $this->country->name;
        }
        if (!empty($this->zone_id) && !empty($this->zone_id)) {
            $location[] = $this->zone->name;
        }
        foreach (['city', 'zip'] as $name) {
            $location[] = $this->$name;
        }
        $location = array_map('urlencode', array_filter(array_map('trim', $location)));
        $location = implode(',', $location);
        $response = app()->httpClient->get('https://maps.googleapis.com/maps/api/geocode/json', [
            'address' => $location,
            'sensor'  => 'false'
        ])->send();

        if (!$response->isOk) {
            return $this;
        }

        $body = json_decode($response->content);
        if (empty($body->status) || $body->status != "OK" || empty($body->results[0]->geometry->location)) {
            return $this;
        }
        $location = $body->results[0]->geometry->location;
        if (!isset($location->lat) || !isset($location->lng)) {
            return $this;
        }

        $this->latitude  = $location->lat;
        $this->longitude = $location->lng;

        return $this;
    }
    /**
     * @return bool
     */
    public function hasReachedRetries()
    {
        return !empty($this->retries) && (int)$this->retries >= 3;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        // return trim(sprintf('%s, %s, %s, %s', $this->country->name, $this->zone->name, $this->city, $this->zip));
        return trim(sprintf('%s, %s, %s', $this->country->name, $this->zone->name, $this->city));
    }
}
