<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('app', 'Invoices');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'id'           => 'invoices',
            'tableOptions' => [
                'class' => 'table table-bordered table-hover table-striped',
            ],
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'invoice_id',
                'order_id',
                [
                    'label'     => 'First Name',
                    'format'    => 'ntext',
                    'attribute' => 'firstName',
                    'value'     => function ($model) {
                        return $model->order->first_name;
                    },
                ],
                [
                    'label'     => 'Last Name',
                    'format'    => 'ntext',
                    'attribute' => 'lastName',
                    'value'     => function ($model) {
                        return $model->order->last_name;
                    },
                ],
                [
                    'label'     => 'Company',
                    'format'    => 'ntext',
                    'attribute' => 'company',
                    'value'     => function ($model) {
                        return $model->order->company_name;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'filter'    => DatePicker::widget(
                        [
                            'model'      => $searchModel,
                            'attribute'  => 'created_at',
                            'options'    => [
                                'class' => 'form-control',
                            ],
                            'dateFormat' => 'yyyy-MM-dd',
                        ]
                    )
                ],
                [
                    'class'    => 'yii\grid\ActionColumn',
                    'template' => '{generate-pdf} {send-invoice}',
                    'buttons'  => [
                        'generate-pdf' => function ($url) {
                            return Html::a(
                                '<i class="fa fa-file-pdf-o"></i>',
                                $url,
                                [
                                    'data-pjax' => '0',
                                    'target'    => '_blank',
                                    'style'     => 'margin-right: 5px',
                                    'data-content'      => t('app', 'Generate PDF'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                                ]
                            );
                        },
                        'send-invoice' => function ($url) {
                            return Html::a(
                                '<i class="fa fa-envelope-o"></i>',
                                $url,
                                [
                                    'data-pjax' => '0',
                                    'data-content'      => t('app', 'Send invoice to client'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                                ]
                            );
                        },
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
