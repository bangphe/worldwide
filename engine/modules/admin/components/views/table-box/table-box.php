<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?=html_encode($title);?>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                <tr>
                    <?php foreach ($columns as $key => $column) { ?>
                    <th><?=html_encode($column);?></th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($data as $item) {
                ?>
                    <tr>
                    <?php
                    foreach ($columns as $key=>$column) {
                    ?>
                        <td>
                            <?= html_encode($item->$key); ?>
                        </td>
                    <?php
                    }
                    ?>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="m-portlet__foot">
        <div class="row align-items-center">
            <div class="col-lg-12 m--align-right">
              <a href="<?=$link;?>" class="btn btn-brand"><?=t('app', 'View all items');?></a>  
            </div>
        </div>
    </div>
</div>