<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AdvertisementsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('app', 'Advertisements');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Create Advertisement'), ['create'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState'=>true,
            ]); ?>
            <?= GridView::widget([
                'id' => 'advertisements',
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover table-striped',
                ],
                'options'          => ['class' => 'table-responsive grid-view'],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'title',
                    'description',
                    'link',
                    [
                        'attribute'=>'start_date',
                        'filter'=>  DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'start_date',
                            'options'=>[
                                'class'=>'form-control',
                            ],
                            'dateFormat' => 'yyyy-MM-dd',
                        ])
                    ],
                    [
                        'attribute'=>'end_date',
                        'filter'=>  DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'end_date',
                            'options'=>[
                                'class'=>'form-control',
                            ],
                            'dateFormat' => 'yyyy-MM-dd',
                        ])
                    ],
                    [
                        'attribute'=>'created_at',
                        'filter'=>  DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'options'=>[
                                'class'=>'form-control',
                            ],
                            'dateFormat' => 'yyyy-MM-dd',
                        ])
                    ],
                    [
                        'attribute'=>'status',
                        'value'=> 'status',
                        'filter' => Html::activeDropDownList($searchModel, 'status', \app\yii\db\ActiveRecord::getStatusesList(),['class'=>'form-control','prompt' => 'All'])
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'style'=>'width:100px',
                            'class'=>'table-actions'
                        ],
                        'template' => '{activate} {deactivate} {update}',
                        'buttons' => [
                            'activate' => function ($url, $model) {
                                return ($model->status === \app\models\Advertisement::STATUS_DEACTIVATED) ? Html::a(
                                    '<i class="fa fa-check-circle"></i>',
                                    $url,
                                    [
                                        'class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only',
                                        'title' => t('app', 'Activate'),
                                        'data-pjax' => '0',
                                        'data-confirm' => t('app', 'Are you sure you want to activate this item?')
                                    ]
                                ) : '';
                            },
                            'deactivate' => function ($url, $model) {
                                return $model->status === \app\models\Advertisement::STATUS_ACTIVE ? Html::a(
                                    '<i class="fa fa-times"></i>',
                                    $url,
                                    [
                                        'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                                        'title' => t('app', 'Deactivate'),
                                        'data-pjax' => '0',
                                        'data-confirm' => t('app', 'Are you sure you want to deactivate this item?')
                                    ]
                                ) : '';
                            },
                            'update' => function ($url, $model) {
                                return Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$model->advertisement_id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
                            },
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
