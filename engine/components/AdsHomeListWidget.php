<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\components;

use app\models\Listing;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\db\Expression;

class AdsHomeListWidget extends Widget
{
    /**
     * @var int number of items to retrieve
     */
    public $quantity = 8;

    /**
     * @var
     */
    public $col = 4;


    /**
     * @return string
     */
    public function run()
    {
        /** @var \app\yii\web\Customer $customer */
        $customer = app()->customer;
        $id_country = $customer->getCountryId();

        $query = Listing::find()->alias('t')
            ->joinWith(['package c1', 'location l'], true, 'INNER JOIN')
            ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
            // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
            ->where(['t.status' => Listing::STATUS_ACTIVE])
            ->limit($this->quantity);

        // $query->andWhere(['>', 'promo_expire_at', new Expression('NOW()')])
        $query->andWhere(['c1.promo_show_featured_area' => 'yes'])
            ->orderBy(new Expression(($id_country ? 'FIELD(l.country_id, ' . $id_country .') DESC, ' : '') . 'rand()'));

        // die(var_dump($query->createCommand()->getRawSql()));
        $ads = $query->all();
        $count = count($ads);

        if (!empty($ads)) {
            return $this->render('ads-list/ads-home-list', [
                'ads' => $ads,
                'col' => $this->col == $count ? $this->col : $count,
            ]);
        }

        return false;
    }
}
