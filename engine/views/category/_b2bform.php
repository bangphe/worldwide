<?php
use app\assets\AdAsset;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use app\helpers\FrontendHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
use app\models\Country;

AdAsset::register($this);

if (isset($_GET['cat'])) 
{
        $querystringcatslug=$_GET['cat'];
		$connection = app()->getDb();
		 $command = $connection->createCommand("Select * from ea_category where parent_id=(Select Category_ID from ea_category where slug='".$querystringcatslug."')");
        $SubCatList = $command->queryAll(); 
		foreach ($list as $category) 
	   {
		   echo "1".$category["name"];
	   }
								
}
else
{

} 									







$picount=options()->get('app.settings.common.homePicNumber');
$script = <<< JS
$(document).ready(function() {
	debugger;
    $('#post-form').yiiActiveForm('add',
        {
			
            "id":"listingimage-imagesgallery",
            "name":"imagesGallery[]",
            "container":".field-listingimage-imagesgallery",
            "input":"#listingimage-imagesgallery",
            "error":".help-block.help-block-error",
            "validate":function (attribute, value, messages, deferred) {
				
				

				
                if ($('.file-preview-frame').length == 0) {
					debugger
                    yii.validation.required(value, messages, {"message": $('#post-form').data('err-msg-gallery')});
                }
				
				var piccount = 0;
				$('.kv-preview-thumb').each(function() {
					piccount++;    
				});
				
				if(piccount> $picount )
				{
					alert('You can upload only $picount images');	
				}
            }
        }
    );
}); 
JS;

$this->registerJs($script, yii\web\View::POS_LOAD);
?>
<section class="post-listing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php $listing_id = (int)($action == 'update') ? $ad->listing_id : 0;
                    $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-listing'   => $listing_id,
                            'data-err-msg-gallery' => t('app', 'Please add at least one image to your ad post')
                        ],
                        'id'        => 'post-form',
                        'method'    => 'post',
                ]); ?>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <h1><?= ($action == 'update') ? t('app', 'Update Ads for B2B') : t('app', 'Post Ads for B2B'); ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($ad, 'title', [
                                            'template'      => '{input} {error}',
                                        ])->textInput([ 'placeholder' => 'What you are looking to benefit as business to business', 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($ad, 'description', [
                                            'template'      => '{input} {error}',
                                        ])->textInput([ 'placeholder' => 'What you are looking to benefit as business to business', 'class' => 'form-control'])->label(false);?>
                                        <!-- /*$form->field($ad, 'description')->widget(CKEditor::className(), [
                                            'options' => ['rows' => 6],
                                            'preset' => 'basic',
                                            'clientOptions' => [
                                                'removePlugins' => 'notification, clipboard, pastetext, pastefromword, tableselection, toolbar',
                                                'contentsCss' => [Yii::getAlias('@web/assets/site/css/customCkeditor.css')],
                                            ]
                                        ])->label(); */ -->
                                    </div>
                                </div>
                                
                                <input type="hidden" id="listing-category_id2" class="form-control" name="Listing5[category_id]" data-original-title="" title="" value="36">
                                <input type="hidden" id="listing-category_id" class="form-control" name="Listing[category_id]" data-original-title="" title="" value="36">
                                

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Price');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'currency_id', [
                                            'template'      => '{input} {error}',
                                        ])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency')])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'price', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Price'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'negotiable', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Contact details');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($customer, 'phone', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Phone'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($ad, 'hide_phone', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($customer, 'email', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Email Address'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($ad, 'hide_email', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($location, 'country_id', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="listing-select-zones-wrapper" data-url="<?=url(['/listing/get-country-zones']);?>" data-zone = <?=(int)$location->zone_id;?>>
                                        <?= $form->field($location, 'zone_id', [
                                            'template'      => '{input} {error}',
                                        ])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <?= $form->field($location, 'city', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($location, 'zip', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Zip code'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="row category-fields" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Category Fields');?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row category-fields" id="category-fields" data-url="<?=url(['/listing/get-category-fields']);?>" style="display: none">
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Photo Gallery');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                        $imagesPreview = [];
                                        $imagesPreviewConfig = [];
                                        if ($action == 'update') {
                                            // sort for images sort_order
                                            usort($uploadedImages, function ($a, $b) { return strnatcmp($a['sort_order'], $b['sort_order']); });
                                            if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                                                $imagesPreview[] = $image->image_path;
                                                $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                                                $imagesPreviewConfig[$key]['url'] = url(['/listing/remove-ad-image']);
                                                $imagesPreviewConfig[$key]['key'] = $image->image_id;
                                            }
                                        }
                                        echo $form->field($images, 'imagesGallery[]')->widget(FileInput::classname(), [
                                            'options' => [
                                                'multiple' => true,
                                                'class' =>'file-loading',
                                                'data-sort-listing-images' => url(['/listing/sort-ad-images']),
                                            ],
                                            'pluginOptions' => [
                                                'initialPreview'=> $imagesPreview,
                                                'initialPreviewConfig' =>
                                                    $imagesPreviewConfig
                                                ,
                                                'initialPreviewAsData'=>true,
                                                'language' => html_encode(options()->get('app.settings.common.siteLanguage', 'en')),
                                                'deleteUrl' => false,
                                                'uploadUrl' => false,
                                                'allowedFileTypes' => ["image"],
                                                'showUpload' => false,
                                                'showRemove' => false,
                                                'showClose' => false,
                                                'browseOnZoneClick' => true,
                                                'browseClass' => 'btn btn-as',
                                                'removeClass' => 'btn btn-as reverse',
                                                'uploadClass' => 'btn btn-as reverse',
                                                'layoutTemplates' =>
                                                    [
                                                        'footer'=> '<div class="file-thumbnail-footer">' . '{progress} {actions}' . '</div>'
                                                    ],
                                                'fileActionSettings' => [
                                                    'showUpload' => false,
                                                    'showDrag' => true,
                                                ],
                                                'overwriteInitial'=>false,
                                            ]
                                        ])->label(false);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="a-center">
                                    <button type="submit" name="post" class="btn-as"><?=t('app','Submit');?></button>
                                    <button type="submit" name="preview" class="btn-as danger-action"><?=t('app','Preview');?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-post-listing-contact" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-notice" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title"><?=t('app', 'Notice');?></h1>
                <a href="javascript:;" class="x-close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            <div class="modal-body">
                <p><?=t('app','Please leave at least one contact option');?></p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn-as black pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>