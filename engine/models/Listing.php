<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/**
 * Class Listing
 * @package app\common\models
 */
class Listing extends \app\models\auto\Listing
{
    const STATUS_ACTIVE             = 'active';
    const STATUS_EXPIRED            = 'expired';
    const STATUS_DRAFT              = 'draft';
    const STATUS_DEACTIVATED        = 'deactivated';
    const STATUS_PENDING_APPROVAL   = 'pending approval';

    const TYPE_LOCAL = 'local';
    const TYPE_INTERNATIONAL = 'local';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id','title', 'description'], 'required'],
            [['title', 'price'], 'trim'],
            [['category_id', 'currency_id', 'location_id', 'negotiable', 'hide_phone', 'hide_email'], 'integer'],
            [['title'], 'string', 'max' => 80],
            [['business_types'], 'string', 'max' => 80],
            [['description'], 'string', 'min' => 30],
            [['terms'], 'string'],
            [['offer'], 'string'],
            [['share'], 'string'],
            [['description'], 'string'],
            [['capital'],'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors[] = [
            'class'     => SluggableBehavior::className(),
            'value' => [$this, 'getSlug'] //https://github.com/yiisoft/yii2/issues/7773
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title'         => t('app', 'Ad title'),
            'price'         => t('app', 'Ad price'),
            'type '         => t('app', 'Ad type'),
            'category_id'   => t('app', 'Ad category'),
            'currency_id'   => t('app', 'Ad currency'),
            'description'   => t('app', 'Description'),
            'negotiable'    => t('app', 'Negotiable Price'),
            'hide_phone'    => t('app', 'Hide phone in ad'),
            'hide_email'    => t('app', 'Hide email in ad'),
        ]);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
		//Bhupi
		if($this->price=="")
		{
			$this->price=0;
		}
		// if($this->currency_id=="")
		// {
		// 	$this->currency_id=1;
		// }

        if($this->type=="")
        {
            $this->type=self::TYPE_LOCAL;
        }

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isNewRecord && $this->customer_id === null) {
            $this->customer_id = app()->customer->identity->id;
        }

        // this will change the way of listing ads ( promo and regular ) v 1.0.1
        if($this->promo_expire_at === null) {
            $this->promo_expire_at = $this->created_at;
        }

        return true;
    }

    /**
     * @return array|null|string|\yii\db\ActiveRecord
     */
    public function generateSlug()
    {
        $unique = \app\helpers\StringHelper::uniqid();
        $exists = $this->findBySlug($unique);

        if (!empty($exists)) {
            return $this->generateSlug();
        }

        return $unique;
    }

    /**
     * @param $slug
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findBySlug($slug)
    {
        return $this->find()->where(array(
            'slug' => $slug,
        ))->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['category_id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(CustomerStore::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['location_id' => 'location_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(ListingPackage::className(), ['package_id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFieldValues()
    {
        return $this->hasMany(CategoryFieldValue::className(), ['listing_id' => 'listing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ListingImage::className(), ['listing_id' => 'listing_id']);
    }

    /**
     * @param $customer
     * @return bool
     */
    public function isOwnedBy($customer)
    {
        $customer_id = $customer;
        if (is_object($customer_id) && !empty($customer_id->customer_id)) {
            $customer_id = $customer_id->customer_id;
        }
        return $customer_id == $this->customer_id;
    }

    /**
     * @return bool
     */
    public function deactivate()
    {
        $this->status = self::STATUS_DEACTIVATED;
        $this->save(false);
        return true;
    }

    /**
     * @return bool
     */
    public function activate()
    {
        if($this->status == self::STATUS_DEACTIVATED) {
            $this->status = self::STATUS_ACTIVE;
            $this->save(false);
        }
        return true;
    }

    /**
     * @return bool
     */
    public static function sendAdActivateEmail($ad = null)
    {
        if ($ad == null || $ad->status == self::STATUS_ACTIVE) {
            return false;
        }

        if (!($modelCustomer = Customer::find()->where(['customer_id'=>$ad->customer_id])->one())) {
            return false;
        }

        return app()->mailSystem->add('ad-is-active', ['listing_id' => $ad->listing_id]);
    }

    /**
     * @return bool
     */
    public static function sendAdDeactivateEmail($ad = null)
    {
        if ($ad == null || $ad->status == self::STATUS_DEACTIVATED) {
            return false;
        }

        if (!($modelCustomer = Customer::find()->where(['customer_id'=>$ad->customer_id])->one())) {
            return false;
        }

        return app()->mailSystem->add('ad-is-deactivated', ['listing_id' => $ad->listing_id]);
    }

    /**
     * @return bool
     */
    public static function sendWaitingApprovalEmail($ad = null)
    {
        if ($ad == null || $ad->status != self::STATUS_PENDING_APPROVAL) {
            return false;
        }

        if (!($modelCustomer = Customer::find()->where(['customer_id'=>$ad->customer_id])->one())) {
            return false;
        }

        return app()->mailSystem->add('ad-is-waiting-approval', ['listing_id' => $ad->listing_id]);
    }

    /**
     * @param null $ad
     * @return string
     */
    public static function getMetaDescription($ad = null)
    {
        return StringHelper::truncate(strip_tags($ad->description), 160, '');
    }

    /**
     * @param $event
     * @return string
     * //https://github.com/yiisoft/yii2/issues/7773
     */
    public function getSlug($event)
    {
        if(!empty($event->sender->slug)) {
            return $event->sender->slug;
        }
        return Inflector::slug($event->sender->title . ' ' . $this->generateSlug());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorite()
    {
        $id = 0;
        if(app()->customer->identity) {
            $id = app()->customer->identity->id;
        }
        return $this->hasOne(ListingFavorite::className(), ['listing_id' => 'listing_id'])
            ->andOnCondition(['customer_id' => $id]);
    }

    /**
     * @return ListingImage|array|null
     */
    public function getMainImage()
    {
        return $this->hasOne(ListingImage::className(), ['listing_id' => 'listing_id'])->andFilterWhere(['sort_order' => '1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStat()
    {
        return $this->hasOne(ListingStat::className(), ['listing_id' => 'listing_id']);
    }

    /**
     * Check whether an ad is negotiable
     *
     * @return bool
     */
    public function isNegotiable()
    {
        return (bool)$this->negotiable;
    }

    /**
     * @param $currencyCode
     * @return bool|string
     */
    public function getPriceAsCurrency($currencyCode)
    {
        if(!$currencyCode) return false;
        return app()->formatter->asCurrency($this->price,$currencyCode,[\NumberFormatter::MIN_FRACTION_DIGITS => 0]);
    }

    /**
     * @return string
     */
    public function getUpdatedDateAsDateTimeFull()
    {
        return app()->formatter->asDatetime($this->updated_at, 'full');
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function getIsExpired()
    {
        return $this->status == self::STATUS_EXPIRED;
    }

    /**
     * @return bool
     */
    public function getIsFavorite()
    {
        return (!empty($this->favorite)) ? true : false;
    }

    public function getIsPromoted()
    {
        return (strtotime(date("Y-m-d H:i:s")) < strtotime($this->promo_expire_at)) ? true : false;
    }

    /**
     * Get string that consist of zone name and country name
     *
     * @return string
     */
    public function getZoneCountryString()
    {
        return $this->location->zone->name . ', ' . $this->location->country->name;
    }

    /**
     * @return int
     */
    public function getOrderCountByLocation($listing_id, $status){
        $count = \app\models\Order::find()->where(['listing_id' => $listing_id])->andWhere(['status' => $status])->count();
        return $count;
    }

    /**
     * @return int
     */
    public static function countAdsPerCategory($category_id){
        $count = self::find()->where(['category_id' => $category_id])->andWhere(['status' => 'active'])->count();
        $children = \app\models\Category::getCategoriesByParentId($category_id);
        $category_ids = [];
        if (!empty($children)) {
            foreach($children as $child) {
                array_push($category_ids, $child->category_id);
            }
            $count = self::find()->where(['category_id' => $category_ids])->andWhere(['status' => 'active'])->count();
        }
        
        return $count;
    }
}