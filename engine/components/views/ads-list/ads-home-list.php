<?php
use app\models\ListingStat;
use app\assets\AdAsset;

/** @var \yii\web\View $this */
/** @var \app\models\Listing[] $ads */
/** @var int $col */

$formatter = app()->formatter;

AdAsset::register($this);

$this->registerCss('.home-promo .item {
        height: 224px
        background: #4DC7A0;
        padding: 1rem;
    }
    
    .home-promo {
        padding-left: 45px;
        margin-bottom: 10px
    }

/*bangucen*/

@media (min-width: 320px) and (max-width: 480px) {
    .navbarheader {margin-top:67px !important; width: auto;}
    #header .header-wrapper {border-bottom:none !important;}
}

.owl-theme .owl-dots,.owl-theme .owl-nav {
    text-align: center;
    -webkit-tap-highlight-color: transparent
}

.owl-theme .owl-nav {
    margin-top: 0px
}

.owl-theme .owl-nav [class*=owl-] {
    color: #FFF;
    font-size: 14px;
    margin: 30px;
    padding: 4px 7px;
    background: #D6D6D6;
    display: inline-block;
    cursor: pointer;
    border-radius: 3px
}

.owl-theme .owl-nav [class*=owl-]:hover {
    background: #869791;
    color: #FFF;
    text-decoration: none
}

.owl-theme .owl-nav .disabled {
    opacity: .5;
    cursor: default
}

.owl-theme .owl-nav.disabled+.owl-dots {
    margin-top: 10px
}

.owl-theme .owl-dots .owl-dot {
    display: inline-block;
    zoom:1}

.owl-theme .owl-dots .owl-dot span {
    width: 10px;
    height: 10px;
    margin: 5px 7px;
    background: #D6D6D6;
    display: block;
    -webkit-backface-visibility: visible;
    transition: opacity .2s ease;
    border-radius: 30px
}

.owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span {
    background: #869791
}
    
.owl-carousel{
  position: relative;
}

.owl-carousel .owl-nav{
  background: #000;
}

.owl-nav .owl-prev{
  position: absolute;
  top:25%;
  left:48px;
  background: #fff !important;
  color: #000 !important;
  opacity: 0.7;
}

.owl-nav .owl-next{
  position: absolute;
  top:25%;
  right:0px;
  background: #fff !important;
  color: #000 !important;
    opacity: 0.7;
}

.owl-nav .fa{
  font-size: 2em;
}

.owl-nav {
  display: none;
}

.owl-carousel:hover .owl-nav {
  display: block;
}

.owl-carousel .owl-item img {
    width: 80% !important;
    background-color: #ccc;
}

.featured {
    height: 185px;
    width: 180px;
}


//.owl-theme .owl-nav .owl-prev {
//  left: 0;
//  padding-left: 20px;
//}
//.owl-theme .owl-nav .owl-next {
//  right: 0;
//  padding-right: 20px;
//}
    
    ');

if ($col == 6) {
    $this->registerJs('var owl = $(\'.home-promo\');
    owl.owlCarousel({
        items:' . $col . ',
        loop:true,
        autoHeight: true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        navText: [\'<i class="fa fa-chevron-circle-left"></i>\',\'<i class="fa fa-chevron-circle-right"></i>\'],
        responsive:{
            0:{
                items:4
            },
            600:{
                items:5
            },
            768:{
                items:2
            },
            992:{
                items:4
            },
            1280:{
                items:5
            }
        },
        dots:false
    });');
} else {
    $this->registerJs('var owl = $(\'.home-promo\');
    owl.owlCarousel({
        items:' . $col . ',
        loop:true,
        autoHeight: true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        nav: true,
        navText: [\'<i class="fa fa-chevron-circle-left"></i>\',\'<i class="fa fa-chevron-circle-right"></i>\'],
        // responsive:{
        //     0:{
        //         items:1
        //     },
        //     600:{
        //         items:3
        //     },
        //     1000:{
        //         items:3
        //     }
        // },
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            768:{
                items:3
            },
            992:{
                items:4
            },
            1280:{
                items:4
            }
        },
        dots:false
    });');
}

?>
<div class="home-promo owl-carousel carousel-responsive owl-theme row" style="margin-top: -7px;">
    <?php
        foreach ($ads as $ad) {
        $check_path = file_exists($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@web').$ad->mainImage->image_path);
    ?>
        <div class="container-fluid no-padding item">
            <a href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>">
                <?php if ($check_path) { ?>
                    <img class="resizeImg featured" src="<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>" alt="" style="margin:0 auto;">
                <?php } else { ?>
                    <img class="resizeImg featured" src="<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>" alt="" style="margin:0 auto;">
                <?php } ?>
            </a>
        </div>
    <?php } ?>
</div>
