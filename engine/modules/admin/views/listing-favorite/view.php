<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = t('app','Listing Favorite') . ' #' . $model->favorite_id;
$this->params['breadcrumbs'][] = ['label' => t('app', 'Listing Favorite'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary orders-view">
    <div class="box-header">
        <div class="pull-left">
                <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="pull-right">
        </div>
    </div>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'favorite_id',
                [
                    'label' => 'Ad title',
                    'value' => ($model->listing_id) ? Html::a($model->ad->title, ['/admin/listings/view', 'id' => $model->listing_id]) : t('app', 'Not Set'),
                    'format' => 'raw',
                ],
                [
                    'label' => 'Customer',
                    'value' => ($model->customer_id) ? Html::a($model->customer->first_name . ' ' . $model->customer->last_name, ['/admin/customers/view', 'id' => $model->customer_id]) : t('app', 'Not Set'),
                    'format' => 'raw',
                ],
                'customer.first_name',
                'customer.last_name',
                'created_at',
                'updated_at',
            ],
        ]) ?>
    </div>

</div>
