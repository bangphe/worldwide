<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * Class Zone
 * @package app\models
 */
class Zone extends \app\models\auto\Zone
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','code'], 'required'],
            [['status'], 'string'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'country_id'        => t('app', 'Country'),
        ]);
    }

}
