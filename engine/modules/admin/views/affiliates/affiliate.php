<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Affiliate;

$this->title = t('app', 'Affiliates');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState'=>true,
            ]); ?>
            <?= GridView::widget([
                'id' => 'customers',
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover table-striped',
                ],
                'options'          => ['class' => 'table-responsive grid-view'],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'affiliate_id',
                    'affiliated_members',
                    'first_name',
                    'last_name',
                    'email',
                    'affiliate_status',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'style'=>'width:100px',
                            'class'=>'table-actions'
                        ],
                        'template' => '{activate} {deactivate}',
                        'buttons' => [
                            'activate' => function ($url, $model) {
                                return ($model->affiliate_status === \app\models\Affiliate::STATUS_DEACTIVATED) ? Html::a(
                                    t('app', 'Activate'),
                                    $url,
                                    [
                                        'title' => t('app', 'Activate'),
                                        'data-pjax' => '0',
                                        'data-confirm' => t('app', 'Are you sure you want to activate this item?')
                                    ]
                                ) : '';
                            },
                            'deactivate' => function ($url, $model) {
                                return ($model->affiliate_status === \app\models\Affiliate::STATUS_ACTIVE) ? Html::a(
                                    t('app', 'Deactivate'),
                                    $url,
                                    [
                                        'title' => t('app', 'Deactivate'),
                                        'data-pjax' => '0',
                                        'data-confirm' => t('app', 'Are you sure you want to deactivate this item?')
                                    ]
                                ) : '';
                            },
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
