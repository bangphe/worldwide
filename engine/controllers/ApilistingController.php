<?php

namespace app\controllers;

use Yii;
use app\models\Listing;
use app\models\ListingImage;
use yii\base\Exception;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class ApilistingController extends ActiveController
{
    public $modelClass = 'app\models\Listing';

    /* Declare actions supported by APIs */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        // unset($actions['update']);
        // unset($actions['delete']);
        // unset($actions['view']);
        unset($actions['index']);

        return $actions;
    }

    public function actionIndex()
    {
        $model = new $this->modelClass;
        if (!empty($_GET)) {
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET),
                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }
    
            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            $provider = new ActiveDataProvider([
                'query' => $model->find(),
                'pagination' => false
            ]);

            return $provider;
        }
    }

    public function actionCreate()
    {
        $model = new $this->modelClass;
        $modelImages = new ListingImage();
        
        if (!empty($_POST)) {
            foreach ($_POST as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            
            $model->load(Yii::$app->getRequest()->getBodyParams(), '');
            $model->customer_id = 1616;
            $model->package_id = 1;
            $model->location_id = 114;
            $model->currency_id = 1;
            $model->category_id = 10;
            $model->slug = $this->getSlug($model);
            $model->listing_type = 'banner';

            // if ($model->validate()) {
                if ($model->save(false)) {
                    $response = Yii::$app->getResponse();
                    $response->setStatusCode(201);
                    $id = implode(',', array_values($model->getPrimaryKey(true)));

                    if (!empty($_FILES['images'])) {
                        $storagePath = Yii::getAlias('@webroot/uploads/images/listings');
                        // Count # of uploaded files in array
                        $total = count($_FILES['images']['name']);
                        $sort = 0;
                        // die(var_dump($total));
        
                        // Loop through each file
                        for($i=0 ; $i < $total; $i++ ) {
                            $sort++;
                            // Get the temp file path
                            $tmpFilePath = $_FILES['images']['tmp_name'][$i];
        
                            // Make sure we have a file path
                            if ($tmpFilePath != ""){
                                $newGalleryImageName = $_FILES['images']['name'][$i];
                                // Setup our new file path
                                $newFilePath = $storagePath . '/' . $_FILES['images']['name'][$i];
        
                                // Upload the file into the temp dir
                                if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                                    // Handle other code here
                                    $modelImages = new ListingImage();
                                    $modelImages->image_path = Yii::getAlias('@web/uploads/images/listings/' . $newGalleryImageName);
                                    $modelImages->listing_id = $id;
                                    $modelImages->sort_order = $sort;
        
                                    $modelImages->save();
                                }
                            }
                        }
                    }
                    $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
                } elseif (!$model->hasErrors()) {
                    throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
                }
            // } else {
            //     throw new \yii\web\HttpException(404, $model->errors);
            //     // throw new ServerErrorHttpException($model->errors);
            // }
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'It requires parameters POST!');
        }
    }

    /**
     * @param $event
     * @return string
     * //https://github.com/yiisoft/yii2/issues/7773
     */
    public function getSlug($event)
    {
        if(!empty($event->slug)) {
            return $event->slug;
        }
        return Inflector::slug($event->title . ' ' . $this->generateSlug());
    }

    /**
     * @return array|null|string|\yii\db\ActiveRecord
     */
    public function generateSlug()
    {
        $unique = \app\helpers\StringHelper::uniqid();
        $exists = $this->findBySlug($unique);

        if (!empty($exists)) {
            return $this->generateSlug();
        }

        return $unique;
    }

    /**
     * @param $slug
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findBySlug($slug)
    {
        return Listing::find()->where(array(
            'slug' => $slug,
        ))->one();
    }
}
?>