<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models\options;

/**
 * Class Common
 * @package app\models\options
 */
class Common extends Base
{
    /**
     * @var string
     */
    public $siteName = 'EasyAds';

    /**
     * @var string
     */
    public $siteEmail = 'contact@easyads.io';

    /**
     * @var
     */
    public $siteCurrency = 'USD';

    /**
     * @var
     */
    public $siteLanguage = 'en';

    /**
     * @var string
     */
    public $siteKeywords = 'Ads, Classified ads, sell, buy, trade, market';

    /**
     * @var string
     */
    public $siteDescription = 'Ads application';

    /**
     * @var int
     */
    public $siteStatus = 1;

    /**
     * @var string
     */
    public $siteOfflineMessage = 'Application is offline, please try again later!';

    /**
     * @var
     */
    public $googleAnalyticsCode;

    /**
     * @var string
     */
    public $siteFacebookId = '';

    /**
     * @var string
     */
    public $siteFacebookSecret = '';

    /**
     * @var int
     */
    public $adminApprovalAds = 0;

    /**
     * @var string
     */
    public $siteGoogleMapsKey = '';

    /**
     * @var string
     */
    public $relatedAds = 'yes';

    /**
     * @var string
     */
    public $footerFirstPageSectionTitle = 'Help';

    /**
     * @var string
     */
    public $footerSecondPageSectionTitle = 'About';

    /**
     * @var int
     */
    public $homePromotedNumber = 8;

    /**
     * @var int
     */
    public $homeNewNumber = 8;

    /**
     * @var int
     */
    public $prettyUrl = 0;
	public $homePicNumber = 5;


    /**
     * @var string
     */
    protected $categoryName = 'app.settings.common';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siteKeywords', 'siteDescription', 'siteStatus', 'siteOfflineMessage', 'siteFacebookId', 'siteFacebookSecret', 'adminApprovalAds', 'siteGoogleMapsKey', 'relatedAds', 'homePromotedNumber' ,'homePicNumber' , 'homeNewNumber', 'prettyUrl'], 'safe'],
            [['siteLanguage', 'siteCurrency','siteName','siteEmail', 'footerFirstPageSectionTitle', 'footerSecondPageSectionTitle'], 'required'],

            [['siteName','siteOfflineMessage', 'googleAnalyticsCode'], 'string', 'max' => 255],
            [['siteEmail'], 'string', 'max' => 100],
            [['footerFirstPageSectionTitle', 'footerSecondPageSectionTitle'], 'string', 'max' => 25],
            [['homePromotedNumber', 'homePicNumber' , 'homeNewNumber'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'siteName'                     => t('app', 'Site name'),
            'siteEmail'                    => t('app', 'Site Email'),
            'siteCurrency'                 => t('app', 'Site Currency'),
            'siteLanguage'                 => t('app', 'Site Language'),
            'siteFacebookId'               => t('app', 'Facebook app id'),
            'siteFacebookSecret'           => t('app', 'Facebook app secret'),
            'adminApprovalAds'             => t('app', 'Admin should approve new ads'),
            'siteGoogleMapsKey'            => t('app', 'Google maps API key'),
            'relatedAds'                   => t('app', 'Related Ads Section'),
            'footerFirstPageSectionTitle'  => t('app', 'First Section Title'),
            'footerSecondPageSectionTitle' => t('app', 'Second Section Title'),
            'googleAnalyticsCode'          => t('app', 'Google Analytics code'),
            'homePromotedNumber'           => t('app', 'Number of promoted ads on homepage'),
			  'homePicNumber'           => t('app', 'Number of Pic for Ad'),
            'homeNewNumber'                => t('app', 'Number of new ads on homepage'),
        ];
    }

    /**
     * @return bool
     */
    public function afterValidate()
    {
        parent::afterValidate();
        return true;
    }
}