<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = t('app', 'Extension Manager');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState'=>true,
            ]); ?>
            <?= GridView::widget([
                'id' => 'extensions',
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover table-striped',
                ],
                'options'          => ['class' => 'table-responsive grid-view'],
                'dataProvider' => $model,
                'columns' => [
                    'name',
                    'description',
                    'version',
                    'author',
                    [
                        'header' => t('app', 'Options'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => [
                            'style'=>'width:60px',
                            'class'=>'table-actions'
                        ],
                        'template' => '{enable} {disable}',
                        'buttons'  => [
                            'enable' => function ($url, $model) {
                                if ($model['status'] == 'enabled') return;
                                return Html::a(
                                    '<i class="fa fa-check"></i>',
                                    url(['/admin/extensions/enable', 'id' => $model['id']]),
                                    [
                                        'data-pjax' => '0',
                                        'style'     => 'margin-right: 5px',
                                        'data-content'      => t('app', 'Enable'),
                                        'data-container'    => 'body',
                                        'data-toggle'       => 'popover',
                                        'data-trigger'      => 'hover',
                                        'data-placement'    => 'top'
                                    ]
                                );
                            },
                            'disable' => function ($url, $model) {
                                if ($model['status'] == 'disabled') return;
                                return Html::a(
                                    '<i class="fa fa-times"></i>',
                                    url(['/admin/extensions/disable', 'id' => $model['id']]),
                                    [
                                        'data-pjax' => '0',
                                        'style'     => 'margin-right: 5px',
                                        'data-content'      => t('app', 'Disable'),
                                        'data-container'    => 'body',
                                        'data-toggle'       => 'popover',
                                        'data-trigger'      => 'hover',
                                        'data-placement'    => 'top'
                                    ]
                                );
                            },
                        ],
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
