<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// Tawk_API.onLoad = function () {
// $('#tawkchat-container iframe').css('margin-left', '145px')
// };
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d85d8a49f6b7a4457e2d98e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<!-- Ebay links -->
<!--<script>window._epn = {campaign: 5337911034, smartPopover:false};</script>-->
<!--<script src="https://epnt.ebay.com/static/epn-smart-tools.js"></script>-->
<!-- End -->

<header id="header" style="min-height: auto !important;">
    <div class="header-wrapper">
        <?php if (session()->get('impersonating_customer')) {?>
        <section id="impersonating">
            You are now impersonating the customer <?= app()->customer->identity->getFullName();?><br />
            Click <a href="<?=url(['/account/impersonate']);?>" >here</a> to return to admin
        </section>
        <?php } ?>
        <section id="notify-container">
            <?= notify()->show();?>
        </section>
        <section class="main-menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="logo">
                            <a href="<?=url(['/']);?>">
                                <!-- <img src="<?//= options()->get('app.settings.theme.siteLogo', \Yii::getAlias('@web/assets/site/img/logo.png'));?>" /> -->
                                <img src="<?= \Yii::getAlias('@web'.options()->get('app.settings.theme.siteLogo')); ?>" />
                            </a>
                            <a href="#nav-mobile" id="link-mobile" class="btn-as transparent mobile-menu pull-left hidden-lg hidden-md hidden-sm" data-toggle="collapse" aria-expanded="false" aria-controls="nav-mobile"><i class="fa fa-bars" aria-hidden="true"></i></a>
                            <!-- <a href="<?//=url(['/listing/post']);?>" class="btn-add pull-right"><i class="fa fa-plus" aria-hidden="true"></i></a> -->
                            <a href="#nav-menu" id="link-menu" class="btn-add pull-right" data-toggle="collapse" aria-expanded="false" aria-controls="nav-menu"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
                            <div id="nav-mobile" class="collapse mobile-menu-wrapper">
                                <ul>
                                    <?php if (app()->customer->isGuest == false) { ?>
                                    <li><a href="<?=url(['account/info']);?>"><i class="fa fa-cog" aria-hidden="true"></i><?=t('app','Account Info');?></a></li>
                                    <?php if (app()->customer->identity->group_id == 2) { ?>
                                        <li><a href="<?=url(['/store/index', 'slug' => app()->customer->identity->stores->slug]);?>"><i class="fa fa-eye" aria-hidden="true"></i><?=t('app','My Store');?></a></li>
                                    <?php } else { ?>
                                        <li><a href="<?=url(['account/info#company-block']);?>"><i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Upgrade');?></a></li>
                                    <?php } ?>
                                    <li><a href="<?=url(['account/my-listings']);?>"><i class="fa fa-tasks" aria-hidden="true"></i><?=t('app','My Ads');?></a></li>
                                    <li><a href="<?=url(['account/favorites']);?>"><i class="fa fa-heart-o" aria-hidden="true"></i><?=t('app','Favorites');?></a></li>
                                    <li><a href="<?=url(['account/invoices']);?>"><i class="fa fa-file-o" aria-hidden="true"></i><?=t('app','Invoices');?></a></li>
                                    <li><a href="<?=url(['account/logout']);?>"><i class="fa fa-power-off" aria-hidden="true"></i><?=t('app','Logout');?></a></li>
                                    <?php } else { ?>
                                   <li><a href="<?=url(['/ ']);?>"><i class="fa fa-home" aria-hidden="true"></i><?=t('app','Home');?></a></li>
                                             <li><a href="<?=url(['account/register']);?>"><i class="fa fa-pencil" aria-hidden="true"></i><?=t('app','Register');?></a></li>
                                        <li><a href="<?=url(['account/login']);?>"><i class="fa fa-user" aria-hidden="true"></i><?=t('app','Login');?></a></li>
                                        <li><a href="<?=url(['/featured']);?>"><i class="fa fa-desktop" aria-hidden="true"></i><?=t('app','Featured Ads');?></a></li>
                                        <li><a href="<?=url(['/promoted']);?>"><i class="fa fa-list" aria-hidden="true"></i><?=t('app','Promoted Ads');?></a></li>
                                        <li><a href="<?=url(['/account/favorites']);?>"><i class="fa fa-heart-o" aria-hidden="true"></i><?=t('app','Favorite Ads');?></a></li>
                                        <li><a href="<?=url(['/contact']);?>"><i class="fa fa-phone" aria-hidden="true"></i><?=t('app','Customer Service');?></a></li>
                                        <li><a href="<?=url(['/listing/post']);?>"><i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Post Ad');?></a></li>

                                    <?php } ?>
                                </ul>
                            </div>
                            <div id="nav-menu" class="collapse mobile-menu-wrapper">
                                <ul>
                                    <li><a href="<?=url(['/']);?>"><i class="fa fa-home" aria-hidden="true"></i><?=t('app','Home');?></a></li>
                                    <li><a href="<?=url(['account/register']);?>"><i class="fa fa-users" aria-hidden="true"></i><?=t('app','Account');?></a></li>
                                    <li><a href="<?=url(['page/contact']);?>"><i class="fa fa-phone" aria-hidden="true"></i><?=t('app','Contact');?></a></li>
                                    <li><a href="<?=url(['page/about-us']);?>"><i class="fa fa-wrench" aria-hidden="true"></i><?=t('app','About Us');?></a></li>
                                    <li><a href="#"><i class="fa fa-desktop" aria-hidden="true"></i><?=t('app','Notifications');?></a></li>
                                    <li><a href="#"><i class="fa fa-edit" aria-hidden="true"></i><?=t('app','Messages');?></a></li>
                                    <li><a href="<?=url(['page/all-categories']);?>"><i class="fa fa-search" aria-hidden="true"></i><?=t('app','Categories');?></a></li>
                                    <li><a href="<?=url(['promoted']);?>"><i class="fa fa-list" aria-hidden="true"></i><?=t('app','Deals');?></a></li>
                                    <li><a href="https://twitter.com/intent/tweet?text=Post%20free%20ads%20on%20https%3A//worldwideadverts.info/%20%23worldwideadverts%20%23freeads%20%23adsposting%20"><i class="fa fa-share-alt" aria-hidden="true"></i><?=t('app','Share');?></a></li>
                                    <li><a href="<?=url(['listing/post/10']);?>"><i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Post New Ad');?></a></li>
                                    <li><a href="<?=url(['search']);?>"><i class="fa fa-search" aria-hidden="true"></i><?=t('app','Filter by');?></a></li>
                                 <!--<?php
                                   // foreach (\app\models\Category::getCategoriesByParentId() as $item) {
                                       // $children = \app\models\Category::getCategoriesByParentId($item->category_id);
                                       // echo '<li>' . \yii\helpers\Html::a(
                                          //  ($item->icon ? '<i class="fa ' . $item->icon . '" aria-hidden="true"></i>' : '') .
                                           // strtoupper($item->name),
                                           // ['/listing/post/' . $item->category_id]
                                        //) . '</li>';
                                   // }
                                ?>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 ">
                      <!--  <?php if (Yii::$app->controller->id == 'account') { ?>
                        &nbsp;&nbsp;&nbsp;
                        <?php } else { ?>
                        <h4 style="text-align: center; padding: 4px 0 0 0"><?=\yii\helpers\Html::a('Featured Ads', ['site/featured'], ['style'=>'font-size: 15px;'])?></h4>
                        <?= \app\components\AdsHomeListWidget::widget([
                            'col' => 6,
                            'quantity' => 16
                        ]);
                        ?>
                        <?php } ?> -->
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs">
                        <div class="main-navigation">
                            <!-- -->
                            <div class="btn-group hidden-xs" id="show_always">
                                <a href="javascript:;" class="btn-as transparent dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-user-o" aria-hidden="true"></i><?=t('app','My account');?>
                                </a>
                                <div class="dropdown-menu">
                                    <?php if (app()->customer->isGuest == false) { ?>
                                        <a href="<?=url(['account/info']);?>"><i class="fa fa-cog" aria-hidden="true"></i><?=t('app','Account Info');?></a>
                                        <?php if (app()->customer->identity->group_id == 2) { ?>
                                            <a href="<?=url(['/store/index', 'slug' => app()->customer->identity->stores->slug]);?>"><i class="fa fa-eye" aria-hidden="true"></i><?=t('app','My Store');?></a>
                                        <?php } else { ?>
                                            <a href="<?=url(['account/info#company-block']);?>"><i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Upgrade');?></a>
                                        <?php } ?>
                                        <a href="<?=url(['account/my-listings']);?>"><i class="fa fa-tasks" aria-hidden="true"></i><?=t('app','My Ads');?></a>
                                        <a href="<?=url(['account/favorites']);?>"><i class="fa fa-heart-o" aria-hidden="true"></i><?=t('app','Favorites');?></a>
                                        <a href="<?=url(['account/invoices']);?>"><i class="fa fa-file-o" aria-hidden="true"></i><?=t('app','Invoices');?></a>
                                        <a href="<?=url(['account/logout']);?>"><i class="fa fa-power-off" aria-hidden="true"></i><?=t('app','Logout');?></a>
                                    <?php } else { ?>
                                        <li>
                                            <a href="<?=url(['/ ']);?>"><i class="fa fa-home" aria-hidden="true"></i><?=t('app','Home');?></a></li>
                                            <a href="<?=url(['account/register']);?>"><i class="fa fa-pencil" aria-hidden="true"></i><?=t('app','Register');?></a>
                                        <a href="<?=url(['account/login']);?>"><i class="fa fa-user" aria-hidden="true"></i><?=t('app','Login');?></a>
                                        <li><a href="<?=url(['/featured']);?>"><i class="fa fa-desktop" aria-hidden="true"></i><?=t('app','Featured Ads');?></a></li>
                                        <li><a href="<?=url(['/promoted']);?>"><i class="fa fa-list" aria-hidden="true"></i><?=t('app','Promoted Ads');?></a></li>
                                        <li><a href="<?=url(['/account/favorites']);?>"><i class="fa fa-heart-o" aria-hidden="true"></i><?=t('app','Favorite Ads');?></a></li>
                                        <li><a href="<?=url(['/contact']);?>"><i class="fa fa-phone" aria-hidden="true"></i><?=t('app','Customer Service');?></a></li>
                                        
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- code state bhupi !-->
                             <div class="btn-group hidden-xs">
                                <a href="<?=url(['/listing/post']);?>"  class="btn-as reverse" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-plus" aria-hidden="true"></i><?=t('app','Post new ad');?>
                                </a>
                                <div class="dropdown-menu" id="main_menu" style="width:200px;">
                                    <?php
                                    /** @var \app\models\Category $item */
                                    foreach (\app\models\Category::getCategoriesByParentId() as $item) {
                                        $children = \app\models\Category::getCategoriesByParentId($item->category_id);

                                        // if ($children) {
                                        //     echo \yii\helpers\Html::a(
                                        //         ($item->icon ? '<i class="fa ' . $item->icon . '" aria-hidden="true"></i>' : '') .
                                        //         strtoupper($item->name),
                                        //         'javascript:void(0)',
                                        //         [
                                        //             'id' => $item->slug . '_menu',
                                        //             'onmouseover' => new \yii\web\JsExpression("show_menu('{$item->slug}')")
                                        //         ]
                                        //     );
                                        //     echo '<div class="dropdown-menu show_sub" style="width:200px; display:none;" id="' . $item->slug . '">';
                                        //     /** @var \app\models\Category $child */
                                        //     foreach ($children as $child) {
                                        //         echo \yii\helpers\Html::a(
                                        //             ($child->icon ? '<i class="fa ' . $child->icon . '" aria-hidden="true"></i>' : '') .
                                        //             strtoupper($child->name),
                                        //             ['/listing/post/' . $child->category_id]
                                        //         );
                                        //     }
                                        //     echo '</div>';
                                        // } else {
                                            echo \yii\helpers\Html::a(
                                                ($item->icon ? '<i class="fa ' . $item->icon . '" aria-hidden="true"></i>' : '') .
                                                strtoupper($item->name),
                                                ['/listing/post/' . $item->category_id]
                                            );
                                        // }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- code state bhupi 
                            <div class="btn-group hidden-xs" >
                                <a href="< ?=url(['/listing/post']);?>" class="btn-as reverse"><i class="fa fa-plus" aria-hidden="true"></i>< ?=t('app','Post new ad');?></a>
                            </div>!-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>

<style>
#header .header-wrapper .main-menu .main-navigation .btn-group .dropdown-menu.show_sub {
    width: 200px;
    left: -200px;
    top: 24px;
}
</style>
<script>
var showed = [];
var current = '';
function show_menu(id){

    if (current == id) {
        return;
    }

    $('#' + id + '_menu').css('background', '#009688');
    $('#'+id).show().css({"top": $('#' + id + '_menu').position().top});

    showed.forEach(function(el){
        hide_menu(el);
    });

    showed = [id];
    current = id;
}
function hide_menu(id){
    $('#' + id + '_menu').css('background', '');
    $('#'+id).hide();
}
</script>