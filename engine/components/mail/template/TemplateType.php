<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\components\mail\template;

abstract class TemplateType
{
    // types of template
    const TEMPLATE_TYPE_AD = 1;
    const TEMPLATE_TYPE_INVOICE = 2;
    const TEMPLATE_TYPE_ADMIN = 3;
    const TEMPLATE_TYPE_CUSTOMER = 4;

    /**
     * @var array list of variables of template
     */
    protected $varsList = [];
    protected $recipient;

    /**
     * Factory Method
     *
     * @param       $type
     * @param array $data
     * @return TemplateTypeAd|TemplateTypeAdmin|TemplateTypeCustomer|TemplateTypeInvoice
     */
    public static function create($type, array $data = [])
    {
        switch ($type) {
            case self::TEMPLATE_TYPE_AD:
                return new TemplateTypeAd($data);
            case self::TEMPLATE_TYPE_INVOICE:
                return new TemplateTypeInvoice($data);
            case self::TEMPLATE_TYPE_ADMIN:
                return new TemplateTypeAdmin($data);
            case self::TEMPLATE_TYPE_CUSTOMER:
                return new TemplateTypeCustomer($data);
            default:
                echo '<pre>';
                var_dump('not found');
                die('</pre>');
        }
    }

    /**
     * @return array
     */
    public function getVarsList()
    {
        return $this->varsList;
    }

    /**
     * Get list of types
     *
     * @param string $filter types separated by comma, example '1,2'
     *
     * @return array
     */
    public static function getTypesList($filter = '')
    {
        $types = [
            self::TEMPLATE_TYPE_AD       => t('app', 'Ad'),
            self::TEMPLATE_TYPE_INVOICE  => t('app', 'Invoice'),
            self::TEMPLATE_TYPE_ADMIN    => t('app', 'Admin'),
            self::TEMPLATE_TYPE_CUSTOMER => t('app', 'Customer'),
        ];

        // apply filter
        if ($filter) {
            $allowed = explode(',', $filter);
            return array_intersect_key($types, array_flip($allowed));
        }

        return $types;
    }

    abstract public function populate();
    abstract public function getRecipient();
}