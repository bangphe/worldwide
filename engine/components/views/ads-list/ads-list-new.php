<?php
use app\models\ListingStat;
$formatter = app()->formatter;
?>
<!-- <style type="text/css">
.test {
    height: auto !important;
    width: auto !important;
}
</style> -->

<section class="listings-list">
    <div class="container">
        <?php if ($title) { ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1><?= $moreLink ? \yii\helpers\Html::a(html_encode($title), $moreLink) : html_encode($title);?></h1>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-9" style="margin-left: -15px;">
                <?php foreach ($ads as $ad) { ?>
                    <?php $isFavorite = (!empty($ad->favorite)) ? true : false; ?>
                    <div class="col-xs-6 col-lg-<?=$col;?> col-md-<?=$col;?> item">
                    <!-- <?php
                    //if(html_encode($ad->getPriceAsCurrency($ad->currency->code))!="$0.00")
                    {
                    ?>
                        <div class="price">
                            <?//=html_encode($ad->getPriceAsCurrency($ad->currency->code));?>
                        </div>
                    <?php
                    }
                    ?> -->
                        <div class="image">
                            <?//= !empty($ad->isPromoted) ? '<div class="promoted"><span>' . t('app', 'Promoted') . '</span></div>' : '' ?>
                            <a href="#" data-listing-id="<?= (int)$ad->listing_id; ?>" data-stats-type="<?= ListingStat::FAVORITE; ?>" data-add-msg="<?= t('app', 'Add to favorites'); ?>" data-remove-msg="<?= t('app', 'Remove Favorite'); ?>" data-favorite-url="<?= url(['/listing/toggle-favorite']); ?>" class="action-icon favorite-listing <?= (!$isFavorite) ? 'track-stats' : ''; ?>">
                                <?php if ($isFavorite) { ?>
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                <?php } else { ?>
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                <?php } ?>
                            </a>
                            <?php if ($ad->mainImage) {
                                $check_path = file_exists($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@web').$ad->mainImage->image_path);
                                if ($check_path) {
                            ?>
                                <a class="img-link" href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>');">
                                    <img class="lazyloaded" src="<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>" data-src="<?= Yii::getAlias('@web'.$ad->mainImage->image_path); ?>" alt="" />
                            <?php } else { ?>
                                <a class="img-link" href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>');">
                                    <img class="lazyloaded" src="<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>" data-src="<?= Yii::getAlias('@web/assets/site/img/img-listing-list-empty.png'); ?>" alt="" />
                            <?php } } ?>
                            </a>
                        </div>
                        <div class="info">
                            <?php
                            // if(html_encode($ad->getPriceAsCurrency($ad->currency->code))!="$0.00")
                            // {
                            ?>
                                <div style="color: #3cce69; font-weight: 600;">
                                    <?=html_encode($ad->getPriceAsCurrency($ad->currency->code));?>
                                </div>
                            <?php
                            // }
                            ?>
                            <a href="<?= url(['category/index', 'slug' => $ad->category->slug]); ?>" class="category"><i class="fa <?= $ad->category->icon ? html_encode($ad->category->icon) : 'fa-tags'; ?>" aria-hidden="true"></i> <?= html_encode($ad->category->name); ?></a>
                            <a href="<?= url(['/listing/index', 'slug' => $ad->slug]); ?>" class="name">
                                <?= html_encode(strtoupper($ad->title)); ?>
                            </a>
                            <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= html_encode($ad->getZoneCountryString()); ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-3" style="text-align: center; padding: 0 50px;">
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/u.cfm?d=769945&m=20990&u=1347546&afftrack=']); ?>" target="_blank">March Madness Sale! Up to $2,000 OFF Top Water Ionizers at TyentUSA! Get $1,600 in FREE Gifts + $150 in BONUS Gifts with code: GOODLUCK</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/u.cfm?d=769949&m=20990&u=1347546&afftrack=']); ?>" target="_blank">Alkaline H2 Hybrid Ionizer On Sale Now</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/u.cfm?d=769948&m=20990&u=1347546&afftrack=']); ?>" target="_blank">$2,000 Off UCE-13 Under-Counter Extreme Water Ionizer</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/u.cfm?d=769947&m=20990&u=1347546&afftrack=']); ?>" target="_blank">$2,000 Off ACE-13 Above-Counter Extreme Water Ionizer</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/u.cfm?d=769943&m=20990&u=1347546&afftrack=']); ?>" target="_blank">$1,000 Off Edge 9000 Water Ionizer</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://shareasale.com/r.cfm?b=1723326&u=1347546&m=19225&urllink=&afftrack=']); ?>" target="_blank">Women's Day Exclusive Deals! Send Flower &amp; Gifts She'll Love! 15% Off</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://www.dailysteals.com/products/apple-sport-loop-for-apple-watch-40-or-44-mm']); ?>" target="_blank">$19.99 w/ DSAPLEW code  -These are Apple-branded watch loops! Selling on Apple.com for $49. We have 9 colors to choose from</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://www.dailysteals.com/products/honeywell-home-programmable-electric-baseboard-heater-thermostat-reads-out-in-celsius-convertible-to-fahrenheit-with-menu']); ?>" target="_blank">$18.99 w/ DSHWELL  code - Well-reviewed programmable thermostat, selling new on elsewhere for $33. Great upgrade for under $20!</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://www.dailysteals.com/products/logitech-g432-dts-x-7-1-surround-sound-wired-pc-ps4-xbox-ps5-gaming-headset']); ?>" target="_blank">$39.99 w/ DSG432 code - This is from Logitech's overstock, so no retail packaging but brand new. Selling elsewhere for $49.99</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://www.dailysteals.com/products/mophie-dual-wireless-charger-qi-certified-7-5w-charging-pad']); ?>" target="_blank">(MFG refurbished) - $14.99 w/ DSMOPHS code - Two Qi charging pads and one USB-A outlet so you can charge 3 devices in total. Selling new on Apple for $70- this one is selling fast!</a>
                <br><br>
                <a href="<?= url(['/site/preview', 'url' => 'https://www.dailysteals.com/products/rocketbook-fusion-smart-notebook-with-1-pen-and-penstation']); ?>" target="_blank">$29.99 -Sells on elsewhere for $35 with only 1 pen. Almost 25,000 reviews. Our bundle has 5 pens and is cheaper!</a>
            </div>
            <div class="col-md-9">
                <div class="twidget-container" id="twidget"></div>
                <br><br>
                <iframe src="https://www.fiverr.com/embed_gigs?id=U2FsdGVkX19cEmkUXq9CTjmBCr8gLarXezhVzraj/fLLTt1ZHGavVHl363y3B5w85aWcRWiIJpCkamz36cxdv0Ujz2Wl3alUg5/mz6NzyiMdj0fl75KIFZIrdTEbcWZlPDxYy5Jz0lOs1e1KnirZjek0I1YrchI1MGLiOztiz941ONTT/LuoesLfuBMREulY3pId6BB1928luYBtvly/aS8MnJwUfLUpQn3RLR8Q/Bl5M6nQ28TQ3U6EaUO/3Wn2lRBfr0rImSxvwjdEbld4pScxKtDbbuLrnWf4jTA7pLV03oWO2kXiLhxcnI+1qgtPbYk66lypwd+4PajFMJ1/aYd6Zpfil6BI49TKcZGGekiPZxySDjIaj8/3T+Y1/wJr78Fuif2KxXMnAvHEcjDeSaSrkDgurV/swVu4lajutZpERb3A4Bdo3fx3ssohd/omE5/tdMAc8sdfVG11mM/FaQ==&affiliate_id=263951&strip_google_tagmanager=true" loading="lazy" data-with-title="true" class="fiverr_nga_frame" frameborder="0" height="5px" width="100%" onload='var s = document.createElement("script");s.setAttribute("src", "https://npm-assets.fiverrcdn.com/nga/onload_iframe.js");document.body.appendChild(s);' ></iframe> 
            </div>
        </div>
    </div>
</section>
