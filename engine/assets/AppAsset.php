<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\assets;

use yii\web\AssetBundle;


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/site/css/style.css',
        'assets/site/css/app.css',
        'assets/site/css/custom.css',
        'assets/site/css/chat.css',
        'assets/cookies/cookies.css',
        'assets/datepicker/css/bootstrap-datepicker.min.css',
        'assets/timepicker/css/bootstrap-timepicker.min.css',
        'assets/sweetalert/sweetalert.css',
        'assets/site/css/loading.css',
        'assets/twidget/twidget.css',
        'assets/summernote/summernote.min.css',
    ];
    public $js = [
        'assets/site/js/main.js',
        'assets/datepicker/js/bootstrap-datepicker.min.js',
        'assets/timepicker/js/bootstrap-timepicker.min.js',
        'assets/sweetalert/sweetalert.min.js',
        'assets/site/js/knockout-3.5.0.js',
        'assets/twidget/jquery.twidget.js',
        'assets/summernote/summernote.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontsAsset',
        'app\assets\LazysizesAsset',
        'app\assets\JqueryPluginsAsset',
        'rmrevin\yii\fontawesome\cdn\AssetBundle',
        'twisted1919\notify\NotifyAsset',
        'app\assets\Select2Asset',
        'app\assets\MCustomScrollbarAsset',
        'kartik\select2\Select2Asset',
        'kartik\select2\ThemeKrajeeAsset',
    ];
}
