<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\modules\admin\controllers;

use app\helpers\CommonHelper;
use app\models\options\Common;
use app\models\options\Email;
use app\models\options\Theme;
use app\models\options\Social;
use app\models\options\Invoice;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Controls the actions for settings section
 *
 * @Class SettingsController
 * @package app\modules\admin\controllers
 */
class SettingsController extends \app\modules\admin\yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * Controls general settings
     *
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new Common();

        if ($model->load(request()->post()) && $model->save()) {
            return $this->refresh();
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Controls theme settings
     *
     * @return string|\yii\web\Response
     */
    public function actionTheme()
    {
        $model = new Theme();

        if ($model->load(request()->post()) && $model->save()) {
            return $this->refresh();
        } else {
            return $this->render('theme', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Controls social settings
     *
     * @return string|\yii\web\Response
     */
    public function actionSocial()
    {
        $model = new Social();

        if ($model->load(request()->post()) && $model->save()) {
            return $this->refresh();
        } else {
            return $this->render('social', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Controls invoice settings
     *
     * @return string|\yii\web\Response
     */
    public function actionInvoice()
    {
        $model = new Invoice();

        if ($model->load(request()->post()) && $model->save()) {
            return $this->refresh();
        } else {
            return $this->render('invoice', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionHtaccessModal()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['admin/index']);
        }

        return $this->renderPartial('_htaccess_modal',['controller'=>$this]);
    }

    /**
     * Tries to write the contents of the htaccess file
     */
    public function actionWriteHtaccess()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['admin/index']);
        }

        response()->format = Response::FORMAT_JSON;

        if (!CommonHelper::isModRewriteEnabled()) {
            return array('result' => 'error', 'message' => t('app', 'Mod rewrite is not enabled on this host. Please enable it in order to use clean urls!'));
        }

        if (!is_file($file = \Yii::getAlias('@webroot') . '/.htaccess')) {
            if (!@touch($file)) {
                return array('result' => 'error', 'message' => t('app', 'Unable to create the file: {file}. Please create the file manually and paste the htaccess contents into it.', array('{file}' => $file)));
            }
        }

        if (!@file_put_contents($file, $this->getHtaccessContent())) {
            return array('result' => 'error', 'message' => t('app', 'Unable to write htaccess contents into the file: {file}. Please create the file manually and paste the htaccess contents into it.', array('{file}' => $file)));
        }

        return array('result' => 'success', 'message' => t('app', 'The htaccess file has been successfully created. Do not forget to save the changes!'));
    }

    /**
     * Will generate the contents of the htaccess file which later
     * should be written in the document root of the application
     */
    public function getHtaccessContent()
    {
        $webApps    = app()->modules;
        $baseUrl    = '/' . trim(Url::base(), '/') . '/';
        $baseUrl    = str_replace('//', '/', $baseUrl);

        return $this->renderPartial('_htaccess', ['webApps' => $webApps, 'baseUrl' => $baseUrl]);
    }
}
