<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\helpers;
use app\models\Country;
use app\models\Zone;
use lysenkobv\GeoIP\Location;

/**
 * Class GeoHelper
 * @package app\helpers
 */
class GeoHelper
{
    /**
     * @var Location
     */
    protected static $location;

    /**
     * @var Country
     */
    protected static $country;

    /**
     * @var string
     */
    protected static $countryIso;

    /**
     * @var Zone
     */
    protected static $zone;

    /**
     * @var string
     */
    protected static $city;

    /**
     * @var string
     */
    protected static $zip_code;

    protected static $country2currency = [
        'BD' => 'BDT',
        'BE' => 'EUR',
        'BF' => 'XOF',
        'BG' => 'BGN',
        'BA' => 'BAM',
        'BB' => 'BBD',
        'WF' => 'XPF',
        'BL' => 'EUR',
        'BM' => 'BMD',
        'BN' => 'BND',
        'BO' => 'BOB',
        'BH' => 'BHD',
        'BI' => 'BIF',
        'BJ' => 'XOF',
        'BT' => 'BTN',
        'JM' => 'JMD',
        'BV' => 'NOK',
        'BW' => 'BWP',
        'WS' => 'WST',
        'BQ' => 'USD',
        'BR' => 'BRL',
        'BS' => 'BSD',
        'JE' => 'GBP',
        'BY' => 'BYR',
        'BZ' => 'BZD',
        'RU' => 'RUB',
        'RW' => 'RWF',
        'RS' => 'RSD',
        'TL' => 'USD',
        'RE' => 'EUR',
        'TM' => 'TMT',
        'TJ' => 'TJS',
        'RO' => 'RON',
        'TK' => 'NZD',
        'GW' => 'XOF',
        'GU' => 'USD',
        'GT' => 'GTQ',
        'GS' => 'GBP',
        'GR' => 'EUR',
        'GQ' => 'XAF',
        'GP' => 'EUR',
        'JP' => 'JPY',
        'GY' => 'GYD',
        'GG' => 'GBP',
        'GF' => 'EUR',
        'GE' => 'GEL',
        'GD' => 'XCD',
        'GB' => 'GBP',
        'GA' => 'XAF',
        'SV' => 'USD',
        'GN' => 'GNF',
        'GM' => 'GMD',
        'GL' => 'DKK',
        'GI' => 'GIP',
        'GH' => 'GHS',
        'OM' => 'OMR',
        'TN' => 'TND',
        'JO' => 'JOD',
        'HR' => 'HRK',
        'HT' => 'HTG',
        'HU' => 'HUF',
        'HK' => 'HKD',
        'HN' => 'HNL',
        'HM' => 'AUD',
        'VE' => 'VEF',
        'PR' => 'USD',
        'PS' => 'ILS',
        'PW' => 'USD',
        'PT' => 'EUR',
        'SJ' => 'NOK',
        'PY' => 'PYG',
        'IQ' => 'IQD',
        'PA' => 'PAB',
        'PF' => 'XPF',
        'PG' => 'PGK',
        'PE' => 'PEN',
        'PK' => 'PKR',
        'PH' => 'PHP',
        'PN' => 'NZD',
        'PL' => 'PLN',
        'PM' => 'EUR',
        'ZM' => 'ZMK',
        'EH' => 'MAD',
        'EE' => 'EUR',
        'EG' => 'EGP',
        'ZA' => 'ZAR',
        'EC' => 'USD',
        'IT' => 'EUR',
        'VN' => 'VND',
        'SB' => 'SBD',
        'ET' => 'ETB',
        'SO' => 'SOS',
        'ZW' => 'ZWL',
        'SA' => 'SAR',
        'ES' => 'EUR',
        'ER' => 'ERN',
        'ME' => 'EUR',
        'MD' => 'MDL',
        'MG' => 'MGA',
        'MF' => 'EUR',
        'MA' => 'MAD',
        'MC' => 'EUR',
        'UZ' => 'UZS',
        'MM' => 'MMK',
        'ML' => 'XOF',
        'MO' => 'MOP',
        'MN' => 'MNT',
        'MH' => 'USD',
        'MK' => 'MKD',
        'MU' => 'MUR',
        'MT' => 'EUR',
        'MW' => 'MWK',
        'MV' => 'MVR',
        'MQ' => 'EUR',
        'MP' => 'USD',
        'MS' => 'XCD',
        'MR' => 'MRO',
        'IM' => 'GBP',
        'UG' => 'UGX',
        'TZ' => 'TZS',
        'MY' => 'MYR',
        'MX' => 'MXN',
        'IL' => 'ILS',
        'FR' => 'EUR',
        'IO' => 'USD',
        'SH' => 'SHP',
        'FI' => 'EUR',
        'FJ' => 'FJD',
        'FK' => 'FKP',
        'FM' => 'USD',
        'FO' => 'DKK',
        'NI' => 'NIO',
        'NL' => 'EUR',
        'NO' => 'NOK',
        'NA' => 'NAD',
        'VU' => 'VUV',
        'NC' => 'XPF',
        'NE' => 'XOF',
        'NF' => 'AUD',
        'NG' => 'NGN',
        'NZ' => 'NZD',
        'NP' => 'NPR',
        'NR' => 'AUD',
        'NU' => 'NZD',
        'CK' => 'NZD',
        'XK' => 'EUR',
        'CI' => 'XOF',
        'CH' => 'CHF',
        'CO' => 'COP',
        'CN' => 'CNY',
        'CM' => 'XAF',
        'CL' => 'CLP',
        'CC' => 'AUD',
        'CA' => 'CAD',
        'CG' => 'XAF',
        'CF' => 'XAF',
        'CD' => 'CDF',
        'CZ' => 'CZK',
        'CY' => 'EUR',
        'CX' => 'AUD',
        'CR' => 'CRC',
        'CW' => 'ANG',
        'CV' => 'CVE',
        'CU' => 'CUP',
        'SZ' => 'SZL',
        'SY' => 'SYP',
        'SX' => 'ANG',
        'KG' => 'KGS',
        'KE' => 'KES',
        'SS' => 'SSP',
        'SR' => 'SRD',
        'KI' => 'AUD',
        'KH' => 'KHR',
        'KN' => 'XCD',
        'KM' => 'KMF',
        'ST' => 'STD',
        'SK' => 'EUR',
        'KR' => 'KRW',
        'SI' => 'EUR',
        'KP' => 'KPW',
        'KW' => 'KWD',
        'SN' => 'XOF',
        'SM' => 'EUR',
        'SL' => 'SLL',
        'SC' => 'SCR',
        'KZ' => 'KZT',
        'KY' => 'KYD',
        'SG' => 'SGD',
        'SE' => 'SEK',
        'SD' => 'SDG',
        'DO' => 'DOP',
        'DM' => 'XCD',
        'DJ' => 'DJF',
        'DK' => 'DKK',
        'VG' => 'USD',
        'DE' => 'EUR',
        'YE' => 'YER',
        'DZ' => 'DZD',
        'US' => 'USD',
        'UY' => 'UYU',
        'YT' => 'EUR',
        'UM' => 'USD',
        'LB' => 'LBP',
        'LC' => 'XCD',
        'LA' => 'LAK',
        'TV' => 'AUD',
        'TW' => 'TWD',
        'TT' => 'TTD',
        'TR' => 'TRY',
        'LK' => 'LKR',
        'LI' => 'CHF',
        'LV' => 'EUR',
        'TO' => 'TOP',
        'LT' => 'LTL',
        'LU' => 'EUR',
        'LR' => 'LRD',
        'LS' => 'LSL',
        'TH' => 'THB',
        'TF' => 'EUR',
        'TG' => 'XOF',
        'TD' => 'XAF',
        'TC' => 'USD',
        'LY' => 'LYD',
        'VA' => 'EUR',
        'VC' => 'XCD',
        'AE' => 'AED',
        'AD' => 'EUR',
        'AG' => 'XCD',
        'AF' => 'AFN',
        'AI' => 'XCD',
        'VI' => 'USD',
        'IS' => 'ISK',
        'IR' => 'IRR',
        'AM' => 'AMD',
        'AL' => 'ALL',
        'AO' => 'AOA',
        'AQ' => '',
        'AS' => 'USD',
        'AR' => 'ARS',
        'AU' => 'AUD',
        'AT' => 'EUR',
        'AW' => 'AWG',
        'IN' => 'INR',
        'AX' => 'EUR',
        'AZ' => 'AZN',
        'IE' => 'EUR',
        'ID' => 'IDR',
        'UA' => 'UAH',
        'QA' => 'QAR',
        'MZ' => 'MZN',
    ];

    protected static function cache()
    {
        if (!isset(self::$country)) {
            self::$country = self::$zone = self::$city = self::$zip_code = self::$countryIso = false;

            /** @var \lysenkobv\GeoIP\GeoIP $geo */
            $geo = \Yii::$app->geoip;
            $ip = $geo->ip();
            self::$location = $ip->location;
//            print_r($ip);

            if (self::$location->lat && self::$location->lng) {
                self::$countryIso = $ip->isoCode;

                $google_response = $geocode = \json_decode(file_get_contents(
                    'https://maps.googleapis.com/maps/api/geocode/json?latlng=' .
                    self::$location->lat . ',' . self::$location->lng . '&sensor=false' .
                    '&key=AIzaSyB7L_qcB1pqhVf1T_j1oukbGoCJH70Y87E'

                ), true);

                if ($google_response && isset($google_response['results'][0]['address_components'])) {

                    foreach ($google_response['results'][0]['address_components'] as $address_component) {
                        if (in_array('country', $address_component['types'])) {
                            self::$country = Country::find()
                                ->select('country_id')
                                ->where(['name' => $address_component['long_name']])
                                ->one();
                        }

                        if (in_array('administrative_area_level_1', $address_component['types'])) {
                            self::$zone = Zone::find()
                                ->select('zone_id')
                                ->where(['name' => $address_component['long_name']])
                                ->one();
                        }

                        if (in_array('locality', $address_component['types'])) {
                            self::$city = $address_component['long_name'];
                        }

                        if (in_array('postal_code', $address_component['types'])) {
                            self::$zip_code = $address_component['long_name'];
                        }
                    }

                    foreach ($google_response['results'][0]['address_components'] as $address_component) {
                        if (!self::$zone
                            && in_array('administrative_area_level_2', $address_component['types'])
                        ) {
                            self::$zone = Zone::find()
                                ->select('zone_id')
                                ->where(['name' => $address_component['long_name']])
                                ->one();

                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * @return Country
     */
    public static function getCountry()
    {
        self::cache();
        return self::$country;
    }

    /**
     * @return string
     */
    public static function getCountryCode()
    {
        self::cache();
        return self::$countryIso;
    }

    /**
     * @return Zone
     */
    public static function getZone()
    {
        self::cache();
        return self::$zone;
    }

    /**
     * @return string
     */
    public static function getCity()
    {
        self::cache();
        return self::$city;
    }

    /**
     * @return string
     */
    public static function getZipCode()
    {
        self::cache();
        return self::$zip_code;
    }

    /**
     * @return mixed|string
     */
    public static function getCurrency()
    {
        return ($code = self::getCountryCode()) && self::$country2currency[$code] ?
            self::$country2currency[$code] :
            'USD';
    }
}
