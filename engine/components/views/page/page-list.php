<h2><?=html_encode($title);?></h2>
<?php if (!empty($pages)) {?>
    <ul class="links two-columns">
        <?php foreach ($pages as $page) { ?>
            <li><a href="<?= url(['pages/index', 'slug' => $page->slug]); ?>"><?=html_encode($page->title);?></a></li>
        <?php } ?>
    </ul>
<?php } ?>
