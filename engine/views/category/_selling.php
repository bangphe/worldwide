<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\helpers\SvgHelper;
use app\helpers\FrontendHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
?>
<div class="listing-search">
    <form method="post"  onsubmit="return savedata()">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "title">Title</label>
                    <input type="text" required id="title" name="title" class="form-control" placeholder = "What is on offer">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "type_of_business">Type of business</label>
                    <input type="text" required id="type_of_business" name="type_of_business" class="form-control" placeholder = "what is the business about">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "location">Location</label>
                    <input type="text" required id="location" name="location" class="form-control" placeholder = "where is the business located">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "description">Description</label>
                    <input type="text" required id="description" name="description" class="form-control" placeholder = "describe the business for sale">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "value">Value</label>
                    <input type="text" required id="value" name="value" class="form-control" placeholder = "Business valuation">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "price">Price</label>
                    <input type="text" required id="price" name="price" class="form-control" placeholder = "The sale price">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "terms_of_business">Terms of business</label>
                    <input type="text" required id="terms_of_business" name="terms_of_business" class="form-control" placeholder = "How do you want to structure the business to business arrangement">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <a class="btn btn-warning" href="/category/business"><i class="fa fa-arrow-left"></i></a>
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <input type="submit" value="Submit" class="btn btn-success">
                </div>
            </div>
        </div>
    </form>
</div>
