<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use app\models\Listing;
use app\models\ListingSearch;
use app\models\Category;
use app\models\Zone;
use app\models\Customer;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use app\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    const ADS_PER_PAGE = 20;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ListingSearch();
        $topCatsWithFirstLevelChildren = Category::getTopCatsWithFirstLevelChildren();

        // Meta tags
        app()->view->registerMetaTag([
            'name'    => 'keywords',
            'content' => options()->get('app.settings.common.siteKeywords', 'EasyAds')
        ]);
        app()->view->registerMetaTag([
            'name'    => 'description',
            'content' => options()->get('app.settings.common.siteDescription', 'EasyAds')
        ]);

        app()->view->title = options()->get('app.settings.common.siteName', 'EasyAds');

        if ($aid = request()->get('aid')) {
            Customer::setAffiliateCookie($aid);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'categories'  => $topCatsWithFirstLevelChildren
        ]);
    }

    /**
     * Displays search page.
     *
     * @return string
     */
    public function actionSearch()
    {
        $searchModel = new ListingSearch();
        $categories = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();

        $categoryPlaceholderText = t('app', 'Choose Category');
        if ($slug = request()->get('slug')) {
            $chosenCategory = Category::findCategoryBySlug($slug);
            if ($chosenCategory) {
                $categoryPlaceholderText = Icon::make($chosenCategory->icon) . ' ' .html_encode($chosenCategory->name);
            }
        }
        
        // ads
        $AdsProvider = $searchModel->categorySearch(request()->queryParams);
        // $AdsProvider->query->andWhere(['c.category_id' => $categories]);
        //->orderBy(['promo_expire_at' => SORT_DESC, 'created_at' => SORT_DESC]);
        //$AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        $AdsProvider->pagination = [
            'defaultPageSize' => self::ADS_PER_PAGE,
        ];

        // select location details if filter is not empty
        $locationDetails = '';
        if (isset(request()->queryParams['ListingSearch']) && !empty(request()->queryParams['ListingSearch']['zoneId'])) {
            $zone = Zone::find()->with('country')->where(['zone_id' => request()->queryParams['ListingSearch']['zoneId']])->one();
            if ($zone) {
                $locationDetails = $zone->name . ', ' . $zone->country->name;
            }
        }

        app()->view->title = t('app', 'Search') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('search-results', [
            'searchModel'               => $searchModel,
            'categories'                => $categories,
            'categoryPlaceholderText'   => $categoryPlaceholderText,
            'adsProvider'               => $AdsProvider,
            'locationDetails'           => $locationDetails,
            'isNothingFound'            => !$AdsProvider->getCount(),
        ]);
    }

    public function actionFeatured($view = null)
    {
        $searchModel = ListingSearch::find()->alias('t')
            ->joinWith(['package c1'], true, 'INNER JOIN')
            ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
            // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
            ->where(['status' => Listing::STATUS_ACTIVE]);

        // $searchModel->andWhere(['>', 'promo_expire_at', new Expression('NOW()')])
            $searchModel->andWhere(['c1.promo_show_featured_area' => 'yes'])
            ->orderBy(['promo_expire_at' => SORT_DESC, 'created_at' => SORT_DESC]);

        // ads
        $AdsProvider = new ActiveDataProvider([
            'query' => $searchModel,
        ]);
        $AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        $AdsProvider->pagination = [
            'defaultPageSize' => 16,
        ];

        app()->view->title = t('app', 'Featured Ads') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        if ($view) {
            \Yii::$app->session->set('ads-view', $view);
        }

        return $this->render('featured', [
            'searchModel'               => $searchModel,
            'adsProvider'               => $AdsProvider,
        ]);
    }

    public function actionPromoted($view = null)
    {
        // $this->bodyClass = 'custom-promoted';
        $searchModel = ListingSearch::find()->alias('t')
            ->joinWith(['package c1'], true, 'INNER JOIN')
            ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
            // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
            ->where(['status' => Listing::STATUS_ACTIVE]);

        // $searchModel->andWhere(['>', 'promo_expire_at', new Expression('NOW()')])
        $searchModel->andWhere(['c1.promo_show_promoted_area' => 'yes'])
            ->orderBy(['promo_expire_at' => SORT_DESC, 'created_at' => SORT_DESC]);

        // ads
        $AdsProvider = new ActiveDataProvider([
            'query' => $searchModel,
        ]);
        $AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        $AdsProvider->pagination = [
            'defaultPageSize' => 16,
        ];

        app()->view->title = t('app', 'Promoted Ads') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        if ($view) {
            \Yii::$app->session->set('ads-view', $view);
        }

        return $this->render('promoted', [
            'searchModel'               => $searchModel,
            'adsProvider'               => $AdsProvider
        ]);
    }

    public function actionNew($view = null)
    {
        $searchModel = ListingSearch::find()->alias('t')
            ->joinWith(['package c1'], true, 'INNER JOIN')
            ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
            // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
            ->andWhere(['status' => Listing::STATUS_ACTIVE]);

        // $searchModel->andWhere('t.promo_expire_at <= t.created_at')
        $searchModel->orderBy(['t.promo_expire_at' => SORT_DESC]);

        // ads
        $AdsProvider = new ActiveDataProvider([
            'query' => $searchModel,
        ]);
        $AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        $AdsProvider->pagination = [
            'defaultPageSize' => 16,
        ];

        app()->view->title = t('app', 'New Ads') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        if ($view) {
            \Yii::$app->session->set('ads-view', $view);
        }

        return $this->render('new', [
            'searchModel'               => $searchModel,
            'adsProvider'               => $AdsProvider
        ]);
    }

    public function actionAllCategories()
    {
        // $searchModel = ListingSearch::find()->alias('t')
        //     ->joinWith(['package c1'], true, 'INNER JOIN')
        //     ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
        //     // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
        //     ->andWhere(['status' => Listing::STATUS_ACTIVE]);

        // // $searchModel->andWhere('t.promo_expire_at <= t.created_at')
        // $searchModel->orderBy(['t.promo_expire_at' => SORT_DESC]);

        // // ads
        // $AdsProvider = new ActiveDataProvider([
        //     'query' => $searchModel,
        // ]);
        // $AdsProvider->sort = ['defaultOrder' => ['promo_expire_at' => SORT_DESC]];
        // $AdsProvider->pagination = [
        //     'defaultPageSize' => 16,
        // ];

        app()->view->title = t('app', 'All Categories') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('all-categories', [
            // 'searchModel'               => $searchModel,
            // 'adsProvider'               => $AdsProvider
        ]);
    }

    public function actionPreview($url)
    {
        app()->view->title = t('app', 'Ads') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->renderPartial('preview', [
            'url' => $url,
        ]);
    }

    /**
     * @return string
     */
    public function actionOffline()
    {
        if(options()->get('app.settings.common.siteStatus', 1) == 1) {
            $this->redirect(['/']);
        }

        app()->view->title = t('app', 'Offline') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('offline', [
           'message' =>  options()->get('app.settings.common.siteOfflineMessage', t('app', 'Application is offline, please try again later!'))
        ]);
    }

    /**
     * @param $url
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionRedirect($url)
    {
        if (substr($url, -1) == '/') {
            return $this->redirect(app()->getRequest()->getBaseUrl() . '/' . rtrim($url, '/'), 301);
        }
        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }

    /**
    * @param $url
    * @return \yii\web\Response
    * @throws NotFoundHttpException
    */
    public function actionHello()
    {
        echo "pankaj Mishra"; exit;
    }

}