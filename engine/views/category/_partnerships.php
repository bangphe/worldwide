<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\helpers\SvgHelper;
use app\helpers\FrontendHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
?>

<div class="listing-search">
    <form method="post"  onsubmit="return savedata()">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "title">Title</label>
                    <input type="text" required id="title" name="title" class="form-control" placeholder = "Looking to merge, buy shares, investing or local partner in a location">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "type_of_business">Business type</label>
                    <input type="text" required id="type_of_business" name="type_of_business" class="form-control" placeholder = "The type of partnership ,local ,international,expansion or merge">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "location">Location</label>
                    <input type="text" required id="location" name="location" class="form-control" placeholder = "base of current operations">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "capital">Capital</label>
                    <input type="text" required id="capital" name="capital" class="form-control" placeholder = "Money required or money to be invested">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "shares">Shares</label>
                    <input type="text" required id="shares" name="shares" class="form-control" placeholder = "percentage or how much of the business is on offer">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "terms_of_partnership">Terms of partnership</label>
                    <input type="text" required id="terms_of_partnership" name="terms_of_partnership" class="form-control" placeholder = "What each partner stands to benefit">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <a class="btn btn-warning" href="/category/business"><i class="fa fa-arrow-left"></i></a>
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <input type="submit" value="Submit" class="btn btn-success">
                </div>
            </div>
        </div>
    </form>
</div>
