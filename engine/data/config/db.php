<?php
return [
    'class' => 'yii\db\Connection',
    'dsn' => '{DB_CONNECTION_STRING}',
    'username' => '{DB_USER}',
    'password' => '{DB_PASS}',
    'charset' => 'utf8',
    'tablePrefix'=> '{DB_PREFIX}',
];