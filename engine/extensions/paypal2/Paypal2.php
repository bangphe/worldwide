<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\extensions\paypal2;

use app\models\Order;
use app\models\OrderTransaction;
use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;

/**
 * Class Paypal
 * @package app\extensions\paypal2
 */
class Paypal2{

    public $name = 'Paypal REST';

    public $author = 'Vladislav Lyshenko';

    public $version = '1.0';

    public $description = 'Paypal payment gateway via REST';

    const TYPE = 'gateway';

    public function run()
    {
        app()->urlManager->addRules(['paypal2' => 'paypal2/ipn']);

        // register ipn controller
        app()->controllerMap['paypal2'] = [
            'class' => 'app\extensions\paypal2\controllers\PaypalController'
        ];
        // event init
        app()->on('app.controller.ad.init', function(){
            if(options()->get('app.gateway.paypal2.status', 'inactive') != 'active'){
                return;
            }

            app()->on('app.ad.gateways.option', function($event){
                echo app()->view->renderFile('@app/extensions/paypal2/views/gateway-frontend-option.php',[
                    'description' => options()->get('app.gateway.paypal2.description', 'EasyAds'),
                ]);
            });

            /*app()->on('app.ad.gateways.form', function($event){
                echo app()->view->renderFile('@app/extensions/paypal2/views/gateway-frontend-form.php');
            });*/

            app()->on('app.controller.ad.package.handlePayment',['app\extensions\paypal2\Paypal2', 'handlePayment']);
        });

        return self::TYPE;
    }

    /**
     * @param $event
     * @throws \Exception
     */
    public static function handlePayment($event)
    {
        if('paypal2' != request()->post('paymentGateway')){
            return;
        }
        $cartTotal = $event->params['cartTotal'];

        if(empty($cartTotal)){
            return;
        }

        /** @var Order $order */
        $order = $event->params['order'];

        //update order status
        $order->status = Order::STATUS_PENDING;

        if(!$order->save(false)){
            throw new \Exception('Something went wrong, please try again later.');
        }

        $price = app()->formatter->asDecimal($cartTotal,2);
        $currency = options()->get('app.settings.common.siteCurrency', 'usd');

        // Create a gateway for the PayPal RestGateway
        // (routes to GatewayFactory::create)
        $gateway = Omnipay::create('PayPal_Rest');

        // Initialise the gateway
        $gateway->initialize(array(
            'clientId' => options()->get('app.gateway.paypal2.cid'),
            'secret'   => options()->get('app.gateway.paypal2.secret'),
            'testMode' => (options()->get('app.gateway.paypal2.mode', 'sandbox') == 'sandbox') ? true : false,
        ));

        $transaction = $gateway->purchase(array(
            'amount'        => $price,
            'currency'      => $currency,
            'description'   => 'Purchase Package',
            'cancelUrl'     => request()->hostInfo . request()->url,
            'returnUrl'     => url(['paypal2/ipn'], true),
        ));

        $response = $transaction->send();

        $transactionReference = null;

        if (!$response->isSuccessful()) {
            throw new \Exception('===' . $response->getMessage());
        }

        $transactionReference = $response->getTransactionReference();

        /** @var OrderTransaction $orderTransaction */
        $orderTransaction = $event->params['transaction'];

        //update order transaction data
        $orderTransaction->gateway                  = 'Paypal2';
        $orderTransaction->type                     = 'purchase';
        $orderTransaction->transaction_reference    = $transactionReference;
        if(!$orderTransaction->save(false)){
            throw new \Exception('Something went wrong, please try again later.');
        }

        //go to paypal
        if ($response->isRedirect()) {
            $event->params['error'] = '';
            $redirect = $response->getRedirectUrl();
            $event->params['redirect'] = $redirect;
        }
    }
}