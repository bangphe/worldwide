<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ea_continents".
 *
 * @property string $code Continent code
 * @property string|null $name
 *
 * @property Country[] $countries
 */
class Continents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ea_continents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Countries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountries()
    {
        return $this->hasMany(Country::className(), ['continent_code' => 'code']);
    }

    /**
     * @return auto\Country[]|array
     */
    public static function getAllContinents()
    {
        return static::find()->all();
    }
}
