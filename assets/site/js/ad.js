jQuery(document).ready(function($) {

    //##### POST AD

    var elemHidePhone = $('.field-listing-hide_phone input[type=checkbox]');
    var elemHideEmail = $('.field-listing-hide_email input[type=checkbox]');

    // Don't allow user to hide both contact methods
    $(document).on('click','.field-listing-hide_phone input[type=checkbox], .field-listing-hide_email input[type=checkbox]',function () {
        if(elemHidePhone.prop('checked') == true && elemHideEmail.prop('checked') == true){
            $('#modal-post-listing-contact').modal('show');
            elemHideEmail.prop('checked',false);
            elemHidePhone.prop('checked',false);
        }
    });

    if ( $('.promote-slider .owl-carousel').length > 0) {
        $('.promote-slider .owl-carousel').owlCarousel({
            nav: false,
            navText: ['', ''],
            items: 1,
            autoWidth:false,
            loop: false,
            margin: 20,
            smartSpeed: 1000,
            mouseDrag: false,
            responsive:{
                768:{
                    items:2
                },
                1280:{
                    items:3
                }
            },
            onInitialize: function(e){

            }
        });
    }

    // $("#input-gallery").fileinput({
    //     language: site.language,
    //     deleteUrl: false,
    //     uploadUrl: false,
    //     allowedFileTypes: ["image"],
    //     showUpload: false,
    //     browseClass: 'btn btn-as',
    //     removeClass: 'btn btn-as reverse',
    //     uploadClass: 'btn btn-as reverse',
    //     layoutTemplates: {
    //         footer: '<div class="file-thumbnail-footer">\n' +
    //         '    {progress} {actions}\n' +
    //         '</div>'
    //     },
    //     fileActionSettings: {
    //         showUpload: false,
    //         showDrag: true,
    //     }
    // });

    // SCROLL *****************************************************************************************************************************************

    $('.category-items').mCustomScrollbar({
        scrollInertia: 0,
        scrollEasing: "liniar"
    });

    $('.column-subcategory').mCustomScrollbar({
        axis:"x",
        mouseWheel:false,
        advanced: {
            autoExpandHorizontalScroll:true,
            updateOnContentResize: true,
            updateOnSelectorChange: true,
        },
        scrollInertia: 0,
        scrollEasing: "liniar"
    });



    // Category Modal **************
    // Desktop
    $(document).on('click','.primary-category li a',function (e) {
        e.preventDefault();

        $('#modal-category .modal-footer #success-selection').hide();
        $('#modal-category .modal-footer .no-category-selected').show();
        $('#modal-category .modal-footer #close-modal').show();

        var clickedParentId = $(this).data('id');
        $('.primary-category li a').removeClass('selected').find('span.arrow').remove();
        $(this).addClass('selected').append('<span class="arrow"></span>');

        $('.column-subcategory-wrapper .column-category').hide();
        $(this).parents('.choose-category').find('.column-category[data-parent="'+clickedParentId+'"]').show();

        if($(this).parents('.choose-category').find('.column-category[data-parent="'+clickedParentId+'"]').length == 0 ){
            $('#modal-category .modal-footer #success-selection').show().data({'selectedId':clickedParentId,'selectedText':$(this).text()});
            $('#modal-category .modal-footer .no-category-selected').hide();
            $('#modal-category .modal-footer #close-modal').hide();
            $('.primary-category li a span.arrow').remove();
        }

    });

    $(document).on('click','.column-subcategory-wrapper .column-category li a',function (e) {
        e.preventDefault();

        $('#modal-category .modal-footer #success-selection').hide();
        $('#modal-category .modal-footer .no-category-selected').show();
        $('#modal-category .modal-footer #close-modal').show();
        var parent = $(this).closest('.column-category');
        var found = false;
        $('.column-subcategory-wrapper .column-category').each(function () {
            if(parent.index() == $(this).index()-1){
                found = true;
            }
            if(found){
                $(this).hide().find('li a').removeClass('selected').find('span.arrow').remove();
            }
        })

        var clickedParentId = $(this).data('id');
        $(this).closest('.column-category').find('a').removeClass('selected').find('span.arrow').remove();
        $(this).addClass('selected').append('<span class="arrow"></span>');
        $(this).parents('.choose-category').find('.column-category[data-parent="'+clickedParentId+'"]').show();
        $('.column-subcategory').mCustomScrollbar('scrollTo','-=250',{
            timeout:150,
            scrollInertia:1500,
            scrollEasing: "liniar",
        });

        if($(this).parents('.choose-category').find('.column-category[data-parent="'+clickedParentId+'"]').length == 0 ){
            $('#modal-category .modal-footer #success-selection').show().data({'selectedId':clickedParentId,'selectedText':$(this).text()});
            $('#modal-category .modal-footer .no-category-selected').hide();
            $('#modal-category .modal-footer #close-modal').hide();
            $(this).find('span.arrow').remove();
        }
    });

    $(document).on('click','#modal-category .modal-footer #success-selection',function (e) {
        e.preventDefault();
        $('#choose-class').text($(this).data('selectedText'));
        $('input#listing-category_id').val($(this).data('selectedId'));

        $.post($('#category-fields').data('url'),{
            category_id:$(this).data('selectedId'),
            listing_id:$('#post-form').data('listing'),
        },function (json) {
            if(json.html) {
                $('.category-fields').show();
                $('#category-fields').html(json.html);
                $('select').select2({
                    width: '100%',
                    language: site.language,
                    dir: site.dir,
                });
            }else{
                $('.category-fields').hide();
            }
        }, 'json');
    });

    // Mobile
    $('.choose-catg-m, .close-categ-m, .close-x-categ-m').on('click', function () {
        $('.subcateg-m').hide();
        $('.maincateg-m').show();
        $('.choose-category-mobile').toggleClass('opened');
        return false;
    });

    $('.categ-item-m, .categ-subitem-m').on('click', function () {
        $('#choose-class-m').text($(this).text());
        var $divSubcateg= $(this).data('subcateg');
        console.log($divSubcateg);

        // if has childs
        if($divSubcateg != '') {
            $('.maincateg-m, .subcateg-m').hide();
            $('#subcateg-' + $divSubcateg).show();
        } else {
            //else submit
            var id = $(this).data('id');
            $('input#listing-category_id').val(id);
            $.post($('#category-fields').data('url'), {
                category_id: id,
                listing_id: $('#post-form').data('listing'),
            }, function (json) {
                if (json.html) {
                    $('.category-fields').show();
                    $('#category-fields').html(json.html);
                    $('select').select2({
                        width: '100%',
                        language: site.language,
                        dir: site.dir,
                    });
                } else {
                    $('.category-fields').hide();
                }
            }, 'json');
            $('.close-x-categ-m').click();
        }
        return false;
    });

    $('.back-categ-m').on('click', function () {
        $('.subcateg-m').hide();
        $('.maincateg-m').show();
        return false;
    });

    // END Category

    $(document).on('change','#location-country_id',function (e) {
        e.preventDefault();

        $.post($('#listing-select-zones-wrapper').data('url'),{country_id:$(this).val()},function (json) {
            $('#listing-select-zones-wrapper select option.gen-op').remove();
            $.each(json.response,function(key,value){
                $('#listing-select-zones-wrapper select').append($('<option class="gen-op">').text(value.name).attr('value', value.zone_id));
            });
            $('select').select2({
                width: '100%',
                language: site.language,
                dir: site.dir,
            });
        }, 'json');
    });

    setTimeout(function () {
        if($('input#listing-category_id').val()){
            var categoryId = $('input#listing-category_id').val();
            $('div#modal-category a[data-id='+categoryId+']').click();
            $('#success-selection').click();
        }

        //autocomplete zone on update
        if($('select#location-country_id').val()){
           $('select#location-country_id').val($('select#location-country_id').val()).trigger('change');
           $(document).ajaxStop(function () {
               $('select#location-zone_id').val($('#listing-select-zones-wrapper').data('zone')).trigger('change');
           });
        }
    },100);


    // ### Ad Package page
    $(document).on('click','.post-listing-promote .item',function () {
       var pricePackage = $(this).data('price');
       $('#paymentGateway').attr('required',false);
       $('#payment-block, #payment-details-block').hide();
       if(parseInt(pricePackage) > 0){
           $('#paymentGateway').attr('required',true);
           $('#payment-block, #payment-details-block').show();
       }

        $.post($(this).data('url'),{
            country_id:$(this).data('country_id'),
            zone_id:$(this).data('zone_id'),
            price:$(this).data('price'),
        },function (json) {
            if(json.html) {
                $('.summary-wrapper').show();
                $('.summary-wrapper').html(json.html);
            }
        }, 'json');
    });

    $('#package-form').on('submit', function (e) {
        $('body').addClass('please-wait');
    });

    $('.file-loading').on('filesorted', function(event, params) {
        $.post($('#listingimage-imagesgallery').data('sort-listing-images'),{images:JSON.stringify(params.stack)},function (json) {
        }, 'json');
    });


    // ### Ad view page
    // SCROLL TOP HEADING *****************************************************************************************************************************
    //var eTopAdHeading = $('.listing-heading').offset().top;
    $(window).scroll(function () {
        var headerHeight = $('.header-wrapper').height();
        // var diff = eTopAdHeading - $(window).scrollTop();
        $(window).scrollTop() > 0 ? $('.listing-heading-wrapper').css({top: headerHeight + 1}).addClass('sticky') : $('.listing-heading-wrapper').removeClass('sticky').removeAttr('style');
    });

    $(window).on('load resize', function(){
        var adHeadingHeight = $('.listing-heading-wrapper').height();
        $('.listing-heading').css({minHeight: adHeadingHeight});
    });

    $('.add-to-favorites').on('click', function () {
        setTimeout(function () {
            $(window).scrollTop($(window).scrollTop()+1);
        },500);
    });

    $('body').on('click', 'button.close', function() {
        $(window).scrollTop($(window).scrollTop()+1);
    });


    // GALLERY *****************************************************************************************************************************************

    if ( $('.small-gallery').length > 0) {
        $('.small-gallery').owlCarousel({
            nav: false,
            items: 1,
            autoWidth: false,
            loop: true,
            margin: 0,
            smartSpeed: 1000,
            mouseDrag: false,
            responsive:{
                900:{
                    items:1
                }
            },
            onInitialize: function(e){

            }
        });

        $('.gallery-left').on('click', function(){
            $('.owl-carousel').trigger('prev.owl.carousel');
        });
        $('.gallery-right').on('click', function(){
            $('.owl-carousel').trigger('next.owl.carousel');
        });
    }
    /* *** */


    var bigGalleryCarousel = function () {
        var $owl = $('.full-gallery');
        $owl.trigger('destroy.owl.carousel');

        if ( $('.full-gallery').length > 0) {

            $owl.owlCarousel({
                nav: false,
                items: 1,
                autoWidth: false,
                loop: false,
                margin: 0,
                smartSpeed: 1000,
                mouseDrag: true,
                responsive:{
                    900:{
                        items:1
                    }
                },
                onInitialize: function(e){
                    $('.resizeImg').each(function(){
                        var self = $(this);
                        var width = self.width();
                        var height = self.height();
                        if (width > height) {
                            self.css({height: "100%", width: "auto"});
                        }
                        else{
                            self.css({width: "100%", height: "auto"});
                        }
                    });
                }
            });

            $('.gallery-left-big').on('click', function(){
                $('.owl-carousel').trigger('prev.owl.carousel');
            });
            $('.gallery-right-big').on('click', function(){
                $('.owl-carousel').trigger('next.owl.carousel');
            });
        }
    }

    $('.big-gallery .x-close, .open-full-gallery img, .img-wrapper .zoom, .thb-wrapper').on('click', function(){
        $('.big-gallery').toggleClass('open');
        bigGalleryCarousel();
    });

    $(window).bind('orientationchange', function(event) {
        bigGalleryCarousel();
    });

    //Handle keyboard navigation
    $(document.documentElement).keyup(function(event) {
        // handle cursor keys
        if (event.keyCode == 37) {
            $('.gallery-left-big').click();
        } else if (event.keyCode == 39) {
            $('.gallery-right-big').click();
        } else if (event.keyCode === 27){
            $('.big-gallery.open .x-close').click();
        }
    });

    $(document).on('click', '#listing-send-msg', function (e) {
        e.preventDefault();
        $.post($('#listing-send-msg').data('url'),{
            customer_id:$('#listing-send-msg').data('customer-id'),
        },function (json) {
            if(json.result == 'success') {
                window.location.href = 'mailto:'+json.response.email;
            }
        }, 'json');
    });

    $(document).on('click', '#listing-show-email', function (e) {
        if($('#listing-show-email').attr('href') == '#'){
            e.preventDefault();
        }
        $.post($('#listing-show-email').data('url'),{
            customer_id:$('#listing-show-email').data('customer-id'),
        },function (json) {
            if(json.result == 'success') {
                $('#listing-show-email').html(json.response.email);
                $('#listing-show-email').attr('href', 'mailto:' + json.response.email);
            }
        }, 'json');
    });

    $(document).on('click', '#listing-show-phone', function (e) {
        if($('#listing-show-phone').attr('href') == '#'){
            e.preventDefault();
        }
        $.post($('#listing-show-phone').data('url'),{
            customer_id:$('#listing-show-phone').data('customer-id'),
        },function (json) {
            if(json.result == 'success') {
                $('#listing-show-phone').html(json.response.phone);
                $('#listing-show-phone').attr('href', 'tel:' + json.response.phone);
            }
        }, 'json');
    });

    $(document).on('click', '.track-stats', function () {
        var $this = $(this);
        $.post(site.statsUrl,{
            stats_type:$this.data('stats-type'),
            listing_id:$this.data('listing-id'),
        },function (json) {
            if(json.result == 'success') {

            } else {

            }
        }, 'json');
    });
});
