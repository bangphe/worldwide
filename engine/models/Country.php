<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

/**
 * Class Country
 * @package app\models
 */
class Country extends \app\models\auto\Country
{
    /**
     * Constants
     */
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['status'], 'string'],
            [['name', 'code'], 'string', 'max' => 100],
        ];
    }

    /**
     * @return auto\Country[]|array
     */
    public static function getActiveCountries()
    {
        return static::find()->where(['status' => self::STATUS_ACTIVE])->all();
    }

}
