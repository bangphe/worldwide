<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */
e
namespace app\modules\admin\controllers;

use app\models\ListingStat;
use app\models\Listing;
use app\models\ListingSearch;
use app\models\ListingImage;
use app\models\Order;
use app\models\OrderTax;
use app\models\OrderTransaction;
use yii\web\NotFoundHttpException;

/**
 * Controls the actions for ads section
 *
 * @Class AdsController
 * @package app\modules\admin\controllers
 */
class ListingsController extends \app\modules\admin\yii\web\Controller
{

    /**
     * Lists all Ads
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ListingSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->pagination->pageSize=10;
        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays stats for a specific ad
     *
     * @param $id
     * @return string
     */
    public function actionStats($id)
    {
        return $this->render('stats', [
            'model' => $this->findStat($id),
            'ad' => $this->findModel($id),
        ]);
    }

    /**
     * Lists all Ads with pending status
     *
     * @return string
     */
    public function actionPendings()
    {
        $searchModel = new ListingSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->query->andWhere(['status' => Listing::STATUS_PENDING_APPROVAL]);
        $dataProvider->pagination->pageSize=10;
        return $this->render('list-pendings', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all Ads with pending status
     *
     * @return string
     */
    public function actionExpired()
    {
        $searchModel = new ListingSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->query->andWhere(['status' => Listing::STATUS_EXPIRED]);
        $dataProvider->pagination->pageSize=10;
        return $this->render('list-expired', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Ads by countries paid
     *
     * @return string
     */
    public function actionPaid()
    {
        $searchModel = new ListingSearch();
        // $dataProvider = $searchModel->countrySearch(request()->queryParams);
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->query->andWhere(['!=', 't.package_id', 1]);
        $dataProvider->pagination->pageSize=10;
        return $this->render('list-countries-paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionExportpaid()
    {
        ini_set('memory_limit', '8192M'); 
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        
        $model = [];

        //get data
        $header = [
            "Country",
            "Zone",
            "City",
            "Package",
            "Ad Spesification",
            "Category",
            "Currency",
            "Price"
        ];

        $model = Listing::find()->where(['!=', 'package_id', 1])->orderBy('listing_id ASC')->all();
        
        $filename = "Report-Ads-By-Location-".date('YmdHi').".csv";
        $newModel = [];
        foreach ($model as $key => $value) {
            $newModel[$key][] = $value->location->country->name;
            $newModel[$key][] = $value->location->zone->name;
            $newModel[$key][] = $value->location->city;
            $newModel[$key][] = $value->package == NULL ? 'Free' : $value->package->title;
            $newModel[$key][] = $value->title;
            $newModel[$key][] = $value->category->name;
            $newModel[$key][] = $value->currency->name;
            $newModel[$key][] = $value->currency->symbol.' '.$value->price;
        }

        $fp = fopen('php://output', 'w');
        
        // $list = array (
        //   array("Peter", "Griffin" ,"Oslo", "Norway"),
        //   array("Glenn", "Quagmire", "Oslo", "Norway")
        // );
        // die(var_dump($list));
        
        header('Content-Type: application/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);

        fputcsv($fp, $header);

        foreach ($newModel as $data)
        {
            fputcsv($fp, $data);
        }
        
        fclose($fp);

        exit;
    }

    /**
     * Lists all Ads by countries unpaid
     *
     * @return string
     */
    public function actionUnpaid()
    {
        $searchModel = new ListingSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->query->andWhere(['t.package_id' => 1]);
        $dataProvider->pagination->pageSize=10;
        return $this->render('list-countries-unpaid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionExportunpaid()
    {
        ini_set('memory_limit', '8192M'); 
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        
        $model = [];

        //get data
        $header = [
            "Country",
            "Zone",
            "City",
            "Package",
            "Ad Spesification",
            "Category",
            "Currency",
            "Day",
            "Week",
            "Month",
            "Year"
        ];

        $model = Listing::find()->where(['package_id' => 1])->orderBy('listing_id ASC')->all();
        
        $filename = "Report-Performance-Report-".date('YmdHi').".csv";
        $newModel = [];
        foreach ($model as $key => $value) {
            $newModel[$key][] = $value->location->country->name;
            $newModel[$key][] = $value->location->zone->name;
            $newModel[$key][] = $value->location->city;
            $newModel[$key][] = $value->package == NULL ? 'Free' : $value->package->title;
            $newModel[$key][] = $value->title;
            $newModel[$key][] = $value->category->name;
            $newModel[$key][] = $value->currency->name;
            $newModel[$key][] = $value->currency->symbol.' '.$value->price;
            $newModel[$key][] = $value->price;
            $newModel[$key][] = $value->price;
            $newModel[$key][] = $value->price;
            $newModel[$key][] = $value->price;
        }

        $fp = fopen('php://output', 'w');
        
        // $list = array (
        //   array("Peter", "Griffin" ,"Oslo", "Norway"),
        //   array("Glenn", "Quagmire", "Oslo", "Norway")
        // );
        // die(var_dump($list));
        
        header('Content-Type: application/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename);

        fputcsv($fp, $header);

        foreach ($newModel as $data)
        {
            fputcsv($fp, $data);
        }
        
        fclose($fp);

        exit;
    }

    /**
     * Handles the activation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        Listing::sendAdActivateEmail($model);
        $model->status = Listing::STATUS_ACTIVE;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    /**
     * Handles the deactivation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);
        Listing::sendAdDeactivateEmail($model);
        $model->status = Listing::STATUS_DEACTIVATED;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    /**
     * View a specific ad in frontend
     *
     * @param $id
     * @return \yii\web\Response
     */
    public function actionAdminPreviewAd($id)
    {
        $ad = Listing::findOne(['listing_id' => (int)$id]);
        if (empty($ad)) {
            notify()->addWarning(t('app', 'Please specify a valid ad'));
            return $this->redirect(['/admin/listings']);
        }

        app()->customer->switchIdentity($ad->customer);

        session()->set('impersonating_customer', true);
        if ($ad->status == Listing::STATUS_PENDING_APPROVAL) {
            session()->set('impersonating_customer_back_url', url(['/admin/listings/pendings']));
        }
        session()->set('impersonating_customer_back_url', url(['/admin/listings']));
        return $this->redirect(['/listing/preview/' . $ad->slug]);
    }

    /**
     * Deletes an existing Agent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteAd($id)
    {
        $ad = $this->findModel($id);
        $ad_image = ListingImage::deleteAll('listing_id = :listing_id', [':listing_id' => $id]);
        $ad_stat = ListingStat::deleteAll('listing_id = :listing_id', [':listing_id' => $id]);
        $ad_order = Order::find()->where(['listing_id'=>$id])->one();
        if (count($ad_order) > 0) {
            $order_tax = OrderTax::deleteAll('order_id = :order_id', [':order_id' => $ad_order->order_id]);
            $order_trans = OrderTransaction::deleteAll('order_id = :order_id', [':order_id' => $ad_order->order_id]);
        }

        $order = Order::deleteAll('listing_id = :listing_id', [':listing_id' => $id]);
        
        if ($ad->delete()) {
            notify()->addSuccess(t('app','Data is successfully deleted.'));
            return $this->redirect(['pendings']);
        }
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Listing::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findStat($id)
    {
        if (($model = ListingStat::findOne($id)) !== null) {
            return $model;
        }
        return false;
    }
}
