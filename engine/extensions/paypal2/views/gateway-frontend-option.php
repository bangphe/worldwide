<?php
use app\extensions\paypal2\PaypalAsset;

PaypalAsset::register($this);
?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
    <div class="item">
        <input type="radio" name="paymentGateway" value="paypal2" id="payment_method_paypal"  />
        <label for="payment_method_paypal">
            <span class="graphic"><?=t('app', 'Paypal');?><i class="fa fa-cc-paypal" aria-hidden="true"></i></span>
            <span class="text"><?=html_encode($description);?></span>
        </label>
    </div>
</div>