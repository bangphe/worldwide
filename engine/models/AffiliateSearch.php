<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Class AffiliateSearch
 * @package app\common\models
 */
class AffiliateSearch extends Affiliate
{
    public $searchPhrase;
    public $affiliated_members;
    public $store;
    public $affiliate_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_id', 'affiliated_members', 'customer_id', 'first_name', 'last_name', 'email'], 'safe'],
            [['affiliated_members', 'customer_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find();
        $query->Where(['!=', 'affiliate_id', '']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => ['defaultOrder' => ['affiliated_members' => SORT_DESC]],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'affiliate_id', $this->affiliate_id])
            ->andFilterWhere(['=', 'affiliated_members', $this->affiliated_members])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email]);
        //echo "<pre>"; print_r($dataProvider); exit;
        return $dataProvider;
    }
}
