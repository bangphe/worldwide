<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\modules\admin\controllers;

use app\models\Affiliate;
use app\models\AffiliateSearch;
use app\models\Customer;
use yii\web\NotFoundHttpException;

/**
 * Controls the actions for ads section
 *
 * @Class AdsController
 * @package app\modules\admin\controllers
 */
class AffiliatesController extends \app\modules\admin\yii\web\Controller
{

    /**
     * Lists all Affiliate
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AffiliateSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->pagination->pageSize=10;
        
        return $this->render('affiliate', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    // /**
    //  * Displays stats for a specific ad
    //  *
    //  * @param $id
    //  * @return string
    //  */
    // public function actionStats($id)
    // {
    //     return $this->render('stats', [
    //         'model' => $this->findStat($id),
    //         'ad' => $this->findModel($id),
    //     ]);
    // }

    // /**
    //  * Lists all Affiliate with pending status
    //  *
    //  * @return string
    //  */
    // public function actionPendings()
    // {
    //     $searchModel = new AffiliateSearch();
    //     $dataProvider = $searchModel->search(request()->queryParams);
    //     $dataProvider->query->andWhere(['status' => Affiliate::STATUS_PENDING_APPROVAL]);
    //     $dataProvider->pagination->pageSize=10;
    //     return $this->render('affiliate-pendings', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    // /**
    //  * Lists all Affiliate by countries paid
    //  *
    //  * @return string
    //  */
    // public function actionPaid()
    // {
    //     $searchModel = new AffiliateSearch();
    //     // $dataProvider = $searchModel->countrySearch(request()->queryParams);
    //     $dataProvider = $searchModel->search(request()->queryParams);
    //     $dataProvider->query->andWhere(['!=', 't.package_id', 1]);
    //     $dataProvider->pagination->pageSize=10;
    //     return $this->render('affiliate-countries-paid', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    // /**
    //  * Lists all Affiliate by countries unpaid
    //  *
    //  * @return string
    //  */
    // public function actionUnpaid()
    // {
    //     $searchModel = new AffiliateSearch();
    //     $dataProvider = $searchModel->search(request()->queryParams);
    //     $dataProvider->query->andWhere(['t.package_id' => 1]);
    //     $dataProvider->pagination->pageSize=10;
    //     return $this->render('affiliate-countries-unpaid', [
    //         'searchModel' => $searchModel,
    //         'dataProvider' => $dataProvider,
    //     ]);
    // }

    /**
     * Handles the activation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->affiliate_status = Affiliate::STATUS_ACTIVE;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    /**
     * Handles the deactivation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);
        $model->affiliate_status = Affiliate::STATUS_DEACTIVATED;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    // /**
    //  * View a specific ad in frontend
    //  *
    //  * @param $id
    //  * @return \yii\web\Response
    //  */
    // public function actionAdminPreviewAd($id)
    // {
    //     $ad = Affiliate::findOne(['affiliate_id' => (int)$id]);
    //     if (empty($ad)) {
    //         notify()->addWarning(t('app', 'Please specify a valid ad'));
    //         return $this->redirect(['/admin/affiliates']);
    //     }

    //     app()->customer->switchIdentity($ad->customer);

    //     session()->set('impersonating_customer', true);
    //     if ($ad->status == Affiliate::STATUS_PENDING_APPROVAL) {
    //         session()->set('impersonating_customer_back_url', url(['/admin/affiliates/pendings']));
    //     }
    //     session()->set('impersonating_customer_back_url', url(['/admin/affiliates']));
    //     return $this->redirect(['/affiliate/preview/' . $ad->slug]);
    // }

    // /**
    //  * Deletes an existing Agent model.
    //  * If deletion is successful, the browser will be redirected to the 'index' page.
    //  * @param integer $id
    //  * @return mixed
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // public function actionDeleteAd($id)
    // {
    //     $ad = $this->findModel($id);
    //     $ad_image = AffiliateImage::deleteAll('affiliate_id = :affiliate_id', [':affiliate_id' => $id]);
    //     $ad_stat = AffiliateStat::deleteAll('affiliate_id = :affiliate_id', [':affiliate_id' => $id]);
    //     $ad_order = Order::find()->where(['affiliate_id'=>$id])->one();
    //     if (count($ad_order) > 0) {
    //         $order_tax = OrderTax::deleteAll('order_id = :order_id', [':order_id' => $ad_order->order_id]);
    //         $order_trans = OrderTransaction::deleteAll('order_id = :order_id', [':order_id' => $ad_order->order_id]);
    //     }

    //     $order = Order::deleteAll('affiliate_id = :affiliate_id', [':affiliate_id' => $id]);
        
    //     if ($ad->delete()) {
    //         notify()->addSuccess(t('app','Data is successfully deleted.'));
    //         return $this->redirect(['index']);
    //     }
    // }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // /**
    //  * @param $id
    //  * @return static
    //  * @throws NotFoundHttpException
    //  */
    // protected function findStat($id)
    // {
    //     if (($model = AffiliateStat::findOne($id)) !== null) {
    //         return $model;
    //     }
    //     return false;
    // }
}
