<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;


$this->title = t('app', 'Ads');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- <style>
.paginationd {
  display: inline-block;
}

.paginationd a {
  color: black;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
}
</style> -->

<div class="row">
    <div class="col-md-12">
        <div class="m-portlet m-portlet--success m-portlet--head-solid-bg">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= Html::encode($this->title); ?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools"></div>
            </div>
            <div class="m-portlet__body">
                <?php Pjax::begin(['enablePushState'=>true]); ?>
                <?= GridView::widget([
                    'id' => 'listings',
                    'tableOptions' => ['class' => 'table table-bordered table-hover table-striped'],
                    'options'          => ['class' => 'table-responsive grid-view'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'summary' => 'Showing <b>{begin}-{end}</b> of <b>{totalCount}</b> items',
                    // 'pager' => [
                    //     'options' => ['class'=>'paginationd'],
                    // ],
                    'columns' => [
                        'title',
                        [
                            'label' => t('app', 'Customer'),
                            'attribute' => 'customer_id',
                            'value' => function($model) {
                                $customer = $model->customer;
                                return $customer ? $customer->first_name.' '.$customer->last_name : t('app','No customer');
                            },
                        ],
                        [
                            'label' => t('app', 'Store'),
                            'attribute' => 'store',
                            'value' => function($model) {
                                $store = $model->store;
                                if($store && $store->status == \app\models\CustomerStore::STATUS_ACTIVE) {
                                    return $store ? $store->store_name : t('app', 'No store');
                                }
                                return t('app', 'No store');
                            },
                        ],
                        [
                            'label' => t('app', 'Category'),
                            'attribute' => 'category_id',
                            'value' => function($model) {
                                $category = $model->category;
                                return $category ? $category->name : t('app','No category');
                            },
                        ],
                        [
                            'label' => t('app', 'Currency'),
                            'attribute' => 'currency_id',
                            'value' => function($model) {
                                $currency= $model->currency;
                                return $currency ? $currency->name : t('app','No currency');
                            },
                        ],
                        [
                            'label' => t('app', 'Location'),
                            'attribute' => 'location_id',
                            'value' => function($model) {
                                $location= $model->location;
                                return $location ? $location->city : t('app','No location');
                            },
                        ],
                        [
                            'label' => t('app', 'Package'),
                            'attribute' => 'package_id',
                            'value' => function($model) {
                                $package= $model->package;
                                return $package ? $package->title : t('app','No package');
                            },
                        ],
                        'remaining_auto_renewal',
                        'status',
                        [
                            'attribute'=>'created_at',
                            'filter'=>  DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_at',
                                'options'=>[
                                    'class'=>'form-control',
                                ],
                                'dateFormat' => 'yyyy-MM-dd',
                            ])
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => [
                                'style'=>'width:100px',
                                'class'=>'table-actions'
                            ],
                            'template' => '{activate} {deactivate} <br> {adminPreviewAd} <br> {adStats} <br> {deleteAd}',
                            'buttons' => [
                                'activate' => function ($url, $model) {
                                    return ($model->status === \app\models\Listing::STATUS_PENDING_APPROVAL || $model->status === \app\models\Listing::STATUS_DEACTIVATED) ? Html::a(
                                        t('app', 'Activate'),
                                        $url,
                                        [
                                            'title' => t('app', 'Activate'),
                                            'data-pjax' => '0',
                                            'data-confirm' => t('app', 'Are you sure you want to activate this item?')
                                        ]
                                    ) : '';
                                },
                                'deactivate' => function ($url, $model) {
                                    return $model->status === \app\models\Listing::STATUS_ACTIVE ? Html::a(
                                        t('app', 'Deactivate'),
                                        $url,
                                        [
                                            'title' => t('app', 'Deactivate'),
                                            'data-pjax' => '0',
                                            'data-confirm' => t('app', 'Are you sure you want to deactivate this item?')
                                        ]
                                    ) : '';
                                },
                                'adminPreviewAd' => function ($url, $model) {
                                    return Html::a(
                                        t('app', 'Preview'),
                                        url(['/admin/listings/admin-preview-ad', 'id' => $model->listing_id]),
                                        [
                                            'title'     => 'Preview',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'adStats' => function ($url, $model) {
                                    return Html::a(
                                        t('app', 'Stats'),
                                        url(['/admin/listings/stats', 'id' => $model->listing_id]),
                                        [
                                            'title'     => 'Stats',
                                            'data-pjax' => '0',
                                        ]
                                    );
                                },
                                'deleteAd' => function ($url, $model) {
                                    return Html::a(
                                        t('app', 'Delete'),
                                        url(['/admin/listings/delete-ad', 'id' => $model->listing_id]),
                                        [
                                            'title'     => 'Delete',
                                            'data-pjax' => '0',
                                            'data-confirm' => t('app', 'Are you sure you want to delete this item?')
                                        ]
                                    );
                                },
                            ],
                        ],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
            </div>
            <div class="m-portlet__foot">
                <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'btn btn-xs btn-success']) ?>
            </div>
        </div>
    </div>
</div>