<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\modules\admin\controllers;

use Yii;

/**
 * Controls the actions for dashboard section
 *
 * @Class DashboardController
 * @package app\modules\admin\controllers
 */
class DashboardController extends \app\modules\admin\yii\web\Controller
{
    /**
     * Lists all widgets in dashboard
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = Yii::$app->db->createCommand("SELECT ea_country.name, ea_country.code, COUNT(t.listing_id) AS count_ads FROM ea_listing t LEFT JOIN ea_location l ON t.location_id = l.location_id LEFT JOIN ea_zone ON l.zone_id = ea_zone.zone_id LEFT JOIN ea_country ON l.country_id = ea_country.country_id WHERE t.status='active' GROUP BY ea_country.country_id ORDER BY count_ads DESC;")->queryAll();
        $model_user = Yii::$app->db->createCommand("SELECT ea_country.name, ea_country.code, COUNT(t.customer_id) AS count_user FROM ea_customer t LEFT JOIN ea_location l ON t.customer_id = l.customer_id LEFT JOIN ea_zone ON l.zone_id = ea_zone.zone_id LEFT JOIN ea_country ON l.country_id = ea_country.country_id WHERE t.status='active' AND l.customer_id IS NOT NULL GROUP BY ea_country.country_id ORDER BY count_user DESC;")->queryAll();
        
        return $this->render('index', [
            'model' => $model,
            'model_user' => $model_user,
        ]);
    }

}
