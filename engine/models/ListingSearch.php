<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\data\SqlDataProvider;

/**
 * Class ListingSearch
 * @package app\common\models
 */
class ListingSearch extends Listing
{
    public $searchPhrase;
    public $zoneId;
    public $minPrice;
    public $maxPrice;
    public $store;
    public $categorySlug;
    public $continent;
    public $searchParam;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'package_id', 'customer_id', 'location_id', 'category_id', 'currency_id', 'status', 'searchPhrase', 'zoneId', 'categorySlug', 'store', 'continent'], 'safe'],
            [['price', 'remaining_auto_renewal', 'minPrice', 'maxPrice', 'negotiable'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Listing::find()->alias('t');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => ['defaultOrder' => ['updated_at' => SORT_DESC]],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        if (strlen($this->customer_id) >= 1) {
            $query->joinWith(['customer' => function ($query) {
                $query->from(Customer::tableName() . ' c1');
            }], true, 'INNER JOIN')
                ->andWhere(['like', 'c1.first_name', $this->customer_id])
                ->orWhere(['like', 'c1.last_name', $this->customer_id]);
        }

        if (strlen($this->store) >= 1) {
            $query->joinWith(['store' => function ($query) {
                $query->from(CustomerStore::tableName() . ' c6');
            }], true, 'INNER JOIN')->andWhere(['like', 'c6.store_name', $this->store])->andWhere(['=', 'c6.status', CustomerStore::STATUS_ACTIVE]);
        }

        if (strlen($this->category_id) >= 1) {
            $query->joinWith(['category' => function ($query) {
                $query->from(Category::tableName() . ' c2');
            }], true, 'INNER JOIN')->andWhere(['like', 'c2.name', $this->category_id]);
        }

        if (strlen($this->currency_id) >= 1) {
            $query->joinWith(['currency' => function ($query) {
                $query->from(Currency::tableName() . ' c3');
            }], true, 'INNER JOIN')->andWhere(['like', 'c3.name', $this->currency_id]);
        }

        if (strlen($this->location_id) >= 1) {
            $query->joinWith(['location' => function ($query) {
                $query->from(Location::tableName() . ' c4');
            }], true, 'INNER JOIN')->andWhere(['like', 'c4.city', $this->location_id]);
        }

        if (strlen($this->package_id) >= 1) {
            $query->joinWith(['package' => function ($query) {
                $query->from(ListingPackage::tableName() . ' c5');
            }], true, 'INNER JOIN')->andWhere(['like', 'c5.title', $this->package_id]);
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 't.title', $this->title])
            ->andFilterWhere(['=', 't.price', $this->price])
            ->andFilterWhere(['=', 't.remaining_auto_renewal', $this->remaining_auto_renewal])
            ->andFilterWhere(['like', 't.created_at', $this->created_at]);

        return $dataProvider;
    }

    public function countrySearch($params)
    {
        $query = new Query;
        $query->select([
                '`c`.`name` AS `country_name`',
                '`c`.`country_id` AS `country_id`',
                'COUNT(`t`.`title`) AS `listing`',
                'SUM(`t`.`price`) AS `total_price`',
                ])  
            ->from('ea_listing t')
            ->join('JOIN', 'ea_location l', '`l`.`location_id` = `t`.`location_id`')
            ->join('JOIN', 'ea_country c', '`c`.`country_id` = `l`.`country_id`')
            ->groupBy('`c`.`name`')
            ->orderBy(new Expression(
                '`c`.`name` ASC'
        ));
        $command = $query->createCommand()->sql;
        // die(var_dump($command));

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 't.title', $this->title])
            ->andFilterWhere(['=', 't.price', $this->price])
            ->andFilterWhere(['=', 't.remaining_auto_renewal', $this->remaining_auto_renewal])
            ->andFilterWhere(['like', 't.created_at', $this->created_at]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function categorySearch($params)
    {
        /** @var \app\yii\web\Customer $customer */
        $customer = app()->customer;
        $id_country = $customer->getCountryId();

        // die(var_dump($params));
        $query = Listing::find()->alias('t')
            ->with(['mainImage', 'category', 'favorite'])
            // ->where(['>', 't.listing_expire_at', new Expression('NOW()')])
            ->where(['t.status' => 'active']);

        if ($id_country != NULL) {
            if (isset($params['ListingSearch']['continent'])) {
                if ($params['ListingSearch']['searchPhrase'] == "") {
                    $query->andWhere(['t.location_id' => $id_country]);
                }
            }
        }
        
        if (isset($params['search_param'])) {
            if ($params['search_param'] == 'country') {
                // $query->andWhere(['ea_country.name' => $params['x']]);
                $query->andFilterWhere(['or',
                    ['like','ea_country.name', $params['x']],
                    ['like','t.title', $params['x']]]);
            } elseif ($params['search_param'] == 'city') {
                // $query->andWhere(['l.city' => $params['x']]);
                $query->andFilterWhere(['or',
                    ['like','l.city', $params['x']],
                    ['like','t.title', $params['x']]]);
            } else {
                // $query->andWhere(['ea_zone.name' => $params['x']]);
                $query->andFilterWhere(['or',
                    ['like','ea_zone.name', $params['x']],
                    ['like','t.title', $params['x']]]);
            }
        }

        if (isset($params['ListingSearch'])) {
            if (isset($params['ListingSearch']['searchPhrase'])) {
                if ($params['ListingSearch']['searchPhrase'] != "") {
                    // $query->andWhere(['like', 'c.name', $params['ListingSearch']['searchPhrase']]);
                    $query->andFilterWhere(['or',
                        ['like', 't.title', $params['ListingSearch']['searchPhrase']],
                        ['like', 'c.name', $params['ListingSearch']['searchPhrase']]]);
                }
            }
            
            if (isset($params['ListingSearch']['continent'])) {
                if ($params['ListingSearch']['continent'] != "") {
                    // $query->andFilterWhere(['or',
                    //     ['ea_country.continent_code' => $params['ListingSearch']['continent']],
                    //     ['like','t.title', $params['ListingSearch']['continent']]]);
                    $query->andWhere(['ea_country.continent_code' => $params['ListingSearch']['continent']]);
                }
            }

            if (isset($params['ListingSearch']['zoneId'])) {
                if ($params['ListingSearch']['zoneId'] != "") {
                    $query->andWhere(['ea_zone.zone_id' => $params['ListingSearch']['zoneId']]);
                }
            }

            if (isset($params['ListingSearch']['currency_id'])) {
                if ($params['ListingSearch']['currency_id'] != "") {
                    $query->andWhere(['t.currency_id' => $params['ListingSearch']['currency_id']]);
                }
            }
        }
        
        if (isset($params['slug'])) {
            if ($params['slug'] != "") {
                $parent = Category::find()->where(['slug' => $params['slug']])->one();
                $category_id = $parent['category_id'];
                $children = \app\models\Category::getCategoriesByParentId($category_id);
                $category_ids = [];
                if (!empty($children)) {
                    foreach($children as $child) {
                        array_push($category_ids, $child->category_id);
                    }
                    $query->andWhere(['t.category_id' => $category_ids]);
                } else {
                    $query->andWhere(['t.category_id' => $category_id]);
                }
            }
        }

        $query->joinWith(['currency', 'location l', 'location.zone', 'location.country', 'category c']);

        $query->orderBy(new Expression(
            'CASE WHEN promo_expire_at > NOW() THEN 1 ELSE 2 END, ' .
            ($id_country ? 'FIELD(l.country_id, ' . $id_country .') DESC, ' : '') .
            'promo_expire_at DESC, updated_at DESC'
        ));
        // $command = $query->createCommand()->getRawSql();
        // die(var_dump($command));

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        // if (!$this->validate()) {
            return $dataProvider;
        // }

        // custom fields filters
        // if (isset($params['CategoryField'])) {
        //     $customFieldsFilters = $params['CategoryField'];
        //     $filterFields = [];
        //     foreach ($customFieldsFilters as $fieldId => $filters) {
        //         if (is_array($filters)) {
        //             foreach ($filters as $filter) {
        //                 if (!empty($filter)) {
        //                     $filterFields[$fieldId] = $filters;
        //                 }
        //             }
        //         } else {
        //             if (!empty($filters)) {
        //                 $filterFields[$fieldId] = $filters;
        //             }
        //         }
        //     }

        //     if (!empty($filterFields)) {
        //         foreach ($filterFields as $fieldId => $filters) {
        //             if (is_array($filters)) {
        //                 $alias = 'cf' . $fieldId;
        //                 $query->joinWith(['categoryFieldValues ' . $alias], true, 'INNER JOIN')
        //                     ->andWhere([$alias . '.field_id' => $fieldId]);

        //                 if (!empty($filters['min'])) {
        //                     $query->andWhere(['>=', $alias . '.value', $filters['min']]);
        //                 }
        //                 if (!empty($filters['max'])) {
        //                     $query->andWhere(['<=', $alias . '.value', $filters['max']]);
        //                 }
        //             } else {
        //                 $alias = 'cf' . $fieldId;
        //                 $query->joinWith(['categoryFieldValues ' . $alias], true, 'INNER JOIN')
        //                     ->andWhere([$alias . '.field_id' => $fieldId])
        //                     ->andWhere([$alias . '.value' => $filters]);
        //             }
        //         }
        //     }
        // }

        // if ($this->zoneId) {
        //     $query->andFilterWhere(['=', '{{%zone}}.zone_id', $this->zoneId]);
        // }

        // if (trim($this->searchPhrase) != '') {

        //     $query->andWhere('(t.title LIKE :searchPhrase OR t.description LIKE :searchPhrase)', [
        //         ':searchPhrase' => '%'.$this->searchPhrase.'%'
        //     ]);
        // }

        // // grid filtering conditions
        // $query->andFilterWhere(['=', 't.remaining_auto_renewal', $this->remaining_auto_renewal])
        //     ->andFilterWhere(['=', 't.negotiable', $this->negotiable])
        //     ->andFilterWhere(['=', '{{%currency}}.currency_id', $this->currency_id])
        //     ->andFilterWhere(['>=', 't.price', $this->minPrice])
        //     ->andFilterWhere(['<=', 't.price', $this->maxPrice]);

        // return $dataProvider;
    }
}
