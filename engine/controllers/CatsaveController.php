<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use app\fieldbuilder\Type;
use app\models\Listing;
use app\models\ListingSearch;
use app\models\Category;
use app\models\CategoryField;
use app\models\Country;
use app\models\Categoryb2b;
use app\models\Zone;
use twisted1919\helpers\Icon;
use yii\filters\VerbFilter;
use app\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\web\Response;

/**
 * Class CategoryController
 * @package app\controllers
 */
class CatsaveController extends Controller
{
    /**
     * Constants
     */
    const ADS_PER_PAGE = 20;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        //$model = new Categoryb2b();
        echo "coming here"; exit;
    }
}