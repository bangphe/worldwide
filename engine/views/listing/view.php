<?php
use app\assets\AdAsset;
use app\helpers\SvgHelper;
use app\models\ListingStat;
use app\models\Category;
use app\models\Customer;
use app\components\AdsListWidget;
use yii\helpers\Url;

AdAsset::register($this);

$showGalleryArrows = (count($images) > 1) ? true : false;
$fullWidthGallery = (count($images) < 4) ? 'full-width' : '' ;
?>
<div class="view-listing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="breadcrumb" style="margin-top: 5px; margin-bottom: 5px;">
                        <li><a href="<?=url(['/']);?>"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                        <?php $_categoryParents = Category::getAllParents($ad->category_id);
                        $categoryParents = array_reverse($_categoryParents);
                        foreach ($categoryParents as $categoryParent) { ?>
                            <li>
                                <a href="<?= url(['category/index', 'slug' => $categoryParent['slug']]); ?>">
                                    <?= html_encode($categoryParent['name']); ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="row icik">
                <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                    <div class="listing-gallery">
                        <div class="img-wrapper <?=$fullWidthGallery;?>">
                            <span class="zoom"><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                            <div class="small-gallery owl-carousel carousel-responsive owl-theme">
                                <?php foreach ($images as $image) { ?>
                                    <div class="item open-full-gallery">
                                        <img class="resizeImg" class="margin:0 auto;" src="<?= \Yii::getAlias('@web'.$image->image_path); ?>" alt=""/>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php if ($showGalleryArrows) { ?>
                                <a href="javascript:;" class="arrow-gallery gallery-left"><?= SvgHelper::getByName('arrow-left');?></a>
                                <a href="javascript:;" class="arrow-gallery gallery-right"><?= SvgHelper::getByName('arrow-right');?></a>
                            <?php } ?>
                        </div>
                        <div class="thb-wrapper">
                            <span class="zoom"><i class="fa fa-search-plus" aria-hidden="true"></i></span>
                            <ul>
                                <?php $counterImage = 0;
                                foreach ($images as $image) {
                                    $counterImage++;
                                    if ($counterImage == 1) continue; ?>
                                    <li><img class="" src="<?=$image->image_path;?>" alt=""/></li>
                                    <?php if ($counterImage == 4) break;
                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="listing-info" style="max-height: 350px;">
                        <div class="listing-user" style="border-bottom: 0px;">
                            <div class="name">
                                <h1><?=html_encode(strtoupper($ad->title));?></h1>
                            </div>
                            <div class="price" style="font-weight: 600; color: #009688; padding-left: 0px;">
                                <?php if(!$ad->isExpired) { ?>
                                    <h2><?=html_encode($ad->getPriceAsCurrency($ad->currency->code));?></h2>
                                    <span><?=$ad->isNegotiable() ? t('app','Negotiable') : '';?></span>
                                <?php } else { ?>
                                    <h2><?=strtoupper(\app\models\Listing::STATUS_EXPIRED);?></h2>
                                <?php } ?>
                            </div>
                            <div class="new-email" style="padding-left: 0px; font-size: 20px;">
                                <?php if (!$ad->hide_email) { ?>
                                <a href="#" data-customer-id="<?=(int)$ad->customer_id;?>" class="link send-message track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::SHOW_MAIL;?>" data-toggle="modal" data-target="#modalEmail"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?=t('app','Send a message');?></a>
                                <?php } ?>
                                <?php if (app()->customer->isGuest == false) { ?>
                                <!-- <a href="javascript:;" id="startChat" data-listing-id="<?=(int)$ad->listing_id;?>" data-customer="<?=$ad->customer->first_name.' '.$ad->customer->last_name;?>" data-customer-id="<?=(int)$ad->customer_id?>" data-user-id="<?=(int)app()->customer->id?>"><span class="glyphicon glyphicon-comment"></span> Open in chat </a> -->
                                <?php } ?>
                            </div>
                            <div class="social-desktop" style="padding-left: 0px;">
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=url(['/listing/index/', 'slug' => $ad->slug], true);?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::FACEBOOK_SHARES;?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a target="_blank" href="https://twitter.com/intent/tweet?text=<?=html_encode($ad->title);?>&url=<?=url(['/listing/index/', 'slug' => $ad->slug], true);?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::TWITTER_SHARES;?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#" class="social-link track-stats" data-url="<?=url(['/listing/send-email']);?>" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::MAIL_SHARES;?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                <?php if($ad->customer->phone != NULL) { ?>
                                <a target="_blank" href="https://api.whatsapp.com/send?text=<?=html_encode($ad->title);?>&url=<?=url(['/listing/index/', 'slug' => $ad->slug], true);?>&phone=<?=$ad->customer->phone;?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="btn-as hidden-md hidden-sm hidden-xs favorite-listing <?=(!$ad->isFavorite) ? 'track-stats' : '';?>" data-stats-type="<?=ListingStat::FAVORITE;?>" data-listing-id="<?=(int)$ad->listing_id;?>" data-add-msg="<?=t('app','Add to favorites');?>" data-remove-msg="<?=t('app','Remove Favorite');?>" data-favorite-url="<?=url(['/listing/toggle-favorite']);?>">
                        <?php if ($ad->isFavorite) { ?>
                            <i class="fa fa-heart" aria-hidden="true"></i> <span><?=t('app','Remove Favorite');?></span>
                        <?php } else { ?>
                            <i class="fa fa-heart-o" aria-hidden="true"></i> <span><?=t('app','Add to favorites');?></span>
                        <?php } ?>
                    </a>
                    <div class="listing-date"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?=$ad->getUpdatedDateAsDateTimeFull();?></div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <div class="listing-info">
                        <div class="listing-user">
                            <div class="name">
                                <?php if (empty($ad->customer->stores)) {
                                    echo html_encode($ad->customer->getFullName());
                                } else { ?>
                                <a href="<?=url(['/store/index', 'slug' => $ad->customer->stores->slug]);?>">
                                    <?=html_encode($ad->customer->stores->store_name);?>
                                </a>
                                <?php } ?>
                            </div>
                            <?php if(!$ad->isExpired) { ?>
                                <?php if (!empty($ad->customer->stores)) { ?>
                                <div class="store">
                                    <a href="<?=url(['/store/index', 'slug' => $ad->customer->stores->slug]);?>">
                                        <?=t('app','Visit Store');?>
                                    </a>
                                </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="type"><?=t('app', ucfirst($ad->type));?> </div>
                            <div class="location"><?=html_encode($ad->location->getAddress());?> </div>
                        </div>
                        <?php 
                            $nearbyusers = Customer::nearByUsers($ad->location->latitude, $ad->location->longitude); 
                            $locationarray = [];
                            
                            foreach ($nearbyusers as $key => $resultvalue) {
                                if ($key == 0) {
                                    $locationarray[$key] = [$resultvalue['address_street'], $resultvalue['latitude'], $resultvalue['longitude'], 'main', $key];
                                } else {
                                    $locationarray[$key] = [$resultvalue['title'] . ' - ' . $resultvalue['address_street'], $resultvalue['latitude'], $resultvalue['longitude'], 'side', $key];
                                }
                            }
                        ?>
                        <div id="map"></div>
                        <script>
                            var map;
                            function initMap() {

                                var iconBase = '<?php echo Url::base(true) . "/uploads/images/site/"; ?>';
                                var icons = {
                                      main: {
                                        icon: iconBase + 'main-map-icon.png'
                                      },
                                      side: {
                                        icon: iconBase + 'other-map-icon.png'
                                      }
                                };
                                var locations = <?php echo json_encode($locationarray); ?>;
                                console.log(locations);
                                map = new google.maps.Map(document.getElementById('map'), {
                                    center: {lat: <?=(float)$ad->location->latitude;?>, lng: <?=(float)$ad->location->longitude;?>},
                                    zoom: 11,
                                    mapTypeControl: false,
                                    zoomControl: true,
                                    scaleControl: true,
                                    streetViewControl: false
                                });

                                var infowindow = new google.maps.InfoWindow();

                                var marker, i;

                                for (i = 0; i < locations.length; i++) {  
                                    marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                        icon: icons[locations[i][3]].icon,
                                        map: map
                                    });

                                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                        return function() {
                                            infowindow.setContent(locations[i][0]);
                                            infowindow.open(map, marker);
                                        }
                                    })(marker, i));
                                }
                            }
                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=<?=html_encode(options()->get('app.settings.common.siteGoogleMapsKey', ''));?>&callback=initMap" async defer></script>
                    </div>
                </div>
            </div>
            <div class="big-gallery">
                <a href="javascript:;" class="x-close"><i class="fa fa-times" aria-hidden="true"></i></a>
                <div class="big-gallery-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-lg-push-1 col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                <div class="listing-heading-gallery">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <h1><?=html_encode(strtoupper($ad->title));?></h1>
                                            <?php if(!$ad->isExpired) { ?>
                                                <a href="#" class="link add-to-favorites favorite-listing <?=(!$ad->isFavorite) ? 'track-stats' : '';?>" data-stats-type="<?=ListingStat::FAVORITE;?>" data-add-msg="<?=t('app','Add to favorites');?>" data-remove-msg="<?=t('app','Remove Favorite');?>" data-favorite-url="<?=url(['/listing/toggle-favorite']);?>" data-listing-id="<?=(int)$ad->listing_id;?>">
                                                    <?php if ($ad->isFavorite) { ?>
                                                        <i class="fa fa-heart" aria-hidden="true"></i> <span><?=t('app','Remove Favorite');?></span>
                                                    <?php } else { ?>
                                                        <i class="fa fa-heart-o" aria-hidden="true"></i> <span><?=t('app','Add to favorites');?></span>
                                                    <?php } ?>
                                                </a>
                                                <?php if (!$ad->hide_email) { ?>
                                                    <a href="#" id="listing-send-msg" data-url="<?=url(['/listing/get-customer-contact']);?>" data-customer-id="<?=(int)$ad->customer->customer_id;?>" class="link send-message track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::SHOW_MAIL;?>"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?=t('app','Send a message');?></a>
                                                <?php } ?>
                                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=url(['/listing/index/', 'slug' => $ad->slug], true);?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::FACEBOOK_SHARES;?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a target="_blank" href="https://twitter.com/intent/tweet?text=<?=html_encode($ad->title);?>&url=<?=url(['/listing/index/', 'slug' => $ad->slug], true);?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::TWITTER_SHARES;?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                <a href="mailto:?subject=<?=html_encode($ad->title);?>&body=<?=t('app', 'Read More:');?><?=url(['/listing/index/', 'slug' => $ad->slug], true);?>" class="social-link track-stats" data-listing-id="<?=(int)$ad->listing_id;?>" data-stats-type="<?=ListingStat::MAIL_SHARES;?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                            <?php } ?>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="price">
                                                <?php if(!$ad->isExpired) { ?>
                                                    <h2><?=html_encode($ad->getPriceAsCurrency($ad->currency->code));?></h2>
                                                    <?=$ad->isNegotiable() ? t('app','Negotiable') : '';?>
                                                <?php } else { ?>
                                                    <h2><?=strtoupper(\app\models\Listing::STATUS_EXPIRED);?></h2>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10 col-lg-push-1 col-md-10 col-md-push-1 col-sm-12 col-xs-12">
                                <div class="full-gallery-wrapper">
                                    <div class="full-gallery owl-carousel owl-theme">
                                        <?php foreach ($images as $image) { ?>
                                            <div class="container-fluid no-padding item">
                                                <img class="resizeImg" class="margin:0 auto;" src="<?=$image->image_path;?>" alt=""/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if ($showGalleryArrows) { ?>
                                        <a href="javascript:;" class="arrow-gallery gallery-left-big"><?= SvgHelper::getByName('arrow-left');?></a>
                                        <a href="javascript:;" class="arrow-gallery gallery-right-big"><?= SvgHelper::getByName('arrow-right');?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if (!empty($ad->categoryFieldValues)) {
                $labelValueFields = [];
                foreach ($ad->categoryFieldValues as $field) {
                    if ($field->field->type->name != 'checkbox' && !empty($field->value)) {
                        $labelValueFields[] = $field;
                    }
                }
                if ($labelValueFields) { ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text no-margin-top"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="listing-custom labels">
                                <?php foreach ($labelValueFields as $field) { ?>
                                    <div class="item labeled">
                                    <span>
                                        <?= html_encode($field->field->label); ?>
                                    </span>
                                        <span>
                                        <?= html_encode($field->value); ?> <?= ($field->field->unit) ? html_encode($field->field->unit) : ''; ?>
                                    </span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php
                }
            }
            ?>

            <?php if (!empty($ad->categoryFieldValues)) {
                $checkboxFields = [];
                foreach ($ad->categoryFieldValues as $field) {
                    if ($field->field->type->name == 'checkbox' && $field->value != 0) {
                        $checkboxFields[] = $field;
                    }
                }
                if ($checkboxFields) { ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="listing-custom">
                                <?php foreach ($checkboxFields as $field) { ?>
                                    <div class="item"><?= html_encode($field->field->label); ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

            <?php 
                if($ad->category_id == '36' || $ad->category_id == '37' || $ad->category_id == '38' || $ad->category_id == '39' || $ad->category_id == '40'|| $ad->category_id == '41'){
                    if($ad->category_id == '36'){
                        $business_types = 'Type of business';
                        $offer = 'Proposal B2B';
                        $terms = 'Terms of business';
                    }
                    if($ad->category_id == '37'){
                        $business_types = 'TYPE OF BUSINESS';
                        $offer = 'BUSINESS VALUATION';
                        $terms = 'TERMS OF SALE';
                    }
                    if($ad->category_id == '38'){
                        $business_types = 'Type of business';
                        $offer = 'Offer';
                        $terms = 'Terms';
                    }
                    if($ad->category_id == '39'){
                        $business_types = 'Type of business';
                        $offer = 'pitch';
                        $terms = 'Terms';
                        $share = 'Share';
                    }
                    if($ad->category_id == '40'){
                        $terms = 'Terms';
                        $share = 'Share';
                    }
                    if($ad->category_id == '41'){
                        $terms = 'Terms';
                        $share = 'Share';
                    }



                    if(isset($ad->business_types)){
                        if(isset($business_types)){
                    ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=$business_types?></span>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <p><?=html_purify($ad->business_types);?></p>
                        </div>
                    </div>
                    <?php
                        }
                    }if(isset($ad->share)){
                        if(isset($share)){
                    ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=$share?></span>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <p><?= $ad->share != NULL ? html_purify($ad->$share) : '';?></p>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    if(isset($ad->offer)){
                        if(isset($offer)){
                    ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=$offer?></span>
                            </div>
                          </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <p><?=html_purify($ad->offer);?></p>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    if(isset($ad->terms)){
                        if($terms){
                    ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=$terms?></span>
                            </div>
                          </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <p><?=html_purify($ad->terms);?></p>
                        </div>
                    </div>
                    <?php
                        }
                    }
                }
            ?>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="separator-text">
                        <span><?= t('app', 'Description');?></span>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <p><?=html_purify($ad->description);?></p>
                </div>
            </div>
        </div>

    <?= AdsListWidget::widget([
        'listType'  => AdsListWidget::LIST_TYPE_PROMOTED,
        'title'     => t('app', 'Promoted ads'),
        'ad'        => $ad,
        'quantity'  => 4
    ]);
    ?>

    <?= AdsListWidget::widget([
        'listType'  => AdsListWidget::LIST_TYPE_RELATED,
        'title'     => t('app', 'Related ads'),
        'ad'        => $ad,
        'quantity'  => 4
    ]);
    ?>
</div>


<div id="box-chat" class="popup-box chat-popup">
    <div class="popup-head">
        <div class="popup-head-left pull-left">
            <img src="http://bootsnipp.com/img/avatars/bcf1c0d13e5500875fdd5a7e8ad9752ee16e7462.jpg" alt="iamgurdeeposahan"> <?//= $ad->customer->first_name.' '.$ad->customer->last_name; ?><span id="customer"></span>
        </div>
        <div class="popup-head-right pull-right">
            <span data-widget="remove" id="endChat" class="pull-right" style="cursor: pointer;"><i class="fa fa-times-circle"></i></span>
        </div>
    </div>
        
    <div class="popup-messages">
        <div class="direct-chat-messages">
            <!-- <div class="chat-box-single-line">
                <abbr class="timestamp">October 8th, 2015</abbr>
            </div> -->
            <div id="chat-details"></div>
            <!--<div class="direct-chat-msg doted-border">
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-left"><?//= app()->customer->identity->first_name; ?></span>
                </div>
                <div class="direct-chat-text">Hey bro, how’s everything going ?</div>
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-timestamp pull-right">3.36 PM</span>
                </div>
            </div>
            <div class="direct-chat-msg doted-border">
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-reply-name pull-right">Singh</span>
                </div>
                <div class="direct-chat-text">Hey bro, how’s everything going ?</div>
                <div class="direct-chat-info clearfix">
                    <span class="direct-chat-timestamp pull-right">3.36 PM</span>
                </div>
            </div> -->
        </div>
    </div>
            
    <div class="popup-messages-footer">
        <div class="input-group">
            <input id="chat-message" type="text" class="form-control input-sm" placeholder="Type your message here..." data-to-user-id="<?=(int)$ad->customer_id;?>" data-from-user-id="<?=(int)app()->customer->identity->id;?>" data-listing-id="<?=(int)$ad->listing_id;?>">
            <span class="input-group-btn">
                <button class="btn btn-warning btn-sm" id="btn-chat" data-to-user-id="<?=(int)$ad->customer_id;?>" data-from-user-id="<?=(int)app()->customer->identity->id;?>" data-listing-id="<?=(int)$ad->listing_id;?>" style="margin-right: -1px;">Send</button>
            </span>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-notice" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title"><?=t('app', 'Send a Message');?></h1>
                <a href="javascript:;" class="x-close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            <div class="modal-body">
                <input type="text" name="message" id="message-email">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <button type="button" class="btn-as delete-favorite" id="sendEmail" data-customer-id="<?=(int)$ad->customer_id;?>" data-sender="<?=app()->customer->identity->id;?>">Send</button>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <button type="button" class="btn-as black pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>