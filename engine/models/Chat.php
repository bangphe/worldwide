<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ea_chat".
 *
 * @property integer $chat_id
 * @property integer $to_customer_id
 * @property integer $from_customer_id
 * @property string $message
 * @property string $status
 * @property string $timestamp
 * @property string $created_at
 *
 * @property Listing $listing
 * @property Customer $toCustomer
 * @property Customer $fromCustomer
 */
class Chat extends \yii\db\ActiveRecord
{
    // when inactive model
    const STATUS_UNREAD = 'unread';

    // when active model
    const STATUS_READ = 'read';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ea_chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_customer_id', 'from_customer_id'], 'integer'],
            [['message'], 'string'],
            [['status'], 'string', 'max' => 10],
            [['timestamp', 'created_at'], 'safe'],
            [['listing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Listing::className(), 'targetAttribute' => ['listing_id' => 'listing_id']],
            [['to_customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['to_customer_id' => 'customer_id']],
            [['from_customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['from_customer_id' => 'customer_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chat_id' => 'Chat ID',
            'to_customer_id' => 'To Customer ID',
            'from_customer_id' => 'From Customer ID',
            'message' => 'Message',
            'status' => 'Status',
            'timestamp' => 'Timestamp',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getListing()
    {
        return $this->hasOne(Listing::className(), ['listing_id' => 'listing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'to_customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromCustomer()
    {
        return $this->hasOne(Customer::className(), ['customer_id' => 'from_customer_id']);
    }
}
