<?php

use \app\modules\admin\components\InfoBoxWidget;
use \app\modules\admin\components\TableBoxWidget;
use \app\modules\admin\components\LatestAdsWidget;
use dosamigos\highcharts\HighCharts;
use yii\helpers\Url;
use app\models\Listing;

$this->title = t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;

?>
<link href="<?= Url::base(); ?>/assets/site/css/categories.css" rel="stylesheet" type="text/css" />

<div class="row">
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
        'boxType'   => InfoBoxWidget::BOX_LIFETIME_SALES,
        'title'     => t('app', 'Lifetime Sales') . ' - ' . options()->get('app.settings.common.siteCurrency', 'usd'),
        'icon'      => 'fa-money-bill'
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
            'boxType'   => InfoBoxWidget::BOX_PAID_ADS,
            'title'     => t('app', 'Paid Ads'),
            'icon'      => 'fa-chart-bar'
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
            'boxType'   => InfoBoxWidget::BOX_FREE_ADS,
            'title'     => t('app', 'Free Ads'),
            'icon'      => 'fa-check'
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
            'boxType'   => InfoBoxWidget::BOX_ORDERS,
            'title'     => t('app', 'Orders'),
            'icon'      => 'fa-shopping-cart'
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
            'boxType'   => InfoBoxWidget::BOX_CUSTOMERS,
            'title'     => t('app', 'Customers'),
            'icon'      => 'fa-users'
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= InfoBoxWidget::widget([
            'boxType'   => InfoBoxWidget::BOX_FAVORITES,
            'title'     => t('app', 'Global Favorite Ads'),
            'icon'      => 'fa-heart'
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-multimedia"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= t('app','Business Chart'); ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <?php $sales = \app\models\Order::getLastMonthsSalesAsArray();?>
                <?= HighCharts::widget([
                    'clientOptions' => [
                        'title' => [
                            'text' => t('app', 'Revenue in ') . options()->get('app.settings.common.siteCurrency', 'usd')
                        ],
                        'xAxis' => [
                            'categories' => $sales['months']
                        ],
                        'yAxis' => [
                            'title' => [
                                'text' => t('app', 'amount')
                            ]
                        ],
                        'series' => [
                            ['name' => options()->get('app.settings.common.siteCurrency', 'usd'), 'data' => $sales['sales']],
                        ]
                    ]
                ]);
                ?>
            </div>
            <div class="m-portlet__foot m--hide">
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <?= LatestAdsWidget::widget([
            'title'     => t('app', 'Recently Added Ads'),
            'allLink'      => url(['/admin/listings'])
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <?= TableBoxWidget::widget([
            'tableType'     => TableBoxWidget::TABLE_ORDERS,
            'title'         => t('app', 'Last Orders'),
            'allLink'       => url(['/admin/orders']),
            'columns'       => [
                    'order_id'          => t('app','ID'),
                    'order_title'       => t('app','Item'),
                    'first_name'        => t('app','First Name'),
                    'last_name'         => t('app','Last Name'),
                    'city'              => t('app','City'),
                    'status'            => t('app','Status'),
                    'total'             => t('app','Total'),
                    'created_at'        => t('app', 'Created'),
            ]
        ]); ?>
    </div>
    <div class="col-md-6">
        <?= TableBoxWidget::widget([
            'tableType'     => TableBoxWidget::TABLE_CUSTOMERS,
            'title'         => t('app', 'Last Customers'),
            'allLink'       => url(['/admin/customers']),
            'columns'       => [
                'customer_id'       => t('app','ID'),
                'first_name'        => t('app','First Name'),
                'last_name'         => t('app','Last Name'),
                'gender'            => t('app','Gender'),
                'source'            => t('app','Source'),
                'status'            => t('app','Status'),
                'created_at'        => t('app', 'Created'),
            ]
        ]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-multimedia"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= t('app','All Ads by Categories'); ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="row">
                    <?php foreach (\app\models\Category::getCategoriesByParentId() as $item) { ?>
                    <div class="col-xs-6 col-md-4 col-lg-3 mb-4">
                        <div class="test-item category-item">
                            <a href="<?= url(['ads/' . $item->slug]); ?>" class="d-block overflow-hidden rounded">
                                <img alt="Agriculture" src="https://testversions.quantiklab.com/classify/public/uploads/categories/1608657722-home.jpg" class="img-fit category-img">
                                <span class="category-overlay-layer">
                                    <span class="d-block center w-100 p-3">
                                        <span class="box-70 mx-auto mb-2"><i class="<?= 'fa ' . $item->icon . ' fa-3x category-icon'; ?>"></i></span>
                                        <span class="d-block text-center category-title"><?= strtoupper($item->name); ?></span>
                                        <span class="d-block text-center category-title"><?= Listing::countAdsPerCategory($item->category_id); ?></span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="m-portlet__foot m--hide">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-multimedia"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= t('app','All Ads by Countries'); ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-striped m-table m-table--head-separator-danger">
                            <thead>
                                <tr>
                                    <td width="5%">#</td>
                                    <td width="40%">Country</td>
                                    <td>Total Ads</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($model != NULL) { ?>
                                <?php $i=1; foreach($model as $m) {?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><span class="<?= "flag flag-".strtolower($m['code']); ?>"></span> <?= $m['name'];?></td>
                                    <td><?= $m['count_ads']; ?></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m--hide">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-multimedia"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= t('app','All Users by Countries'); ?>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-section">
                    <div class="m-section__content">
                        <table class="table table-striped m-table m-table--head-separator-danger">
                            <thead>
                                <tr>
                                    <td width="5%">#</td>
                                    <td width="40%">Country</td>
                                    <td>Total User</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($model != NULL) { ?>
                                <?php $i=1; foreach($model_user as $m) {?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><span class="<?= "flag flag-".strtolower($m['code']); ?>"></span> <?= $m['name'];?></td>
                                    <td><?= $m['count_user']; ?></td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m--hide">
            </div>
        </div>
    </div>
</div>