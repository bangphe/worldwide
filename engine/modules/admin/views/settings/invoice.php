<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->title = t('app', 'Invoice Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12 listings-packages-<?= $action; ?>">
        <div class="m-portlet m-portlet--head">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= Html::encode($this->title); ?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools"></div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="m-portlet__body">
                <div class="email-settings-form">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'prefix')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Prefix to be used in all PDF invoices'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-8">
                            <?= $form->field($model, 'notes')->textArea([
                                'data-content'      => t('app', 'Notes to be displayed at the end of PDF invoice'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php $logo = options()->get('app.settings.invoice.logo', \Yii::getAlias('@web/assets/site/img/logo.png'));?>
                            <?= $form->field($model, 'logoUpload')->widget(\kartik\file\FileInput::classname(), [
                                'options' => [
                                    'class'=>'image-upload',
                                    'data-image'=>$logo,
                                ],
                                'pluginOptions' => [
                                    'language' => options()->get('app.settings.common.siteLanguage', 'en'),
                                    'overwriteInitial'=> true,
                                    'showClose'=> false,
                                    'showRemove' => false,
                                    'showCaption'=> false,
                                    'showBrowse'=> true,
                                    'browseOnZoneClick'=> true,
                                    'removeLabel'=> '',
                                    'removeIcon'=> '<i class="glyphicon glyphicon-remove"></i>',
                                    'removeTitle'=> 'Cancel or reset changes',
                                    'elErrorContainer'=> '.image-upload-error',
                                    'msgErrorClass'=> 'alert alert-block alert-danger',
                                    'defaultPreviewContent'=> '<img src="'.$logo.'" alt="Your logo" style="width:400px">',
                                    'layoutTemplates'=> ['main2'=> '{preview} {remove} {browse}'],
                                    'allowedFileTypes' => ["image"],
                                ]
                            ])->label();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot">
                <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                <?= Html::a(t('app', 'Cancel'), ['index'], ['class' => 'btn btn-xs btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
