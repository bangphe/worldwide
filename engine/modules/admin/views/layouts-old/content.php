<?php

use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

?>
<div class="content-wrapper">
    <?= notify()->show();?>
    <section class="content-header">
        <h1>&nbsp;</h1>
        <?=
        Breadcrumbs::widget(
            [
                'homeLink' => ['label' => t('app', 'Home'),
                               'url' => app()->params['adminUrl']],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>
    <section class="content <?= app()->controller->id . '-' . app()->controller->action->id;?>">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>