<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\components;

use app\models\Advertisement;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\db\Expression;

class AdvertisementsListWidget extends Widget
{
    /**
     * @var int number of items to retrieve
     */
    public $quantity = 12;

    /**
     * @var
     */
    public $col = 1;

    public $title = 'Advertisements';


    /**
     * @return string
     */
    public function run()
    {
        /** @var \app\yii\web\Customer $customer */
        $customer = app()->customer;
        $id_country = $customer->getCountryId();

        $query = Advertisement::find()->alias('a')
            ->where(['a.status' => Advertisement::STATUS_ACTIVE])
            ->andWhere(['<=', 'a.start_date', new Expression('NOW()')])
            ->andWhere(['>=', 'a.end_date', new Expression('NOW()')])
            ->limit($this->quantity);

        $ads = $query->all();
        $count = count($ads);
        // echo "<pre>"; print_r($query);
        //echo "<pre>"; print_r($query->createCommand()->sql); exit;
        if (!empty($ads)) {
            return $this->render('ads-list/advertisement-list', [
                'ads' => $ads,
                'col' => $this->col == $count ? $this->col : $count,
            ]);
        }

        return false;
    }
}
