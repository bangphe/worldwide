<?php
use yii\helpers\Url;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Email Registrasi Akun</title>

    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->
    
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet" type="text/css">
    
    <!-- CSS Reset -->
    <style>

        /**
         * SimpleApp
         * http://notification-emails.com/
         * Last Modified: 10/05/2016
        **/
        @font-face {
          font-family: 'Noto Sans';
          font-style: normal;
          font-weight: 400;
          src: local('Noto Sans'), local('NotoSans'), url(http://fonts.gstatic.com/s/notosans/v6/C7bP6N8yXZ-PGLzbFLtQKRJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
          unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
        }

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
                
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto; 
        }
        
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        
        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }
      
    </style>
    
    <!-- Progressive Enhancements -->
    <style>
        
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #e05858 !important;
            border-color: #e05858 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 480px) {

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid,
            .fluid-centered {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }
            /* And center justify these ones. */
            .fluid-centered {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }
        
            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }
            .logo {
                width:80%
            }
                
        }

    </style>

</head>
<body width="100%" bgcolor="#fafafa" style="margin: 0;">

    <center style="width: 100%; background: #fafafa;">
        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            Information about your Account on Worldwideadverts using <?= ucwords(strtolower($model->customer->first_name)); ?>.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <div style="max-width: 680px; margin: auto;">
            <!--[if mso]>
            <table cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Header : BEGIN -->
            <!-- <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" role="presentation">
                <tr>
                    <td style="padding: 20px 0; text-align: center"></td>
                </tr>
            </table> -->
            <!-- Email Header : END -->
            
            <!-- Email Body : BEGIN -->
            <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" role="presentation">
                
                <!-- Hero Image, Flush : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff" style="border-top-left-radius:3px;border-top-right-radius:3px;padding:10px 40px; color:#000; border-bottom: 1px solid #bdbaba;">
                        <img src="https://worldwideadverts.info/uploads/images/site/336e62100000.png" alt="worldwideadverts" width="35%" border="0" align="left" style="border-top-left-radius:3px;border-top-right-radius:3px;color:#fff" class="logo">
                    </td>
                </tr>
                <!-- Hero Image, Flush : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 20px 40px; font-family: 'Noto Sans', arial, sans-serif; font-size: 13px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    <h3 style="text-align: center;">Congratulations, <?= ucwords(strtolower($model->customer->first_name)); ?>. Your Ad is Active.</h3>
                                     Thank you for posting your ad <b><?= $model->title; ?></b> on Worldwideadverts.<br /><br />Your Ad is active now in our system, and will be visible in no time.<br>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 40px; font-family: 'Noto Sans', arial, sans-serif; font-size: 13px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    This is your ad information:<br>
                                    Title          : <i><?= $model->title; ?></i><br>
                                    Description    : <i><?= $model->description; ?></i><br>
                                    Category       : <i><?= $model->category->name; ?></i>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px 40px; font-family: 'Noto Sans', arial, sans-serif; font-size: 13px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    Click the button below to manage your Ad.
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px 40px; font-family: 'Noto Sans', arial, sans-serif; font-size: 13px; mso-height-rule: exactly; line-height: 20px; color: #555555;">
                                    <!-- Button : Begin -->
                                    <table cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                                        <tr>
                                            <td style="border-radius: 4px; background: #f36b6b; text-align: center;" class="button-td">
                                                <a href="https://worldwideadverts.info/account/my-listings" style="background: #f36b6b; border: 15px solid #f36b6b; font-family: 'Noto Sans', arial, sans-serif; font-size: 14px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 4px; font-weight: bold;" class="button-a">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <span style="color:#ffffff">Login</span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- Button : END -->
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td height="" align="center" bgcolor="#ffffff" style="border-bottom: 1px solid #e1e1e1;"></td>
                </tr>
                <tr><td height="35" bgcolor="#ffffff"></td></tr>
                <tr>
                    <td align="center" bgcolor="#ffffff">
                        <font face="Open Sans, sans-serif" style="font-size:14px;" color="#000000">
                            <a href="https://worldwideadverts.info" style="color: #009688; text-decoration: none;"><?= html_encode(options()->get('app.settings.common.siteName', app_param('app.name')));?></a>
                        </font>
                        <br />
                        <font face="Open Sans, sans-serif" style="font-size:11px;" color="#000000">
                            &copy; <?= html_encode(options()->get('app.settings.common.siteName', 'EasyAds'));?> <?= date('Y');?>. <?=t('app','All rights reserved');?>.
                        </font>
                    </td>
                </tr>
                <tr><td height="35" bgcolor="#ffffff"></td></tr>
            </table>
            <!-- Email Body : END -->

            <!-- Email Footer : BEGIN -->
            <!-- <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;" role="presentation">
                <tr>
                    <td style="padding: 20px 0; text-align: center"></td>
                </tr>
            </table> -->
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>