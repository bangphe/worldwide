<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\modules\admin\controllers;

use app\models\Listing;
use app\models\ListingSearch;
use app\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Controls the actions for orders section
 *
 * @Class OrdersController
 * @package app\modules\admin\controllers
 */
class AdsController extends \app\modules\admin\yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all the orders
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ListingSearch();
        // $dataProvider = $searchModel->search(request()->queryParams);
        // $dataProvider->query->andWhere(['t.package_id' => 1]);
        // $dataProvider->pagination->pageSize=10;

        // $category = $this->findCategoryBySlug($slug);
        // die(var_dump($category));
        // $searchCategories = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();
        // $categories = Category::getHierarchyOfCategoriesBySlug($slug, true);
        // $childCategories = Category::find()->where(['parent_id' => $category->category_id, 'status' => Category::STATUS_ACTIVE])->all();

        $dataProvider = $searchModel->categorySearch(request()->queryParams);
        $dataProvider->pagination = [
            'defaultPageSize' => 10,
        ];
        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a specific order
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Listing::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * Find category by unique slug
     *
     * @param $slug
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    protected function findCategoryBySlug($slug)
    {
        if (($category = Category::findOne(['slug' => $slug, 'status' => Category::STATUS_ACTIVE])) !== null) {
            return $category;
        }
        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }
}
