<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Create Category'), ['create'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState' => true,
        ]); ?>
        <?= GridView::widget([
            'id'           => 'categories',
            'tableOptions' => [
                'class' => 'table table-bordered table-hover table-striped',
            ],
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'sort_order',
                [
                    'attribute' => 'name',
                    'value'     => function ($model) {
                        return $model->getFullName();
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'filter'    => DatePicker::widget([
                        'model'      => $searchModel,
                        'attribute'  => 'created_at',
                        'options'    => [
                            'class' => 'form-control',
                        ],
                        'dateFormat' => 'yyyy-MM-dd',
                    ])
                ],
                [
                    'attribute' => 'status',
                    'value'     => 'status',
                    'filter'    => Html::activeDropDownList($searchModel, 'status', ['active' => 'active', 'inactive' => 'inactive',], ['class' => 'form-control', 'prompt' => 'All'])
                ],
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'contentOptions' => [
                        'style' => 'width:100px',
                        'class' => 'table-actions'
                    ],
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$model->category_id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="fa fa-trash"></i>',['delete','id'=>$model->category_id],
                            [
                                'class'=>'btn btn-danger m-btn m-btn--icon m-btn--icon-only',
                                'data'=>[
                                    'method'=>'post',
                                    'confirm'=>'Are you sure you want to delete this item?',
                                ]
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
