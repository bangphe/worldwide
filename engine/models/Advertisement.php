<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use app\helpers\DateTimeHelper;
use app\helpers\StringHelper;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;


/**
 * Class Advertisement
 * @package app\models
 */
class Advertisement extends \app\models\auto\Advertisement
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DEACTIVATED = 'deactivated';

    public $avatarUpload;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'link', 'start_date', 'end_date'], 'trim'],
            [['status'], 'safe'],
            [['title', 'link'], 'string', 'max' => 255],
            [['timezone'], 'in', 'range' => array_keys(DateTimeHelper::getTimeZones())],
            [['avatarUpload'],
                'file',
                'mimeTypes' => 'image/jpeg, image/png, image/pjpeg',
                'checkExtensionByMimeType' => true,
                'wrongMimeType' => t('app','Please try to upload an image.')
            ],
            [['title', 'link', 'description'], 'required'],
        ];
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->advertisement_uid;
    }

    /**
     * @return bool
     */
    public function deactivate()
    {
        $this->status = self::STATUS_DEACTIVATED;
        $this->save(false);
        return true;
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($advertisement_id)
    {
        return static::findOne($advertisement_id);
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        return true;
    }

    /**
     * afterValidate
     */
    public function afterValidate()
    {
        parent::afterValidate();
        $this->handleUploadedImage();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->advertisement_id;
    }


    /**
     * handleUploadedImage
     */
    protected function handleUploadedImage()
    {
        if ($this->hasErrors()) {
            return;
        }

        if (!($avatarUpload = UploadedFile::getInstance($this, 'avatarUpload'))) {
            return;
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/advertisement');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                notify()->addError(t('app', 'The images storage directory({path} does not exists and cannot be created!', ['path' =>$storagePath]));
                return;
            }
        }

        $newAvatarName = $avatarUpload->name;
        $avatarUpload->saveAs($storagePath . '/' . $newAvatarName);
        if (!is_file($storagePath . '/' . $newAvatarName)) {
            notify()->addError(t('app', 'Cannot move the avatar into the correct storage folder!'));
            return;
        }
        $existing_file = $storagePath . '/' . $newAvatarName;
        $newAvatarName = substr(sha1(time()),0,6) . $newAvatarName;
        copy($existing_file , $storagePath . '/' . $newAvatarName );
        $avatar = str_replace('Upload', '', $avatarUpload);
        $this->avatar = Yii::getAlias('@web/uploads/images/advertisement/' . $newAvatarName);
    }

    /**
     * Get list of all advertisements as array of advertisement id and full name
     *
     * @return array [advertisement_id => advertisement_full_name]
     */
    public static function getListOfAdvertisements()
    {
        $result = [];

        $advertisements = self::find()->all();
        foreach ($advertisements as $advertisement) {
            $result[$advertisement->advertisement_id] = $advertisement->getFullName();
        }

        return $result;
    }
}
