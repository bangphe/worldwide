<?php
use app\assets\AdAsset;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
// use dosamigos\ckeditor\CKEditor;
use app\helpers\FrontendHelper;
use app\models\Category;
use yii\helpers\ArrayHelper;
use app\models\Currency;
use app\models\Country;

AdAsset::register($this);

if (isset($_GET['cat'])) 
{
    $querystringcatslug=$_GET['cat'];
	$connection = app()->getDb();
    $command = $connection->createCommand("Select * from ea_category where parent_id=(Select Category_ID from ea_category where slug='".$querystringcatslug."')");
    $SubCatList = $command->queryAll(); 
	foreach ($list as $category) 
   {
	   echo "1".$category["name"];
   }
								
}
else
{

} 									

$heading = isset($selected_category[0]->name) ? $selected_category[0]->name : 'Post your ad';

$picount=options()->get('app.settings.common.homePicNumber');
$script = <<< JS
$(document).ready(function() {
	debugger;
    $('#post-form').yiiActiveForm('add',
        {
            "id":"listingimage-imagesgallery",
            "name":"imagesGallery[]",
            "container":".field-listingimage-imagesgallery",
            "input":"#listingimage-imagesgallery",
            "error":".help-block.help-block-error",
            "validate":function (attribute, value, messages, deferred) {
                if ($('.file-preview-frame').length == 0) {
					debugger
                    yii.validation.required(value, messages, {"message": $('#post-form').data('err-msg-gallery')});
                }
				
				var piccount = 0;
				$('.kv-preview-thumb').each(function() {
					piccount++;    
				});
				
				if(piccount> $picount )
				{
					alert('You can upload only $picount images');	
				}
            }
        }
    );
}); 
JS;

//$this->registerJs($script, yii\web\View::POS_LOAD);

$this->registerJs("(function ($) {
    $.fn.serializeJSON = function () {
        var json = {};
        jQuery.map($(this).serializeArray(), function (n, _) {
            json[n['name']] = n['value'];
        });
        return json;
    };
})(jQuery);

// $('#modal-category').modal('show');
var listingid = $('#listing-category_id').val();
if(listingid == 11){
    $('.event-start').css('display','block');
    $('.event-end').css('display','block');
}

ensureNumber = function (n) {
    n = parseInt(n, 10);
    if (isNaN(n) || n <= 0) {
        n = 0;
    }
    return n;
};

saveFormToCookie = function (form) {
    var name = $(form).attr('id');
    var data = $(form).serializeJSON();
    Cookies.set(name, data, {expires: 365});
};

loadFormFromCookie = function (form) {
    var name = $(form).attr('id');
    var data = Cookies.get(name);

    if (typeof data === 'undefined') {
        return;
    }

    JSON.parse(data, function (key, value) {
        if (typeof (value) !== 'object' && key != 'ListingImage[imagesGallery][]') {
        
            var el = $(form).find('*[name=\"' + key + '\"]');

            if (el.is('input')) {
                if (false) {
                    // code formatting stub
                } else if (el.attr('type') === 'number') {
                    var old = el.val();
                    
                    if (old != value) {
                        el.val(ensureNumber(value));
                    }
                } else if (el.attr('type') === 'checkbox') {
                    if (el.val() === value) $(el).prop('checked', true);
                } else if (el.attr('type') === 'radio') {
                    $.each(el, function (_, elc) {
                        if (elc.value === value) $(elc).prop('checked', true);
                    });
                } else {
                    var old = el.val();
                    
                    if (old != value) {
                        el.val(value);
                    }
                }
            } else if (el.is('select')) {
                var old = el.val();

                if (old != value) {
                    el.val(value);
                }
            } else if (el.is('textarea')) {
                var old = el.val();
                
                if (old != value) {
                    el.val(value);
                }
            }
            
            el.change();
        }
    });
};

$('#listing-category').change(function() {
    console.log('asdasd');
    getChildren($(this).val());
});

setTimeout(function(){
    // loadFormFromCookie($('#post-form'));
    
    // CKEDITOR.instances['listing-description'].setData($('#listing-description').value);
    // $('#listing-description').val($('#listing-description').value);

    $('#post-form').on('keyup change', 'input, select, textarea', function(){
        saveFormToCookie($('#post-form'));
    });
    
    $('#post-form').on('submit', function(){
        // console.log('icikiwir');
        Cookies.remove('post-form');
    });
    // getSubcategory();
}, 1000);

", \yii\web\View::POS_LOAD);

if (!empty($selected_category)) {
    if (isset($selected_category[0])) {
        $tes = $selected_category[0]->parent_id != NULL ? $selected_category[0]->parent_id : $selected_category[0]->category_id;
        // die(var_dump($tes));
        $this->registerJs("
            setTimeout(function(){
                $('div#modal-category a[data-id=" . $tes . "]').click();
                getSubcategory();
            }, 1000);
        ", \yii\web\View::POS_LOAD);
    }
}
// else {
    // $this->registerJs("
// setTimeout(function(){    
    // var categoryId = $('input#listing-category_id').val();
    // if (categoryId) {
        // $('div#modal-category a[data-id='+categoryId+']').click();
        // $('#success-selection').click();
    // }
// }, 300);
// ", \yii\web\View::POS_LOAD);
// }

?>
<style>
    .cke_chrome {
        border-top-color: #d1d1d1;
        border-left-color: #d1d1d1;
        border-right-color: #d1d1d1;
        border-bottom-style: #d1d1d1;
    }
    .cke_bottom {
        display: none;
    }
    .file-preview {
        /*margin-top: -15px;*/
        margin-bottom: -1px;
        border-left-color: #d1d1d1;
        border-right-color: #d1d1d1;
        border-top-style: none;
        border-bottom-style: none;
        background-color: #d1d1d1;
    }
    .file-drop-zone-title {
        color: #4a4a4a;
        padding: 15px 10px;
    }
    .separator-text {
        margin: 20px 0 20px 0;
    }

    .file-caption {
        border-bottom-color: #d1d1d1;
        border-left-color: #d1d1d1;
        border-right-color: #d1d1d1;
        border-top-style: none;
        background-color: #d1d1d1;
    }

    .kv-fileinput-caption {
        height: 8% !important;
    }

    .field-location-zone_id {
        margin-right: -24px !important;
    }

    .field-listing-type {
        margin-right: -24px !important;
    }

    .select2-container--default {
        width: 96% !important;
    }
</style>
<section class="post-listing" style="<?php if(isset($childCategory->slug) && !empty($childCategory->slug)){?>display: none;<?php }?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php $listing_id = (int)($action == 'update') ? $ad->listing_id : 0;
                    $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-listing' => $listing_id,
                            // 'data-err-msg-gallery' => t('app', 'Please add at least one image to your ad post'),
                            'class' => 'form-horizontal',
                        ],
                        'id'        => 'post-form',
                        'method'    => 'post',
                        'encodeErrorSummary' => false,
                        'errorSummaryCssClass' => 'help-block',
                ]); ?>
                <div class="block" style="margin-bottom: 20px !important; padding-bottom: 20px !important;">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                            <?php
                            if (!empty($selected_category)) {
                                if($selected_category[0]->slug == 'selling' || $selected_category[0]->slug == 'buying') {
                            ?>
                                <h1 id="ad_post_heading"><?= ($action == 'update') ? t('app', 'Update your ad') : $heading.' Business' ?></h1>
                            <?php 
                            } else {
                            ?>
                                <h1 id="ad_post_heading"><?= ($action == 'update') ? t('app', 'Update your ad') : $heading?></h1>
                            <?php 
                            } } else {
                            ?>
                                <h1 id="ad_post_heading"><?= ($action == 'update') ? t('app', 'Update your ad') : $heading?></h1>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <div id="extra-field" style="margin-bottom: 20px;">
                                    <?= $form->errorSummary($ad); ?>
                                    <?= $form->errorSummary($customer); ?>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="title" class="col-md-2 control-label">Title</label>
                                        <div class="col-md-8">
                                            <?= $form->field($ad, 'title', [
                                                'template'      => '{input} {error}',
                                            ])->textInput([ 'placeholder' => t('app','Title'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(); ?>
                                        </div>
                                    </div>
                                </div>
                            
                                <!-- <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="country" class="col-md-2 control-label">Location</label>
                                        <div class="col-md-4">
                                            <?//= $form->field($location, 'country_id', [
                                              //  'template'      => '{input} {error}',
                                              //  ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
                                            ?>
                                        </div>
                                        <div class="col-md-4" id="listing-select-zones-wrapper" data-url=<?//=url(['/listing/get-country-zones']);?> data-zone=<?//=(int)$location->zone_id;?>>
                                            <?//= $form->field($location, 'zone_id', [
                                              //  'template'      => '{input} {error}',
                                            //])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
                                            ?>
                                        </div>
                                    </div>
                                </div> -->

                                <!-- <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="city" class="col-md-2 control-label">&nbsp;</label>
                                        <div class="col-md-4">
                                            <?//= $form->field($location, 'city', [
                                              //  'template'      => '{input} {error}',
                                            //])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control', 'style' => 'width: 97%;'])->label(false); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?//= $form->field($location, 'zip', [
                                              //  'template'      => '{input} {error}',
                                            //])->textInput(['placeholder' => t('app','Zip code'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false); ?>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="col-md-12 form-price" style="display: block;">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="title" class="col-md-2 control-label">Price</label>
                                        <div class="col-md-4">
                                            <?= $form->field($ad, 'currency_id', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency'), 'value' => $customer->currency_id != null ? $customer->currency_id : 1])->label(false); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?= $form->field($ad, 'price', [
                                                'template'      => '{input} {error}',
                                            ])->textInput(['placeholder' => t('app','Price'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>

                                <?php
                                if(isset($selected_category[0]->name) && !empty($selected_category[0]->name)){
                                ?>
                                <input type="hidden" id="listing-category_id" class="form-control" name="Listing[category_id]" data-original-title="" title="" value="<?=$selected_category[0]->category_id?>">

                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="title" class="col-md-2 control-label">Category</label>
                                        <!-- <div class="col-md-4" style="padding-right: 24px;">
                                            <?= $form->field($ad, 'type', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList(['local' => 'Local', 'international' => 'International'],['class'=>'','prompt' => t('app','Choose Type')])->label(false); ?>
                                        </div> -->
                                        <div class="col-md-8" style="padding-right: 6px;">
                                            <?= $form->field($ad, 'category_id', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList([],['class'=>'','prompt' => t('app','Choose Sub Category'), 'id' => 'cat-id', 'style' => 'width: 100% !important;'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: -8px;">
                                        <label for="title" class="col-md-2 control-label">Category</label>
                                        <div class="col-md-4" style="padding-right: 24px;">
                                            <?= $form->field($ad, 'category', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList(Category::getAllCategoriesParent(),['class'=>'','prompt' => t('app','Choose Category')])->label(false); ?>
                                        </div>
                                        <div class="col-md-4" style="padding-right: 6px;">
                                            <?= $form->field($ad, 'category_id', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList([],['class'=>'','prompt' => t('app','Choose Sub Category'), 'id' => 'cat-id'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                    
                                <div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog add-category" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="choose-category">

                                                    <div class="column-category primary-category">
                                                        <h4>Categories</h4>
                                                        <div class="category-items mCustomScrollbar mCS-autoHide">
                                                            <ul>
                                                                <?php foreach ($categories as $category) {
                                                                    if (empty($category->parent_id)) { ?>
                                                                        <li>
                                                                            <a href="#" data-id="<?=(int)$category->category_id;?>">
                                                                                <span><i class="fa <?=html_encode($category->icon);?>" aria-hidden="true"></i></span>
                                                                                <span><?=html_encode($category->name);?></span>
                                                                            </a>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                        <div class="column-subcategory mCustomScrollbar mCS-autoHide">
                                                            <div class="column-subcategory-wrapper">
                                                                <?php $sortedCategories =  \app\helpers\FrontendHelper::getCategoriesSorted($categories);
                                                                foreach ($sortedCategories as $sortedCategoryId=>$sortedCategory) { ?>
                                                                    <div class="column-category" data-parent="<?=(int)$sortedCategoryId;?>" style="display: none">
                                                                        <h4><?=html_encode($sortedCategory['name']);?></h4>
                                                                        <div class="category-items mCustomScrollbar mCS-autoHide">
                                                                            <ul>
                                                                                <?php foreach ($sortedCategory['children'] as $childCategory) { ?>
                                                                                    <li>
                                                                                        <a href="#" data-id="<?=(int)$childCategory->category_id;?>" class="subcateg" onclick="change_title('<?=$childCategory->name?>', '<?=$childCategory->slug?>')">
                                                                                            <span><?=html_encode($childCategory->name);?></span>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <span class="no-category-selected"><?=t('app','Please select a specific category');?></span>
                                                            <button id="success-selection" type="button" class="btn-as" style="display: none" data-dismiss="modal"><?= t('app', 'Submit');?></button>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <button id="close-modal" type="button" class="btn-as danger-action pull-right" data-dismiss="modal"><?= t('app', 'Cancel');?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row hidden">
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <?= $form->field($customer, 'phone', [
                                                'template'      => '{input} {error}',
                                            ])->textInput(['placeholder' => t('app','Phone'), 'class' => 'form-control'])->label(false); ?>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <?= $form->field($ad, 'hide_phone', [
                                                'options' => [
                                                    'class' => 'checkbox'
                                                ],
                                            ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <?= $form->field($customer, 'email', [
                                                'template'      => '{input} {error}',
                                            ])->textInput(['placeholder' => t('app','Email Address'), 'class' => 'form-control'])->label(false); ?>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <?= $form->field($ad, 'hide_email', [
                                                'options' => [
                                                    'class' => 'checkbox'
                                                ],
                                            ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                        </div>
                                    </div>

                                    <div class="row category-fields" style="display: none">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="separator-text">
                                                <span><?=t('app','Category Fields');?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row category-fields" id="category-fields" data-url="<?=url(['/listing/get-category-fields']);?>" style="display: none">
                                    </div>

                                    <div class="col-md-12 event-start" style="display: none;">
                                        <div class="form-group" style="margin-bottom: -8px;">
                                            <label for="title" class="col-md-2 control-label">Event Start</label>
                                            <div class="col-md-4">
                                                <?= $form->field($ad, 'event_date_start', [
                                                    'template'      => '{input} {error}',
                                                ])->textInput(['placeholder' => t('app','Date Start'), 'class' => 'form-control datepicker', 'style' => 'width: 96%;'])->label(false); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $form->field($ad, 'event_time_start', [
                                                    'template'      => '{input} {error}',
                                                ])->textInput(['class' => 'form-control times', 'style' => 'width: 100%;'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 event-end" style="display: none;">
                                        <div class="form-group" style="margin-bottom: -8px;">
                                            <label for="title" class="col-md-2 control-label">Event End</label>
                                            <div class="col-md-4">
                                                <?= $form->field($ad, 'event_date_end', [
                                                    'template'      => '{input} {error}',
                                                ])->textInput(['placeholder' => t('app','Date End'), 'class' => 'form-control datepicker', 'style' => 'width: 96%;'])->label(false); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $form->field($ad, 'event_time_end', [
                                                    'template'      => '{input} {error}',
                                                ])->textInput(['class' => 'form-control times', 'style' => 'width: 100%;'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom: 4px; margin-top: -20px;">
                                            <label for="title" class="col-md-2 control-label">&nbsp;</label>
                                            <div class="col-md-8" style="margin-left: -16px;">
                                                <?//= $form->field($ad, 'negotiable', [
                                                  //  'options' => [
                                                  //      'class' => 'checkbox'
                                                  //  ],
                                                //])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom: -8px;">
                                            <label for="description" class="col-md-2 control-label">Description</label>
                                            <div class="col-md-8">
                                                <?= $form->field($ad, 'description', [
                                                    'template'      => '{input} {error}',
                                                ])->textArea(['placeholder' => t('app','Description'), 'class' => 'form-control', 'rows' => 3, 'style' => 'height: 80px;', 'id' => 'summernote'])->label(false); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($selected_category[0]->name != "Jobs") { ?>
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom: -8px;">
                                            <label for="photos" class="col-md-2 control-label">Add photos</label>
                                            <div class="col-md-8">
                                            <?php
                                            $imagesPreview = [];
                                            $imagesPreviewConfig = [];
                                            if (isset($uploadedImages)) {
                                                // sort for images sort_order
                                                //usort($uploadedImages, function ($a, $b) { return $a['listing_id'] && strnatcmp($a['sort_order'], $b['sort_order']); });
                                                if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                                                    $imagesPreview[] = $image->image_path;
                                                    $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                                                    $imagesPreviewConfig[$key]['url'] = url(['/listing/remove-ad-image']);
                                                    $imagesPreviewConfig[$key]['key'] = $image->image_id;
                                                }
                                                // print_r($uploadedImages);
                                            }
                                            echo $form->field($images, 'imagesGallery[]')->widget(FileInput::classname(), [
                                                'options' => [
                                                    'multiple' => true,
                                                    'class' =>'file-loading',
                                                    'data-sort-listing-images' => url(['/listing/sort-ad-images']),
                                                ],
                                                'pluginOptions' => [
                                                    'initialPreview'=> $imagesPreview,
                                                    'initialPreviewConfig' =>
                                                        $imagesPreviewConfig
                                                    ,
                                                    'initialPreviewAsData'=>true,
                                                    'language' => html_encode(options()->get('app.settings.common.siteLanguage', 'en')),
                                                    'deleteUrl' => false,
                                                    'uploadUrl' => \yii\helpers\Url::to(['upload']),
                                                    'uploadAsync' => true,
                                                    'uploadExtraData' => ['id' => intval($ad->listing_id)],
                                                    'allowedFileTypes' => ["image"],
                                                    'showUpload' => false,
                                                    'showRemove' => false,
                                                    'showClose' => false,
                                                    'browseOnZoneClick' => true,
                                                    'autoOrientImage' => true,
                                                    'browseClass' => 'btn btn-as',
                                                    'removeClass' => 'btn btn-as reverse',
                                                    'uploadClass' => 'btn btn-as reverse',
                                                    'layoutTemplates' =>
                                                        [
                                                            'footer'=> '<div class="file-thumbnail-footer">' . '{progress} {actions}' . '</div>'
                                                        ],
                                                    'fileActionSettings' => [
                                                        'showUpload' => false,
                                                        'showDrag' => true,
                                                        'autoOrientImage' => true,
                                                    ],
                                                    'overwriteInitial'=>false,
                                                ],
                                                'pluginEvents' => [
                                                    'filebatchselected' => new \yii\web\JsExpression(
                                                    'function(event, files) {
                                                        jQuery(event.currentTarget).fileinput("upload");
                                                    }')
                                                ]
                                            ])->label(false);
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block" style="margin-bottom: 20px;">
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="a-center">
                                    <a href="javascript:void(0)" onclick="goBack()" class="btn-as warning-action"><i class="fa fa-arrow-left"></i></a>
                                    <button type="submit" name="post" class="btn-as"><?=t('app','Submit');?></button>
                                    <button type="submit" name="preview" class="btn-as danger-action"><?=t('app','Preview');?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-post-listing-contact" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-notice" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title"><?=t('app', 'Notice');?></h1>
                <a href="javascript:;" class="x-close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            <div class="modal-body">
                <p><?=t('app','Please leave at least one contact option');?></p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn-as black pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function change_title(category_name, slug){
        //$("#listing-title").before( "<label>TITLE</label>" );
        if(slug == 'b2b'){
            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" value="<?=$ad->business_types?>" id="business_types" type="text" placeholder="What is the business about" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Proposal B2B</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="What you are looking for a merger, trading, business or investment partner"><?=$ad->offer?></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of business</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="How do you want to structure the business to business arrangement"><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';

            var title = 'What you are looking to benefit as business to business';
            $('#listing-title').attr('placeholder', title);
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'selling'){
            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" value="<?=$ad->business_types?>" type="text" placeholder="What is the business about" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>VALUE</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="business valuation"><?=$ad->offer?></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of sale</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="How the sale will be conducted"><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';



            var title = 'What is on offer';
            var city = 'Where is the business located';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            $('#descrp').attr('placeholder','Describe the business for sale');
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'buying'){
            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" value="<?=$ad->business_types?>" type="text" placeholder="Type business you are looking to buy/invest" class="form-control">';
            extra_field += '</div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Capital</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input type="text" name="Listing[capital]" class="form-control"  placeholder="Money to be invested" value=""> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Offer</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="Capital available for the purchase/investment"><?=$ad->offer?></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What you want as a buyer"><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';

            var title = 'What is being offered';
            var city = 'Which locations are you looking to invest in ';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            $('#descrp').attr('placeholder','Give a brief description of what you are looking for');
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'partnerships'){
            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" value="<?=$ad->business_types?>" type="text" placeholder="The type of partnership ,local ,international,expansion or merge" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>PITCH</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="Give details of what you require or are looking for"><?=$ad->offer?></textarea> ';
            extra_field += '</div></div>';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[share]" value="<?=$ad->share?>" type="text" placeholder="Percentage or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of partnership</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What each partner stands to benefit"><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';

            var title = 'Looking to merge, buy shares, investing or local partner in a location';
            var city = 'Which locations are you looking to invest in';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'investment-funding'){

            var extra_field = '';
             
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[share]" value="<?=$ad->share?>" type="text" placeholder="Percentage or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of the funding</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder=""><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';
            
            var business_plan_uploadable = 'Business plan'; 
            var price_title = 'CAPITAL/LOA';
            var title = 'Type of funding loan, Shares to be bought back at a later date, returns on investment or funding with benefits to the investors';
            $('#listing-title').attr('placeholder', title);
            $('#business_plan_uploadable').html(business_plan_uploadable);
            $('#price_title').html(price_title);
            $('#descrp').attr('placeholder','what the funding is for and how it is going to be invested in the business');
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'investment-proposal'){

            var extra_field = '';
             
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input type="text" name="Listing[share]" value="<?=$ad->share?>" placeholder="Percentage  or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of the Investment</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What each partner stands to benefit"><?=$ad->terms?></textarea> ';
            extra_field += '</div></div>';

            var business_plan_uploadable = 'Business plan'; 
            var title = 'What you want to do as a business';
            var price_title = 'Capital/loan';
            $('#listing-title').attr('placeholder', title);
            $('#business_plan_uploadable').html(business_plan_uploadable);
            $('#price_title').html(price_title);
            //$('#extra-field').html(extra_field);
        }
        if(slug == 'selling' || slug == 'buying'){
            category_name = category_name+' Business';
        }
        if (slug == 'vacancies' || slug == 'applying-searching') {
            $('.form-price').css('display','none');
        }
        if(slug == 'events'){
            $('.event-start').css('display','block');
            $('.event-end').css('display','block');
        }
        $('#ad_post_heading').html(category_name);
        $('#business_types').change(function (){
            var business_types_val = $('#business_types').val();
            // console.log(business_types_val);
        });

        $('.post-listing').css('display','block');
    }

    function goBack() {
        window.history.back();
    }

    function getSubcategory() {
        var cat_id = '<?= !empty($selected_category) ? $selected_category[0]->category_id : 0; ?>';
        var cat_name = '<?= !empty($selected_category) ? $selected_category[0]->name : 0; ?>';
        model.isLoading(true);
        $.ajax({
            url: '<?= url(['/listing/get-sub-category']); ?>',
            type: 'POST',
            data: {
                category_id : cat_id
            },
            success: function (data) {
                model.isLoading(false);
                if (data.response.length > 0) {
                    $.each(data.response, function (key, value) {
                        $('#cat-id').append('<option value="' + value.category_id + '">' + value.name + '</option>');
                    });
                } else {
                    $('#cat-id').append('<option value="' + cat_id + '">' + cat_name + '</option>');
                }
            }
        });
    }

    function getChildren(parent_id) {
        model.isLoading(true);
        var cat_name = $('#listing-category').select2('data')[0].text;
        $.ajax({
            url: '<?= url(['/listing/get-sub-category']); ?>',
            type: 'POST',
            data: {
                category_id : parent_id
            },
            success: function (data) {
                model.isLoading(false);
                $('#cat-id').html('');
                if (data.response.length > 0) {
                    $.each(data.response, function (key, value) {
                        $('#cat-id').append('<option value="' + value.category_id + '">' + value.name + '</option>');
                    });
                } else {
                    $('#cat-id').append('<option value="' + parent_id + '">' + cat_name + '</option>');
                }
            }
        });
    }
</script>