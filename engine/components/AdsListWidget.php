<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\components;

use app\models\Listing;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\db\Expression;

class AdsListWidget extends Widget
{
    /**
     * types of available lists
     */
    const LIST_TYPE_PROMOTED = 1;
    const LIST_TYPE_NEW = 2;
    const LIST_TYPE_RELATED = 3;
    const LIST_TYPE_FEATURED = 4;

    /**
     * @var int number of items to retrieve
     */
    public $quantity = 8;

    /**
     * @var int one of constants
     */
    public $listType;

    /**
     * @var string title of list
     */
    public $title;

    /**
     * @var
     */
    public $ad;

    /**
     * @var
     */
    public $col = 3;

    /**
     * @var bool to show empty template
     */
    public $emptyTemplate = false;

    public $moreLabel = false;

    public $moreLink = false;

    public $searchModel;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->quantity == 3) {
            $this->col = 4;
        }

        if (!in_array(
            $this->listType,
            [self::LIST_TYPE_PROMOTED, self::LIST_TYPE_NEW, self::LIST_TYPE_RELATED, self::LIST_TYPE_FEATURED]
        )) {
            throw new InvalidConfigException('"' . $this->listType . '" list type is not allowed.');
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        /** @var \app\yii\web\Customer $customer */
        $customer = app()->customer;
        $id_country = $customer->getCountryId();
        $params = $this->searchModel;

        $query = Listing::find()->alias('t')
            ->joinWith(['package c1', 'location l', 'category c'], true, 'INNER JOIN')
            ->with(['favorite', 'currency', 'mainImage', 'category', 'location.zone', 'location.country'])
            // ->where(['>', 'listing_expire_at', new Expression('NOW()')])
            ->where(['t.status' => Listing::STATUS_ACTIVE])
            ->limit($this->quantity);

        $order = ($id_country ? 'FIELD(l.country_id, ' . $id_country .') DESC, ' : '');

        if ($this->getIsFeaturedList()) {
            $query->andWhere(['>', 'promo_expire_at', new Expression('NOW()')])
                ->andWhere(['c1.promo_show_featured_area' => 'yes'])
                ->orderBy(new Expression($order. 'rand()'));
        } elseif ($this->getIsPromotedList()) {
            // $query->andWhere(['>', 'promo_expire_at', new Expression('NOW()')])
            $query->andWhere(['c1.promo_show_promoted_area' => 'yes'])
                ->orderBy(new Expression($order. 'rand()'));
        } elseif ($this->getIsNewList()) {
            // $query->andWhere('t.promo_expire_at <= t.created_at')
            $query->orderBy(new Expression($order . 't.promo_expire_at DESC'));
        } elseif ($this->getIsRelatedList()) {
            if (!empty($params)) {
                if ($params['searchPhrase'] != "") {
                    if ($params['searchPhrase'] != "") {
                        $query->andWhere(['like', 'c.name', $params['searchPhrase']]);
                        // $query->andFilterWhere(['or',
                        //     ['like', 't.title', $params['searchPhrase']],
                        //     ['like', 'c.name', $params['searchPhrase']]]);
                    }
                }
            }
            $query->orderBy(new Expression($order. 'rand()'));
        }
        // die(var_dump($query->createCommand()->getRawSql()));
        
        // if ad loaded to this lists (eg. ad view)
        if (!empty($this->ad)){
            $query->andWhere(['<>' ,'listing_id' , $this->ad->listing_id]);
        }

        $ads = $query->all();

        if (!empty($ads)) {
            if ($this->listType == self::LIST_TYPE_PROMOTED) {
                return $this->render('ads-list/ads-list', [
                    'ads' => $ads,
                    'title' => $this->title,
                    'col' => $this->col,
                    'isPromoted' => $this->getIsPromotedList(),
                    'moreLabel' => $this->moreLabel,
                    'moreLink' => $this->moreLink,
                ]);
            } elseif ($this->listType == self::LIST_TYPE_RELATED) {
                return $this->render('ads-list/ads-list-related', [
                    'ads' => $ads,
                    'title' => $this->title,
                    'col' => $this->col,
                    'isPromoted' => $this->getIsPromotedList(),
                    'moreLabel' => $this->moreLabel,
                    'moreLink' => $this->moreLink,
                ]);
            } else {
                return $this->render('ads-list/ads-list-new', [
                    'ads' => $ads,
                    'title' => $this->title,
                    'col' => $this->col,
                    'isPromoted' => $this->getIsPromotedList(),
                    'moreLabel' => $this->moreLabel,
                    'moreLink' => $this->moreLink,
                ]);
            }
        }

        if ($this->emptyTemplate) {
            return $this->render('ads-list/ads-list-empty', []);
        }

        return false;
    }

    /**
     * Check whether the list is promoted
     *
     * @return bool
     */
    public function getIsPromotedList()
    {
        return self::LIST_TYPE_PROMOTED == $this->listType;
    }

    /**
     * Check whether the list is promoted
     *
     * @return bool
     */
    public function getIsFeaturedList()
    {
        return self::LIST_TYPE_FEATURED == $this->listType;
    }

    /**
     * Check whether the list is new
     *
     * @return bool
     */
    public function getIsNewList()
    {
        return self::LIST_TYPE_NEW == $this->listType;
    }

    /**
     * Check whether the list is related
     *
     * @return bool
     */
    public function getIsRelatedList()
    {
        return self::LIST_TYPE_RELATED == $this->listType;
    }
}
