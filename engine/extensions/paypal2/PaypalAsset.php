<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\extensions\paypal2;

use yii\web\AssetBundle;

/**
 * Class PaypalAsset
 * @package app\extensions\paypal2
 */
class PaypalAsset extends AssetBundle
{
    public $sourcePath = '@app/extensions/paypal/assets';

    public $css = [

    ];
    public $js = [
        'paypal.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}