<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->store_name;
$this->params['breadcrumbs'][] = ['label' => t('app', 'Stores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="m-portlet m-portlet--head">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= Html::encode($this->title); ?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->store_id], ['class' => 'btn btn-xs btn-success']) ?>
                        </li>
                        <li class="m-portlet__nav-item">
                            <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->store_id], [
                                'class' => 'btn btn-xs btn-danger',
                                'data' => [
                                    'confirm' => t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </li>
                    </ul>                    
                </div>
            </div>
            <div class="m-portlet__body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'store_id',
                        [
                            'attribute' => 'customer_id',
                            'value' => function($model) {
                                $customer = $model->customer;
                                return $customer ? $customer->fullName : 'Unknown';
                            },
                        ],
                        'store_name',
                        'company_name',
                        'company_no',
                        'vat',
                        'status',
                        'created_at',
                        'updated_at',
                    ],
                ]) ?>
            </div>
            <div class="m-portlet__foot">
            </div>
        </div>
    </div>
</div>
