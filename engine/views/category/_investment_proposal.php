<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\helpers\SvgHelper;
use app\helpers\FrontendHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
?>

<div class="listing-search">
    <form method="post"  onsubmit="return savedata()">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "title">Title</label>
                    <input type="text" required id="title" name="title" class="form-control" placeholder = "What you want to do as a business">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "business_plan">Business plan</label>
                    <input type="file" id="business_plan" name="business_plan" class="form-control">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "capital">Capital</label>
                    <input type="text" required id="capital" name="capital" class="form-control" placeholder = "money required">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "Shares">Shares %</label>
                    <input type="text" required id="shares" name="shares" class="form-control" placeholder = "Percentage  or how much of the business is on offer">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "location">Location</label>
                    <input type="text" required id="location" name="location" class="form-control" placeholder = "Location">
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <label for = "terms_of_business">Terms of Investment</label>
                    <input type="text" required id="terms_of_business" name="terms_of_business" class="form-control" placeholder = "Terms of Investment">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <a class="btn btn-warning" href="/category/business"><i class="fa fa-arrow-left"></i></a>
                    <label for = "terms_of_business"><br>&nbsp;</label>
                    <input type="submit" value="Submit" class="btn btn-success">
                </div>
            </div>
        </div>
    </form>
</div>
