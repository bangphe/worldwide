<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use app\fieldbuilder\Type;
use app\helpers\GeoHelper;
use app\models\Chat;
use app\models\Country;
use app\models\Currency;
use app\models\Listing;
use app\models\ListingFavorite;
use app\models\ListingImage;
use app\models\ListingPackage;
use app\models\ListingStat;
use app\models\Category;
use app\models\CategoryField;
use app\models\CategoryFieldValue;
use app\models\Customer;
use app\models\Gateway;
use app\models\Location;
use app\models\Order;
use app\models\OrderTax;
use app\models\OrderTransaction;
use app\models\Tax;
use app\models\Zone;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use app\yii\web\Controller;
use app\helpers\CommonHelper;
use yii\helpers\Url;

/**
 * Class ListingController
 * @package app\controllers
 */
class ListingController extends Controller
{
    /**
     * init
     */
    public function init()
    {
        parent::init();
        app()->trigger('app.controller.ad.init');
        // $this->bodyClass = 'custom-skin-2';
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($slug)
    {
        $ad = $this->findAdBySlug($slug);

        if (!$ad->isActive && !$ad->isExpired) {
            if(!$ad->isOwnedBy(app()->customer->id)){
                throw new NotFoundHttpException(t('app', 'The ad is not active yet, please try again later.'));
            }
            notify()->addWarning(t('app', 'Your ad is not active yet and it\'s visible only to you!'));
        }

        if($ad->isExpired) {
            notify()->addWarning(t('app', 'This ad is expired!'));
        }

        app()->view->registerMetaTag([
            'name'    => 'description',
            'content' => Listing::getMetaDescription($ad)
        ]);

        ListingStat::track(ListingStat::TOTAL_VIEWS, $ad->listing_id);

        app()->view->title = ucwords($ad->title);

        // hook action for adding ad image to facebook meta object in head
        $mainImage = $ad->mainImage->image;
        app()->on('app.header.beforeScripts', function() use ($mainImage) {
            echo '<meta name="og:image" content="'.Url::base(true) . $mainImage.'"/>';
        });

        // #INFO
        // relation with getRelation gets the whole ActiveQuery, that you can manipulate
        // if you get by $ad->relation, you will have a result of ActiveRecord

        return $this->render('view', [
            'ad'                => $ad,
            'images'            => $ad->getImages()
                ->orderBy(['sort_order'=>SORT_ASC])
                ->limit($ad->package->pictures)
                ->all(),
            'categories'        => new Category(),
        ]);
    }

    /**
     * @param $slug
     * @return string|Response
     */
    public function actionPreview($slug)
    {
        if (!(\session_id())) {
            \session_start();
        }

        if (app()->customer->isGuest == true) {
            return $this->redirect(['/']);
        }

        $ad = $this->findAdBySlug($slug);

        if (!$ad->isOwnedBy(app()->customer->id)) {
            return $this->redirect(['/']);
        }

        app()->view->title = t('app','Preview - ') . $ad->title;

        return $this->render('preview', [
            'ad'                => $ad,
            'images'            => $ad->getImages()
                ->orderBy(['sort_order'=>SORT_ASC])
                ->limit($ad->package->pictures)
                ->all(),
            'categories'        => new Category(),
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionPost($slug="")
    {
        if (app()->customer->isGuest) {
            notify()->addWarning(t('app', 'Please login before performing this action.'));
            return $this->redirect(['/account/login']);
        }

        $customerLocation = Location::find()->where(['customer_id' => app()->customer->id])->one();
        if ($customerLocation == null) {
            notify()->addWarning(t('app', 'Please fill the location on your account info first, before performing this action.'));
            return $this->redirect(['/account/info']);
        }

        $uploadedImages = ListingImage::find()
            ->where(['image_id' => \Yii::$app->session->get('new_images', [])])
            ->orderBy('sort_order ASC, image_id ASC')
            ->all();

        $ad = new Listing();
        $images = new ListingImage();
        $location = $customerLocation;
        $categories = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();
        $customer = Customer::findOne(app()->customer->id);
        $customer->scenario = 'post_ad';

        $selected_category = Category::find()->where(['category_id' => $slug])->all();

        if ($ad->load(request()->post()) &&
            $customer->load(request()->post())
        ) {
            if ($selected_category[0]->slug == 'events') {
                $ad->event_date_start = $_POST['Listing']['event_date_start'];
                $ad->event_date_end = $_POST['Listing']['event_date_end'];
                $ad->event_time_start = $_POST['Listing']['event_time_start'];
                $ad->event_time_end = $_POST['Listing']['event_time_end'];
            }
            
            $transaction = db()->beginTransaction();
            $error = false;
            try {
                $this->saveDraft($ad, $location, $customer, $images);
                $transaction->commit();
            } catch (\Exception $e) {
                notify()->addError($e->getMessage());
                $error = true;
                $transaction->rollBack();
            }
            if (!$error) {
                $target = 'package';//(request()->post('post', 'no') == '') ? 'package' : 'preview';
                return $this->redirect(url(['/listing/' . $target, 'slug' => $ad->slug]));
            }
        } else {
            $country = GeoHelper::getCountry();
            $zone = GeoHelper::getZone();

            // $location->country_id = $country ? $country->country_id : null;
            // $location->zone_id = $zone ? $zone->zone_id : null;
            // $location->city = GeoHelper::getCity();
            // $location->zip = GeoHelper::getZipCode();

            $ad->currency_id = Currency::find()
                ->select('currency_id')
                ->where(['code' => GeoHelper::getCurrency()])
                ->scalar();
        }

        app()->view->title = t('app','Post Ad') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('form_new', [
            'action'                => 'create',
            'ad'                    => $ad,
            'images'                => $images,
            'location'              => $location,
            'categories'            => $categories,
            'customer'              => $customer,
            'selected_category'     => $selected_category,
            'uploadedImages'    => $uploadedImages,
        ]);
    }

    /*Category listing as required*/

    /**
     * @return string|Response
     */

    public function actionB2bform()
    {
        if (app()->customer->isGuest) {
            notify()->addWarning(t('app', 'Please login before performing this action.'));
            return $this->redirect(['/account/login']);
        }


        $ad = new Listing();
        $images = new ListingImage();
        $location = new Location();
        $categories = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();
        $customer = Customer::findOne(app()->customer->id);
        $customer->scenario = 'post_ad';

        if ($ad->load(request()->post()) &&
            $location->load(request()->post()) &&
            $customer->load(request()->post()) &&
            $images->load(request()->post())
        ) {
            $transaction = db()->beginTransaction();
            $error = false;
            try {
                $this->saveDraft($ad, $location, $customer, $images);
                $transaction->commit();
            } catch (\Exception $e) {
                notify()->addError($e->getMessage());
                $error = true;
                $transaction->rollBack();
            }
            if (!$error) {
                $target = (request()->post('post', 'no') == '') ? 'package' : 'preview';
                return $this->redirect(url(['/listing/' . $target, 'slug' => $ad->slug]));
            }
        }

        app()->view->title = t('app','Post Your Custom Ad') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('_b2bform', [
            'action'        => 'create',
            'ad'            => $ad,
            'images'        => $images,
            'location'      => $location,
            'categories'    => $categories,
            'customer'      => $customer,
        ]);
    }

    /*Category listing as required*/


    /**
     * @param $slug
     * @return string|Response
     */
    public function actionUpdate($slug)
    {
        if (app()->customer->isGuest) {
            return $this->redirect(['/listing/post']);
        }

        $ad = Listing::find()->where(['slug' => $slug])->one();
        if (empty($ad)) {
            return $this->redirect(['/listing/post']);
        }

        $customer = $ad->customer;
        $customer->scenario = 'post_ad';
        if (!$ad->isOwnedBy(app()->customer->id)) {
            return $this->redirect(['/listing/post']);
        }

        $uploadedImages  = $ad->getListingImages()->orderBy('sort_order ASC')->all();
        $images          = new ListingImage();
        $location        = $ad->location;
        $categories      = Category::find()->where(['status' => Category::STATUS_ACTIVE])->all();
        $fields          = $ad->categoryFieldValues;

        $selected_category = Category::find()->where(['category_id' => $ad->category_id])->all();
        
        if ($ad->load(request()->post()) &&
            // $location->load(request()->post()) &&
            $customer->load(request()->post())
        ) {
            $transaction = db()->beginTransaction();
            $error = false;
            try {
                $this->saveDraft($ad, $location, $customer, $images);
                $transaction->commit();
            } catch (\Exception $e) {
                notify()->addError($e->getMessage());
                $error = true;
                $transaction->rollBack();
            }
            if (!$error) {
                $target = (request()->post('post', 'no') == '') ? 'package' : 'preview';
                return $this->redirect(url(['/listing/' . $target, 'slug' => $ad->slug]));
            }
        }

        app()->view->title = t('app','Update Ad') . ' - ' . options()->get('app.settings.common.siteName', 'EasyAds');

        return $this->render('form_new', [
            'action'            => 'update',
            'ad'                => $ad,
            'images'            => $images,
            'location'          => $location,
            'fields'            => $fields,
            'categories'        => $categories,
            'customer'          => $customer,
            'uploadedImages'    => $uploadedImages,
            'selected_category'     => $selected_category,
        ]);
    }

    /**
     * @param $ad
     * @param $location
     * @param $customer
     * @param $images
     * @return mixed
     * @throws \Exception
     */
    protected function saveDraft($ad, $location, $customer, $images)
    {
        // if (!($location->save())) {
        //     throw new \Exception(t('app', 'Your form has a few errors, please fix them and try again!'));
        // }
        if (!($customer->save())) {
            throw new \Exception(t('app', 'Your form has a few errors, please fix them and try again!'));
        }
        $ad->location_id = $location->location_id;
        $ad->status = $ad::STATUS_DRAFT;

        if (!$ad->save()) {
            throw new \Exception(t('app', 'Your form has a few errors, please fix them and try again!'));
        }

        $oldFields = $ad->categoryFieldValues;
        if (!empty($oldFields)) {
            foreach ($oldFields as $oldField) {
                $oldField->delete();
            }
        }

        $fieldsPost = request()->post('CategoryField');
        if (!empty($fieldsPost)) {
            foreach ($fieldsPost as $fieldId=>$fieldPost) {
                $fields = new CategoryFieldValue();
                $fields->field_id = (int)$fieldId;
                $fields->listing_id = (int)$ad->listing_id;
                $fields->value = $fieldPost;
                $fields->save();
            }
        }

        $jobs_category = [7, 8, 9];
        if (!in_array($ad->category_id, $jobs_category)) {
            $images->handleUploadedImages((int)$ad->listing_id);
        }

        return $ad->listing_id;
    }

    /**
     * @param $slug
     * @return string|Response
     */
    public function actionPackage($slug)
    {
        if (app()->customer->isGuest) {
            return $this->redirect(['/listing/post']);
        }

        $ad = Listing::find()->where(['slug'=>$slug])->one();
        if (empty($ad)) {
            return $this->redirect(['/listing/post']);
        }

        $customer = $ad->customer;
        $customer->scenario = 'post_ad';
        if (!$ad->isOwnedBy(app()->customer->id)) {
            return $this->redirect(['/listing/post']);
        }

        $location = $ad->location;
        $packages = ListingPackage::find()->all();

        if (request()->post()) {
            $transaction = db()->beginTransaction();
            $error = false;
            try {
                $adPost = request()->post('Listing');
                $packageId = ArrayHelper::getValue($adPost, 'package_id');
                if (empty($packageId)) {
                    throw new \Exception(t('app', 'Please select a package first to finish your posting.'));
                }

                $package        = ListingPackage::find()->where(['package_id'=>$packageId])->one();
                $locationTax    = Tax::calculateTax($ad->location->country_id, $ad->location->zone_id, $package->price);
                $cart           = CommonHelper::getCartTotal($locationTax, $package->price);
                $cartTotal      = $cart['total'];

                //make new order
                $order = Order::createNew($ad, $package, $cart);
                if (empty($order)) {
                    throw new \Exception(t('app', 'Something went wrong with your order, please try again later.'));
                }
                //populate OrderTax
                if (!empty($locationTax)) {
                    foreach ($locationTax as $tax) {

                        $orderTax               = new OrderTax();
                        $orderTax->order_id     = $order->order_id;
                        $orderTax->tax_name     = $tax->name;
                        $orderTax->tax_percent  = $tax->percent;
                        $orderTax->tax_price    = $tax->price;
                        $orderTax->save(false);
                    }
                }
                // process payment methods and purchase action in the handlePayment
                if ($package->price > 0) {

                    //make new transaction
                    $orderTransaction = OrderTransaction::createNew($order);
                    if (empty($orderTransaction)) {
                        throw new \Exception(t('app', 'Something went wrong with your order transaction, please try again later.'));
                    }

                    if (request()->post('paymentGateway')) {
                        $event = new \app\yii\base\Event();
                        $event->params = [
                            'transaction'   => $orderTransaction,
                            'order'         => $order,
                            'customer'      => $customer,
                            'cartTotal'     => $cartTotal,
                            'location'      => $location
                        ];
                        app()->trigger('app.controller.ad.package.handlePayment', $event);
                    }
                } else {
                    // make paid status for free packages
                    $order->status = Order::STATUS_PAID;
                    $order->save(false);
                }
                // For methods with redirect
                if (!empty($event->params['redirect'])) {
                    $ad->package_id = $packageId;
                    $ad->save(false);
                    $transaction->commit();
                    return $this->redirect($event->params['redirect']);
                }

                //Continue processing if not a method with redirect
                //Check status of order after getting back from payment procedure
                if ($order->status != Order::STATUS_PAID) {
                    throw new \Exception(t('app', 'Something went wrong with your order verification, please try again later.'));
                }

                //update ad dates
                $ad->promo_expire_at = null;
                $ad->listing_expire_at = new Expression('NOW() + INTERVAL ' . (int)$package->listing_days . ' DAY');
                if ((int)$package->promo_days > 0) {
                    $ad->promo_expire_at = new Expression('NOW() + INTERVAL ' . (int)$package->promo_days . ' DAY');
                }

                $ad->remaining_auto_renewal = 0;
                if ((int)$package->auto_renewal > 0) {
                    $ad->remaining_auto_renewal = $package->auto_renewal;
                }

                //Check admin approval option to set the right status for ad
                $adminApproval = options()->get('app.settings.common.adminApprovalAds', 1);
                if ($adminApproval > 0) {
                    $ad->status = Listing::STATUS_PENDING_APPROVAL;
                    Listing::sendWaitingApprovalEmail($ad);
                } else {
                    Listing::sendAdActivateEmail($ad);
                    $ad->status = Listing::STATUS_ACTIVE;
                }

                $ad->package_id = $packageId;
                $ad->save(false);
                $this->sendEmail($ad->listing_id);
                $transaction->commit();
            } catch (\Exception $e) {
                notify()->addError($e->getMessage());
                $error = true;
                $transaction->rollBack();
            }

            if (!$error) {
                notify()->addSuccess(t('app','Your ad was successfully created and will be active soon'));
                return $this->redirect(['account/my-listings']);
            }
        }
        app()->view->title = t('app','Package') . ' - ' . $ad->title;

        return $this->render('form_package', [
            'ad'        => $ad,
            'packages'  => $packages,
        ]);
    }

    /**
     * @return array|Response
     */
    public function actionGetSummary()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $countryId      = request()->post('country_id');
        $zoneId         = request()->post('zone_id');
        $price          = request()->post('price');

        $locationTax = Tax::calculateTax($countryId, $zoneId, $price);

        $cart = CommonHelper::getCartTotal($locationTax, $price);

        return ['result' => 'success', 'html' => $this->renderPartial('summary',
                [
                    'subtotal' => $cart['subtotal'],
                    'tax'      => $cart['locationTax'],
                    'total'    => $cart['total'],
                ]
            )
        ];
    }

    /**
     * @return array|Response
     */
    public function actionGetCategoryFields()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $categoryId = request()->post('category_id');
        $adId       = request()->post('listing_id');
        $categoryFields = CategoryField::find()->where(['category_id'=>$categoryId])->orderBy(['sort_order'=>SORT_ASC])->all();

        $typeData = [];
        foreach ($categoryFields as $field) {
            $typeData[] = [
                'type' => $field->type,
                'field' => $field,
            ];
        }

        foreach ($typeData as $data) {
            $type  = $data['type'];
            $field = $data['field'];
            if (!is_file(\Yii::getAlias('@'. str_replace('\\', '/', $type->class_name) . '.php'))) {
                continue;
            }
            $className = $type->class_name;
            $component = new $className();

            if (!($component instanceof Type)) {
                continue;
            }

            $component->field = $field;
            $component->params = [
                'categoryId' => $categoryId,
                'adId' => $adId,
            ];
            $component->handleFrontendFormDisplay();
        }

        return ['result' => 'success', 'html' => $this->renderPartial('custom-fields')];
    }

    /**
     * @return array|Response
     */
    public function actionGetSubCategory()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $categoryId = request()->post('category_id');
        if (empty($categoryId)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        $subCategory = Category::find()->select(['category_id','name'])->where(['parent_id' => (int)$categoryId])->all();

        return ['result' => 'success', 'response' => $subCategory];
    }

    /**
     * @return array|Response
     */
    public function actionGetCountryZones()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $countryId = request()->post('country_id');
        if (empty($countryId)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        $countryZones = Zone::find()->select(['zone_id','name'])->where(['country_id' => (int)$countryId])->all();

        return ['result' => 'success', 'response' => $countryZones];
    }

    /**
     * @return array|Response
     */
    public function actionRemoveAdImage()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $imageId = request()->post('key');
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        $imageId = ListingImage::findOne(['image_id' => $imageId]);
        if (empty($imageId)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        // if all images are deleted then the ad is not active anymore
        $countImages = ListingImage::find()->where(['listing_id' => $imageId->listing_id])->count();
        if ($countImages == 1) {
            $ad = Listing::find()->where(['listing_id' => $imageId->listing_id])->one();
            $ad->status = Listing::STATUS_DRAFT;
            $ad->save(false);
        }

        $imageId->delete();

        return ['result' => 'success', 'response' => $imageId];
    }

    public function actionUpload()
    {
        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $error = null;
        $models = [];

        $images = new ListingImage();
        $transaction = db()->beginTransaction();

        try {
            $models = $images->handleUploadedImages((int)request()->post('id', 0));
            $transaction->commit();

        } catch (\Exception $e) {
            $error = $e->getMessage() . ' at ' . $e->getFile() . ":" . $e->getLine() . PHP_EOL . $e->getTraceAsString();
            $transaction->rollBack();
        }

        if (!$error && $models) {
            $model = reset($models);
            $expPath = explode('/', $model->image_path);
            $imageName = $expPath[4];
            $resp = [
                'initialPreview' => [
                    $model->image_path
                ],
                'initialPreviewConfig' => [[
                    'caption' => $imageName,
                    'url' => url(['/listing/remove-ad-image']),
                    'key' => $model->image_id,
                    'exif' => null,
                ]]
            ];
        } else {
            $resp = ['error' => $error ? $error : ($images->hasErrors() ? $images->getFirstErrors() : 'Unknown')];
        }

        return $resp;
    }

    /**
     * @return array|Response
     */
    public function actionSortAdImages()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $images = request()->post('images');
        $images = json_decode($images);

        if (empty($images)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        foreach ($images as $sortOrder => $image) {
            $_image = ListingImage::findOne(['image_id' => $image->key]);
            $_image->sort_order = $sortOrder + 1;
            $_image->save(false);
        }

        return ['result' => 'success'];
    }

    /**
     * @return array|Response
     */
    public function actionGetCustomerContact()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $customerId = request()->post('customer_id');
        if (empty($customerId)) {
            return ['result' => 'error', 'html' => t('app', 'Something went wrong...')];
        }

        $customerEmail = Customer::find()->select(['email', 'phone'])->where(['customer_id' => $customerId])->one();

        return ['result' => 'success', 'response' => $customerEmail];
    }

    /**
     * @return array|Response
     */
    public function actionToggleFavorite()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        if (app()->customer->isGuest) {
            return ['result' => 'error', 'msg' => t('app', 'You need to be logged in to add favorites')];
        }

        $customer_id = app()->customer->id;
        if (empty($customer_id)) {
            return ['result' => 'error', 'msg' => t('app', 'Something went wrong...')];
        }

        $adId = request()->post('listing_id');
        $ad = Listing::findOne($adId);
        if (empty($ad)) {
            return ['result' => 'error', 'msg' => t('app', 'Something went wrong...')];
        }

        $adFavorite = ListingFavorite::find()->where(['listing_id' => $adId, 'customer_id' => $customer_id])->one();
        if (!empty($adFavorite)) {
            $adFavorite->delete();

            return ['result' => 'success', 'action' =>'added', 'msg' => t('app', 'Removed from Favorites')];
        }

        $adFavorite              = new ListingFavorite();
        $adFavorite->listing_id       = $adId;
        $adFavorite->customer_id = $customer_id;
        $adFavorite->save();

        return ['result' => 'success', 'action' =>'removed', 'msg' => t('app', 'Added to Favorites')];
    }

    /**
     * @return array|Response
     */
    public function actionDelete()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        if (app()->customer->isGuest) {
            return ['result' => 'error', 'msg' => t('app', 'You need to be logged in to make this action')];
        }

        $customer_id = app()->customer->id;
        if (empty($customer_id)) {
            return ['result' => 'error', 'msg' => t('app', 'Something went wrong...')];
        }

        $adId = request()->post('listing_id');
        $ad = Listing::find()->where(['listing_id'=>$adId, 'customer_id' => $customer_id])->one();
        if (empty($ad)) {
            return ['result' => 'error', 'msg' => t('app', 'Something went wrong...')];
        }

        if ($ad->delete()) {
            return ['result' => 'success', 'msg' => t('app', 'Your Ad was deleted!')];
        }
        return ['result' => 'error', 'msg' => t('app', 'We couldn\'t delete your ad, please try again later')];
    }

    /**
     * @return array|Response
     */
    public function actionTrackStats()
    {
        /* allow only ajax calls */
        if (!request()->isAjax) {
            return $this->redirect(['listing/post']);
        }

        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $adId = request()->post('listing_id');
        $statsType = request()->post('stats_type');
        $ad = Listing::find()->where(['listing_id'=>$adId])->one();
        if (empty($ad)) {
            return ['result' => 'error', 'msg' => t('app', 'Something went wrong...')];
        }

        ListingStat::track($statsType, $adId);
        return ['result' => 'success'];
    }

    public function actionSetMessage()
    {
        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $listing_id = request()->post('listing_id');
        $customer_id = request()->post('customer_id');
        $messagesList = [];
        $chatIds = [];
        $model = Chat::find()
            ->where(['listing_id' => $listing_id])
            ->andWhere(['to_customer_id' => $customer_id])
            ->orderBy('chat_id ASC')
            ->all();
        foreach ($model as $val) {
            $chatIds[] = $val->chat_id;
        }
        $dt = Chat::updateAll(['status' => Chat::STATUS_READ], ['in', 'chat_id', $chatIds]);

        return [ 'status' => 'success', 'result' => $dt ];
    }

    public function actionLoadMessage()
    {
        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $listing_id = (int)request()->post('listing_id');
        $customer_id = (int)request()->post('customer_id');
        $user_id = (int)request()->post('user_id');
        $sender = Customer::findOne($customer_id);
        $messagesList = [];
        $data = [];
        $model = Chat::find()
            ->where(['listing_id' => $listing_id])
            ->andWhere(['to_customer_id' => $customer_id])
            ->orderBy('chat_id ASC')
            ->all();
        foreach ($model as $value) {
            $messagesList['listing_id'] = $value->listing_id;
            $messagesList['chat_id'] = $value->chat_id;
            $messagesList['message'] = $value->message;
            $messagesList['timestamp'] = $value->timestamp;
            $messagesList['first_name'] = $value->fromCustomer->first_name;
            $data[] = $messagesList;
        }

        return [ 'status' => 'success', 'result' => $data, 'sender' => $customer_id == $user_id ? $data[0]['first_name'] : $sender->first_name ];
    }

    public function actionSaveMessage()
    {
        $chat = new Chat();
        /* set the output to json */
        response()->format = Response::FORMAT_JSON;

        $listing_id = request()->post('listing_id');
        $to_customer_id = request()->post('to_user_id');
        $from_customer_id = request()->post('from_user_id');
        $message = request()->post('chat_message');

        // if ($to_customer_id == $from_customer_id) {
        //     $dataChat = Chat::find()
        //         ->where(['listing_id' => $listing_id])
        //         ->andWhere([])
        //         ->one();
        // }

        $chat->listing_id = $listing_id;
        $chat->to_customer_id = $to_customer_id;
        $chat->from_customer_id = $from_customer_id;
        $chat->message = $message;
        $chat->created_at = date('Y-m-d H:i:s');
        if ($chat->save()) {
            $model = Chat::findOne($chat->chat_id);
            return [ 'status' => 'success', 'result' => $model, 'sender' => app()->customer->identity->first_name ];
        }

        // die(var_dump($adId));

        return [ 'status' => 'success' ];
    }

    public function actionSendAdEmail()
    {
        $id = request()->post('user_id');
        $message = request()->post('message');
        $sender_id = request()->post('sender_id');
        $model = Customer::findOne($id);
        $modelSender = Customer::findOne($sender_id);
        
        try {
            // send email
            $email = mailer()->compose(['html' =>'message-email'],['model'=>$model, 'message'=>$message, 'sender'=>$modelSender])
                ->setFrom(['admin@worldwideadverts.info' => 'Administrator Worldwideadverts'])
                ->setTo($model->email)
                ->setSubject("You've received a new messages")
                ->send();
            // if ($email->send()) {
            //     return ['result' => 'success', 'response' => $model];
            // }
        } catch (Exception $e) {
            throw new Exception("Error Processing Request", $e);
        }

        return true;
    }
    
    public function actionUpdateads($slug)
    {
        $listingIds = [];
        $model = Listing::find()
            ->where(['status' => 'active'])
            ->andWhere(['<=', 'created_at', $year])
            ->orderBy('listing_id ASC')
            ->all();
        die(var_dump($model->createCommand()->getRawSql()));
        foreach ($model as $val) {
            $listingIds[] = $val->listing_id;
        }
        
        $listing = Listing::updateAll(['status' => Listing::STATUS_EXPIRED], ['in', 'listing_id', $listingIds]);
    }

    /**
     * @param $slug
     * @return Listing
     * @throws NotFoundHttpException
     */
    protected function findAdBySlug($slug)
    {
        if (($ad = Listing::findOne(['slug' => $slug])) !== null) {
            return $ad;
        }

        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }

    /**
     * @param $id
     * @return Listing
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Listing::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
    }

    protected function sendEmail($id)
    {
        $model = $this->findModel($id);
        
        try {
            // send email
            $email = mailer()->compose(['html' =>'ad-email'],['model'=>$model])
                ->setFrom(['admin@worldwideadverts.info' => 'Administrator Worldwideadverts'])
                ->setTo($model->customer->email)
                ->setSubject('Your Ad is Active Now')
                ->send();
            // if ($email->send()) {
            //     return ['result' => 'success', 'response' => $model];
            // }
        } catch (Exception $e) {
            throw new Exception("Error Processing Request", $e);
        }

        return true;
    }
}