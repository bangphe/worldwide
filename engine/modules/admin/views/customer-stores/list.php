<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;


$this->title = t('app', 'Customer Stores');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?= Html::a(t('app', 'Refresh'), ['index'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin([
            'enablePushState'=>true,
        ]); ?>
        <?= GridView::widget([
            'id' => 'customer-stores',
            'tableOptions' => [
                'class' => 'table table-bordered table-hover table-striped',
            ],
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'store_name',
                'company_name',
                [
                    'attribute' => 'customer_id',
                    'value' => function($model) {
                        $customer = $model->customer;
                        return $customer ? $customer->fullName : 'Unknown';
                    },
                ],
                [
                    'attribute'=>'created_at',
                    'filter'=>  DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'options'=>[
                            'class'=>'form-control',
                        ],
                        'dateFormat' => 'yyyy-MM-dd',
                    ])
                ],
                [
                    'attribute'=>'status',
                    'value'=> 'status',
                    'filter' => Html::activeDropDownList($searchModel, 'status', \app\yii\db\ActiveRecord::getStatusesList(),['class'=>'form-control','prompt' => 'All'])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => [
                        'style'=>'width:100px',
                        'class'=>'table-actions'
                    ],
                    'template' => '{view} {update} {delete}',
                    'buttons'  => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="fa fa-eye"></i>',['view','id'=>$model->store_id],['class'=>'btn btn-default m-btn m-btn--icon m-btn--icon-only']);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil-alt"></i>',['update','id'=>$model->store_id],['class'=>'btn btn-success m-btn m-btn--icon m-btn--icon-only']);
                        },
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
