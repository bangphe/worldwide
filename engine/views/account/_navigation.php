<?php
use app\helpers\SvgHelper;
$segment = app()->controller->action->id;
?>
<section class="my-account-navigation">
    <h1><?=t('app', 'My Account');?></h1>
    <div class="myaccount-menu">
        <div id="account-nav">
            <a href="<?= url(['account/my-listings']);?>" class="<?=($segment == 'my-listings') ? 'active' : '';?>"><?= t('app','My Ads');?></a>
            <a href="<?= url(['account/favorites']);?>" class="<?=($segment == 'favorites') ? 'active' : '';?>"><?= t('app','Favorite Ads');?></a>
            <a href="<?= url(['account/invoices']);?>" class="<?=($segment == 'invoices') ? 'active' : '';?>"><?= t('app','Invoices');?></a>
            <a href="<?= url(['account/info']);?>" class="<?=($segment == 'info') ? 'active' : '';?>"><?= t('app','Account Info');?></a>
        </div>
    </div>
    <div class="myaccount-menu-mobile">
        <div class="menu-subtitles">
            <ul>
                <li class="<?=($segment == 'my-listings') ? 'active' : '';?>">
                    <a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false">
                        <?= t('app','My Ads');?><?= SvgHelper::getByName('arrow-top');?><?= SvgHelper::getByName('arrow-bottom');?>
                    </a>
                </li>
                <li class="<?=($segment == 'favorites') ? 'active' : '';?>">
                    <a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false">
                        <?= t('app','Favorite Ads');?><?= SvgHelper::getByName('arrow-top');?><?= SvgHelper::getByName('arrow-bottom');?>
                    </a>
                </li>
                <li class="<?=($segment == 'invoices') ? 'active' : '';?>">
                    <a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false">
                        <?= t('app','Invoices');?><?= SvgHelper::getByName('arrow-top');?><?= SvgHelper::getByName('arrow-bottom');?>
                    </a>
                </li>
                <li class="<?=($segment == 'info') ? 'active' : '';?>">
                    <a href="javascript:;" data-toggle="collapse" data-target="#menu-options" aria-expanded="false">
                        <?= t('app','Account Info');?><?= SvgHelper::getByName('arrow-top');?><?= SvgHelper::getByName('arrow-bottom');?>
                    </a>
                </li>
            </ul>
        </div>
        <div id="menu-options" class="menu-options collapse">
            <ul>
                <li class="<?=($segment == 'my-listings') ? 'active' : '';?>"><a href="<?= url(['account/my-listings']);?>"><?= t('app','My Ads');?></a></li>
                <li class="<?=($segment == 'favorites') ? 'active' : '';?>"><a href="<?= url(['account/favorites']);?>"><?= t('app','Favorite Ads');?></a></li>
                <li class="<?=($segment == 'invoices') ? 'active' : '';?>"><a href="<?= url(['account/invoices']);?>"><?= t('app','Invoices');?></a></li>
                <li class="<?=($segment == 'info') ? 'active' : '';?>"><a href="<?= url(['account/info']);?>"><?= t('app','Account Info');?></a></li>
            </ul>
        </div>
    </div>
</section>