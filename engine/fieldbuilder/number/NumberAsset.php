<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\fieldbuilder\number;

use yii\web\AssetBundle;

class NumberAsset extends AssetBundle
{
    public $sourcePath = '@app/fieldbuilder/number/assets';

    public $css = [

    ];
    public $js = [
        'field.js',
    ];

    public $depends = [
        'app\assets\AdminLteAsset',
    ];
}