<!--googleoff: all-->
<script src="<?= Yii::getAlias('@web/assets/cookies/cookies.js');?>"></script>
<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#f9fafb",
      "text": "#838391"
    },
    "button": {
      "background": "#4b81e8"
    }
  },
  "theme": "classic",
  // "position": "bottom-right",
  "content": {
    "message": "<b>Information about cookies</b><div class='cc-space'><br></div>This website is using cookies. By continuing to browse the site, you are agreeing to our use of cookies.",
    "dismiss": "I accept",
    "link": "Find out more",
    "href": "/page/cookie-policy"
  }
})});
</script>
<!--googleon: all-->