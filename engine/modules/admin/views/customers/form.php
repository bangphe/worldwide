<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Zone */

if ($action == 'update') {
    $this->title = t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Customer',
        ]) . $model->first_name . ' ' .$model->last_name;
    $this->params['breadcrumbs'][] = ['label' => t('app', 'Customers'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->first_name . ' ' .  $model->last_name, 'url' => ['view', 'id' => $model->customer_id]];
    $this->params['breadcrumbs'][] = t('app', 'Update');
}else{
    $this->title = t('app', 'Create Customer');
    $this->params['breadcrumbs'][] = ['label' => t('app', 'Customers'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="row">
    <div class="col-md-12 listings-packages-<?= $action; ?>">
        <div class="m-portlet m-portlet--head">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-placeholder-2"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?= Html::encode($this->title); ?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools"></div>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="m-portlet__body">
                <div class="users-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'status')->dropDownList([ 'active' => 'active', 'inactive' => 'inactive', ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'password', [
                                'template'      => '{input} {error}',
                            ])->passwordInput(['placeholder' => t('app','Password'), 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'passwordConfirm', [
                                'template'      => '{input} {error}',
                            ])->passwordInput(['placeholder' => t('app','Retype Password'), 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot">
                <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?= Html::a(t('app', 'Cancel'), ['index'], ['class' => 'btn btn-xs btn-danger']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
