$(document).ready(function(){
    $('[data-toggle = "popover"]').popover();

    $(document).ajaxComplete(function() {
        $('[data-toggle = "popover"]').popover();
    });

    $('[data-switch-style = "bootstrap"]').bootstrapSwitch();

    /**
     * Header
     */
    // custom toggle adminlte
    $(document).on('click','.custom-toggle',function () {
        $('body').addClass('sidebar-mini');
    });


    /**
     * Categories Page
     */
    // confirm dialog if save and no fields set
    $(document).on('submit','.categories-create form, .categories-update form',function () {
        if((!$('.list-fields').find('.field-row').length) && (!$('.list-fields-parent').find('.field-row').length)){
            if(!confirm($(this).data('message'))){
                return false;
            }
        }
    });

    // switch load parent fields
    $(document).on('switchChange.bootstrapSwitch','#categories-switch-inherit-parent-fields',function(e, state){
        var parentId = $(this).data('parent-id');
        var parentPath = $(this).data('parent-path');
        var noParentFieldMsg = $(this).data('no-parent-field-msg');

        $('.list-fields').find('#parentHasNoFieldsMsg').remove();
       if(state) {
           $('.list-fields').find('.callout.callout-warning').hide();

           $('.categories-fields-update > div:first-child, .categories-fields-create > div:first-child').after('<div class="box-body list-fields-parent"></div>');

           $('.list-fields-parent').load(parentPath + '?id=' + parentId + ' .list-fields > .field-row', function (response, status, xhr) {
               // change input hidden of data received to not overwrite
               $('.list-fields-parent').find('input[type=hidden]').val('0');
               if($('.list-fields-parent').is(':empty')) {
                   $('.list-fields').prepend('<div id="parentHasNoFieldsMsg" class="callout callout-warning">' + noParentFieldMsg + '</div>');
                   if(!$('.list-fields').find('.field-row').length){
                       $('.list-fields .callout.callout-warning').show();
                   }
               }
           });
       } else {
           $('.categories-fields-update .list-fields-parent, .categories-fields-create .list-fields-parent').remove();
           if(!$('.list-fields').find('.field-row').length){
               $('.list-fields .callout.callout-warning').show();
           }
       }
    });

    // change data-parent-id on create category to work with switch load fields
    $(document).on('change','.categories-create #category-parent_id, .categories-update #category-parent_id', function () {
        if($(this).val()) {
            $('#categories-switch-inherit-parent-wrapper').show();
            $('#categories-switch-inherit-parent-fields').data('parent-id',$(this).val());
        } else {
            $('#categories-switch-inherit-parent-wrapper').hide();
        }
    });

    // select icon for a category
    $(document).on('click','.fa-icons > a',function(e){
        e.preventDefault();
        var selectedIcon = $(this).data('icon');
        $('.fa-icons a').removeClass('active');
        $(this).addClass('active');
        $(this).parents('.row').find('input[type=hidden]#category-icon').val(selectedIcon);
    });

    /**
     * Pages page
     */
    var currentSlug = '';

    $('#sluggable-title').on('change', function () {
        var currentValue = $(this).val();

        if (currentValue && !currentSlug) {
            $('#sluggable-slug').val(getSlug(currentValue));
        }
    });

    $('#sluggable-slug').on('input', function () {
        currentSlug = $(this).val();
    });

    // javascript range to generate range like php function
    var range = function(start, end, step) {
        var range = [];
        var typeofStart = typeof start;
        var typeofEnd = typeof end;

        if (step === 0) {
            throw TypeError("Step cannot be zero.");
        }

        if (typeofStart == "undefined" || typeofEnd == "undefined") {
            throw TypeError("Must pass start and end arguments.");
        } else if (typeofStart != typeofEnd) {
            throw TypeError("Start and end arguments must be of same type.");
        }

        typeof step == "undefined" && (step = 1);

        if (end < start) {
            step = -step;
        }

        if (typeofStart == "number") {
            while (step > 0 ? end >= start : end <= start) {
                range.push(start);
                start += step;
            }
        } else {
            throw TypeError("Only number type are supported");
        }

        return range;
    };

    // retrieve list for sort pages in section
    $('#page-section').on('change', function (e) {
        $.post($('#page-sort_order').data('url'), {section_id: $(this).val(), page_id: $('#page-sort_order').data('page-id')}, function (json) {
            $('#page-sort_order option:gt(0)').remove();
            // generate new range like in action, to sort result in right way
            // because of incorrect sort of keys in javascript
            var rangeToOrderResponse = range(-30, 30, 1);

            if (json.result == 'success') {
                rangeToOrderResponse.forEach(function (key) {
                    if (key in json.response) {
                        $('#page-sort_order').append($('<option>').text(json.response[key]).attr('value', json.response[key]));
                    }
                });
            }
        }, 'json');
    });

    /**
     * Taxes Page
     */

    $(document).on('change','#tax-country_id',function (e) {
        e.preventDefault();

        $.post($('#taxes-select-zones-wrapper').data('url'),{country_id:$(this).val()},function (json) {
            $('#taxes-select-zones-wrapper select option.gen-op').remove();
            $.each(json.response,function(key,value){
                $('#taxes-select-zones-wrapper select').append($('<option class="gen-op">').text(value.name).attr('value', value.zone_id));
            });
        }, 'json');
    });

    setTimeout(function () {
        //autocomplete zone on update
        if($('select#tax-country_id').val() && $('#taxes-select-zones-wrapper').data('zone')){
            $('select#tax-country_id').val($('select#tax-country_id').val()).trigger('change');
            $(document).ajaxStop(function () {
                $('select#tax-zone_id').val($('#taxes-select-zones-wrapper').data('zone')).trigger('change');
            });
        }
    },100);

    /**
     * Gateways Page
     */

    $('.gateways-index .tab-content div:first').addClass('active');
    $('.gateways-index .nav-tabs li:first').addClass('active');

    /**
     * Settings
     */

    $('select#common-prettyurl').on('change', function(){
        var $this = $(this);
        if ($this.val() == 1) {
            $('.clean-urls-action').show();
        } else {
            $('.clean-urls-action').hide();
        }
    });

    $(document).on('click', 'button.btn-write-htaccess', function(){
        var $this = $(this);
        $this.button('loading');
        $.get($this.data('remote'), {}, function(json){
            $this.button('reset');
            var notifyContainer = notify.getOption('container');
            notify.remove();
            notify.setOption('container', '.modal-message');
            if (json.result === 'success') {
                notify.addSuccess(json.message);
            } else {
                notify.addError(json.message);
            }
            notify.show();
            notify.setOption('container', notifyContainer);
        }, 'json');
    });

});
