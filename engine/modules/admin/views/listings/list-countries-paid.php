<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\DatePicker;


$this->title = t('app', 'Ads by Location');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="m-portlet">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                    <i class="flaticon-multimedia"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    <?= Html::encode($this->title); ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <?php if(app()->user->identity->group_id == 5) : ?>
                    <?= Html::a(t('app', 'Export'), ['exportpaid'], ['class' => 'm-portlet__nav-link btn btn-info m-btn m-btn--pill m-btn--air']) ?>
                    <?php endif; ?>
                    <?= Html::a(t('app', 'Refresh'), ['paid'], ['class' => 'm-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air']) ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php Pjax::begin(['enablePushState'=>true]); ?>
        <?= GridView::widget([
            'id' => 'countries-ads',
            'tableOptions' => ['class' => 'table table-bordered table-hover table-striped'],
            'options'          => ['class' => 'table-responsive grid-view'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'label' => t('app', 'Country'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->country->name : t('app','No country');
                    },
                ],
                [
                    'label' => t('app', 'Zone'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->zone->name : t('app','No zone');
                    },
                ],
                [
                    'label' => t('app', 'City'),
                    'attribute' => 'location_id',
                    'value' => function($model) {
                        $location = $model->location;
                        return $location ? $location->city : t('app','No city');
                    },
                ],
                [
                    'label' => t('app', 'Package'),
                    'attribute' => 'package_id',
                    'value' => function($model) {
                        $package = $model->package;
                        return $package ? $package->title : t('app','No package');
                    },
                ],
                [
                    'label' => t('app', 'Ad Spesification'),
                    'attribute' => 'title',
                    'value' => function($model) {
                        return $model->title;
                    },
                ],
                [
                    'label' => t('app', 'Category'),
                    'attribute' => 'category_id',
                    'value' => function($model) {
                        $category = $model->category;
                        return $category->parent_id != null ? $category->getParentById($category->parent_id) : $category->name;
                    },
                ],
                [
                    'label' => t('app', 'Currency'),
                    'attribute' => 'currency_id',
                    'value' => function($model) {
                        $currency= $model->currency;
                        return $currency ? $currency->name : t('app','No currency');
                    },
                ],
                [
                    'label' => t('app', 'Price'),
                    'attribute' => 'price',
                    'value' => function($model) {
                        return html_encode($model->getPriceAsCurrency($model->currency->code));
                    }
                ],
                [
                    'label' => t('app', 'No of paid ads'),
                    'value' => function($model) {
                        return $model->getOrderCountByLocation($model->listing_id, 'paid');
                    }
                ],
                [
                    'label' => t('app', 'No of unpaid ads'),
                    'value' => function($model) {
                        return $model->getOrderCountByLocation($model->listing_id, 'pending');
                    }
                ],
                [
                    'label' => t('app', 'Total paid'),
                    'value' => function($model) {
                        $paid = $model->getOrderCountByLocation($model->listing_id, 'paid');
                        $unpaid = $model->getOrderCountByLocation($model->listing_id, 'pending');
                        return $paid + $unpaid;
                    }
                ]
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
