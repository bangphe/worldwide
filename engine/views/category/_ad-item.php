<?php
use yii\helpers\StringHelper;
use app\models\ListingStat;

$isFavorite = (!empty($model->favorite)) ? true : false;
?>

    <?php if(html_encode($model->getPriceAsCurrency($model->currency->code))!="$0.00") {?>
    <div class="price">
        <?=html_encode($model->getPriceAsCurrency($model->currency->code));?>
        <?= $model->isNegotiable() ? '<span>' . t('app', 'Negotiable') . '</span>' : ''; ?>
    </div>
    <?php } ?>

    <div class="image">
        <?= !empty($model->isPromoted) ? '<div class="promoted"><span>' . t('app', 'Promoted') . '</span></div>' : '' ?>
        <a href="#" data-listing-id="<?= (int)$model->listing_id; ?>" data-stats-type="<?= ListingStat::FAVORITE; ?>" data-add-msg="<?= t('app', 'Add to favorites'); ?>" data-remove-msg="<?= t('app', 'Remove Favorite'); ?>" data-favorite-url="<?= url(['/listing/toggle-favorite']); ?>" class="action-icon favorite-listing <?= (!$isFavorite) ? 'track-stats' : ''; ?>">
            <?php if ($isFavorite) { ?>
                <i class="fa fa-heart" aria-hidden="true"></i>
            <?php } else { ?>
                <i class="fa fa-heart-o" aria-hidden="true"></i>
            <?php } ?>
        </a>
        <?php $check_path = file_exists($_SERVER['DOCUMENT_ROOT'].Yii::getAlias('@web').$model->mainImage->image_path);
            if ($model->mainImage->image_path != NULL && $check_path) {
        ?>
            <a class="img-link" href="<?= url(['/listing/index', 'slug' => $model->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web'.$model->mainImage->image_path); ?>');">
                <img src="<?= Yii::getAlias('@web'.$model->mainImage->image_path); ?>" alt="" />
        <?php } else { ?>
            <a class="img-link" href="<?= url(['/listing/index', 'slug' => $model->slug]); ?>" style="background-image: url('<?= Yii::getAlias('@web/assets/site/img/no-image.png'); ?>');">
                <img src="<?= Yii::getAlias('@web/assets/site/img/no-image.png'); ?>" alt="" />
        <?php } ?>
        </a>
    </div>
    <div class="info">
        <a href="<?= url(['category/index', 'slug' => $model->category->slug]); ?>" class="category"><i class="fa <?= html_encode($model->category->icon); ?>" aria-hidden="true"></i> <?= html_encode($model->category->name); ?></a>
        <a href="<?= url(['/listing/index/', 'slug' => $model->slug]); ?>" class="name">
            <span class="title"><?= html_encode($model->title); ?></span>
            <span><?= StringHelper::truncate(strip_tags(html_purify($model->description)), 80); ?></span>
        </a>
        <div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= html_encode($model->getZoneCountryString()); ?></div>
    </div>