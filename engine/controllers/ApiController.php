<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\controllers;

use Yii;
use app\yii\web\Controller;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{
    public function actionIndex()
    {
        $this->layout = 'blank';
        return $this->render('index', []);
    }
}