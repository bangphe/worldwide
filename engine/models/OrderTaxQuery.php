<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrderTax]].
 *
 * @see Invoice
 */
class OrderTaxQuery extends \app\models\auto\OrderTaxQuery
{

}
