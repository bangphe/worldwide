<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Currency;
use app\models\Language;

$this->title = t('app', 'General Settings');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#general"><?=t('app','General');?></a></li>
        <li><a data-toggle="tab" href="#listings"><?=t('app','Ads');?></a></li>
        <li><a data-toggle="tab" href="#seo"><?=t('app','SEO');?></a></li>
        <li><a data-toggle="tab" href="#api"><?=t('app','API Keys');?></a></li>
        <li><a data-toggle="tab" href="#page-sections"><?=t('app','Footer');?></a></li>
		
        <li class="pull-right">
            <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i>&nbsp&nbsp'.t('app', 'Cancel'), ['index'], ['class' => 'btn btn-xs btn-success']) ?>
        </li>
    </ul>
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
        <div id="general" class="tab-pane fade in active">
            <div class="box-body">
                <div class="general-settings-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteName')->textInput([
                                    'maxlength'         => true,
                                    'data-content'      => t('app', 'Represents the site in invoices and sets the title of home page'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteEmail')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Email of the site in all the invoices'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteCurrency')->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'code', 'name'),[
                                    'class'=>'form-control',
                                    'prompt' => 'Please select',
                                    'data-content'      => t('app', 'Currency used to do payments for packages'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]);?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteLanguage')->dropDownList(ArrayHelper::map(Language::find()->all(),
                                function($model, $defaultValue) {
                                    return (!empty($model->region_code)) ? $model->language_code . '-' . $model->region_code : $model->language_code;
                                },
                                'name'),[
                                        'class'=>'form-control',
                                        'prompt' => 'Please select',
                                        'data-content'      => t('app', 'Represents the site Language'),
                                        'data-container'    => 'body',
                                        'data-toggle'       => 'popover',
                                        'data-trigger'      => 'hover',
                                        'data-placement'    => 'top'
                            ]);?>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <div class="pull-right">
                        <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="clearfix"><!-- --></div>
                </div>
            </div>
        </div>
        <div id="seo" class="tab-pane fade in">
            <div class="box-body">
                <div class="general-settings-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteKeywords')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Meta Keywords of home page'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteDescription')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Meta Description of home page.'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteStatus')->dropDownList([1 => 'Online', 0 => 'Offline' ],[
                                'data-content'      => t('app', 'If set to offline, application will be locked for all frontend customers, admin will have access everywhere.'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'siteOfflineMessage')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Message to display when site status is offline.'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'googleAnalyticsCode')->textInput([
                                'maxlength' => true,
                                'data-content'      => t('app', 'Google analytics code should look like this UA-0000000-0.'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'prettyUrl')->dropDownList([1 => t('app','Yes, enable pretty url'), 0 => t('app','No, do not use pretty url') ],[
                                'data-content'      => t('app', 'Enabling this will remove the index.php part of your urls.'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group clean-urls-action" style="<?php if ($model->prettyUrl != 1){?>display:none<?php }?>">
                                <label><?= t('app', 'Action');?></label> <br />
                                <a data-toggle="modal" data-remote="<?=url(['settings/htaccess-modal']);?>" href="#writeHtaccessModal" class="btn btn-primary btn-flat"><?php echo t('app', 'Generate htaccess')?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="clearfix"><!-- --></div>
                </div>
            </div>
        </div>
        <div id="listings" class="tab-pane fade in">
            <div class="box-body">
                <div class="general-settings-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'adminApprovalAds')->dropDownList([1 => t('app','Yes'), 0 => t('app','No') ],[
                                'data-content'      => t('app', 'If it\'s set to yes then all ads will have "Pending Approval" status until an admin activates them, otherwise ads will be active immediately.'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'relatedAds')->dropDownList(['yes' => t('app','Yes'), 'no' => t('app','No') ],[
                                'data-content'      => t('app', 'If it\'s set to yes then on every ad page will show a widget of related ads to that specific ad page.'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'homePromotedNumber')->textInput([
                                'data-content'      => t('app', 'Number of pic ads to display in the  ads section (e.g. 4, 8, 12...)'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'homeNewNumber')->textInput([
                                'data-content'      => t('app', 'Number of new ads to display in the new ads section (e.g. 4, 8, 12...)'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
					 <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'homePicNumber')->textInput([
                                'data-content'      => t('app', 'Number of promoted ads to display in the promoted ads section (e.g. 4, 8, 12...)'),
                                'data-container'    => 'body',
                                'data-toggle'       => 'popover',
                                'data-trigger'      => 'hover',
                                'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="clearfix"><!-- --></div>
                </div>
            </div>
        </div>
        <div id="api" class="tab-pane fade in">
            <div class="box-body">
                <div class="general-settings-form">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'siteFacebookId')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'ID from Facebook app used for Social Login'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'siteFacebookSecret')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'API Secret key from Facebook app used for Social Login'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'siteGoogleMapsKey')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'API Key from Google app used for maps all over the site'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="clearfix"><!-- --></div>
                </div>
            </div>
        </div>
        <div id="page-sections" class="tab-pane fade in">
            <div class="box-body">
                <div class="general-settings-form">
                    <div class="row">
                        <div class="col-lg-6">
                            <?= $form->field($model, 'footerFirstPageSectionTitle')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Title of the first middle widget in the footer'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                        <div class="col-lg-6">
                            <?= $form->field($model, 'footerSecondPageSectionTitle')->textInput([
                                    'maxlength' => true,
                                    'data-content'      => t('app', 'Title of the second middle widget in the footer'),
                                    'data-container'    => 'body',
                                    'data-toggle'       => 'popover',
                                    'data-trigger'      => 'hover',
                                    'data-placement'    => 'top'
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="pull-right">
                        <?= Html::submitButton(t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <div class="clearfix"><!-- --></div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<!-- MODAL HTACCESS -->
<div class="modal fade" id="writeHtaccessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>