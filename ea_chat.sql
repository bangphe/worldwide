# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Database: easyads2018
# Generation Time: 2019-07-02 23:56:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ea_chat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ea_chat`;

CREATE TABLE `ea_chat` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL,
  `to_customer_id` int(11) DEFAULT NULL,
  `from_customer_id` int(11) DEFAULT NULL,
  `message` text,
  `status` varchar(10) DEFAULT 'unread',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`chat_id`),
  KEY `listing_id` (`listing_id`),
  KEY `to_customer_id` (`to_customer_id`),
  KEY `from_customer_id` (`from_customer_id`),
  CONSTRAINT `ea_chat_ibfk_1` FOREIGN KEY (`listing_id`) REFERENCES `ea_listing` (`listing_id`) ON UPDATE CASCADE,
  CONSTRAINT `ea_chat_ibfk_2` FOREIGN KEY (`to_customer_id`) REFERENCES `ea_customer` (`customer_id`) ON UPDATE CASCADE,
  CONSTRAINT `ea_chat_ibfk_3` FOREIGN KEY (`from_customer_id`) REFERENCES `ea_customer` (`customer_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
