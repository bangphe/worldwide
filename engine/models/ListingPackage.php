<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * Class ListingPackage
 * @package app\common\models
 */
class ListingPackage extends \app\models\auto\ListingPackage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price', 'listing_days', 'promo_days', 'auto_renewal', 'pictures'], 'trim'],
            [['title'], 'unique', 'targetAttribute' => 'title'],
            [['price', 'listing_days', 'promo_days', 'auto_renewal', 'pictures'], 'integer'],
            [['promo_show_featured_area','promo_show_promoted_area','promo_show_at_top','promo_sign','recommended_sign'], 'string', 'max' => 3],
            [['title'], 'string', 'max' => 20],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'title'                     => t('app', 'Package title'),
            'price'                     => t('app', 'Package Price'),
            'listing_days'              => t('app', 'Days ad expires'),
            'promo_days'                => t('app', 'Days promo feature expires'),
            'promo_show_featured_area'  => t('app', 'Ad Featured'),
            'promo_show_promoted_area'  => t('app', 'Ad Promoted'),
            'promo_show_at_top'         => t('app', 'Ad priority'),
            'promo_sign'                => t('app', 'Ad Promo label'),
            'recommended_sign'          => t('app', 'Package Recommended label'),
            'auto_renewal'              => t('app', 'Auto renew'),
            'pictures'              => t('app', 'Amount Of Pictures'),
        ]);
    }
}