<?php
use app\models\Category;

$categories =  Category::getTopCatsWithFirstLevelChildren();

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.navbarheader-wrapper {
    background-color: #5042f4;
}

.navbarheader {
    overflow: hidden;
    background-color: #5042f4;
    font-family: Arial;
    display: table;
    margin: 0 auto;
    min-width: fit-content;
}

.navbarheader a {
    float: left;
    font-size: 14px;
    color: white;
    text-align: center;    
    text-decoration: none;
}

.dropdownheader {
    float: left;
    overflow: hidden;
}

.dropdownheader .dropbtnheader {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding -top: 16px;
    background-color: inherit;
}

.navbarheader a:hover, .dropdownheader:hover .dropbtnheader {
    background-color: #ff9900;
}

.dropdownheader-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 4;
}

.dropdownheader-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdownheader-content a:hover {
    background-color: #ddd;
}

.dropdownheader:hover .dropdownheader-content {
    display: block;
}
</style>

<!-- <?php if (Yii::$app->controller->id == 'account') { ?>
  <div></div>
<?php } else { ?>
<div class="navbarheader-wrapper">
  <div class="navbarheader">
  <?php foreach ($categories as $key) { ?>
    <div class="dropdownheader">
      <button class="dropbtnheader">
        <a href="<?= empty($key->children) ? url(['category/index', 'slug' => $key['slug']]) : '#main-categ-' . (int)$key['category_id'] ?>">
          <span><i class="fa <?= html_encode($key['icon']); ?>" ></i></span>
          <span><?= html_encode($key['name']); ?></span>
        </a>
      </button>

    <?php
      $childCats = [];
      $childCats[$key->category_id] = $key['children'];
      
      if (!empty($childCats)) {
       foreach ($childCats as $parentId => $cats) {
     ?>
         <div class="dropdownheader-content">
           <?php foreach ($cats as $childKey => $childCat) { ?>
              <a href="<?= empty($key->children) ? url(['category/index', 'slug' => $childCat->slug]) : '#main-categ-' . (int)$childCat->category_id ?>" data-toggle="" class="categ-item customclassheaderlink"><span><i class="fa <?= html_encode($childCat->icon); ?>" aria-hidden="true"></i></span><span><?= html_encode($childCat->name); ?></span></a>
           <?php } ?>
         </div>
         <?php
       }
    }
  ?>
  </div>
  <?php
  }
  ?>
  </div>
</div>
<?php } ?> -->

<!-- <div class="categories-mobile">
  <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <a href="javascript:;" class="browse collapsed" data-toggle="collapse" data-target="#category-list" aria-expanded="false">Browse categories</a>
          <div id="category-list" class="collapse" aria-expanded="false" style="height: 0px;">
            <?php foreach ($categories as $category) : ?>
              <div class="panel">
                  <div class="panel-heading">
                    <?php if (!empty($category->children)) : ?>
                      <a href="<?= '#main-categ-' . (int)$category['category_id'] ?>" data-toggle="collapse" data-parent="#category-list" class="collapsed" aria-expanded="false">
                        <span><i class="fa <?= html_encode($category['icon']); ?>" aria-hidden="true"></i></span>
                        <span><?= html_encode($category['name']); ?></span>
                      </a>
                    <?php else : ?>
                      <a href="<?= url(['category/index', 'slug' => $category['slug']]); ?>"><span><i class="fa fa-diamond" aria-hidden="true"></i></span>
                        <span><?= html_encode($category['name']); ?></span>
                      </a>
                    <?php endif; ?>
                  </div>
                  <?php if (!empty($category->children)) : ?>
                    <div id="<?= 'main-categ-' . (int)$category['category_id']; ?>" class="panel-collapse collapse" aria-expanded="false">
                      <ul>
                          <?php foreach ($category->children as $categoryChild) : ?>
                          <li><a href="<?= url(['category/index', 'slug' => $categoryChild['slug']]); ?>"><?= html_encode($categoryChild['name']); ?></a></li>
                          <?php endforeach; ?>
                      </ul>
                    </div>
                  <?php endif; ?>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
    </div>
  </div>
</div> -->