<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\modules\admin\controllers;

use app\models\Advertisement;
use app\models\AdvertisementSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Controls the actions for advertisement section
 *
 * @Class AdvertisementsController
 * @package app\modules\admin\controllers
 */
class AdvertisementsController extends \app\modules\admin\yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all advertisements
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AdvertisementSearch();
        $dataProvider = $searchModel->search(request()->queryParams);
        $dataProvider->pagination->pageSize=10;

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a specific advertisement
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new advertisement
     *
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Advertisement();

        if ($model->load(request()->post()) && $model->save()) {
            //$model->sendRegistrationEmail();
            notify()->addSuccess(t('app','Your action is complete.'));
            return $this->redirect(['view', 'id' => $model->advertisement_id]);
        } else {
            return $this->render('form', [
                'action'=> 'create',
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates a specific advertisement
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $advertisement = request()->post('Advertisement');

        if ($model->load(request()->post()) && $model->save()) {
            notify()->addSuccess(t('app','Your action is complete.'));
            return $this->redirect(['view', 'id' => $model->advertisement_id]);
        } else {
            return $this->render('form', [
                'action'=> 'update',
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes a specific advertisement
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->deactivate();

        notify()->addSuccess(t('app','Your action is complete.'));

        return $this->redirect(['/admin/advertisements']);
    }

    /**
     * Handles the activation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        $model->status = Advertisement::STATUS_ACTIVE;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    /**
     * Handles the deactivation for a specific ad
     *
     * @param $id
     * @return void|\yii\web\Response
     */
    public function actionDeactivate($id)
    {
        $model = $this->findModel($id);
        $model->status = Advertisement::STATUS_DEACTIVATED;
        if (!$model->save(false)) {
            notify()->addError(t('app','Couldn\'t complete your action, please try again later.'));
            return;
        }
        notify()->addSuccess(t('app','Your action is complete'));
        return $this->redirect(request()->referrer);
    }

    /**
     * @param $id
     * @return static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Advertisement::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
