<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
$theme = Url::base().'/assets/metronic';
$request = Yii::$app->request;
if ($request->enableCsrfValidation) {
    $this->registerMetaTag(['name' => 'csrf-param', 'content' => $request->csrfParam]);
    $this->registerMetaTag(['name' => 'csrf-token', 'content' => $request->getCsrfToken()]);
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>World Wide Adverts | <?= $this->title; ?></title>
        <meta name="description" content="worldwideadverts.info">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <?php $this->head() ?>
        <!--begin::Web font -->

        <!-- <script src="<?= $theme; ?>/admin/js/jquery.min.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!--end::Web font -->

        <!--begin::Base Styles -->  
        <link href="<?= $theme; ?>/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/demo/demo11/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?= $theme; ?>/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
        <!--end::Base Styles -->
        <link rel="icon" type="image/png" sizes="16x16" href="<?= $theme; ?>/favicon.ico">
    </head>
    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="m-content--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside--offcanvas-default">
        <?php $this->beginBody() ?>
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        <header id="m_header" class="m-grid__item m-header"  m-minimize-offset="200" m-minimize-mobile-offset="200" style="background-color: #fff !important;">
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">     
                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-light " style="background-color: #fff !important;">
                        <div class="m-stack m-stack--ver m-stack--general m-stack--fluid">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="<?= url(['/admin/dashboard']); ?>" class="m-brand__logo-wrapper">
                                    <img alt="" src="<?= $theme; ?>/logo-ww.png"/>
                                </a>  
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>
                                <!-- END -->
                                
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>
                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>
                    <!-- END: Brand -->         
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                        <!-- BEGIN: Topbar -->
                        <div id="m_header_topbar" class="m-topbar m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline">
                                    <li class="m-nav__item m-topbar__user-profile  m-dropdown m-dropdown--medium m-dropdown--arrow  m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__userpic">
                                                <img src="<?= $theme; ?>/apple-touch-icon.png" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                            </span>
                                            <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                                <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                            </span>
                                            <span class="m-topbar__username m--hide">Nick</span>                    
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center">
                                                    <div class="m-card-user m-card-user--skin-light">
                                                        <div class="m-card-user__pic">
                                                            <img src="<?= $theme; ?>/apple-touch-icon.png" class="m--img-rounded m--marginless" alt=""/>
                                                        </div>
                                                        <div class="m-card-user__details">
                                                            <span class="m-card-user__name m--font-weight-500"><?= ucwords(app()->user->identity->first_name) . ' ' . ucwords(app()->user->identity->last_name);?></span>
                                                            <a href="" class="m-card-user__email m--font-weight-300 m-link"><?= app()->user->identity->email; ?></a>
                                                            <span class="m-card-user__email m--font-weight-300"><?= ucwords(app()->user->identity->group->name);?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav m-nav--skin-light">
                                                            <li class="m-nav__section m--hide">
                                                                <span class="m-nav__section-text">Section</span>
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="<?=url(['/admin/users/view/', 'id'=>app()->user->id]);?>"" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                    <span class="m-nav__link-title">  
                                                                        <span class="m-nav__link-wrap">      
                                                                            <span class="m-nav__link-text">My Profile</span>      
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <li class="m-nav__separator m-nav__separator--fit">
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <?= Html::a(
                                                                    'Logout',
                                                                    ['/admin/logout'],
                                                                    ['data-method' => 'post', 'class' => 'btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder']
                                                                ) ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- END: Topbar -->
                    </div>
                </div>
            </div>
        </header>
        <!-- END: Header -->

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
            
            <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-light ">   
                <!-- BEGIN: Aside Menu -->
                <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">       
                    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                        <li class="m-menu__section m-menu__section--first">
                            <h4 class="m-menu__section-text">Administrator</h4>
                            <i class="m-menu__section-icon flaticon-more-v3"></i>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'dashboard' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/dashboard/index']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-home"></i>
                                <span class="m-menu__link-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'listings' && (Yii::$app->controller->action->id == 'index' || Yii::$app->controller->action->id == 'pendings' || Yii::$app->controller->action->id == 'expired') ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-edit"></i>
                                <span class="m-menu__link-text">Ads Listing</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Ads Listing</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'index') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/listings']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">All Ads</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'pendings') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/listings/pendings']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Pending Ads</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'expired') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/listings/expired']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Expired Ads</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'advertisements' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/advertisements']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-newspaper"></i>
                                <span class="m-menu__link-text">Advertisements</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'affiliates' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/affiliates']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-bullhorn"></i>
                                <span class="m-menu__link-text">Affiliates</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'listings-packages' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/listings-packages']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-clipboard-list"></i>
                                <span class="m-menu__link-text">Packages</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'categories' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/categories']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-keyboard"></i>
                                <span class="m-menu__link-text">Categories</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'pages' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/pages']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-columns"></i>
                                <span class="m-menu__link-text">Pages</span>
                            </a>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'paid') || (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'unpaid') ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-file-alt"></i>
                                <span class="m-menu__link-text">Reports</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Reports</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'paid') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/listings/paid']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Ads by Location</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= (Yii::$app->controller->id == 'listings' && Yii::$app->controller->action->id == 'unpaid') ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/listings/unpaid']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Performance Report</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'customers' || Yii::$app->controller->id == 'customer-stores' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-id-card-alt"></i>
                                <span class="m-menu__link-text">Customers</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Customers</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'customers' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/customers']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Customers</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'customer-stores' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/customer-stores']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Stores</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'users' || Yii::$app->controller->id == 'groups' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-sign"></i>
                                <span class="m-menu__link-text">Admins</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Admins</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'users' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/users']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Users</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'groups' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/groups']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Groups</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'settings' || Yii::$app->controller->id == 'gateways' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-tags"></i>
                                <span class="m-menu__link-text">Settings</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Settings</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'settings' && Yii::$app->controller->action->id == 'index' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/settings/index']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">General</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'gateways' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/gateways']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Gateways</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->action->id == 'invoice' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/settings/invoice']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Invoice</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->action->id == 'social' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/settings/social']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Social</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'orders' || Yii::$app->controller->id == 'order-transactions' || Yii::$app->controller->id == 'taxes' || Yii::$app->controller->id == 'invoices' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-money-bill"></i>
                                <span class="m-menu__link-text">Finance</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Finance</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'orders' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/orders']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Orders</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'order-transactions' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/order-transactions']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Transactions</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'taxes' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/taxes']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Taxes</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'invoices' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/invoices']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Invoices</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'currencies' || Yii::$app->controller->id == 'languages' || Yii::$app->controller->id == 'countries' || Yii::$app->controller->id == 'zones' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-recycle"></i>
                                <span class="m-menu__link-text">Localization</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Localization</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'currencies' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/currencies']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Currencies</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'languages' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/languages']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Languages</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'countries' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/countries']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Countries</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'zones' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/zones']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Zones</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item m-menu__item--submenu <?= Yii::$app->controller->id == 'mail-templates' || Yii::$app->controller->id == 'mail-accounts' || Yii::$app->controller->id == 'mail-queue' ? 'm-menu__item--open m-menu__item--expanded' : ''; ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                <i class="m-menu__link-icon fa fa-envelope-square"></i>
                                <span class="m-menu__link-text">Email System</span>
                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="m-menu__submenu ">
                                <span class="m-menu__arrow"></span>
                                <ul class="m-menu__subnav">
                                    <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
                                        <span class="m-menu__link">
                                            <span class="m-menu__link-text">Email System</span>
                                        </span>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'mail-templates' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true" >
                                        <a href="<?= url(['/admin/mail-templates']); ?>" class="m-menu__link m-menu__toggle">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Templates</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'mail-accounts' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/mail-accounts']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Accounts</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item <?= Yii::$app->controller->id == 'mail-queue' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                                        <a href="<?= url(['/admin/mail-queue']); ?>" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Queue</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'extensions' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/extensions']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-external-link-alt"></i>
                                <span class="m-menu__link-text">Extension Manager</span>
                            </a>
                        </li>
                        <li class="m-menu__item <?= Yii::$app->controller->id == 'admin-action-logs' ? 'm-menu__item--active' : ''; ?>" aria-haspopup="true"  m-menu-link-redirect="1">
                            <a href="<?= url(['/admin/admin-action-logs']); ?>" class="m-menu__link ">
                                <i class="m-menu__link-icon fa fa-list-alt"></i>
                                <span class="m-menu__link-text">Admin Activity Log</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->                            
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                
                <!-- BEGIN: Subheader -->
                <div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title m-subheader__title--separator"><?= $this->title; ?></h3>
                            <?= Breadcrumbs::widget([
                                'homeLink' => [
                                    'label' => '<i class="m-nav__link-icon la la-home"></i>',
                                    'url' => Yii::$app->getHomeUrl() . 'admin/default/index',
                                    'template' => '<li class="m-nav__item m-nav__item--home">{link}</li><li class="m-nav__separator">-</li>',
                                    'class'=>'m-nav__link m-nav__link--icon',
                                    'encode' => false,
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'activeItemTemplate' => '<li class="m-nav__item">{link}</li>',
                                'itemTemplate' => '<li class="m-nav__item">{link}</li><li class="m-nav__separator">-</li>', // template for all links
                                'options' => ['class'=>'m-subheader__breadcrumbs m-nav m-nav--inline'],
                            ]); ?>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->

                    <div class="m-content">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
            <!-- end:: Body -->
            
            <!-- begin::Footer -->
            <footer class="m-grid__item m-footer ">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                <?= date('Y'); ?> &copy; Worldwideadverts
                            </span>
                        </div>
                        <!-- <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                            <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                                <li class="m-nav__item">
                                    <a href="#" class="m-nav__link">
                                        <span class="m-nav__link-text">About</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="#"  class="m-nav__link">
                                        <span class="m-nav__link-text">Privacy</span>
                                    </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </footer>
            <!-- end::Footer --> 
        </div>
        <!-- end:: Page -->

        <!-- begin::Scroll Top -->
        <div id="m_scroll_top" class="m-scroll-top">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->

        <!--begin::Base Scripts -->
        <script src="<?= $theme; ?>/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?= $theme; ?>/demo/demo11/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->

        <?php $this->endBody() ?>
    </body>
    <!-- end::Body -->
</html>
<?php $this->endPage() ?>