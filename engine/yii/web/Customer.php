<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\yii\web;

use app\models\Country;
use yii\web\User as BaseUser;

/**
 * Class Customer
 * @package app\yii\web
 */
class Customer extends BaseUser
{
    protected $id_country;

    public function getCountryId()
    {
        if ($this->id_country === null && \Yii::$app->has('geoip') && $geoip = \Yii::$app->geoip) {
            $this->id_country = Country::find()
                ->select('country_id')
                ->where(['code' => $geoip->ip()->isoCode])
                ->limit(1)
                ->scalar();
        }

        return $this->id_country;
    }
}
