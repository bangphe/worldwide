<?php
use app\assets\AccountAsset;
use yii\bootstrap\ActiveForm;
use app\helpers\DateTimeHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
use app\models\Country;
use yii\helpers\Url;

AccountAsset::register($this);

echo $this->render('_navigation.php', []);

$avatarImage = ($modelAbout->avatar) ? $modelAbout->avatar : Yii::getAlias('@web/assets/admin/img/default.jpg');
?>

<div class="container">
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<?php $form = ActiveForm::begin([
					    'id'        => 'form-change-avatar',
					    'method'    => 'post',
					    'action'    => ['account/update-info-about'],
					    'enableAjaxValidation' => false,
					]); ?>
					<div class="image-upload-wrapper">
                        <div class="image-upload-error" style="display: none"></div>
                        <?= $form->field($modelAbout, 'avatarUpload')->widget(\kartik\file\FileInput::classname(), [
                            'options' => [
                                'class'=>'image-upload',
                                'data-image'=>$avatarImage,
                            ],
                            'pluginOptions' => [
                                'initialPreviewAsData'=>true,
                                'language' => options()->get('app.settings.common.siteLanguage', 'en'),
                                'uploadUrl' => Url::to(['change-avatar']),
                                'uploadExtraData' => ['customer_id' => $modelAbout->id],
                                'maxFileSize'=> 1500,
                                'showClose'=> false,
                                'showRemove' => false,
                                'showCaption'=> false,
                                'showBrowse'=> false,
                                'browseOnZoneClick'=> true,
                                'removeLabel'=> '',
                                'removeIcon'=> '<i class="glyphicon glyphicon-remove"></i>',
                                'removeTitle'=> 'Cancel or reset changes',
                                'elErrorContainer'=> '.image-upload-error',
                                'msgErrorClass'=> 'alert alert-block alert-danger',
                                'defaultPreviewContent'=> '<img src="'.$avatarImage.'" alt="Your Avatar" style="width:160px"><h6 class="text-muted" style="font-size: 12px !important;">' . t('app','Click to change picture') . '</h6>',
                                'layoutTemplates'=> ['main2'=> '{preview} {remove} {browse}'],
                                'allowedFileTypes' => ["image"],
                                'fileActionSettings' => [
                                    'showUpload' => false,
                                    'showDrag' => true,
                                    'autoOrientImage' => true,
                                ],
                                'overwriteInitial'=>false,
                            ],
                            'pluginEvents' => [
                                'filebatchselected' => new \yii\web\JsExpression(
                                'function(event, files) {
                                    jQuery(event.currentTarget).fileinput("upload");
                                }')
                            ]
                        ])->label(false);
                    ?>
                    </div>
                    <?php ActiveForm::end(); ?>
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<?= ucfirst(app()->customer->identity->first_name).' '.ucfirst(app()->customer->identity->last_name); ?>
					</div>
					<div class="profile-usertitle-job">
						<?= $modelAbout->customer_type == "Company" ? "Company" : "Customer"; ?>
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<button type="button" class="btn btn-success btn-sm">Active</button>
					<!-- <button type="button" class="btn btn-danger btn-sm">Message</button> -->
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav" id="myTabs" role="tablist">
						<li class="active">
							<a href="#home"><i class="glyphicon glyphicon-home"></i> General Information </a>
						</li>
						<li>
							<a href="#account"><i class="glyphicon glyphicon-user"></i> Account Setting </a>
						</li>
						<li>
							<a href="#tasks"><i class="glyphicon glyphicon-ok"></i> Change Password </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
			<div class="tab-content">
            	<div class="tab-pane fade in active" id="home">
            		<div class="profile-content" >
					   <?php $form = ActiveForm::begin([
                                'id'        => 'form-change-about',
                                'method'    => 'post',
                                'action'    => ['account/update-info-about'],
                                'enableAjaxValidation' => false,
                                // 'options'	=> [
                                // 	'class' => 'form-horizontal',
                                // ],
                            ]); ?>
                            <div class="row">
                            	<h4 class="text-center">General Information</h4><hr>
                            	<div class="form-group" style="margin-bottom: -8px;">
									<?php if ($modelAbout->customer_type != 'Company') { ?>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">First Name</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'first_name', [
		                                        'template'      => '{input} {error}',
			                                	])->textInput([ 'placeholder' => t('app','First Name'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
			                                ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Last Name</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'last_name', [
		                                        'template'      => '{input} {error}',
			                                	])->textInput([ 'placeholder' => t('app','Last Name'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
			                                ?>
		                                </div>
		                            </div>
									<?php } else { ?>
									<div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Company Name</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'company_name', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','Company Name'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
									<div class="col-md-6">
										<label for="title" class="col-md-5 control-label">Company Reg Number</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'company_number', [
	                                        	'template'      => '{input} {error}',
	                                    		])->textInput(['placeholder' => t('app','Company Registration Number'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
	                                    	?>
		                                </div>
	                                </div>
									<?php } ?>
		                        </div>
                            </div>
							<?php if ($modelAbout->customer_type == 'Company') { ?>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">VAT Number</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'vat_number', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','VAT Number'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
										<label for="title" class="col-md-5 control-label">Company Email</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'email', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','Company Email'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
										<label for="title" class="col-md-5 control-label">Website</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'website', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','Website'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
										<label for="title" class="col-md-5 control-label">Company Number</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'phone', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','Company Number'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                        </div>
                            </div>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Country</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'country_id', [
	                                        	'template'      => '{input} {error}',
		                                        ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6" id="listing-select-zones-wrapper" data-url=<?=url(['/listing/get-country-zones']);?> data-zone=<?=(int)$modelLocation->zone_id;?>>
		                            	<label for="title" class="col-md-5 control-label">Zone</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'zone_id', [
	                                        	'template'      => '{input} {error}',
		                                    	])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
		                                    ?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
									<div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">City</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'city', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Currency</label>
		                                <div class="col-md-12">
											<?= $form->field($modelAbout, 'currency_id', [
	                                        	'template'      => '{input} {error}',
	                                    		])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency')])->label(false);
	                                    	?>
		                                </div>
	                                </div>
		                        </div>
                            </div>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
									<div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Business Description</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'business_description', [
	                                        	'template'      => '{input} {error}',
	                                    		])->textInput(['placeholder' => t('app','Business Description'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
	                                    	?>
		                                </div>
		                            </div>
									<div class="col-md-6">
										<label for="title" class="col-md-5 control-label">Company Address</label>
										<div class="col-md-12">
											<?= $form->field($modelAbout, 'address_street', [
												'template'      => '{input} {error}',
												])->textInput(['placeholder' => t('app','Company Address'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
											?>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
							<?php if ($modelAbout->customer_type != 'Company') { ?>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Phone</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'phone', [
		                                        'template'      => '{input} {error}',
			                                	])->textInput([ 'placeholder' => t('app','Phone'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
			                                ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Gender</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'gender', [
	                                            'template'      => '{input} {error}',
	                                    		])->dropDownList([ 'M' => 'Male', 'F' => 'Female', ],['prompt' => t('app','Gender')])->label(false);
	                                    	?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
							<div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Currency</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'currency_id', [
	                                        	'template'      => '{input} {error}',
	                                    		])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency')])->label(false);
	                                    	?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Date of Birth</label>
		                                <div class="col-md-12">
											<div class="col-md-4" style="margin-left: -15px;">
												<?= $form->field($modelAbout, 'birthdayDay', [
													'template'      => '{input} {error}',
													])->dropDownList(DateTimeHelper::getStaticMonthDays(),['prompt' => t('app','Day'), 'style' => 'width: 100%;'])->label(false);
												?>
											</div>
											<div class="col-md-4" style="margin-left: -15px;">
												<?= $form->field($modelAbout, 'birthdayMonth', [
													'template'      => '{input} {error}',
													])->dropDownList(DateTimeHelper::getStaticMonths(),['prompt' => t('app','Month'), 'style' => 'width: 100%;'])->label(false);
												?>
											</div>
											<div class="col-md-4" style="margin-left: -15px;">
												<?= $form->field($modelAbout, 'birthdayYear', [
													'template'      => '{input} {error}',
													])->dropDownList(DateTimeHelper::getStaticBirthdayYears(),['prompt' => t('app','Year'), 'style' => 'width: 100%;'])->label(false);
												?>
											</div>
		                                </div>
		                            </div>
		                        </div>
                            </div>
                            <div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Country</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'country_id', [
	                                        	'template'      => '{input} {error}',
		                                        ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6" id="listing-select-zones-wrapper" data-url=<?=url(['/listing/get-country-zones']);?> data-zone=<?=(int)$modelLocation->zone_id;?>>
		                            	<label for="title" class="col-md-5 control-label">Zone</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'zone_id', [
	                                        	'template'      => '{input} {error}',
		                                    	])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
		                                    ?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
                            <div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">City</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'city', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Zip</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelLocation, 'zip', [
	                                        	'template'      => '{input} {error}',
	                                    		])->textInput(['placeholder' => t('app','Zip code'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
	                                    	?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
                            <div class="row">
                            	<div class="form-group" style="margin-bottom: -8px;">
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Address Street</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'address_street', [
		                                        'template'      => '{input} {error}',
		                                    	])->textInput(['placeholder' => t('app','Street'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
		                                    ?>
		                                </div>
	                                </div>
		                            <div class="col-md-6">
		                            	<label for="title" class="col-md-5 control-label">Address House</label>
		                                <div class="col-md-12">
		                                	<?= $form->field($modelAbout, 'address_house', [
	                                        	'template'      => '{input} {error}',
	                                    		])->textInput(['placeholder' => t('app','House'), 'class' => 'form-control', 'style' => 'width: 100%;'])->label(false);
	                                    	?>
		                                </div>
		                            </div>
		                        </div>
                            </div>
							<?php } ?>
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="submit" id="submit-account-info-about" class="btn-as" value="Submit"><?=t('app','Save Changes');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
		            </div>
            	</div>
            	<div class="tab-pane fade" id="account">
            		<div class="profile-content">
				   	<?php $form = ActiveForm::begin([
                        'id'        => 'form-change-email',
                        'method'    => 'post',
                        'action'    => ['account/update-email'],
                        'enableAjaxValidation' => false,
                    ]); ?>
                    <div class="row">
                    	<h4 class="text-center">Account Setting</h4><hr>
                        <div class="col-md-12">
                        	<div class="form-group" style="margin-bottom: -8px;">
                        		<label for="title" class="col-md-5 control-label">Current Password</label>
	                            <div class="col-md-12">
	                            	<?= $form->field($modelEmail, 'passwordCurrent', [
	                                    'template'      => '{input} {error}',
	                            	])->passwordInput([ 'placeholder' => t('app','Enter current password'), 'class' => 'form-control'])->label(false); ?>
	                            </div>
                        	</div>
                        </div>
                        <div class="col-md-12">
                        	<div class="form-group" style="margin-bottom: -8px;">
                        		<label for="title" class="col-md-5 control-label">New Email Address</label>
	                            <div class="col-md-12">
	                            	<?= $form->field($modelEmail, 'newEmail', [
	                                    'template'      => '{input} {error}',
	                            	])->textInput([ 'placeholder' => t('app','Enter New Email'), 'class' => 'form-control'])->label(false); ?>
	                            </div>
                        	</div>
                        </div>
                        <div class="col-md-12" style="text-align: center;">
                            <button type="submit" class="btn-as" value="Submit"><?=t('app','Save Changes');?></button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
		            </div>
            	</div>
            	<div class="tab-pane fade" id="tasks">
            		<div class="profile-content">
					   <?php $form = ActiveForm::begin([
                            'id'        => 'form-change-password',
                            'method'    => 'post',
                            'action'    => ['account/update-password'],
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                        ]); ?>
                            <div class="row">
                            	<h4 class="text-center">Change Password</h4><hr>
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <p><?=t('app','Passwords are case-sensitive and must be at least 6 characters');?>.</p>
                                </div>
                                <div class="col-md-12">
                                	<div class="form-group" style="margin-bottom: -8px;">
	                                	<label for="title" class="col-md-5 control-label">Current Password</label>
	                                    <div class="col-md-12">
	                                    	<?= $form->field($modelPassword, 'passwordCurrent', [
	                                            'template'      => '{input} {error}',
	                                    	])->passwordInput([ 'placeholder' => t('app','Enter current password'), 'class' => 'form-control'])->label(false); ?>
	                                    </div>
	                                </div>
                                </div>
                                <div class="col-md-12">
                            		<div class="form-group" style="margin-bottom: -8px;">
                            			<label for="title" class="col-md-5 control-label">New Password</label>
	                                    <div class="col-md-12">
	                                    	<?= $form->field($modelPassword, 'password', [
	                                            'template'      => '{input} {error}',
	                                    	])->passwordInput([ 'placeholder' => t('app','Enter new password'), 'class' => 'form-control'])->label(false); ?>
	                                    </div>
                            		</div>
                                </div>
                                <div class="col-md-12">
                                	<div class="form-group" style="margin-bottom: -8px;">
                            			<label for="title" class="col-md-5 control-label">Confirm New Password</label>
	                                    <div class="col-md-12">
	                                    	<?= $form->field($modelPassword, 'passwordConfirm', [
	                                            'template'      => '{input} {error}',
	                                    	])->passwordInput([ 'placeholder' => t('app','Reenter new password'), 'class' => 'form-control'])->label(false); ?>
	                                    </div>
                            		</div>
                                </div>
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="submit" class="btn-as" value="Search"><?=t('app','Save Changes');?></button>
                                </div>
                            </div>
                        <?php ActiveForm::end(); ?>
		            </div>
            	</div>
            </div>
		</div>
	</div>
</div>