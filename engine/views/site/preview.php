<html>
    <head>
        <meta charset="utf-8">
        <title>World Wide Adverts</title>
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
        <link rel="stylesheet" media="all" href="https://worldwideadverts.info/assets/site/css/preview.css?v=<?= time(); ?>">
        <link rel="stylesheet" media="all" href="https://worldwideadverts.info/assets/site/css/preview-core.css?v=<?= time(); ?>">
        <style>
            
        </style>
    </head>
    <body style="margin:0px;padding:0px;overflow:hidden;height:100%;">
        <div class="preview__header">
            <div class="preview__envato-logo">
                <a class="header-envato_market" href="https://worldwideadverts,info">Envato Market</a>
            </div>

            <div id="js-preview__actions" class="preview__actions">
                <div class="preview__action--close">
                    <a class="header-remove-frame" href="https://worldwideadverts.info">
                    <i class="e-icon -icon-cancel"></i><span>Back to Worldwideadverts</span></a>
                </div>
            </div>
        </div>
        <iframe id="iframeww" src="<?= $url; ?>" frameborder="0" style="overflow:hidden;height:100%;" height="100%" width="100%"></iframe>

        <script>
        document.getElementById("iframeww").onload = function() {
            var iframeTitle = document.getElementById("iframeww").contentDocument.title;
            if (document.title != iframeTitle) {
                document.title = iframeTitle;
            }
        }
        </script>
    </body>
</html>