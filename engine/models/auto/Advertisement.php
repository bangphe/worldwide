<?php

namespace app\models\auto;

use Yii;

/**
 * This is the model class for table "{{%advertisement}}".
 *
 * @property integer $advertisement_id
 * @property string $title
 * @property integer $description
 * @property string $link
 * @property string $avatar
 * @property string $start_date
 * @property string $end_date
 * @property string $timezone
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdminActionLog[] $adminActionLogs
 * @property Group $group
 */
class Advertisement extends \app\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%advertisement}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'start_date', 'end_date', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['timezone'], 'string', 'max' => 50],
            [['title', 'link', 'avatar'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'advertisement_id' => 'Advertisement ID',
            'title' => 'Title',
            'description' => 'Description',
            'link' => 'Link',
            'avatar' => 'Image',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'timezone' => 'Timezone',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminActionLogs()
    {
        return $this->hasMany(AdminActionLog::className(), ['changed_by' => 'advertisement_id']);
    }

    /**
     * @inheritdoc
     * @return AdvertisementQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AdvertisementQuery(get_called_class());
    }
}
