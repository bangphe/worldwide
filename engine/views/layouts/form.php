<?php
use app\assets\AdAsset;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use app\helpers\FrontendHelper;
use yii\helpers\ArrayHelper;
use app\models\Currency;
use app\models\Country;

AdAsset::register($this);

if (isset($_GET['cat'])) 
{
        $querystringcatslug=$_GET['cat'];
		$connection = app()->getDb();
		 $command = $connection->createCommand("Select * from ea_category where parent_id=(Select Category_ID from ea_category where slug='".$querystringcatslug."')");
        $SubCatList = $command->queryAll(); 
		foreach ($list as $category) 
	   {
		   echo "1".$category["name"];
	   }
								
}
else
{

} 									

$sub = $selected_category[0]->name == 'selling ' ? $selected_category[0]->name.' Business' : $selected_category[0]->name;
$sub = $selected_category[0]->name == 'buying ' ? $selected_category[0]->name.' Business' : $selected_category[0]->name;

$heading = isset($sub) ? $sub : 'Post your ad';


$picount=options()->get('app.settings.common.homePicNumber');
$script = <<< JS
$(document).ready(function() {
	debugger;
    $('#post-form').yiiActiveForm('add',
        {
			
            "id":"listingimage-imagesgallery",
            "name":"imagesGallery[]",
            "container":".field-listingimage-imagesgallery",
            "input":"#listingimage-imagesgallery",
            "error":".help-block.help-block-error",
            "validate":function (attribute, value, messages, deferred) {
				
				

				
                if ($('.file-preview-frame').length == 0) {
					debugger
                    yii.validation.required(value, messages, {"message": $('#post-form').data('err-msg-gallery')});
                }
				
				var piccount = 0;
				$('.kv-preview-thumb').each(function() {
					piccount++;    
				});
				
				if(piccount> $picount )
				{
					alert('You can upload only $picount images');	
				}
            }
        }
    );
}); 
JS;

$this->registerJs($script, yii\web\View::POS_LOAD);
?>
<section class="post-listing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php $listing_id = (int)($action == 'update') ? $ad->listing_id : 0;
                    $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'data-listing'   => $listing_id,
                            'data-err-msg-gallery' => t('app', 'Please add at least one image to your ad post')
                        ],
                        'id'        => 'post-form',
                        'method'    => 'post',
                ]); ?>
                <div class="block">
                    <div class="block-heading">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <h1 id="ad_post_heading"><?= ($action == 'update') ? t('app', 'Update your ad') : $heading ?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-8 col-lg-push-2 col-md-8 col-md-push-2 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($ad, 'title', [
                                            'template'      => '{input} {error}',
                                        ])->textInput([ 'placeholder' => t('app','Title'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                </div>

                                <!-- extra field -->
                                <div id="extra-field"></div>
                                
                                <!-- extra field -->

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($ad, 'description')->widget(CKEditor::className(), [
                                            'options' => ['rows' => 6],
                                            'preset' => 'basic',
                                            'clientOptions' => [
                                                'removePlugins' => 'notification, clipboard, pastetext, pastefromword, tableselection, toolbar',
                                                'contentsCss' => [Yii::getAlias('@web/assets/site/css/customCkeditor.css')],
                                            ]
                                        ])->label(); ?>
                                    </div>
                                </div>


                                <!-- chooseCategory -->
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Choose category');?></span>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php
							if (isset($_GET['cat'])) 
							{
								?>
								
								<input type="hidden" id="listing-category_id2" class="form-control" name="Listing5[category_id]" data-original-title="" title="" value="8">
								
								<select id="listing-category_id" class="form-control" name="Listing[category_id]" >
								
								<?php
								foreach($SubCatList as $category)
								{
									?>
									<option id="<?php echo $category["category_id"]; ?>" value="<?php echo $category["category_id"]; ?>"><?php echo $category["name"]; ?></option>
									<?php
									
								}
								?>
								</select>
								<?php
								
							}
							
							else
							{
								if(isset($selected_category[0]->name) && !empty($selected_category[0]->name)){
                                ?>
                                <input type="hidden" id="listing-category_id2" class="form-control" name="Listing5[category_id]" data-original-title="" title="" value="<?=$selected_category[0]->category_id?>">
                                <input type="hidden" id="listing-category_id" class="form-control" name="Listing[category_id]" data-original-title="" title="" value="<?=$selected_category[0]->category_id?>">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                        <div class="form-group hidden-xs">
                                            <a href="#" id="choose-class" class="form-control" data-toggle="modal" data-target="#modal-category"><?=$selected_category[0]->name?></a>
                                        </div>
                                        <div class="form-group hidden-lg hidden-md hidden-sm">
                                            <a href="javascript:;" id="choose-class-m" class="form-control choose-catg-m"><?=t('app', 'Choose category');?></a>
                                        </div>
                                        <?= $form->field($ad, 'category_id', [
                                            'template'      => '{input} {error}',
                                        ])->hiddenInput(['class' => 'form-control', 'value' => $selected_category[0]->name])->label(false); ?>
                                    </div>
                                </div>
                            <?php
                                }else{
                                 ?>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                            <div class="form-group hidden-xs">
                                                <a href="#" id="choose-class" class="form-control" data-toggle="modal" data-target="#modal-category"><?=t('app', 'Choose category');?></a>
                                            </div>
                                            <div class="form-group hidden-lg hidden-md hidden-sm">
                                                <a href="javascript:;" id="choose-class-m" class="form-control choose-catg-m"><?=t('app', 'Choose category');?></a>
                                            </div>
                                            <?= $form->field($ad, 'category_id', [
                                                'template'      => '{input} {error}',
                                            ])->hiddenInput(['class' => 'form-control'])->label(false); ?>
                                        </div>
                                    </div>
                                 <?php    
                                }
							}
							?>
                            <div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog add-category" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="choose-category">

                                                    <div class="column-category primary-category">
                                                        <h4>Categories</h4>
                                                        <div class="category-items mCustomScrollbar mCS-autoHide">
                                                            <ul>
                                                                <?php foreach ($categories as $category) {
                                                                    if (empty($category->parent_id)) { ?>
                                                                        <li>
                                                                            <a href="#" data-id="<?=(int)$category->category_id;?>">
                                                                                <span><i class="fa <?=html_encode($category->icon);?>" aria-hidden="true"></i></span>
                                                                                <span><?=html_encode($category->name);?></span>
                                                                            </a>
                                                                        </li>
                                                                <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="column-subcategory mCustomScrollbar mCS-autoHide">
                                                        <div class="column-subcategory-wrapper">
                                                            <?php $sortedCategories =  \app\helpers\FrontendHelper::getCategoriesSorted($categories);
                                                            foreach ($sortedCategories as $sortedCategoryId=>$sortedCategory) { ?>
                                                                <div class="column-category" data-parent="<?=(int)$sortedCategoryId;?>" style="display: none">
                                                                    <h4><?=html_encode($sortedCategory['name']);?></h4>
                                                                    <div class="category-items mCustomScrollbar mCS-autoHide">
                                                                        <ul>
                                                                            <?php foreach ($sortedCategory['children'] as $childCategory) { ?>
                                                                            <li>
                                                                                <a href="#" data-id="<?=(int)$childCategory->category_id;?>" class="subcateg" onclick="change_title('<?=$childCategory->name?>', '<?=$childCategory->slug?>')">
                                                                                    <span><?=html_encode($childCategory->name);?></span>
                                                                                </a>
                                                                            </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <span class="no-category-selected"><?=t('app','Please select a specific category');?></span>
                                                        <button id="success-selection" type="button" class="btn-as" style="display: none" data-dismiss="modal"><?= t('app', 'Submit');?></button>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <button id="close-modal" type="button" class="btn-as danger-action pull-right" data-dismiss="modal"><?= t('app', 'Cancel');?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            <div class="choose-category-mobile">
                                    <a href="#" class="close-x-categ-m"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    <div class="maincateg-m">
                                        <div class="heading">
                                            <a href="#" class="close-categ-m"><i class="fa fa-arrow-left" aria-hidden="true"></i></a> <?=t('app', 'Choose category');?>
                                        </div>
                                        <ul class="categ-items">
                                            <?php foreach ($categories as $category) {
                                                if (empty($category->parent_id)) { ?>
                                                <li>
                                                    <a href="#" data-id="<?=(int)$category->category_id;?>" class="categ-item-m" data-subcateg="<?= (!empty($category->children)) ? html_encode($category->slug) : '';?>">
                                                        <span><i class="fa <?=html_encode($category->icon);?>" aria-hidden="true"></i></span>
                                                        <span><?=html_encode($category->name);?></span>
                                                    </a>
                                                </li>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php $sortedCategories = FrontendHelper::getCategoriesSorted($categories);
                                    foreach ($sortedCategories as $sortedCategoryId=>$sortedCategory) { ?>
                                        <div id="subcateg-<?=html_encode($sortedCategory['slug']);?>" class="subcateg-m">
                                            <div class="heading">
                                                <a href="#" data-parent="<?=(int)$sortedCategoryId;?>" class="back-categ-m">
                                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                                </a>
                                                <?=html_encode($sortedCategory['name']);?>
                                            </div>
                                            <ul class="categ-items">
                                                <?php foreach ($sortedCategory['children'] as $childCategory) { ?>
                                                    <li>
                                                        <a href="#" data-id="<?=(int)$childCategory->category_id;?>" data-subcateg="<?= (!empty($childCategory->children)) ? html_encode($childCategory->slug) : '';?>" class="categ-subitem-m">
                                                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                                            <?=html_encode($childCategory->name);?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                            </div>






                                <!-- price -->




















                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span id="price_title"><?=t('app','Price');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'currency_id', [
                                            'template'      => '{input} {error}',
                                        ])->dropDownList(ArrayHelper::map(Currency::getActiveCurrencies(), 'currency_id', 'name'),['class'=>'','prompt' => t('app','Currency')])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'price', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Price'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($ad, 'negotiable', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Contact details');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($customer, 'phone', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Phone'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($ad, 'hide_phone', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($customer, 'email', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Email Address'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($ad, 'hide_email', [
                                            'options' => [
                                                'class' => 'checkbox'
                                            ],
                                        ])->checkbox(['template' => '{input} {label}'],['value'=>false]); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <?= $form->field($location, 'country_id', [
                                                'template'      => '{input} {error}',
                                            ])->dropDownList(ArrayHelper::map(Country::getActiveCountries(), 'country_id', 'name'),['class'=>'', 'prompt' => t('app','Country'),])->label(false);
                                        ?>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="listing-select-zones-wrapper" data-url="<?=url(['/listing/get-country-zones']);?>" data-zone = <?=(int)$location->zone_id;?>>
                                        <?= $form->field($location, 'zone_id', [
                                            'template'      => '{input} {error}',
                                        ])->dropDownList(array(),['class'=>'','prompt' => t('app','Zone')])->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <?= $form->field($location, 'city', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','City'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <?= $form->field($location, 'zip', [
                                            'template'      => '{input} {error}',
                                        ])->textInput(['placeholder' => t('app','Zip code'), 'class' => 'form-control'])->label(false); ?>
                                    </div>
                                </div>
                                <div class="row category-fields" style="display: none">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span><?=t('app','Category Fields');?></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row category-fields" id="category-fields" data-url="<?=url(['/listing/get-category-fields']);?>" style="display: none">
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="separator-text">
                                            <span id="business_plan_uploadable"><?=t('app','Photo Gallery');?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                        $imagesPreview = [];
                                        $imagesPreviewConfig = [];
                                        if ($action == 'update') {
                                            // sort for images sort_order
                                            usort($uploadedImages, function ($a, $b) { return strnatcmp($a['sort_order'], $b['sort_order']); });
                                            if ($uploadedImages) foreach ($uploadedImages as $key => $image) {
                                                $imagesPreview[] = $image->image_path;
                                                $imagesPreviewConfig[$key]['caption'] = $image->image_path;
                                                $imagesPreviewConfig[$key]['url'] = url(['/listing/remove-ad-image']);
                                                $imagesPreviewConfig[$key]['key'] = $image->image_id;
                                            }
                                        }
                                        echo $form->field($images, 'imagesGallery[]')->widget(FileInput::classname(), [
                                            'options' => [
                                                'multiple' => true,
                                                'class' =>'file-loading',
                                                'data-sort-listing-images' => url(['/listing/sort-ad-images']),
                                            ],
                                            'pluginOptions' => [
                                                'initialPreview'=> $imagesPreview,
                                                'initialPreviewConfig' =>
                                                    $imagesPreviewConfig
                                                ,
                                                'initialPreviewAsData'=>true,
                                                'language' => html_encode(options()->get('app.settings.common.siteLanguage', 'en')),
                                                'deleteUrl' => false,
                                                'uploadUrl' => false,
                                                'allowedFileTypes' => ["image"],
                                                'showUpload' => false,
                                                'showRemove' => false,
                                                'showClose' => false,
                                                'browseOnZoneClick' => true,
                                                'browseClass' => 'btn btn-as',
                                                'removeClass' => 'btn btn-as reverse',
                                                'uploadClass' => 'btn btn-as reverse',
                                                'layoutTemplates' =>
                                                    [
                                                        'footer'=> '<div class="file-thumbnail-footer">' . '{progress} {actions}' . '</div>'
                                                    ],
                                                'fileActionSettings' => [
                                                    'showUpload' => false,
                                                    'showDrag' => true,
                                                ],
                                                'overwriteInitial'=>false,
                                            ]
                                        ])->label(false);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block">
                    <div class="block-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="a-center">
                                    <button type="submit" name="post" class="btn-as"><?=t('app','Submit');?></button>
                                    <button type="submit" name="preview" class="btn-as danger-action"><?=t('app','Preview');?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-post-listing-contact" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-notice" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title"><?=t('app', 'Notice');?></h1>
                <a href="javascript:;" class="x-close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></a>
            </div>
            <div class="modal-body">
                <p><?=t('app','Please leave at least one contact option');?></p>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn-as black pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/Javascript">  
    function change_title(category_name, slug){
        



        if(slug == 'b2b'){

            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" id="business_types" type="text" placeholder="What is the business about" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Proposal B2B</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="What you are looking for a merger, trading, business or investment partner"></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of business</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="How do you want to structure the business to business arrangement"></textarea> ';
            extra_field += '</div></div>';

            var title = 'What you are looking to benefit as business to business';
            $('#listing-title').attr('placeholder', title);
            $('#extra-field').html(extra_field);
        }
        if(slug == 'selling'){

            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" type="text" placeholder="What is the business about" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>VALUE</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="business valuation"></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of sale</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What you want as a buyer"></textarea> ';
            extra_field += '</div></div>';



            var title = 'What is on offer';
            var city = 'Where is the business located';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            $('#extra-field').html(extra_field);
        }
        if(slug == 'buying'){



            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" type="text" placeholder="Type business you are looking to buy/invest" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Offer</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="Capital available for the purchase/investment"></textarea> ';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What you want as a buyer"></textarea> ';
            extra_field += '</div></div>';

            var title = 'What is being offered';
            var city = 'Which locations are you looking to invest in ';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            $('#extra-field').html(extra_field);
        }
        if(slug == 'partnerships'){



            var extra_field = '';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Type of business</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[business_types]" type="text" placeholder="The type of partnership ,local ,international,expansion or merge" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>PITCH</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[offer]" class="form-control" name="proposal_b2b" placeholder="Give details of what you require or are looking for"></textarea> ';
            extra_field += '</div></div>';
            
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[share]" type="text" placeholder="Percentage or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of partnership</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What each partner stands to benefit"></textarea> ';
            extra_field += '</div></div>';

            var title = 'Looking to merge, buy shares, investing or local partner in a location';
            var city = 'Which locations are you looking to invest in';
            $('#listing-title').attr('placeholder', title);
            $('#location-city').attr('placeholder', city);
            $('#extra-field').html(extra_field);
        }
        if(slug == 'investment-funding'){

            var extra_field = '';
             
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input name="Listing[share]" type="text" placeholder="Percentage or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of the funding</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What each partner stands to benefit"></textarea> ';
            extra_field += '</div></div>';
            
            var business_plan_uploadable = 'Business plan'; 
            var price_title = 'Capital/loan';
            var title = 'Type of funding loan, Shares to be bought back at a later date, returns on investment or funding with benefits to the investors';
            $('#listing-title').attr('placeholder', title);
            $('#business_plan_uploadable').html(business_plan_uploadable);
            $('#price_title').html(price_title);
            $('#extra-field').html(extra_field);
        }
        if(slug == 'investment-proposal'){

            var extra_field = '';
             
            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Shares</span>';
            extra_field += '</div></div></div>';


            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<input type="text" name="Listing[share]" placeholder="Percentage  or how much of the business is on offer" class="form-control">';
            extra_field += '</div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <div class="separator-text">';
            extra_field += '<span>Terms of the Investment</span>';
            extra_field += '</div></div></div>';

            extra_field += '<div class="row">';
            extra_field += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
            extra_field += '<textarea name="Listing[terms]" class="form-control" name="terms_of_business" placeholder="What each partner stands to benefit"></textarea> ';
            extra_field += '</div></div>';

            var business_plan_uploadable = 'Business plan'; 
            var title = 'What you want to do as a business';
            var price_title = 'Capital/loan';
            $('#listing-title').attr('placeholder', title);
            $('#business_plan_uploadable').html(business_plan_uploadable);
            $('#price_title').html(price_title);
            $('#extra-field').html(extra_field);
        }
        if(category_name == 'selling' || category_name == 'buying'){
            category_name = category_name.' BUSINESS';
        }

        $('#ad_post_heading').html(category_name);
        $('#business_types').change(function (){
            var business_types_val = $('#business_types').val(category_name);
        });
    }
</script>