<?php
use yii\widgets\ActiveForm;
use app\assets\AccountAsset;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\ArrayHelper;
use app\models\Country;

AccountAsset::register($this);
?>
<div class="sign-up">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-push-4 col-md-4 col-md-push-4 col-sm-6 col-sm-push-3 col-xs-12">
                <h1><?=t('app','Register');?></h1>
                <!--<label><input type="radio" name="customer_type" value="individual">Individual</label><label> <input type="radio" name="customer_type" value="company"> Company
                </label>-->
                
                <?php $form = ActiveForm::begin([
                    'id'        => 'signup-form',
                    'method'    => 'post',
                ]); ?>
                    <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">-->
                        <?//= $form->errorSummary($model); ?>
                    <!--</div>-->

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div style="float:left;">
                        <?php 
                            //$form->field($model, 'customer_type')->radioList(array('Individual'=>'Individual','Company'=>'Company'))->label(false);; 
                            //$form->field($model, 'customer_type')->radio(['Individual' => 'Individual', 'Company' => 'Company'])->label(true);;
                            $model->customer_type = 'Individual';
                            echo $form->field($model, 'customer_type')->radio(array('label'=>'Individual', 'id'=>'Individual' ,'value'=>'Individual'));
                            
                        ?>
                        </div>
                        <div style="float:right;">
                            <?php
                                echo $form->field($model, 'customer_type')->radio(array('label'=>'Company','id'=>'Company','value'=>'Company'));
                                /*echo $form->field($model, 'customer_type')->radio(array(
                                'label'=>'Individual',
                                'labelOptions'=>array('style'=>'position: relative !important;left: -0.1em !important;')))
                                ->label(false);*/
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div id="individual-info">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'first_name', [
                                        'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','First Name'), 'class' => 'form-control with-addon'])->label(false); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'last_name', [
                                        'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Last Name'), 'class' => 'form-control with-addon'])->label(false); ?>
                            </div>                          
                        </div>
                        <div id="company-info">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'company_name', [
                                        'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Company Name'), 'class' => 'form-control with-addon'])->label(false); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'company_number', [
                                        'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Company Number'), 'class' => 'form-control with-addon'])->label(false); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?= $form->field($model, 'vat_number', [
                                        'template'      => '{input} {error}',
                                ])->textInput([ 'placeholder' => t('app','Vat Number'), 'class' => 'form-control with-addon'])->label(false); ?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'email', [
                                    'template'      => '{input} {error}',
                            ])->textInput(['placeholder' => t('app','Email'), 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'password', [
                                    'template'      => '{input} {error}',
                            ])->passwordInput(['placeholder' => t('app','Password'), 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?= $form->field($model, 'passwordConfirm', [
                                    'template'      => '{input} {error}',
                            ])->passwordInput(['placeholder' => t('app','Retype Password'), 'class' => 'form-control with-addon'])->label(false); ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <button type="submit" class="btn-as" value="Search"><?=t('app','register ');?></button>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <a href="<?= url(['account/login']);?>" class="btn-as" style="margin-top: 8px;"><?= t('app','Login');?></a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <a href="<?= url(['account/forgot']);?>" class="btn-as" style="margin-top: 8px;"><?= t('app','Forgot Password?');?></a>
                            </div>
                        </div>
                        <?php if (options()->get('app.settings.common.siteFacebookId', '') && options()->get('app.settings.common.siteFacebookSecret', '')) { ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="separator-text">
                                <span><?=t('app','OR Register with');?></span>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="a-center">
                                <?php
                                    $authAuthChoice = AuthChoice::begin([
                                        'baseAuthUrl' => ['account/auth']
                                    ]);
                                    
                                    ?>
                                    <table align="center" width="100%">
                                        <tr>
                                        <?php
                                            foreach ($authAuthChoice->getClients() as $client) {
                                                //echo $authAuthChoice->clientLink($client,'facebook',['class'=>'btn-as facebook']);
                                                //echo $authAuthChoice->clientLink($client,$client->name,['class'=>'btn-as ']);
                                                ?>
                                                <td width="50px">
                                               <?php
                                                echo $authAuthChoice->clientLink($client);
                                                ?>
                                                </td>
                                                <?php
                                            }
                                            AuthChoice::end();
                                        ?>
                                        </tr>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<style>
input[type=radio] {
  position: relative !important;
  left: -0.1em !important;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('#company-info').hide();
    $('#Individual').on("click", function(e){
    $('#company-info').hide();
    $('#individual-info').show();
   
});
$('#Company').on("click", function(e){
    $('#company-info').show();
    $('#individual-info').hide();
    });
});
</script>