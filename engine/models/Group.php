<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

/**
 * Class Group
 * @package app\models
 */
class Group extends \app\models\auto\Group
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['status'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function getAllRoutesAccess()
    {
        return GroupRouteAccess::findAllByGroupId((int)$this->group_id);
    }
}