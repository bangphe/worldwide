<?php

/**
 *
 * @package    EasyAds
 * @author     Abude Bayassi <abude.bayassi@easyads.io>
 * @link       https://www.easyads.io
 * @copyright  2017 EasyAds (https://www.easyads.io)
 * @license    https://www.easyads.io
 * @since      1.0
 */

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use app\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Customer
 * @package app\models
 */
class Customer extends \app\models\auto\Customer implements IdentityInterface
{
    // when inactive model
    const STATUS_INACTIVE = 'inactive';

    // when active model
    const STATUS_ACTIVE = 'active';

    // when deactivated
    const STATUS_DEACTIVATED = 'deactivated';

    /**
     * @var string
     */
    public $passwordConfirm = '';

    /**
     * @var string
     */
    public $passwordCurrent = '';

    /**
     * @var string
     */
    public $password = '';

    /**
     * @var string
     */
    public $newEmail = '';

    /**
     * @var string
     */
    public $newEmailRecovery = '';

     /**
     * @var int
     */
    public $birthdayYear = 0;

    /**
     * @var int
     */
    public $birthdayMonth = 0;

    /**
     * @var int
     */
    public $birthdayDay = 0;

    /**
     * @var bool
     */
    private $_customer = false;

    /**
     * @var
     */
    public $_adsCount;

    public $avatarUpload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'email', 'email_recovery', 'first_name', 'last_name',
                'phone','company_name','company_number','vat_number',
                'address_country', 'address_city', 'address_street',
                'address_house', 'address_flat', 'ip_location'
            ], 'trim'],
            [['email','newEmail'], 'unique', 'targetAttribute' => 'email'],
            [['email_recovery','newEmailRecovery'], 'unique', 'targetAttribute' => 'email_recovery'],
            [['email','newEmail', 'email_recovery', 'newEmailRecovery'], 'email'],
            [['status'], 'safe'],
            [['birthdayYear', 'birthdayMonth', 'birthdayDay'], 'integer'],
            [['gender'], 'string', 'max' => 1],
            [['first_name', 'last_name', 'email','company_name','company_number','vat_number','customer_type'], 'string', 'max' => 100],
            [['address_country', 'address_city', 'address_street'], 'string', 'max' => 128],
            [['address_house', 'address_flat'], 'string', 'max' => 32],
            [['password'], 'string', 'length' => [6, 100]],

         //   [['first_name', 'last_name', 'email', 'password', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_CREATE],
            [[ 'email', 'password', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_CREATE],
            [['passwordConfirm'], 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CREATE, 'message' => 'Password does not match.'],

            [['email'], 'required', 'on' => 'createFB'],

            [['email', 'address_street'], 'required', 'on' => self::SCENARIO_UPDATE],

            [['password', 'passwordConfirm', 'passwordCurrent'], 'required', 'on' => 'update_password'],
            [['passwordCurrent'], 'validatePasswordCurrent', 'on' => 'update_password'],
            [['password','passwordConfirm'], 'string', 'length' => [6, 100], 'on' => 'update_password'],
            [['passwordConfirm'], 'compare', 'compareAttribute' => 'password', 'on' => 'update_password'],

            [['passwordCurrent'], 'required', 'on' => 'update_email'],
            [['passwordCurrent'], 'validatePasswordCurrent', 'on' => 'update_email'],

            [['email'], 'required', 'on' => 'post_ad'],
            [['phone'], 'match', 'pattern' => '/^[().+\d -]{7,15}$/'],

            [['avatarUpload'],
                'file',
                'mimeTypes' => 'image/jpeg, image/png, image/pjpeg',
                'checkExtensionByMimeType' => true,
                'wrongMimeType' => t('app','Please try to upload an image.'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'group_id'               => t('app', 'Group'),
            'password'               => t('app', 'Password'),
            'newEmail'               => t('app', 'New Email'),
            'newEmailRecovery'       => t('app', 'New Recovery Email'),
            'passwordConfirm'        => t('app', 'Confirm Password'),
            'passwordCurrent'        => t('app', 'Current Password'),
            'birthdayYear'           => t('app', 'Birthday Year'),
            'birthdayMonth'          => t('app', 'Birthday Month'),
            'birthdayDay'            => t('app', 'Birthday Day'),
            'adsCount'               => t('app', 'Ads Count'),
            'ip_location'            => t('app', 'Ip Location'),
        ]);
    }


    /**
     * handleUploadedImage
     */
    public static function handleUploadedImage($model)
    {
        if ($model->hasErrors()) {
            return;
        }

        if (!($avatarUpload = UploadedFile::getInstance($model, 'avatarUpload'))) {
            return;
        }

        $storagePath = Yii::getAlias('@webroot/uploads/images/avatar');
        if (!file_exists($storagePath) || !is_dir($storagePath)) {
            if (!@mkdir($storagePath, 0777, true)) {
                notify()->addError(t('app', 'The images storage directory({path} does not exists and cannot be created!', ['path' =>$storagePath]));
                return;
            }
        }

        $newAvatarName = $avatarUpload->name;
        $avatarUpload->saveAs($storagePath . '/' . $newAvatarName);
        if (!is_file($storagePath . '/' . $newAvatarName)) {
            notify()->addError(t('app', 'Cannot move the avatar into the correct storage folder!'));
            return;
        }
        $existing_file = $storagePath . '/' . $newAvatarName;
        $newAvatarName = substr(sha1(time()),0,6) . $newAvatarName;
        copy($existing_file , $storagePath . '/' . $newAvatarName );
        $avatar = str_replace('Upload', '', $avatarUpload);
        $model->avatar = Yii::getAlias('@web/uploads/images/avatar/' . $newAvatarName);
        $model->save(false);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!empty($this->password)) {
            $this->password_hash = app()->getSecurity()->generatePasswordHash($this->password);
        }

        if(!empty($this->newEmail)){
            $this->email = $this->newEmail;
        }

        if(!empty($this->newEmailRecovery)){
            $this->email_recovery = $this->newEmailRecovery;
        }

        if ($this->isNewRecord) {
            $this->customer_uid = $this->generateUid();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function afterValidate()
    {
        parent::afterValidate();

        if(!empty($this->birthdayDay) && !empty($this->birthdayMonth) && !empty($this->birthdayYear))
        {
            $date = date_create_from_format('d/m/Y', $this->birthdayDay . '/' . $this->birthdayMonth . '/' . $this->birthdayYear);
            $this->birthday = $date->format('Y-m-d H:i:s');
        }

        // $this->handleUploadedImage();

        return true;
    }

    /**
     * @return bool
     */
    public function afterFind()
    {
        parent::afterFind();

        if(!empty($this->birthday))
        {
            // die(var_dump(date('d', strtotime($this->birthday))));
            $this->birthdayDay      = date('j', strtotime($this->birthday));
            $this->birthdayMonth    = date('m', strtotime($this->birthday));
            $this->birthdayYear     = date('Y', strtotime($this->birthday));
        }
        
        return true;
    }

    /**
     * @param $customer_uid
     * @return auto\Customer|array|null
     */
    public function findByUid($customer_uid)
    {
        return $this->find()->where(array(
            'customer_uid' => $customer_uid,
        ))->one();
    }

    /**
     * @return string
     */
    public function generateUid()
    {
        $unique = StringHelper::uniqid();
        $exists = $this->findByUid($unique);

        if (!empty($exists)) {
            return $this->generateUid();
        }

        return $unique;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->customer_uid;
    }

    /**
     * @return bool
     */
    public function deactivate()
    {
        $this->status = self::STATUS_DEACTIVATED;
        $this->save(false);
        return true;
    }

    /**
     * @return bool
     */
    public function activate()
    {
        if($this->status == self::STATUS_DEACTIVATED) {
            $this->status = self::STATUS_ACTIVE;
            $this->save(false);
        }
        return true;
    }

    /**
     * @param int|string $customer_id
     * @return static
     */
    public static function findIdentity($customer_id)
    {
        return static::findOne($customer_id);
    }

    /**
     * @param mixed $token
     * @param null $type
     * @return static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token, 'status' => 'active']);
    }

    /**
     * @param $email
     * @return static
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @param $key
     * @return static
     */
    public static function findByPasswordResetKey($key)
    {
        return static::findOne(['password_reset_key' => $key, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->customer_id;
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePasswordCurrent($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $customer = $this->getCustomer();

            if (!$customer || !(security()->validatePassword($this->passwordCurrent, $customer->password_hash))) {
                $this->addError($attribute, t('app', 'Incorrect password.'));
            }
        }
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePassword($password)
    {
        return security()->validatePassword($password, $this->password_hash);
    }

    /**
     * @param $password
     * @return string
     */
    public static function hashPassword($password)
    {
        return security()->generatePasswordHash($password);
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return app()->customer->login($this->getCustomer(), 0);
        }
        return false;
    }


    /**
     * @return Customer|bool
     */
    public function getCustomer()
    {
        if ($this->_customer === false) {
            $this->_customer = Customer::findByEmail($this->email);
        }

        return $this->_customer;
    }

    /**
     * @return bool
     */
    public function sendRegistrationEmail()
    {
        if (!($model = Customer::findByEmail($this->email))) {
            return false;
        }

        return app()->mailSystem->add('registration-confirmation', ['customer_email' => $model->email]);
    }

    /**
     * @return int|string
     */
    public function getAdsCount(){
        if($this->_adsCount !== null){
            return $this->_adsCount;
        }
        if(empty($this->customer_id)){
            return null;
        }
        $this->setAdsCount(\app\models\Listing::find()->where(['customer_id'=>$this->customer_id])->count());
        return $this->_adsCount;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setAdsCount($count = null){
        if($count !== null && $count != ''){
            $this->_adsCount = $count;
        }
        return $this;
    }

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_ACTIVE   => t('app', 'Active'),
            self::STATUS_INACTIVE => t('app', 'Inactive'),
            self::STATUS_DEACTIVATED  => t('app', 'Deactivated'),
        ];
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return trim(sprintf('%s %s', $this->first_name, $this->last_name));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasOne(CustomerStore::className(), ['customer_id' => 'customer_id']);
    }

    /**
     * @return string
     */
    public static function registerAffiliate()
    {
        $customer_id = Yii::$app->customer->identity->customer_id;
        if ($customer_id != '') {
            $customerData = Customer::findIdentity($customer_id);
            
            if ($customerData->affiliate_id == '') {
                $encryption_key = md5($customerData->customer_id . $customerData->first_name . $customerData->last_name);

                $customer = Customer::findOne($customer_id);
                $customer->affiliate_id = $encryption_key;
                $customer->update();
                
                return $encryption_key;
            } else {
                return $customerData->affiliate_id;
            }
        } else {
            return '';
        }
    }

    /**
     * @return bool
     */
    public static function setAffiliateCookie($affiliate_id)
    {
        // $cookival = Yii::$app->response->cookies;
        // $aid = $cookival->getValue('aid', '');
        
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'aid',
            'value' => $affiliate_id,
        ]));

        return true;
    }

    /**
     * @return bool
     */
    public static function affiliateRequest($affiliate_id)
    {
        
        $customerData = Customer::findOne(['affiliate_id' => $affiliate_id]);
        if (!empty($customerData)) {
            $membercount = $customerData->affiliated_members+1;
            
            Customer::updateAll([
              'affiliated_members' => $membercount
            ],
            [
              'affiliate_id' => $affiliate_id
            ]);
            return true;
        } else {
            return true;
        }

    }

    /**
     * @return string|Response
     */
    public static function getUserIP() {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }

        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    /**
     * @return bool
     */
    public static function updateUserIP($customer_id,$ipadd) {
        $customerData = Customer::findOne(['customer_id' => $customer_id,'ip_address' => '']);
        if (!empty($customerData)) {
            $ipdatajson = file_get_contents('https://geoip-db.com/json');
            $ipdata = json_decode($ipdatajson);
            
            if (!empty($ipdata)) {
                $ip_address = $ipdata->IPv4;
                $ip_location = $ipdata->city . ', ' . $ipdata->state . ', ' . $ipdata->country_name . '-' . $ipdata->postal;
                $ip_latlng = $ipdata->latitude . ',' . $ipdata->longitude;

                Customer::updateAll([
                  'ip_address' => $ip_address,
                  'ip_location' => $ip_location,
                  'ip_latlng' => $ip_latlng
                ],
                [
                  'customer_id' => $customer_id
                ]);
                
                return true;
            }
            return true;
        }
        return true;
    }

    /**
     * @return array
     */
    public static function nearByUsers($latitude,$longitude) {
        if($latitude != '' && $longitude != '') {
          $connection = Yii::$app->getDb();
          $command = $connection->createCommand("SELECT l.customer_id, l.location_id, l.latitude, l.longitude, li.title, li.slug, c.first_name, c.last_name, c.company_name, c.address_street, ( 6371 * acos ( cos ( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance FROM ea_location as l LEFT JOIN ea_customer as c on l.customer_id = c.customer_id LEFT JOIN ea_listing as li on li.location_id = l.location_id where c.status = 'active' HAVING distance < 40 ORDER BY distance LIMIT 0 , 20");

          $result = $command->queryAll();
          return $result;
        } else {
          return array();
        }
    }

}