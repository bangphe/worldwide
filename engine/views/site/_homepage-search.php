<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
?>

<style>
.search-bar:focus {
    border: 1px solid orange !important;
}
.main-search form .input-group .fa {
    left: 15px !important;
}
</style>

<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-push-2 col-md-12 col-sm-12 col-xs-12">

        <?php $form = ActiveForm::begin([
            'method'      => 'get',
            'fieldConfig' => ['options' => ['class' => 'input-group']],
            'action'      => ['site/search'],
            'options'     => [
                'class' => 'search-form'
            ]
        ]); ?>
            <div class="row">
                <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="input-group"> -->
                        <!-- <?= $form->field($searchModel, 'zoneId')->widget(Select2::classname(), [
                            'options'       => ['placeholder' => t('app', 'Location')],
                            'pluginOptions' => [
                                'language'           => app()->language,
                                'allowClear'         => true,
                                'minimumInputLength' => 3,
                                'ajax'               => [
                                    'url'      => url('category/location', true),
                                    'dataType' => 'json',
                                    'data'     => new JsExpression('function(params) { return {term:params.term}; }')
                                ],
                                'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                                'templateResult'     => new JsExpression('function(location) { return location.text; }'),
                                'templateSelection'  => new JsExpression('function (location) { return location.text; }'),
                            ],
                        ])->label(false); ?> -->
                    <!-- </div>
                </div> -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <!--<?//= $form->field($searchModel, 'searchPhrase', [
                      //  'template' => "{label}\n<i class='fa fa-search' aria-hidden='true'></i>\n{input}\n{hint}\n{error}"
                    //])->textInput(['placeholder' => t('app', 'Keywords...')])->label(false) ?>-->

                    <div class="input-group">
                        <div class="input-group-btn search-panel">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="background-color: #eee; width: 45px;" onclick="window.location.href='<?= url(['/site/all-categories']); ?>'">
                                <span><i class="glyphicon glyphicon-th"></i></span>
                            </button>
                            <!-- <ul class="dropdown-menu" role="menu"> -->
                                <?php
                                    // foreach (\app\models\Category::getCategoriesByParentId() as $item) {
                                    //     echo '<li>';
                                    //     echo '<a href="#'.$item->name.'">'.$item->name.'</a>';
                                    //     echo '</li>';
                                    // }
                                ?>
                                <!-- <li><a href="#continent">Continent</a></li> -->
                                <!-- <li><a href="#country">Country</a></li> -->
                                <!--<li><a href="#city">City</a></li>-->
                            <!-- </ul> -->
                        </div>

                        <!-- <input type="hidden" name="ListingSearch[searchParam]" value="all" id="search_param"> -->
                        <input type="text" class="form-control search-bar" name="ListingSearch[searchPhrase]" placeholder="Search">
                        <span class="input-group-btn">
                            <button class="btn btn-default" style="background-color: orange; border: 1px solid orange;" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </div>
                <!-- <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12"> -->
                    <!--<?= Html::submitButton(t('app', 'Search'), ['class' => 'btn-as']) ?>-->
                <!-- </div> -->
            </div>
        <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>