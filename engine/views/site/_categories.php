<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.navbarheader-wrapper {
    background-color: #5042f4;
}

.navbarheader {
    overflow: hidden;
    background-color: #5042f4;
    font-family: Arial;
    display: table;
    margin: 0 auto;
    min-width: fit-content;
}

.navbarheader a {
    float: left;
    font-size: 14px;
    color: white;
    text-align: center;    
    text-decoration: none;
}

.dropdownheader {
    float: left;
    overflow: hidden;
}

.dropdownheader .dropbtnheader {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding -top: 16px;
    background-color: inherit;
}

.navbarheader a:hover, .dropdownheader:hover .dropbtnheader {
    background-color: #ff9900;
}

.dropdownheader-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 4;
}

.dropdownheader-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdownheader-content a:hover {
    background-color: #ddd;
}

.dropdownheader:hover .dropdownheader-content {
    display: block;
}
</style>

<div class="navbarheader-wrapper">
  <div class="navbarheader">
  <?php

  foreach ($categories as $key) 
  {
  	?>
  	<div class="dropdownheader">
      <button class="dropbtnheader">
  	
  	 <a href="<?= empty($cat->children) ? url(['category/index', 'slug' => $key[slug]]) : '#main-categ-' . (int)$key[category_id] ?>" 
  	 ><span><i class="fa <?= html_encode($key[icon]); ?>" ></i>
  	 </span><span><?= html_encode($key[name]); ?></span></a>
     
      
  	<?php // echo $key[name]; ?>
         </button>
  	<?php
  	//echo $key[name];
  	  $childCats = [];
  	$childCats[$cat->category_id] =$key[children];
  	
  	if (!empty($childCats)) 
  	{
  		
  		 foreach ($childCats as $parentId => $cats) 
  		 { 
  		 ?><div class="dropdownheader-content">
  		 <?php
  		 foreach ($cats as $childKey => $childCat) {
  			 ?>
  			<a href="<?= empty($cat->children) ? url(['category/index', 'slug' => $childCat->slug]) : '#main-categ-' . (int)$childCat->category_id ?>" data-toggle="" class="categ-item customclassheaderlink"><span><i class="fa <?= html_encode($childCat->icon); ?>" aria-hidden="true"></i></span><span><?= html_encode($childCat->name); ?></span></a>
  			
  			<?php
  		 }
  		 ?></div>
  		 <?php
  		 }
  		
  	}
  ?>
  </div>
  <?php
  }
  ?>
  </div>
</div>