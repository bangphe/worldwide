<?php
use app\components\AdsListWidget;
use app\components\AdvertisementsListWidget;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<section class="main-search">
    <?= $this->render('_homepage-search', ['searchModel' => $searchModel]); ?>
</section>

<?php if (Yii::$app->controller->id != 'account') { ?>
<section class="listings-list">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1><?= \yii\helpers\Html::a(html_encode('Featured Ads'), ['site/featured']);?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= \app\components\AdsHomeListWidget::widget([
                    'col' => 6,
                    'quantity' => 20
                ]);
                ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?= $promoted = AdsListWidget::widget([
    'listType' => AdsListWidget::LIST_TYPE_PROMOTED,
    'title' => t('app', 'Promoted ads'),
    'quantity' => options()->get('app.settings.common.homePromotedNumber', 8),
    'moreLink' => ['site/promoted'],
]) ?>

<?= $advertisement = AdvertisementsListWidget::widget([
    'title' => t('app', 'Advertisements'),
    'quantity' => options()->get('app.settings.common.homePromotedNumber', 12),
]) ?>

<?php
    if (!empty($promoted)) {
        echo AdsListWidget::widget([
            'listType' => AdsListWidget::LIST_TYPE_NEW,
            'title' => t('app', 'New ads'),
            'emptyTemplate' => false,
            'quantity' => options()->get('app.settings.common.homeNewNumber', 8),
            'moreLink' => ['site/new'],
        ]);
    } else {
        echo AdsListWidget::widget([
            'listType' => AdsListWidget::LIST_TYPE_NEW,
            'title' => t('app', 'New ads'),
            'emptyTemplate' => true,
            'quantity' => options()->get('app.settings.common.homeNewNumber', 8),
            'moreLink' => ['site/new'],
        ]);
    }
?>

<style>
#myBtn {
  display: none;
  position: fixed;
  bottom: 15px;
  /*right: 30px;*/
  left: 15px;
  z-index: 99;
  border: none;
  outline: none;
  background-color: #5042f4;
  color: white;
  cursor: pointer;
  padding: 8px;
  border-radius: 10px;
  font-size: 12px;
}

#myBtn:hover {
  background-color: #ff5c33;
}
</style>

<button onclick="topFunction()" id="myBtn" title="Top">Top</button>
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>