<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use app\helpers\LanguageHelper;
use app\yii\base\Event;

AppAsset::register($this);
$formatter = app()->formatter;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>" dir="<?= LanguageHelper::getLanguageOrientation();?>">
    <head>
        <meta charset="<?= app()->charset ?>">
        <meta name="google-signin-client_id" content="186985254845-v2ltstj354s4mj4m5hpis8kukq2vo3g4.apps.googleusercontent.com">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=47MGxqKekj">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=47MGxqKekj">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=47MGxqKekj">
        <link rel="manifest" href="/site.webmanifest?v=47MGxqKekj">
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=47MGxqKekj" color="#5bbad5">
        <link rel="shortcut icon" href="/favicon.ico?v=47MGxqKekj">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <?= Html::csrfMetaTags() ?>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
        <link rel="stylesheet prefetch" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
        <title><?= Html::encode($this->title) ?></title>
        <?php app()->trigger('app.header.beforeScripts', new Event(['params' => []])); ?>

        <script type="text/javascript">
            var site = {
                'url' : '<?=options()->get('app.settings.urls.siteUrl', '');?>',
                'language' : '<?=$formatter->locale;?>',
                'dateFormat' : '<?=$formatter->dateFormat?>',
                'timeFormat' : '<?=$formatter->timeFormat;?>',
                'datetimeFormat' : '<?=$formatter->datetimeFormat;?>',
                'dir' : '<?= LanguageHelper::getLanguageOrientation();?>',
                'statsUrl' : '<?= url(['/listing/track-stats']);?>',
            };
        </script>

        <?php $this->head() ?>
    </head>
    <body class="<?= $this->context->bodyClass; ?>">
        <div class="overlay-loading">
            <div class="icon-placeholder">
                <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
            </div>
        </div>

        <?= $this->render(
            'header.php',
            []
        ) ?>

        <?php $this->beginBody() ?>

        <div id="content">
            <?= $content; ?>
        </div>

        <?php $this->endBody() ?>

        <section>
            <?= $this->render(
                'menu.php',
                []
            ) ?>
        </section>

        <?= $this->render(
            'footer.php',
            []
        ) ?>
        <?= \Yii::$app->view->renderFile('@app/views/_loader.html');?>
        <?php include(realpath(dirname(__FILE__) . '/../../cookies/cookies.inc.php')); ?>
    </body>
    <script>
        $('#link-menu').on('click', function(){
            $('#nav-mobile').collapse('hide');
        });
        
        $('#link-mobile').on('click', function(){
            $('#nav-menu').collapse('hide');
        });

        $('.search-panel .dropdown-menu').find('a').click(function(e) {
            e.preventDefault();
            var param = $(this).attr("href").replace("#","");
            var concept = $(this).text();
            $('.search-panel span#search_concept').text(concept);
            $('.input-group #search_param').val(param);
        });

        $('#summernote').summernote({
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
    </script>
</html>
<?php $this->endPage() ?>
